bitcoin.tx.segwit module
========================

.. automodule:: bitcoin.tx.segwit
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
