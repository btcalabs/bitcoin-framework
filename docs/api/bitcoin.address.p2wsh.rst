bitcoin.address.p2wsh module
============================

.. automodule:: bitcoin.address.p2wsh
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
