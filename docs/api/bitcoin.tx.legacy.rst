bitcoin.tx.legacy module
========================

.. automodule:: bitcoin.tx.legacy
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
