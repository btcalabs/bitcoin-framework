bitcoin.tx.signaturedata.segwit module
======================================

.. automodule:: bitcoin.tx.signaturedata.segwit
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
