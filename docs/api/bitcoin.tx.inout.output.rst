bitcoin.tx.inout.output module
==============================

.. automodule:: bitcoin.tx.inout.output
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
