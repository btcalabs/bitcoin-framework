bitcoin.contract.multisig module
================================

.. automodule:: bitcoin.contract.multisig
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
