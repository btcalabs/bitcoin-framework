bitcoin.script.std.np2wsh module
================================

.. automodule:: bitcoin.script.std.np2wsh
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
