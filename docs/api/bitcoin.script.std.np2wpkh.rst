bitcoin.script.std.np2wpkh module
=================================

.. automodule:: bitcoin.script.std.np2wpkh
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
