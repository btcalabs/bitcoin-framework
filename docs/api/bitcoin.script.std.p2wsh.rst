bitcoin.script.std.p2wsh module
===============================

.. automodule:: bitcoin.script.std.p2wsh
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
