bitcoin.tx.signaturedata.legacy module
======================================

.. automodule:: bitcoin.tx.signaturedata.legacy
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
