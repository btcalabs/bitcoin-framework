bitcoin.script.std.p2wpkh module
================================

.. automodule:: bitcoin.script.std.p2wpkh
    :members:
    :undoc-members:
    :show-inheritance:
    :inherited-members:
    :ignore-module-all:
