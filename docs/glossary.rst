########
Glossary
########

************
Transactions
************

.. _g-invalid-transaction:

invalid transaction
   a transaction that cannot be relayed because it does not follow the
   Bitcoin protocol consensus rules

.. _g-unspendable-transaction

unspendable transaction
   a transaction whose outputs cannot be spent because no valid `ScriptSig`_
   can redeem it

.. _scriptsig: https://bitcoin.org/en/glossary/signature-script
