#########
Addresses
#########

********
Purposes
********
Bitcoin addresses were intended to be used as means of transferring
information through external ways like email, phone, .... The first purpose
of Bitcoin addresses is to inform someone where to send their funds to
create a transaction and perform a payment. We call those `payment
address`_ es. But addresses can transfer more information as you can see in
`other address types`_.

.. note::

   Addresses are not part of the Bitcoin protocol, but they are widely known
   and used by its community so they have been implemented too.

Payment address
===============
Payment addresses allow to transfer securely a ``ScriptPubKey`` (precisely,
a `standard`_ ``ScriptPubKey``) so a transaction can be generated with the
proper outputs so the receiver will be able to spend their funds later.

.. _standard: https://bitcoin.org/en/developer-guide#standard-transactions

In order to avoid mistakes and send money to the incorrect ``ScriptPubKey``,
all addresses contain a `checksum`_

.. _checksum: https://en.wikipedia.org/wiki/Checksum

Every ``ScriptPubKey`` type has its matching `payment address`_ so the same
``ScriptPubKey`` can be generated once the address is decoded and validated
using its checksum.

In our framework, all existing ``ScriptPubKeys`` have their respective
addresses implemented and tested:

- :class:`bitcoin.P2PKHAddress` (`base58`_ encoding)
- :class:`bitcoin.P2SHAddress` (`base58`_ encoding)
- :class:`bitcoin.P2WPKHAddress` (`bech32`_ encoding)
- :class:`bitcoin.P2WSHAddress` (`bech32`_ encoding)
- Nested P2SH-P2WPKH (called :class:`NP2WPKHAddress`, `base58`_ encoding)
- Nested P2SH-P2WSH (called :class:`NP2WSHAddress`, `base58`_ encoding)

Other address types
===================
As mentioned before, not all addresses contain information to create
transactions and perform payments. Addresses can also transfer public keys
and private keys. You can check the full list of address types `here`__

.. _address_prefixes: https://en.bitcoin.it/wiki/List_of_address_prefixes

__ address_prefixes_

In our framework, the following addresses have been implemented:

- :class:`bitcoin.WIFAddress`

*********
Encodings
*********
In order to encode binary information into a human readable string, addresses
use encodings to transform bytes into characters.

Base58 encoding
===============
Since addresses in Bitcoin were invented, they used `base58`_ encoding to
store byte information in a human readable string. All addresses use this
encoding except for native `SegWit`_ addresses, that use `Bech32 encoding`_

.. _base58: https://en.bitcoin.it/wiki/Base58Check_encoding
.. _segwit: https://en.bitcoin.it/wiki/Segregated_Witness

Format
------

In general terms, a Bitcoin address in `base58`_ encodes information
following this format:

::

    [prefix][information][checksum]

Where every field specified is binary (bytes). Each field means the following:

Prefix
^^^^^^

1 or more bytes.

Indicates the kind of address and the network where the address information
is valid. See :attr:`bitcoin.params.MAINNET_PREFIXES` and
:attr:`bitcoin.params.TESTNET_PREFIXES` to check the implemented address
prefixes or `here`__ to see the full list of used prefixes.

__ address_prefixes_

Information
^^^^^^^^^^^

Byte size depending on the address type.

The value to store in the address. For instance, a 20-byte array for a public
key hash or reedem script hash, 32 byte for a private key... This bytes are
big endian encoded.

Checksum
^^^^^^^^

4 bytes.

Allows to detect errors in the `prefix` + `information` fields. See
:meth:`bitcoin.checksum` to see how it is implemented

.. hint::
   To use `base58`_ encoding: check the methods :meth:`bitcoin.b58encode` and
   :meth:`bitcoin.b58decode`. To check its implementation, check the
   :mod:`bitcoin.encoding.base58` module.

Bech32 encoding
===============

In order to specify if payment addresses must use `SegWit`_ or not, a new kind
of addresses was invented: `bech32`_ addresses. This type of addresses
encode native `SegWit`_ ``ScriptPubKey`` s.

.. _bech32: https://en.bitcoin.it/wiki/Bech32

.. hint::
   To use `bech32`_ encoding: check the methods :meth:`bitcoin.bech32encode`
   and :meth:`bitcoin.bech32decode`. To check its implementation, check the
   :mod:`bitcoin.encoding.bech32` module.
