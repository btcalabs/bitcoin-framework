.. Documentation style guide:
   https://documentation-style-guide-sphinx.readthedocs.io

#################################
Introduction
#################################

This framework aims to be a Bitcoin transaction development sandbox so any
kind of valid transaction can be created easily.

***********************
What it can actually do
***********************
Currently the framework offers following features:

- **Create** customizable Bitcoin transactions
- **Prevent** creating :ref:`invalid transactions <g-invalid-transaction>`
- **Deploy** Bitcoin smart contracts using its scripting language
  (and embed them into transactions easily)
- **Sign** and verify signatures embedded in transactions

*************************
What we want it to do too
*************************
What we are working on to offer in next releases (it could be called an open
*roadmap*):

- **Execute** smart contracts (*scripting system* execution engine)
- **Connect** via `JSON-RPC`_ to send and receive transactions to / from the
  network (using a defined Bitcoin network node)
- **Warn** implementing a logging system
  (to alert when creating for instance `non-standard` transactions)

.. _non-standard: https://github.com/libbitcoin/libbitcoin-server/wiki/Standardness
.. _json-rpc: https://en.bitcoin.it/wiki/API_reference_(JSON-RPC)

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   self
   topics/index
   Reference <api/bitcoin>
   glossary

******************
Indices and tables
******************

- :ref:`genindex`
- :ref:`modindex`
