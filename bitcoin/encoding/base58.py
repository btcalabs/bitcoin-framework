"""Provides methods to encode/decode to/from base58.

**Sources:**

- https://en.bitcoin.it/wiki/Base58Check_encoding
"""

# Libraries
import math

# Relative imports
from .errors import DecodingAlphabetError

# Constants
ALPHABET = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz'
"""
    str: Base58 character set sorted.
"""


def encode(value: bytes) -> str:
    """Encodes data using base58 convention into an human-readable string.

    Args:
        value: Data as :py:class:`bytes` to convert.

    Returns:
        :py:class:`str` containing converted data using base58 convention.
    """
    assert isinstance(value, bytes), "Expected bytes, given: %s" % (
        str(type(value)))
    output = ""
    # Convert bytes to integer
    number = int.from_bytes(value, byteorder='big')

    while number > 0:
        number, residuum = divmod(number, 58)
        output += ALPHABET[residuum]

    # Convert leading zeros
    leading_zeros = 0

    for element in value:
        if element == 0:
            leading_zeros += 1
        else:
            break

    output += ALPHABET[0] * leading_zeros

    return output[::-1]


def decode(value: str) -> bytes:
    """Decodes a base58 encoded :py:class:`str` into :py:class:`bytes`.

    Args:
        value: :py:class:`str` to decode from.

    Returns:
        Decoded bytes into a :py:class:`bytes` object.

    Raises:
        DecodingAlphabetError: Character trying to decode is not
                               valid: does not belong to :py:const:`ALPHABET`.
    """
    assert isinstance(value, str), "Expected a string object, given: " + \
                                   str(type(value))
    number = 0

    for char in value:
        number *= 58
        if char not in ALPHABET:
            raise DecodingAlphabetError()
        number += ALPHABET.index(char)

    # Convert integer to bytes
    number_bytes = number.to_bytes(math.ceil(number.bit_length()/8), "big")

    # Check leading zeros to put them back again
    leading_zeros = 0

    for char in value:
        if char == ALPHABET[0]:
            leading_zeros += 1
        else:
            break
    number_bytes = b'\x00'*leading_zeros + number_bytes

    return number_bytes
