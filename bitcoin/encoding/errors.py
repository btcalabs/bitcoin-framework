"""Declares the exceptions that can occur around the package."""


# Errors
class DecodingAlphabetError(ValueError):
    """Character not valid to decode from, not in alphabet."""
    pass


class DecodingChecksumError(ValueError):
    """The checksum for that address is not correct."""
    pass


class DecodingLengthError(ValueError):
    """The length of the address is not valid."""
    pass


class DecodingUnknownError(ValueError):
    """An error during the decoding stage is produced by some reason."""
    pass


class Bech32VersionError(ValueError):
    """The version is not valid for a Bech32 encoding."""
    pass
