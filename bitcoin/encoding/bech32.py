"""Provides methods to encode/decode to/from bech32.

**Sources:**

- https://github.com/sipa/bech32/blob/master/ref/python/segwit_addr.py
"""
# Libraries
from typing import Tuple

# Relative imports
from .errors import DecodingAlphabetError, DecodingChecksumError, \
    DecodingLengthError, DecodingUnknownError, Bech32VersionError

# Constants
ALPHABET = 'qpzry9x8gf2tvdw0s3jn54khce6mua7l'
"""
    str: Bech32 character set sorted by its numerical representation.
"""

BECH32_VERSION_MIN = 0
"""
    int: Minimum value of the version that can be represented in the bech32
    alphabet.
"""

BECH32_VERSION_MAX = len(ALPHABET) - 1
"""
    int: Maximum value of the version that can be represented in the bech32
    alphabet.
"""


def _polymod(values):
    generator = [0x3b6a57b2, 0x26508e6d, 0x1ea119fa, 0x3d4233dd, 0x2a1462b3]
    check = 1

    for value in values:
        top = check >> 25
        check = (check & 0x1ffffff) << 5 ^ value
        for i in range(5):
            check ^= generator[i] if ((top >> i) & 1) else 0

    return check


def _hrp_expand(hrp):
    """Expand the HRP into values for checksum computation."""
    return [ord(x) >> 5 for x in hrp] + [0] + [ord(x) & 31 for x in hrp]


def _create_checksum(hrp, data):
    """Compute the checksum values given HRP and data."""
    values = _hrp_expand(hrp) + data
    pm = _polymod(values + [0, 0, 0, 0, 0, 0]) ^ 1
    return [(pm >> 5 * (5 - i)) & 31 for i in range(6)]


def _verify_checksum(hrp, data):
    return _polymod(_hrp_expand(hrp) + data) == 1


def _convertbits(data, frombits, tobits, padding=True):
    """General power-of-2 base conversion."""
    acc, bits = 0, 0
    res = []
    maxv = (1 << tobits) - 1
    max_acc = (1 << (frombits + tobits - 1)) - 1

    for value in data:
        if value < 0 or (value >> frombits):
            return None  # pragma: no cover

        acc = ((acc << frombits) | value) & max_acc
        bits += frombits
        while bits >= tobits:
            bits -= tobits
            res.append((acc >> bits) & maxv)

    if padding:
        if bits:
            res.append((acc << (tobits - bits)) & maxv)
    elif bits >= frombits or ((acc << (tobits - bits)) & maxv):
        return None

    return res


def encode(hrp: str, version: int, program: bytes) -> str:
    """Encodes bytes using bech32 convention into an human-readable string.

    Args:
        hrp: Human-readable-part for the address.
        version: Witness version for the address (ust be technically in the
                 range [0,31], due to the alphabet has only 32 characters and
                 version  is just one character of the final representation).
        program: Witness program to encode into the address.

    Returns:
        Bech32 encoded address

    Raises:
        Bech32VersionError: Version passed as a parameter is not valid.
    """
    if not BECH32_VERSION_MIN <= version <= BECH32_VERSION_MAX:
        raise Bech32VersionError

    data = [version] + _convertbits(program, 8, 5)

    checksum = _create_checksum(hrp, data)

    data = data + checksum

    data_coded = ''
    for el in data:
        data_coded += ALPHABET[el]

    return hrp + '1' + data_coded


def decode(address: str) -> Tuple[str, int, bytes]:
    """Decodes a bech32 address into a bytes object.

    Args:
        address: Bech32 address to decode.

    Returns:
        ``(hrp, version, program)`` retrieved from the bech32 address.

    Raises:
        DecodingLengthError: Length of the address retrieved is not a
                             possible value.
        DecodingAlphabetError: Character trying to decode is not
                               valid; does not belong to :py:const:`ALPHABET`.
        ChecksumError: Checksum calculated is not the checksum carried by
                       the address. There's an error in the address.
        DecodingUnknownError: There's an error during the decoding.
    """
    hr_index = address.rfind('1')

    # Check position
    if hr_index < 1 or hr_index > len(address) - 7 or len(address) > 90:
        raise DecodingLengthError

    hrp = address[:hr_index]
    data = address[hr_index + 1:]

    # Check charset
    for char in data:
        if char not in ALPHABET:
            raise DecodingAlphabetError

    data_decoded = [ALPHABET.find(el) for el in data]

    # Check checksum
    if not _verify_checksum(hrp, data_decoded):
        raise DecodingChecksumError

    # Retrieve version & checksum
    version = data_decoded[0]
    data_decoded = data_decoded[1:-6]

    decoded = _convertbits(data_decoded, 5, 8, False)

    # Checks
    if decoded is None:
        raise DecodingUnknownError

    if len(decoded) < 2 or len(decoded) > 40:
        raise DecodingLengthError

    return hrp, version, bytes(decoded)
