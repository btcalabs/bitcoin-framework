"""Provides methods to allow encoding and decoding Bitcoin addresses."""

# Relative imports
from .base58 import encode as b58encode, decode as b58decode
from .bech32 import encode as bech32encode, decode as bech32decode
from .errors import DecodingAlphabetError, DecodingChecksumError, \
    DecodingLengthError, DecodingUnknownError, Bech32VersionError

__all__ = [
    "b58encode", "b58decode", "bech32encode", "bech32decode",
    "DecodingAlphabetError", "DecodingChecksumError", "DecodingLengthError",
    "DecodingUnknownError", "Bech32VersionError"
]
