"""Bitcoin Framework package.

The aim of this package is to provide a puzzle-friendly framework to compose
easily any Bitcoin transaction, specially those containing smart contracts
"""
# Relative imports: modules
# # Addresses
from .address import Address, P2PKHAddress, P2SHAddress, WIFAddress, \
    find_prefix, AddressPrefixError, AddressLengthError, \
    AddressTypeError, AddressChecksumError, P2WSHAddress, P2WPKHAddress, \
    find_network_by_hrp, NP2WPKHAddress, NP2WSHAddress, \
    Base58EncodableAddress, Bech32EncodableAddress, AddressHRPError, \
    PaymentAddress, AddressFactory

# # Crypto
from .crypto import ECDSAPublicKey, ECDSAPrivateKey, ECDSASignature, \
    ripemd160, sha1, sha256, ripemd160_sha256, hash160, double_sha256, \
    hash256, hmac_sha512, checksum, CHECKSUM_SIZE, ExtendedECDSAPublicKey, \
    ExtendedECDSAPrivateKey

# # Encodes
from .encoding import b58encode, b58decode, bech32encode, bech32decode, \
    DecodingAlphabetError, DecodingChecksumError, DecodingLengthError, \
    DecodingUnknownError, Bech32VersionError

# # Field
from .field import IntField, U2BLEInt, U4BLEInt, U8BLEInt, S4BLEInt, VarInt, \
    Opcode, OP_PUSHDATA_MIN, OP_PUSHDATA_MAX, \
    OP_PUSHDATA_MAX_SIZE, OP_PUSHDATA_MAX_SIZE_CONSENSUS, OP_0, OP_1, OP_N, \
    get_op_code_n, OP_PUSHDATA1, OP_PUSHDATA2, OP_PUSHDATA4, OP_IF, OP_ELSE,\
    OP_ENDIF,  OP_DROP, OP_DUP, OP_OVER, OP_EQUAL, OP_EV, OP_EQUALVERIFY, \
    OP_RIPEMD160,  OP_SHA1, OP_SHA256, OP_HASH160, OP_HASH256, \
    OP_CODESEPARATOR, OP_CS,  OP_CHECKSIG, OP_CMS, OP_CHECKMULTISIG, OP_CLTV, \
    OP_CHECKLOCKTIMEVERIFY, ScriptData, ScriptNum, OP_2, OP_3, OP_4, OP_5, \
    OP_6, OP_7, OP_8, OP_9, OP_10, OP_11, OP_12, OP_13, OP_14, OP_15, OP_16, \
    OP_PUSHDATA, OP_RETURN

# # PSBT
from .psbt import PSBT_GLOBAL_UNSIGNED_TX, PSBT_IN_NON_WITNESS_UTXO, \
    PSBT_IN_WITNESS_UTXO, PSBT_IN_PARTIAL_SIG, PSBT_IN_SIGHASH_TYPE, \
    PSBT_IN_REDEEM_SCRIPT, PSBT_IN_WITNESS_SCRIPT, PSBT_IN_BIP32_DERIVATION, \
    PSBT_IN_FINAL_SCRIPTSIG, PSBT_IN_FINAL_SCRIPTWITNESS, \
    PSBT_OUT_REDEEM_SCRIPT, PSBT_OUT_WITNESS_SCRIPT, \
    PSBT_OUT_BIP32_DERIVATION, PSBTItem, NoParamPSBTItem, PSBTScope, PSBT, \
    PSBTRegistry

# # Script
from .script import Script, TransactionScript, ScriptSig, ScriptPubKey, \
    NP2WPKHScriptPubKey, NP2WPKHScriptSig, \
    NP2WSHScriptPubKey, NP2WSHScriptSig, P2PKHScriptPubKey, P2PKHScriptSig, \
    P2SHScriptPubKey, P2SHScriptSig, P2WPKHScriptPubKey, P2WPKHScriptSig, \
    P2WSHScriptPubKey, P2WSHScriptSig, StdSHLengthError, StdPKHLengthError, \
    StdScriptPubKey, StdP2SHScriptPubKey, StdP2PKHScriptPubKey, \
    StdSegWitScriptPubKey, StdSegWitScriptPubKeyProgramError, \
    StdSegWitScriptPubKeyVersionError, Witness, StdScriptSig, \
    StdP2SHScriptSig, StdP2PKHScriptSig, StdPaymentScript, NullDataScriptPubKey


# # Transaction Input and Outputs
# # Transactions
from .tx import Tx, TxInput, TxOutpoint, TxOutput, TxInputOutput, \
    HashType, DEFAULT_HASHTYPE, SignatureData, SegWitSignatureData, \
    LegacySignatureData, HashTypeValue, HashTypeFlag


# Files
# # Parameters
from .params import BlockchainParams, TESTNET, MAINNET, \
    get_default_network, set_default_network, AddressTypes, BIP32_PRIVKEY, \
    BIP32_PUBKEY, WIF, NoDefaultBlockchainError, BLOCKCHAINS, \
    get_address_type_from_index, PKH_SIZE, SH_SIZE_SEGWIT, SH_SIZE_LEGACY, \
    REGTEST, KnownBlockchainParams

# # Standards
from .standards import P2PK, P2PKH, NP2WPKH, P2WPKH, P2SH, NP2WSH, P2WSH, \
    StdPaymentTypes, StdPaymentImplementations, PaymentStandard, \
    get_payment_standard

# # Units
from .units import SATOSHIS_IN_A_BITCOIN, btc_to_satoshi, satoshi_to_btc, \
    UnitsPrecisionError, UnitsFormatError, UnitsNegativeError, \
    satoshi_to_bit, satoshi_to_mbitcoin

# Exports
# # address
__all__ = [
    "AddressTypeError", "AddressLengthError", "AddressChecksumError",
    "AddressPrefixError", "Address", "P2PKHAddress", "P2SHAddress",
    "find_prefix", "WIFAddress", "P2WSHAddress", "P2WPKHAddress",
    "find_network_by_hrp", "NP2WPKHAddress", "NP2WSHAddress",
    "Base58EncodableAddress", "Bech32EncodableAddress", "AddressHRPError",
    "PaymentAddress", "AddressFactory"
]

# # contract
__all__ += [
]

# # crypto
__all__ += [
    "ECDSAPublicKey", "ECDSAPrivateKey", "ECDSASignature", "ripemd160", "sha1",
    "sha256", "ripemd160_sha256", "hash160", "double_sha256", "hash256",
    "hmac_sha512", "checksum", "CHECKSUM_SIZE", "ExtendedECDSAPublicKey",
    "ExtendedECDSAPrivateKey"
]

# # encoding
__all__ += [
    "b58encode", "b58decode", "bech32encode", "bech32decode",
    "DecodingAlphabetError", "DecodingChecksumError", "DecodingLengthError",
    "DecodingUnknownError", "Bech32VersionError"
]

# # field
__all__ += [
    "U2BLEInt", "U4BLEInt", "U8BLEInt", "S4BLEInt", "VarInt",
    "Opcode", "OP_PUSHDATA_MIN", "OP_PUSHDATA_MAX", "OP_PUSHDATA_MAX_SIZE",
    "OP_PUSHDATA_MAX_SIZE_CONSENSUS", "OP_0", "OP_1", "IntField",
    "get_op_code_n", "OP_PUSHDATA1", "OP_PUSHDATA2", "OP_PUSHDATA4", "OP_IF",
    "OP_ELSE", "OP_ENDIF", "OP_DROP", "OP_DUP", "OP_OVER", "OP_EQUAL", "OP_EV",
    "OP_EQUALVERIFY", "OP_RIPEMD160", "OP_SHA1", "OP_SHA256", "OP_HASH160",
    "OP_HASH256", "OP_CODESEPARATOR", "OP_CS", "OP_CHECKSIG", "OP_CMS",
    "OP_CHECKMULTISIG", "OP_CLTV", "OP_CHECKLOCKTIMEVERIFY", "ScriptData",
    "ScriptNum", "OP_2", "OP_3", "OP_4", "OP_5", "OP_6", "OP_7", "OP_8",
    "OP_9", "OP_10", "OP_11", "OP_12", "OP_13", "OP_14", "OP_15", "OP_16",
    "OP_N", "OP_PUSHDATA", "OP_RETURN"
]

# # psbt
__all__ += [
    "PSBT", "PSBTItem", "NoParamPSBTItem", "PSBTScope", "PSBTRegistry",
    "PSBT_GLOBAL_UNSIGNED_TX", "PSBT_IN_NON_WITNESS_UTXO",
    "PSBT_IN_WITNESS_UTXO", "PSBT_IN_PARTIAL_SIG", "PSBT_IN_SIGHASH_TYPE",
    "PSBT_IN_REDEEM_SCRIPT", "PSBT_IN_WITNESS_SCRIPT",
    "PSBT_IN_BIP32_DERIVATION", "PSBT_IN_FINAL_SCRIPTSIG",
    "PSBT_IN_FINAL_SCRIPTWITNESS", "PSBT_OUT_REDEEM_SCRIPT",
    "PSBT_OUT_WITNESS_SCRIPT", "PSBT_OUT_BIP32_DERIVATION",
]

# # script
__all__ += [
    "Script", "TransactionScript", "ScriptSig", "ScriptPubKey",
    "NP2WPKHScriptPubKey", "NP2WPKHScriptSig",
    "NP2WSHScriptPubKey", "NP2WSHScriptSig", "P2PKHScriptPubKey",
    "P2PKHScriptSig", "P2SHScriptPubKey", "P2SHScriptSig",
    "P2WPKHScriptPubKey", "P2WPKHScriptSig", "P2WSHScriptPubKey",
    "P2WSHScriptSig", "StdPKHLengthError", "StdSHLengthError",
    "StdScriptPubKey", "StdP2PKHScriptPubKey", "StdP2SHScriptPubKey",
    "StdSegWitScriptPubKey", "StdSegWitScriptPubKeyProgramError",
    "StdSegWitScriptPubKeyVersionError", "Witness", "StdScriptSig",
    "StdP2SHScriptSig", "StdP2PKHScriptSig", "StdPaymentScript",
    "NullDataScriptPubKey"
]

# # tx
__all__ += [
    "Tx", "HashType", "DEFAULT_HASHTYPE", "TxInputOutput", "TxInput",
    "TxOutpoint", "TxOutput", "SignatureData", "SegWitSignatureData",
    "LegacySignatureData", "HashTypeFlag", "HashTypeValue"
]

# # Params
__all__ += [
    "BlockchainParams", "MAINNET", "TESTNET", "get_default_network",
    "set_default_network", "BLOCKCHAINS", "AddressTypes", "BIP32_PRIVKEY",
    "BIP32_PUBKEY", "WIF", "get_address_type_from_index",
    "NoDefaultBlockchainError", "PKH_SIZE", "SH_SIZE_LEGACY", "SH_SIZE_SEGWIT",
    "REGTEST", "KnownBlockchainParams"
]

# # Standards
__all__ += [
    "P2PK", "P2PKH", "NP2WPKH", "P2WPKH", "P2SH", "NP2WSH", "P2WSH",
    "StdPaymentTypes", "StdPaymentImplementations", "PaymentStandard",
    "get_payment_standard"]

# # Units
__all__ += [
    "SATOSHIS_IN_A_BITCOIN", "btc_to_satoshi", "satoshi_to_btc",
    "UnitsPrecisionError", "UnitsFormatError", "UnitsNegativeError",
    "satoshi_to_bit", "satoshi_to_mbitcoin"
]
