"""Defines a Bitcoin transaction model.

In Bitcoin, a transaction defines how funds (bitcoins) are moved. The
transaction specifies one or more inputs to be spent and one or more outputs
where the funds will be moved if transaction is accepted as valid on the
blockchain by consensus.

Transaction models must be able to represent transactions of the blockchain and
be able to be serialized into an array of bytes and deserialized from an array
of bytes

**Sources:**

- https://en.bitcoin.it/wiki/Transaction
"""

# Libraries
import json
import logging
from collections import OrderedDict
from typing import Sequence, Tuple, List  # noqa: F401

# Relative imports
from .inout import TxInput, TxOutput
from ..crypto import double_sha256
from ..field import S4BLEInt, U4BLEInt, VarInt
from ..interfaces import Serializable, JSONType
from ..script import Witness

# Constants
LOGGER = logging.getLogger(__name__)
"""
    logger for that file.
"""

DEFAULT_VERSION = 2
"""
    int: default version of the transaction, currently (2018-07-05)
most used version is 1, but version 2 is accepted following the consensus
rule defined in `BIP068 \
<https://github.com/bitcoin/bips/blob/master/bip-0068.mediawiki>`_
"""
DEFAULT_LOCKTIME = 0
"""
    int: time (as number of blocks or as UNIX timestamp) at which transaction
is unlocked so it can be spent.

By default, will be zero as we want non-locked transactions by default

``if lockTime < 500000000``: specifies block number at which tx is unlocked
``if locktime >= 500000000``: specifies UNIX time stamp at which tx is unlocked

If all inputs have a sequence with ``0xffffffff`` value, then this is ignored.
and as the feature of sequence is disabled, this is always 0

**Sources:**

- https://bitcoin.stackexchange.com/q/2025
"""
DEFAULT_MARKER = b'\x00'
"""
    bytes: Marker set in SegWit transactions to avoid non-upgraded nodes to
           try to understand them.
"""
DEFAULT_FLAG = b'\x01'
"""
    bytes: Flag set in SegWit transaction with the same purpose as
           :py:attr:`DEFAULT_MARKER`.
"""
TRANSACTION_SIZE_MIN = U4BLEInt.BYTES_SIZE + len(VarInt(0)) + len(VarInt(0)) +\
                       U4BLEInt.BYTES_SIZE
"""
    int: Minimum size in bytes of a serialized transaction.
         ``version, inputs_count(0), outputs_count(0), locktime``
"""


class Tx(Serializable):
    """Bitcoin transaction.

    It contains basic fields: ``version``, ``inputs``, ``outputs`` and
    ``locktime``.

    Attributes:
        _version (S4BLEInt): Version of the transaction.
        _inputs (List[TxInput, ...]): List of inputs to spend.
        _outputs (List[TxOutput, ...]): List of outputs to send funds to.
        _locktime (U4BLEInt): Time after which the transaction will be valid.
    """
    __slots__ = "_version", "_inputs", "_outputs", "_locktime"

    def __init__(self, version: int = DEFAULT_VERSION,
                 inputs: Sequence[TxInput] = None,
                 outputs: Sequence[TxOutput] = None,
                 locktime: int = DEFAULT_LOCKTIME) -> None:
        """Initializes a Bitcoin transaction.

        If no arguments passed, initializes an empty transaction.

        Args:
            version (int): (Optional) Version of the transaction.
                           Default is :py:const:`DEFAULT_VERSION`
            inputs (Sequence[TxInput]): List of inputs to be spend.
            outputs (Sequence[TxOutput]): List of outputs to send the funds.
            locktime (U4BLEInt): (Optional) Time after which the transaction
                                 will be valid. Default is ``0`` (it can be
                                 spent any time).

                                 See :py:const:`DEFAULT_LOCKTIME` for more
                                 information.
        """
        # Type assertions
        assert isinstance(version, int)
        assert isinstance(locktime, int)

        # Defaults
        inputs = list() if inputs is None else list(inputs)
        outputs = list() if outputs is None else list(outputs)

        # Type assertions after defaults
        assert all(isinstance(
            tx_in, TxInput) for tx_in in inputs)
        assert all(isinstance(
            tx_out, TxOutput) for tx_out in outputs)

        # Assignments
        self._version = S4BLEInt(version)
        self._inputs = list()  # type: List[TxInput]
        self._outputs = list()  # type: List[TxOutput]
        self._locktime = U4BLEInt(locktime)

        # Transaction parent references
        for tx_in in inputs:
            self.add_input(tx_in)

        for tx_out in outputs:
            self.add_output(tx_out)

    def serialize(self, force_legacy: bool = False) -> bytes:
        """Serializes the transaction.

        If the transaction has any *witness* in any of the inputs,
        the transaction will be serialized using the *SegWit* serialization
        as specified in `BIP 141`_, otherwise it will serialized as a
        `classic transaction`_.

        .. _BIP 141: https://github.com/bitcoin/bips/blob/master/\
        bip-0141.mediawiki

        .. classic trasaction: http://learnmeabitcoin.com/glossary/\
        transaction-data

        Returns:
            Transaction's network serialization.
        """
        # Version
        version = self._version.serialize()

        # SegWit marker and flag (if any)
        if self.has_witness and not force_legacy:
            marker = DEFAULT_MARKER
            flag = DEFAULT_FLAG
        else:
            marker = b''
            flag = b''

        # Inputs
        # # Inputs count
        inputs_count = VarInt(len(self._inputs)).serialize()
        # # Serialized inputs
        inputs = [i.serialize() for i in self._inputs]

        # Outputs
        # # Outputs count
        outputs_count = VarInt(len(self._outputs)).serialize()
        # # Serialized outputs
        outputs = [o.serialize() for o in self._outputs]

        # Witnesses (if any)
        witnesses = list()
        if self.has_witness and not force_legacy:
            for tx_in in self._inputs:
                witnesses.append(tx_in.witness.serialize())

        # Locktime
        locktime = self._locktime.serialize()

        return b''.join([version, marker, flag, inputs_count] + inputs + [
            outputs_count] + outputs + witnesses + [locktime])

    @classmethod
    def deserialize(cls, data: bytes) -> "Tx":
        """Deserializes a Bitcoin transaction.

        Args:
            data: :py:class:`bytes` object containing a transaction to
                  deserialize

        Returns:
            New :py:class:`Tx` instance.

        Raises:
            ValueError: To few bytes to deserialize
            ValueError: Some field could not be deserialized.
            ValueError: Invalid length fields or missing data to deserialize.
        """
        # Check enough size provided
        if len(data) < TRANSACTION_SIZE_MIN:
            raise ValueError("Not enough transaction bytes to deserialize.")

        offset = 0
        has_witnesses = False

        # Version
        version = S4BLEInt.deserialize(data[offset:4])
        offset += len(version)

        # Detect marker and flag
        if data[offset] == DEFAULT_MARKER[0]:
            # May be SegWit. Read next byte.
            flag = data[offset + 1]
            if flag == DEFAULT_FLAG[0]:
                # Is a SegWit transaction
                has_witnesses = True
                offset += 2
            elif flag > DEFAULT_FLAG[0]:
                LOGGER.error("Invalid transaction detected. Is not a segwit "
                             "one, and can not have outputs without inputs.")
                pass  # pragma: no cover

        # Inputs
        # # Inputs number
        number_of_inputs = VarInt.deserialize(data[offset:])
        offset += len(number_of_inputs)

        # # Deserialize inputs
        inputs = list()
        for input_i in range(number_of_inputs.value):
            in_i = TxInput.deserialize(data[offset:])
            inputs.append(in_i)
            offset += len(in_i)

        # Outputs
        # # Outputs number
        number_of_outputs = VarInt.deserialize(data[offset:])
        offset += len(number_of_outputs)

        # # Deserialize outputs
        outputs = list()
        for output_i in range(number_of_outputs.value):
            # Deserialize outputs
            out_i = TxOutput.deserialize(data[offset:])
            outputs.append(out_i)
            offset += len(out_i)

        # Witnesses (if any)
        if has_witnesses:
            for i in range(len(inputs)):
                # Read witness
                witness = Witness.deserialize(data[offset:])
                offset += len(witness)
                # Assign witness
                tx_input = inputs[i]
                tx_input.script.witness = witness

        # # Locktime
        locktime = U4BLEInt.deserialize(data[offset:offset + 4])
        offset += len(locktime)

        # # Check remaining
        if offset != len(data):
            raise ValueError(
                "Serialization finished, but there's still data to handle. " +
                "Offset is at byte %d" % offset)

        # Create object
        return cls(version.value, inputs, outputs, locktime.value)

    def add_input(self, tx_input: TxInput) -> None:
        """Adds a transaction input to the inputs list.

        Also sets the input's parent transaction to this transaction.

        Args:
            tx_input: input to add to the inputs list
        Raises:
            ValueError if the input already belongs to another transaction
        """
        # Type assertions
        assert isinstance(tx_input, TxInput)

        # Checks
        # if input is already linked to a tx, remove from the previous
        if tx_input._tx is not None:
            raise ValueError("The input already belongs to another "
                             "transaction, remove it from there first.")

        # New assignment
        tx_input._tx = self  # type: ignore
        self._inputs += [tx_input]

    def remove_input(self, tx_input: TxInput) -> None:
        """Removes the passed input from the internal list.

        Args:
            tx_input: TxInput to remove from the list.
        """
        # Type assertions
        assert isinstance(tx_input, TxInput)

        if tx_input in self._inputs:
            # Remove assignment
            tx_input._tx = None
            self._inputs.remove(tx_input)

    def empty_inputs(self) -> None:
        """Removes all the inputs for the current transaction."""
        for inp in self._inputs:
            inp._tx = None

        self._inputs = list()

    def add_output(self, tx_output: TxOutput) -> None:
        """Adds a transaction output to the outputs list.

        Also sets the outputs's parent transaction to this transaction.

        Args:
            tx_output: output to add to the list.
        Raises:
            ValueError if the output already belongs to another transaction.
        """
        # Type assertions
        assert isinstance(tx_output, TxOutput)

        # Checks
        # if input is already linked to a tx, remove from the previous
        if tx_output._tx is not None:
            raise ValueError("The output already belongs to another "
                             "transaction, remove it from there first.")

        # New assignment
        tx_output._tx = self  # type: ignore
        self._outputs += [tx_output]

    def remove_output(self, tx_output: TxOutput) -> None:
        """Removes the passed output from the internal list.

        Args:
            tx_output: TxOutput to remove from the list.
        """
        # Type assertions
        assert isinstance(tx_output, TxOutput)

        if tx_output in self._outputs:
            # Remove assignment
            tx_output._tx = None
            self._outputs.remove(tx_output)

    def empty_outputs(self) -> None:
        """Removes all the outputs for the current transaction."""

        for outp in self._outputs:
            outp._tx = None

        self._outputs = list()

    @property
    def version(self) -> int:
        """Returns the version of the transaction."""
        return self._version.value

    @version.setter
    def version(self, version: int):
        """Sets a new version for the transaction

        Args:
            version: new transaction version
        """
        self._version.value = version

    @property
    def inputs(self) -> Sequence[TxInput]:
        """Returns the inputs of the transaction as a sequence

        Returns:
            transaction inputs
        """
        return self._inputs

    @inputs.setter
    def inputs(self, inputs: Sequence[TxInput]):
        """Sets new inputs for the transaction

        Args:
            inputs: new transaction inputs (as a sequence)
        """
        # Remove the existing inputs
        for tx_in in self._inputs:
            self.remove_input(tx_in)
        # Add the new inputs
        for tx_in in inputs:
            self.add_input(tx_in)

    @property
    def outputs(self) -> Sequence[TxOutput]:
        """Returns the outputs of the transaction as a sequence

        Returns:
            transaction outputs
        """
        return self._outputs

    @outputs.setter
    def outputs(self, outputs: Sequence[TxOutput]):
        """Sets new outputs for the transaction

        Args:
            outputs: new transaction outputs (as a sequence)
        """
        # Remove the existing outputs
        for tx_out in self._outputs:
            self.remove_output(tx_out)
        # Add the new outputs
        for tx_out in outputs:
            self.add_output(tx_out)

    @property
    def locktime(self) -> int:
        """Returns the locktime of the transaction."""
        return self._locktime.value

    @locktime.setter
    def locktime(self, locktime: int):
        """
        Sets a new locktime for the transaction

        Args:
            locktime: new transaction locktime
        """
        self._locktime.value = locktime

    @property
    def has_witness(self) -> bool:
        """Checks if any input has a *^witness*

        Returns:
            ``True`` if any *witness* has been found in any of the inputs.
        """
        return any([tx_input.has_witness for tx_input in self.inputs])

    @property
    def id(self) -> bytes:
        """Returns the transaction identifier (``txid``).

        Calculates the transaction ID serializing the transaction and
        calculating the double SHA256 of it, and reversing it.

        **Sources:**

        - https://en.bitcoin.it/wiki/Transaction
        - http://learnmeabitcoin.com/glossary/txid

        Returns:
            bytes: transaction id as a bytes object
        """
        return double_sha256(self.serialize(True))[::-1]

    @property
    def hash(self) -> bytes:
        """Returns the transaction serialization's hash.

        Will be ``txid`` if it is a transaction without witnesses or
        ``wtxid```if any input has a *witness*.
        """
        return double_sha256(self.serialize())[::-1]

    @property
    def weight(self) -> int:
        """Returns the weight for the current transaction.

        Weight units are a measurement used to compare the size of different
        Bitcoin transactions to each other in proportion to the consensus-
        enforced maximum block size limit.
        """
        return 3*len(self.serialize(force_legacy=True))+len(self)

    @property
    def vsize(self) -> int:
        """Returns the virtual size for the current transaction.

        Virtual size (``vsize``), also called virtual bytes (vbytes), are an
        alternative measurement, with one vbyte being equal to four weight
        units. That means the maximum block size measured in vsize is 1 million
        vbytes.
        """
        return (self.weight + 3) // 4

    def encode(self) -> JSONType:
        legacy_serialization = self.serialize(force_legacy=True)
        return OrderedDict((
            ("hex", legacy_serialization.hex()),
            ("txid", self.id.hex()),
            ("hash", self.hash.hex()),
            ("size", len(legacy_serialization)),
            ("vsize", self.vsize),
            ("weight", self.weight),
            ("version", self.version),
            ("locktime", self.locktime),
            ("vin", [tx_input.encode() for tx_input in self.inputs]),
            ("vout", [tx_output.encode() for tx_output in self.outputs]),))

    def __str__(self) -> str:
        """Prints the transaction in the same (JSON) format as Bitcoin Core."""
        return json.dumps(self.encode(), indent=4)
