"""
Models the input section of a Bitcoin transaction, that contains:

- ``utxo_id``: UTXO reference's transaction ``id``
- ``utxo_n``: UTXO reference's output number
- ``script``: or ``scriptSig``, a script authorizing the transfer
- ``sequence``: version of the input

The low-level specification of the input is `here
<https://en.bitcoin.it/wiki/Protocol_documentation#tx>`_
"""
# Relative imports
import json
from collections import OrderedDict

from .inout import TxInputOutput
from ...field import U4BLEInt, VarInt
from ...interfaces import Serializable, JSONType
from ...script import ScriptSig, Witness

# Constants
DEFAULT_SEQUENCE = 0xffffffff
"""
    int: default sequence number for inputs (final value)
"""
TX_INPUT_SIZE_MIN = 41


class TxInput(TxInputOutput[ScriptSig]):
    """Models a transaction input in a Bitcoin transaction.

    Allows to handle all fields in a transaction input: UTXO (previous
    transaction id and output number), the ``scriptSig``, and sequence number.

    Also allows to use Segregated Witness, by specifying the property
    ``is_witness``, that will use the ``scriptSig`` data as witnesses

    Internally, the attributes contain Field objects that when serialized,
    will output the bytes required in the protocol specification for a
    transaction input. The external interface, though, allows the user to
    use Python data types to set fields' values. Through property setters
    it will construct the required Field objects so that values specified by
    the user end up being the bytes the protocol expects when serializing.

    Attributes:
        _utxo_id (bytes): UTXO reference's previous transaction ``id``
        _utxo_n (U4BLEInt): UTXO reference's previous transaction output number
        _script (ScriptSig): ``bitcoin.ScriptSig`` to spend the
        referred UTXO
        _sequence (U4BLEInt): transaction version as defined by the sender
    """
    __slots__ = "_utxo", "_sequence"

    def __init__(self, utxo_id: bytes, utxo_n: int, script: ScriptSig = None,
                 sequence: int = DEFAULT_SEQUENCE) -> None:
        """Initializes a transaction input.

        It needs everything a transaction input contains: the UTXO to spend (
        its *txid* and output number), the ``bitcoin.ScriptSig`` and the
        sequence
        number. You can also specify if this input ``bitcoin.ScriptSig``
        will be
        treated as a ``witness`` and the parent transaction (where this
        input is used)

        Args:
            utxo_id: UTXO previous transaction id
            utxo_n: UTXO previous transaction output number
            script: ``bitcoin.ScriptSig`` that allows to spend the UTXO
            sequence: Sequence number (input version)
            tx: Transaction where this input is being used
        """
        # Defaults
        script = script if script is not None else ScriptSig()

        # Type assertions
        assert isinstance(script, ScriptSig)

        # Initializations
        super().__init__(script)

        # Store attributes
        self._utxo = TxOutpoint(utxo_id, utxo_n)
        self._sequence = U4BLEInt(sequence)

    def serialize(self) -> bytes:
        """Serializes the transaction input into a bytes object."""
        # UTXO reference
        utxo = [self._utxo.serialize()]
        # Script (first its length, then the script)
        script = [VarInt(len(self._script)).serialize(),
                  self._script.serialize()]
        # Sequence number
        sequence = self._sequence.serialize()

        # Serialize everything and return
        return b''.join(utxo + script + [sequence])

    @classmethod
    def deserialize(cls, data: bytes) -> "TxInput":
        """Creates a TxInput object given its serialization.

        Args:
            data: bytes to deserialize and create a TxInput from.

        Raises:
            ValueError: Not enough bytes to build a correct object.
        """
        if len(data) < TX_INPUT_SIZE_MIN:
            raise ValueError("Not enough bytes to create an input from.")
        offset = 0

        utxo = TxOutpoint.deserialize(data)
        offset += 36

        script_len = VarInt.deserialize(data[offset:])
        offset += len(script_len)

        script_bytes = data[offset:offset+script_len.value]
        offset += len(script_bytes)

        sequence = U4BLEInt.deserialize(data[offset:offset+4])

        script = ScriptSig.deserialize(script_bytes)

        return cls(utxo.id, utxo.index, script, sequence.value)

    @property
    def utxo_id(self) -> bytes:
        """Returns the UTXO reference's previous transaction ``id``.

        Note:
            In big endian despite its serialization is in little endian.
        """
        return self._utxo.id

    @property
    def utxo_n(self) -> int:
        """Returns the UTXO reference's previous transaction output number."""
        return self._utxo.index

    @property
    def sequence(self) -> int:
        """Returns the ``sequence``'s field value."""
        return self._sequence.value

    @sequence.setter
    def sequence(self, value: int) -> None:
        """Sets the sequence value.

        Args:
            value (int): ``sequence`` value to be set
        """
        self._sequence.value = value

    @property
    def has_witness(self) -> bool:
        """Returns if the script has a ``witness``."""
        return self.script.has_witness

    @property
    def script(self) -> ScriptSig:
        """Returns the associated ``ScriptSig``."""
        return self._script

    @script.setter
    def script(self, new_script: ScriptSig) -> None:
        """Sets the new script of the transaction input."""
        assert isinstance(new_script, ScriptSig)
        TxInputOutput.script.__set__(self, new_script)  # type: ignore

    @property
    def witness(self) -> Witness:
        """Returns the witness associated to this input, if any.

        Returns:
            The witness as a :py:obj:`Script` object. If no witness is
            present, the object will be empty.
        """
        return self.script.witness

    @property
    def outpoint(self) -> "TxOutpoint":
        """Returns the referred output currently being spent."""
        return self._utxo

    def encode(self) -> JSONType:
        """Encodes the input as a dictionary in the Bitcoin Core format."""
        return OrderedDict((
            ("txid", self.utxo_id.hex()),
            ("vout", self.utxo_n),
            ("scriptSig", self.script.encode()),
            ("sequence", self.sequence),
            ("txinwitness", self.script.witness.encode())
        ))

    def __str__(self) -> str:
        return json.dumps(self.encode())


# Constant
TX_OUTPUT_REFERENCE_SIZE = 36


class TxOutpoint(Serializable):
    """Defines a reference to an Output.

    Useful when having to create a reference to an UTXO.

    Attributes:
        _txid:
        _index:
    """

    __slots__ = "_txid", "_index"

    def __init__(self, txid: bytes, out_index: int):
        """Initializes an TxOutpoint.

        Args:
            id: Transaction id where the output belongs to.
            n: Index of the output into the referred transaction.

        Raises:
            ValueError: If an argument has an invalid content (id must be a
                        valid hash and n has to be positve).
        """
        assert isinstance(txid, bytes)
        assert isinstance(out_index, int)

        # Invalid id length
        if len(txid) != 32:
            raise ValueError
        # An index can not be negative
        if out_index < 0:
            raise ValueError

        self._txid = txid[::-1]
        self._index = U4BLEInt(out_index)

    def serialize(self) -> bytes:
        """Serializes the TxOutpoint into a bytes object."""
        return b''.join([self._txid, self._index.serialize()])

    @classmethod
    def deserialize(cls, data: bytes) -> "TxOutpoint":
        """Creates a TxOutpoint object given its serialization.

        Args:
            data: bytes to deserialize in order to create a TxOutpoint.

        Raises:
            ValueError: Not enough bytes to build a correct object.
        """
        if len(data) < TX_OUTPUT_REFERENCE_SIZE:
            raise ValueError("Not enough bytes provided")

        offset = 0

        txid = data[offset:offset+32][::-1]
        offset += 32

        n = U4BLEInt.deserialize(data[offset:offset+4])
        offset += 4

        return cls(txid, n.value)

    @property
    def id(self) -> bytes:
        return self._txid[::-1]

    @property
    def index(self) -> int:
        return self._index.value
