"""Transaction output model.

..
    NOTE: Transaction model is not referenced either in typing hints or
    documentation because can't be imported due to circular references.

**Test sources:**
- https://api.blockcypher.com/v1/btc/main/txs/\
0077624bf51d4e177706396cc36247c990219a69c6557e51d98ceef24fe49da4
"""
# Libraries
from collections import OrderedDict
from typing import List  # NOQA: F401

# Relative imports
from .inout import TxInputOutput
from ...interfaces import Serializable, JSONType  # NOQA: F401
from ...field import U8BLEInt, VarInt
from ...script import ScriptPubKey
from ...units import satoshi_to_btc

# Constants
TX_OUTPUT_SIZE_MIN = len(U8BLEInt(0)) + len(VarInt(0))
"""
    int: minimum number of bytes a transaction output takes
"""


class TxOutput(TxInputOutput[ScriptPubKey]):
    """Models a transaction output in a Bitcoin transaction.

    Attributes:
        _value (U8BLEInt): Value in satoshis of the output
        _script (ScriptPubKey): Locking script
    """
    __slots__ = "_value"

    def __init__(self, satoshis: int, script: ScriptPubKey) -> None:
        """Initializes a transaction output with satoshis' value and script

        You can optionally specify the transaction the output belongs to.

        Args:
            satoshi (int): Number of satoshis of the output
            script (ScriptPubKey): Pubkey script of the output
            tx (Tx): Transaction the output belongs to
        """
        # Type checks
        assert isinstance(satoshis, int)
        assert isinstance(script, ScriptPubKey)

        # Positive amount
        if satoshis < 0:
            raise ValueError("Number of satoshis must be positive")

        # Initialize
        super().__init__(script)

        # Store values
        self._value = U8BLEInt(satoshis)

    def serialize(self) -> bytes:
        """Serializes the transaction output."""
        serializable = list()  # type: List[Serializable]
        # Add output value
        serializable.append(self._value)
        # Add script length and script itself
        serializable.append(VarInt(len(self._script)))
        serializable.append(self._script)
        # Serialize
        return b''.join([item.serialize() for item in serializable])

    @classmethod
    def deserialize(cls, data: bytes) -> "TxOutput":
        """Deserializes some transaction output bytes into an object.

        Args:
            data: bytes to deserialize and become a transaction output.

        Raises:
            ValueError: not enough bytes to build a correct object
        """
        # Check minimum length for a transaction output
        if len(data) < TX_OUTPUT_SIZE_MIN:
            raise ValueError("Not enough bytes to deserialize a transaction "
                             "output. Given %d, minimum %d" % (
                                len(data), TX_OUTPUT_SIZE_MIN))

        # Deserialization
        offset = 0

        # # Satoshis value
        satoshis = U8BLEInt.deserialize(data[offset:offset+8])
        offset += 8

        # # Script length
        script_len = VarInt.deserialize(data[offset:])
        offset += len(script_len)

        # # Script
        script_bytes = data[offset:offset+script_len.value]
        offset += len(script_bytes)
        script = ScriptPubKey.deserialize(script_bytes)  # type: ScriptPubKey

        return cls(satoshis.value, script)

    @property
    def value(self) -> int:
        """Returns the value of the output in satoshis."""
        return self._value.value

    @property
    def script(self) -> ScriptPubKey:
        """Returns the associated ``ScriptPubKey``."""
        return self._script

    @script.setter
    def script(self, new_script: ScriptPubKey) -> None:
        """Sets the new script of the transaction output."""
        assert isinstance(new_script, ScriptPubKey)
        TxInputOutput.script.__set__(self, new_script)  # type: ignore

    @property
    def btc(self) -> str:
        """Returns the output value in bitcoins.

        See also:
            :py:meth:`bitcoin.satoshi_to_btc` to see why a :py:class:`str` is
            returned and not a :py:class:`float`
        """
        return satoshi_to_btc(self._value.value)

    def encode(self) -> JSONType:
        tx_output = OrderedDict((
            ("value", float(self.btc)),
            ("n", None),
            ("scriptPubKey", self.script.encode())
        ))
        # n if has related transaction
        if self._tx is None:
            tx_output.pop("n")
        else:
            tx_output["n"] = self._tx.outputs.index(self)
        return tx_output

    def __str__(self, space: str = "", output_n: int = None) -> str:
        """Returns the transaction output in a useful, printable way."""
        out = ""
        if output_n is not None:
            out += "%s// output %02d\n" % (space, output_n)
        out += "%svalue:        %s (%s BTC)\n" % \
            (space, self._value, satoshi_to_btc(self._value.value))
        script = ScriptPubKey() if self._script is None else self._script
        out += "%sscriptSize:   %s\n" % (space, VarInt(len(script)))
        out += "%sscript:       %s\n" % (space, str(self._script))
        return out
