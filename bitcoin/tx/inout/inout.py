"""Defines the base for a transaction input and a transaction output.

In order to link transaction inputs and outputs to the transaction they
belong to, we define the class ``TxInputOutput``. This handles the two-way
link between a transaction input or output and the transaction they belong
two. Then, a transaction can know their inputs and outputs and those can know
the transaction they belong to.

Also handles the link of the _script_ they contain, so the script can know the
transaction input or output they belong to and if the script is moved to
another output or input, both new and old input / output will get notified
about the change so the links are updated.

As it is a link and not a list of links, a transaction input or output can
only be assigned to one transaction. If assigned to multiple ones, just the
last one will have the link to it.

In order to place this link to parent transaction, this class is a base for a
transaction input, ``TxInput`` and for a transaction output ``TxOutput``.

This link can be dynamically changed, so the setter will update and inform
the parent transaction of the change if necessary (and remove the input or
output from the transaction inputs / outputs if needed).
"""

# Libraries
from abc import ABCMeta
from typing import TypeVar, Generic

# Relative imports
from ...interfaces import Serializable
from ...script import TransactionScript

# Constants
T = TypeVar("T", bound=TransactionScript)


class TxInputOutput(Generic[T], Serializable, metaclass=ABCMeta):
    """Transaction input / output _script_ and _tx_ reference handler

    Handles two way references to the transaction input / output parent
    transaction and contained script.

    Attributes:
        _script (TransactionScript): Script to send the output to or to
                                     spend an input
        _tx (Tx): Transaction the input / output belongs to
    """
    __slots__ = "_script", "_tx"

    def __init__(self, script: T) -> None:
        """Initializes the transaction input / output references."""
        self._script = script
        self.script = script
        self._tx = None

    @property
    def script(self) -> T:
        """Returns the script of the input / output contains, if any."""
        return self._script

    @script.setter
    def script(self, new_script: T) -> None:
        """Sets the script of the input / output.

        Args:
            new_script (TransactionScript): Script of the input / output
                Will also set the input / output of the script as itself.
        Raises:
            ValueError: if the script already belongs to an input/output.
        """
        # Type assertions
        assert isinstance(new_script, TransactionScript)

        # Check if new script has been assigned already
        if new_script._parent is not None:
            raise ValueError("That script is already linked to one i/o.")

        # Update current script reference
        if hasattr(self, "_script"):
            self._script._parent = None
        new_script._parent = self
        self._script = new_script
