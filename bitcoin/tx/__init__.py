"""
Models a Bitcoin transaction and gives methods to also calculate the
pseudo-transactions used for signing
"""
# Relative imports
from .inout import TxInputOutput, TxInput, TxOutpoint, TxOutput
from .model import Tx
from .signaturedata import DEFAULT_HASHTYPE, HashType, SignatureData, \
    SegWitSignatureData, LegacySignatureData, HashTypeValue, HashTypeFlag

# Exports
__all__ = [
    "Tx", "HashType", "DEFAULT_HASHTYPE", "TxInputOutput", "TxInput",
    "TxOutpoint", "TxOutput", "SignatureData", "SegWitSignatureData",
    "LegacySignatureData", "HashTypeFlag", "HashTypeValue"
]
