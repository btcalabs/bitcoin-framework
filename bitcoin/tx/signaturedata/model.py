"""Defines how must behave a SignatureData object.

This object should collect and  provide all the data needed in order to make a
signature of one transaction and its related data.
"""

# Libraries
import logging
from abc import ABCMeta, abstractmethod

# Relative imports
from .hashtype import HashType, DEFAULT_HASHTYPE
from ..inout import TxOutput
from ..model import Tx
from ...crypto import double_sha256
from ...interfaces import Serializable

# Constants
LOGGER = logging.getLogger(__name__)


class SignatureData(Serializable, metaclass=ABCMeta):
    """Defines how the message that will be signed finally is created.

    Attributes:
        _tx (Tx): Transaction being signed
        _input_n (int): Index of the input where the signature goes to
        _utxo (TxOutput): Previous output being spent by the input specified
                          by the index
        _hashtype (HashType): Type of signature being computed

    """

    __slots__ = "_tx", "_input_n", "_utxo", "_hashtype"

    def __init__(self, tx: Tx, input_n: int, utxo: TxOutput,
                 hashtype: HashType = DEFAULT_HASHTYPE) -> None:

        # Checks
        assert len(tx.inputs)
        assert input_n < len(tx.inputs)

        if not len(utxo.script):
            LOGGER.warning("Trying to spend an output with an empty script.")

        # Assignments
        inp = tx.inputs[input_n]

        # If the output is related to one Tx
        if utxo._tx is not None:
            # Check vout
            if utxo._tx.id != inp.utxo_id:
                raise ValueError("Input is not spending the passed UTXO.")
            if utxo._tx.outputs.index(utxo) != inp.utxo_n:
                raise ValueError("The output passed is not related to that "
                                 "input")
        self._tx = tx
        self._input_n = input_n
        self._utxo = utxo
        self._hashtype = hashtype

    @abstractmethod
    def msg(self) -> bytes:
        """Creates the message that will be laterly signed."""
        raise NotImplementedError

    def signable_msg(self) -> bytes:
        return double_sha256(self.msg())

    def serialize(self) -> bytes:
        return self.signable_msg()

    @classmethod
    def deserialize(cls, data: bytes) -> "SignatureData":
        raise NotImplementedError
