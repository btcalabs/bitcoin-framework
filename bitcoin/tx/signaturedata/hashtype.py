"""Defines all the different ways a signature message can be generated."""

# Libraries
from enum import Enum
from typing import Tuple, Union

# Relative imports
from ...field import Field, U4BLEInt, S4BLEInt


# Enums
class HashTypeValue(Enum):
    """Available hashtype values."""
    ALL = 0x01
    NONE = 0x02
    SINGLE = 0x03


class HashTypeFlag(Enum):
    """Available hashtype value modifiers."""
    ANYONECANPAY = 0x80


# Constants
HASHTYPE_VALUES_LIST = [hashtype.value for hashtype in HashTypeValue]
HASHTYPE_VALUES_LIST.sort(reverse=True)
HASHTYPE_VALUES = tuple(HASHTYPE_VALUES_LIST)
"""
    List[int]: List of hashtype values, sorted descendent
"""
HASHTYPE_MAP = {hashtype.value: hashtype for hashtype in HashTypeValue}
"""
    dict: Maps the hashtype value to its name
"""


class HashType(Field):
    """Hashtype field.

    Specifies how to create the signature message and therefore its hash.

    **Sources:**

    - https://en.bitcoin.it/wiki/OP_CHECKSIG
    - https://bitcoin.org/en/developer-guide#signature-hash-types

    Attributes:
        _flags: Flags of the hashtype.
    """

    def __init__(self, value: HashTypeValue,
                 flags: Union[Tuple[HashTypeFlag, ...], HashTypeFlag] = None)\
            -> None:
        """Initializes the hashtype given the value of it."""
        # Type assertions
        assert isinstance(value, HashTypeValue)
        assert ((isinstance(flags, tuple)
                and all([isinstance(f, HashTypeFlag) for f in flags]))) \
            or isinstance(flags, HashTypeFlag) or flags is None

        # Store value
        super().__init__(value)

        # Store flags
        if isinstance(flags, tuple):
            self._flags = flags
        elif isinstance(flags, HashTypeFlag):
            self._flags = (flags, )
        else:
            self._flags = tuple()

    @property
    def flags(self) -> Tuple[HashTypeFlag, ...]:
        """List of flags of the hashtype."""
        return self._flags

    @property
    def hashtype(self) -> HashTypeValue:
        """Returns the hashtype enum item."""
        return self._value

    def has_flag(self, flag: HashTypeFlag) -> bool:
        """Returns if the hashtype has the specified flag."""
        assert isinstance(flag, HashTypeFlag)

        return flag in self._flags if self._flags is not None else False

    def serialize(self, compressed=False) -> bytes:
        """Serializes the hashtype applying flags to the value.

        Args:
            compressed: Returns the ``hashtype`` in just 1 byte, as expected in
                        a signature field inside a ``ScriptSig``
        """
        # Apply flags
        value = self._value.value

        if self._flags is not None:
            for flag in self._flags:
                value |= flag.value

        return bytes([value]) if compressed else U4BLEInt(value).serialize()

    @classmethod
    def deserialize(cls, data: bytes) -> "HashType":
        """Deserializes the hashtype given, extracting its value and flags.

        Raises:
            ValueError: Hashtype has not been found.
        """
        assert isinstance(data, bytes)

        # Convert to int
        data_value = U4BLEInt.deserialize(data).value

        # Extract flags
        flags = tuple(f for f in HashTypeFlag if data_value & f.value != 0)

        # Extract value
        value = None
        for hashtype in HASHTYPE_VALUES:
            if data_value & 0x1f == hashtype:
                value = hashtype
                break

        # Check if got value and match it with the enum
        if value is None:
            hashtype = HashTypeValue.ALL
        else:
            hashtype = HASHTYPE_MAP[value]

        return cls(hashtype, flags)

    @classmethod
    def from_integer(cls, n: int) -> "HashType":
        """Deserializes the integer into a hashtype.

        Raises:
            ValueError: Number to big to be deserialized
        """
        assert isinstance(n, int)

        # Not in range
        if not -0xffffffff <= n < 0xffffffff:
            raise ValueError("Number not in range to be deserialized as a "
                             "hashtype.")

        # Convert to bytes
        n_bytes = S4BLEInt(n).serialize()

        return cls.deserialize(n_bytes)

    def __repr__(self) -> str:
        """Represents the hashtype, with its flags if any."""
        def repr_item(item):
            return "{}.{}".format(item.__class__.__name__,
                                  item.name)

        def repr_list(items):
            return "({}{})".format(
                ", ".join([repr_item(item) for item in items]),
                ", " if len(items) == 1 else ""
            )

        if len(self._flags) == 0:
            return "{}({})".format(self.__class__.__name__,
                                   repr_item(self._value))
        else:
            return "{}({}, {})".format(self.__class__.__name__,
                                       repr_item(self._value),
                                       repr_list(self._flags))

    def __str__(self) -> str:
        """Returns the hashtype representation with flags.

        Contains the name of the hashtype value and its flags, appended using
        the vertical bar character "``|``".

        For instance:
        ``ALL|ANYONECANPAY``
        """
        text = self._value.name
        for flag in self._flags:
            text += "|{}".format(flag.name)
        return text


# Constants
DEFAULT_HASHTYPE = HashType(HashTypeValue.ALL)
"""
    HashTypeValue: default hashtype to use if not specified
"""
