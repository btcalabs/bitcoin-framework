"""Creates the data to sign in a witness signature."""

# Relative imports
from .hashtype import HashTypeFlag, HashTypeValue
from .model import SignatureData, HashType, DEFAULT_HASHTYPE
from ..inout import TxOutput
from ..model import Tx
from ...crypto import double_sha256
from ...field import U4BLEInt, U8BLEInt


class SegWitSignatureData(SignatureData):

    def __init__(self, tx: Tx, input_n: int, utxo: TxOutput,
                 hashtype: HashType = DEFAULT_HASHTYPE) -> None:

        # Assert TxOuput has value
        if utxo.value == 0:
            raise ValueError("SegWit signature data requires a reference to "
                             "the UTXO's value, that obviously can't be 0")
        elif len(utxo.script) == 0:
            raise ValueError("Empty ScriptPubKey can't be signed.")

        # TODO: that's a fix for mypy, solve that
        self._tx = tx
        self._input_n = input_n
        self._utxo = utxo
        self._hashtype = hashtype

        super().__init__(tx, input_n, utxo, hashtype)

    def msg(self) -> bytes:
        """Creates the data whose hash will be signed in SegWit inputs.

        **Sources:**

        - https://github.com/bitcoin/bips/blob/master/bip-0143.mediawiki
        """
        # 1. nVersion: Version
        version = U4BLEInt(self._tx.version).serialize()

        # 2. hashPrevouts: Hash of previous outputs
        hash_prevouts = bytes(32)  # ANYONECANPAY
        if not self._hashtype.has_flag(HashTypeFlag.ANYONECANPAY):
            # Serialize all inputs' UTXOs
            utxos_serializations = []
            for tx_input in self._tx.inputs:
                utxos_serialization = \
                    tx_input.utxo_id[::-1] + \
                    U4BLEInt(tx_input.utxo_n).serialize()
                utxos_serializations.append(utxos_serialization)
            utxos = b''.join(utxos_serializations)
            # Double hash them
            hash_prevouts = double_sha256(utxos)

        # 3. hashSequence: Hash of the sequence
        hash_sequence = bytes(32)  # ANYONECANPAY, SINGLE, NONE
        if not self._hashtype.has_flag(HashTypeFlag.ANYONECANPAY) and \
            self._hashtype.value != HashTypeValue.SINGLE and \
                self._hashtype.value != HashTypeValue.NONE:
            sequences_serializations = list()
            for tx_input in self._tx.inputs:
                sequences_serializations.append(
                    tx_input.sequence.to_bytes(4, "little"))
            sequences = b''.join(sequences_serializations)
            hash_sequence = double_sha256(sequences)

        # 4. outpoint: Outpoint
        tx_input = self._tx.inputs[self._input_n]
        outpoint = tx_input.utxo_id[::-1] + \
            U4BLEInt(tx_input.utxo_n).serialize()

        # 5. scriptCode: scriptCode of the input
        # TODO(davidlj): P2WPKH to legacy P2PKH
        # TODO(davidlj): P2WSH OP_CODESEPARATOR special remove
        if self._utxo.script.serialize()[0] == 0 and \
                len(self._utxo.script.serialize()[2:]) == 20:
            script_code = b'\x19\x76\xa9\x14%b\x88\xac' %  \
                          self._utxo.script.serialize()[2:]

        # 6. value
        value = U8BLEInt(self._utxo.value).serialize()

        # 7. nSequence: sequence of the input
        sequence = tx_input.sequence.to_bytes(4, "little")

        # 8. hashOutputs: Hash of the tx outputs
        hash_outputs = bytes(32)  # ANYONECANPAY, NONE
        if self._hashtype.value != HashTypeValue.SINGLE and \
                self._hashtype.value != HashTypeValue.NONE:
            outputs_serializations = []
            for output in self._tx.outputs:
                outputs_serializations.append(output.serialize())
            outputs = b''.join(outputs_serializations)
            hash_outputs = double_sha256(outputs)

        elif self._hashtype.value == HashTypeValue.SINGLE \
                and self._input_n < len(self._tx.outputs):
            output = self._tx.outputs[self._input_n]
            hash_outputs = double_sha256(output.serialize())

        # 9. nLocktime: Locktime of the transaction
        locktime = self._tx.locktime.to_bytes(4, "little")

        # 10. sighash type of the signature
        hashtype = self._hashtype.serialize()

        return b''.join([version, hash_prevouts, hash_sequence, outpoint,
                         script_code, value, sequence, hash_outputs,
                         locktime, hashtype])
