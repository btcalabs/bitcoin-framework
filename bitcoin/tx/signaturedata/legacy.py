"""Defines and creates all the data that will be signed in a legacy tx.

Creates and fulfills all the data needed to create the signature for the
current transaction depending on the sighash selected (by default sighash
ALL is selected).

**Sources:**
- http://enetium.com/resources/Bitcoin.pdf
- https://en.bitcoin.it/wiki/OP_CHECKSIG
"""

# Relative imports
from .hashtype import HashTypeValue, HashTypeFlag
from .model import SignatureData
from ..model import Tx
from ...field import S4BLEInt, U4BLEInt, VarInt, OP_CODESEPARATOR
from ...script import ScriptSig, ScriptPubKey


class LegacySignatureData(SignatureData):

    def msg(self) -> bytes:
        """Computes the data for the message that will be finally signed."""
        # Prepare a copy of the transaction
        signable_tx = Tx.deserialize(self._tx.serialize())

        # Define final serialization fields
        f_version = S4BLEInt(signable_tx.version).serialize()
        f_inputs_n = VarInt(len(signable_tx.inputs)).serialize()
        f_inputs = bytes()
        f_outputs_n = VarInt(len(signable_tx.outputs)).serialize()
        f_outputs = bytes()
        f_locktime = U4BLEInt(signable_tx.locktime).serialize()
        f_hashtype = self._hashtype.serialize()

        # Assign the scriptpubkey to the input to sign excluding all the
        # occurrences of OP_CODESEPARATOR in it
        script_code = list()
        for el in self._utxo.script.items:
            if el != OP_CODESEPARATOR():
                script_code.append(el)
        signable_tx.inputs[self._input_n].script = ScriptSig(script_code)

        # Empty other scripts
        for i in range(len(signable_tx.inputs)):
            if i != self._input_n:
                signable_tx.inputs[i].script = ScriptSig()

        # Apply corrections to match hashtype conventions
        if self._hashtype.value == HashTypeValue.ALL:
            # Requirements already satisfied
            pass

        elif self._hashtype.value == HashTypeValue.NONE or \
                self._hashtype.value == HashTypeValue.SINGLE:
            # Set to 0 all the sequence numbers except the current signed input
            for i in range(len(signable_tx.inputs)):
                if i != self._input_n:
                    signable_tx.inputs[i].sequence = 0

            if self._hashtype.value == HashTypeValue.NONE:
                # The number of transaction outputs is set to zero
                f_outputs_n = VarInt(0).serialize()
                # All transaction outputs are removed
                signable_tx.empty_outputs()
                # signable_tx.outputs = list()

            if self._hashtype.value == HashTypeValue.SINGLE:
                # The number of transaction outputs is set to the currently
                # signed transaction input index plus one
                f_outputs_n = VarInt(self._input_n + 1).serialize()
                # All transaction outputs up to the currently signed one are
                # emptied
                for tx_out in signable_tx.outputs[self._input_n + 1:]:
                    signable_tx.remove_output(tx_out)
                for out in signable_tx.outputs[:-1]:
                    out._value.value = 0xffffffffffffffff
                    out.script = ScriptPubKey()

        if self._hashtype.has_flag(HashTypeFlag.ANYONECANPAY):
            # The number of transaction inputs is set to one
            f_inputs_n = VarInt(1).serialize()
            # All transaction inputs, except for the currently signed one,
            # are removed
            signed_input = signable_tx.inputs[self._input_n]
            signable_tx.empty_inputs()
            signable_tx.add_input(signed_input)

        # Fill inputs
        for inp in signable_tx.inputs:
            f_inputs += inp.serialize()

        # Fill outputs
        for out in signable_tx.outputs:
            f_outputs += out.serialize()

        # Join all fields and return
        serialization = b''.join([
            f_version, f_inputs_n, f_inputs, f_outputs_n, f_outputs,
            f_locktime, f_hashtype
        ])

        return serialization

    @classmethod
    def deserialize(cls, data: bytes) -> "LegacySignatureData":
        raise NotImplementedError
