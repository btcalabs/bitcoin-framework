"""Creates data to be signed in order to authorize transactions."""

# Relative imports
from .hashtype import HashType, HashTypeFlag, HashTypeValue, DEFAULT_HASHTYPE
from .legacy import LegacySignatureData
from .model import SignatureData
from .segwit import SegWitSignatureData

__all__ = [
    "HashType", "HashTypeFlag", "HashTypeValue", "DEFAULT_HASHTYPE",
    "LegacySignatureData", "SignatureData", "SegWitSignatureData"
]
