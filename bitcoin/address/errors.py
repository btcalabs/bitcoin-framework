"""Declares the exceptions that can occur around the address package."""
# Libraries
from typing import Union, Tuple

# Relative imports
from ..params import AddressTypes


class AddressPrefixError(ValueError):
    """Prefix* not known (or missing). Therefore if present, it is not valid.

    Note:
       *Prefix stands for a `base58`_ address byte prefix.

       .. _base58: https://en.bitcoin.it/wiki/Base58Check_encoding

    Note:
       Just the prefixes for the implemented addresses are listed.

       Known prefixes are listed in
       :py:class:`bitcoin.params.MAINNET_PREFIXES` and
       :py:class:`bitcoin.params.TESTNET_PREFIXES`

       Full list of known addresses prefixes is available at:

       https://en.bitcoin.it/wiki/List_of_address_prefixes
    """
    pass


class AddressChecksumError(ValueError):
    """Checksum check failed for an address."""
    pass


class AddressLengthError(ValueError):
    """Address bytes length invalid for a specific address type."""
    pass


class AddressTypeError(ValueError):
    """Address bytes deserialized belong to another address type.

    For instance, if deserializing / decoding a :py:class:`P2SHAddress` using
    the :py:class:`bitcoin.P2PKHAddress` class.
    """
    __slots__ = "expected", "actual"

    def __init__(self, expected: Union[AddressTypes, Tuple[AddressTypes, ...]],
                 actual: AddressTypes = None) -> None:
        """Initializes given the expected and actual types.

        Args:
            expected: expected address type to receive
                      (or a list of expected addresses types)
            actual: received address type
        """
        self.expected = expected
        self.actual = actual

        expected_name = expected.name if isinstance(expected, AddressTypes) \
            else " or ".join([t.name for t in expected])
        actual_name = actual.name if isinstance(actual, AddressTypes) \
            else "unknown address type"

        super().__init__("Address type error: expected %s, actual: %s" % (
            expected_name, actual_name))
