"""WIF address model.

Models a WIF (*Wallet Import Format*) address type, that contains a
private ECDSA key.

**Sources:**
- https://en.bitcoin.it/wiki/Wallet_import_format
"""

# Relative imports
from .errors import AddressTypeError, AddressLengthError
from .base58encodable import Base58EncodableAddress
from ..crypto import ECDSAPrivateKey, ECDSAPublicKey
from ..params import get_default_network, AddressTypes, BlockchainParams

# Constants
PREFIX_SIZE = 1
"""
    int: size in bytes of the WIF address prefixes
"""
PRIVATE_KEY_SIZE = 32
"""
    int: size in bytes of an ECDSA private key (256 bits as using ``secp256k1``
    )
"""
COMPRESSED_SUFFIX = b'\x01'
"""
    bytes: byte to append to the WIF address prior encoding it in order to
    indicate we want the derived public key of that private key contained
    to be compressed.
"""
COMPRESSED_SIZE = len(COMPRESSED_SUFFIX)
"""
    int: size in bytes of the byte appended to the end of a WIF address that
    indicates whether the public key derived must be have a compressed
    serialization by default or not
"""
BASE_ADDRESS_SIZE = PREFIX_SIZE + PRIVATE_KEY_SIZE + \
                    Base58EncodableAddress.CHECKSUM_SIZE
"""
    int: size in bytes of a WIF address, without the compressed byte suffix
"""
ADDRESS_SIZE_UNCOMPRESSED = BASE_ADDRESS_SIZE
"""
    int: size in bytes of WIF addresses marked to derive an uncompressed
    serialization of the public key
"""
ADDRESS_SIZE_COMPRESSED = BASE_ADDRESS_SIZE + COMPRESSED_SIZE
"""
    int: size in bytes of WIF addresses marked to derive a compressed
    serialization of the public key
"""


# Errors
class AddressPrivateKeyLengthError(ValueError):
    """The private key value length is not valid."""
    pass


# Classes
class WIFAddress(Base58EncodableAddress):
    """`WIF`_ address model.

    .. _wif: https://en.bitcoin.it/wiki/Wallet_import_format

    `Wallet Import Format`__ address model.

    __ WIF_

    Allows to serialize, deserialize, encode and decode WIF addresses
    so the private key can be easily set and retrieved. Also allows to derive
    the public key from the private key in the specified form (compressed or
    uncompressed).

    Attributes:
        _compressed (bool): whether if asking for the public key, the object
                            will have by default compressed serialization.
        _private_key (ECDSAPrivateKey): private key of the address
    """
    __slots__ = "_compressed", "_private_key"

    def __init__(self, private_key: ECDSAPrivateKey, compressed: bool = True,
                 network: BlockchainParams = None) -> None:
        """Initializes a WIF address given the private key and network.

        Args:
            private_key: private ECDSA key to set in the address
            compressed: if public key must have compressed
                        serialization by default. ``True`` by default.
            network: network the address operates on.
                     Will use default (
                     :py:meth:`bitcoin.params.get_default_network`)
                     if no network is passed
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(private_key, ECDSAPrivateKey)
        assert isinstance(network, BlockchainParams)

        # Compute values
        prefix = network.address_prefix(AddressTypes.WIF)
        value = private_key.serialize()

        if compressed:
            value = value + COMPRESSED_SUFFIX

        # Initialize
        super().__init__(prefix, value)

        # Assignments
        self._compressed = compressed
        self._private_key = private_key

    # Properties
    @property
    def private_key(self) -> ECDSAPrivateKey:
        """Private key object the address represents."""
        return self._private_key

    @property
    def type_byte(self) -> bytes:
        """Returns the type byte that marks if the privkey is compressed."""
        return self._data[-1:] if self.is_compressed else b''

    @property
    def data(self) -> bytes:
        """Data transferred in the ``WIF`` address.

        It comprehends a private key exponent (256 bits, 32 bytes) and a byte
        indicating whether if the public key derived must be represented in
        the compressed form.
        """
        return self._data

    @property
    def public_key(self) -> ECDSAPublicKey:
        """Public key object the address represents.

        The public key will have either compressed or uncompressed form by
        default depending on the form specified in the WIF address.
        """
        public_key = self._private_key.to_public()
        return ECDSAPublicKey(*public_key.point, self._compressed)

    @property
    def is_compressed(self) -> bool:
        """If the public key's serialization will be compressed or not."""
        return self._compressed

    # Serialize / deserialize
    def serialize(self) -> bytes:
        """Serializes the WIF address using the private key object."""
        return b"%b%b%b%b" % (
            self._prefix,
            self._private_key.serialize(),
            COMPRESSED_SUFFIX if self.is_compressed else b'',
            self.checksum
        )

    @classmethod
    def deserialize(cls, address: bytes) -> "WIFAddress":
        """Deserializes a WIF address given the address bytes.

        Tries to guess the prefix in order to get the type and network the
        private key must be used on and then obtains the private key while
        also checking if the provided checksum bytes are correct.

        Args:
            address: address to deserialize

        Returns:
            a new :py:class:`WIFAddress` object

        Raises:
            AddressLengthError: address bytes do not have proper length
            AddressTypeError: address bytes do not represent a WIF address
            AddressChecksumError: address bytes contain invalid checksum
        """
        # Check length and detect compression
        if len(address) == ADDRESS_SIZE_UNCOMPRESSED:
            compressed = False
        elif len(address) == ADDRESS_SIZE_COMPRESSED:
            compressed = True
        else:
            raise AddressLengthError

        # Basic deserialization
        address_model = Base58EncodableAddress.deserialize(address)

        # Type check
        if address_model.type != AddressTypes.WIF:
            raise AddressTypeError(AddressTypes.WIF, address_model.type)

        # Separe compressed byte
        private_key_value = address_model.data
        if compressed:
            private_key_value = private_key_value[:-1]
            # TODO: check the compressed byte is b'\x01' or implement BIP178
        private_key = ECDSAPrivateKey(
            int.from_bytes(private_key_value, "big"))

        return cls(private_key, compressed, address_model.network)
