"""Base58 encodable addresses (legacy & nested SegWit)."""

# Libraries
from typing import Optional, Tuple

# Relative imports
from .model import Address
from .errors import AddressPrefixError, AddressChecksumError
from ..crypto import checksum
from ..encoding import b58encode, b58decode
from ..params import BlockchainParams, AddressTypes, get_default_network, \
    BLOCKCHAINS, get_address_type_from_index


# Methods
def find_prefix(address: bytes) -> Optional[Tuple[BlockchainParams,
                                                  AddressTypes]]:
    """Returns the network and address type given an address with its prefix.

    Given an address, tries to find the network and type of the address and
    returns a tuple containing them, or ``None`` if prefix can't be found.

    Args:
        address: Address as bytes object to find its prefix first and
                 therefore its type and network

    Returns:
        Tuple containing the network and type or ``None`` if prefix couldn't be
        guessed
    """
    # Find network and type
    found_prefix = False
    # Loop networks
    for network in BLOCKCHAINS:
        # Loop network prefixes
        for prefix in network.address_prefixes:
            # Check if prefix matches
            if address.startswith(prefix):
                atype_index = network.address_prefixes.index(prefix)
                atype = get_address_type_from_index(atype_index + 1)
                found_prefix = True
                # Stop network prefixes loop if found prefix
                break
        # Stop network loop if found prefix
        if found_prefix:
            break

    # Return match if any
    return (network, atype) if found_prefix else None


class Base58EncodableAddress(Address):
    """Legacy and nested SegWit addresses model (they use ``base58`` encoding).

    Attributes:
        _prefix (bytes): Address prefix to identify its network and type
    """
    __slots__ = "_prefix",

    CHECKSUM_SIZE = 4
    """
        int: Size in bytes of the checksum obtained using the
        :py:meth:`bitcoin.crypto.checksum` method
    """

    def __init__(self, prefix: bytes, data: bytes) -> None:
        """Initializes an address based on its prefix and value.

        Args:
            prefix: Address prefix as bytes.
            data: Address information.

        Raises:
            AddressPrefixError: Prefix passed is not known.
        """
        # Type assertions
        assert isinstance(prefix, bytes)
        assert isinstance(data, bytes)

        # Obtain information from prefix
        found_info = find_prefix(prefix)
        if found_info is None:
            raise AddressPrefixError(
                "Given address prefix (%s) is not valid" % prefix.hex())
        else:
            network, atype = found_info

        # Assignments
        self._prefix = prefix

        super().__init__(atype, data, network)

    @staticmethod
    def from_type_and_network(atype: AddressTypes,
                              data: bytes, network: BlockchainParams = None) \
            -> "Base58EncodableAddress":
        """Creates a Base58EncodableAddress given the network, type and script.

        The prefix is obtained using the network and type of address.

        Args:
            atype: Address type (``P2PKH``, ``P2SH``, ...)
            data: Data contained in the address
            network: Network of the address
                     Will use selected default if not passed

        Returns:
            New ``base58`` address with those values.
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(atype, AddressTypes)
        assert isinstance(data, bytes)
        assert isinstance(network, BlockchainParams)

        # Obtain prefix
        prefix = network.address_prefix(atype)

        # Build and return address
        return Base58EncodableAddress(prefix, data)

    # Properties
    @property
    def checksum(self) -> bytes:
        """Checksum to introduce in the address serialization."""
        return checksum(self._prefix + self._data)

    @property
    def prefix(self) -> bytes:
        """Returns the address prefix."""
        return self._prefix

    # Serialization
    def serialize(self) -> bytes:
        """Combines all the information necessary to encode it using base58.

        Returns the prefix, data and checksum joined all together.
        """
        return self.prefix + self._data + self.checksum

    @classmethod
    def deserialize(cls, address: bytes) -> "Base58EncodableAddress":
        """Converts bytes into a :py:class:`Base58EncodableAddress` object.

        Given an address as an array of bytes, try to guess information from
        the address using the prefix (type of address and network).

        After that, splits the rest of bytes into the checksum and the data,
        checks if the checksum is valid and returns a new address object with
        everything set.

        Args:
            address: Address to deserialize

        Returns:
            The :py:class:`Base58EncodableAddress` obtained

        Raises:
            AddressPrefixError: Prefix unknown or missing prefix
            AddressChecksumError: Address included checksum is invalid
        """
        # Find prefix
        found_info = find_prefix(address)
        if found_info is None:
            raise AddressPrefixError(
                "Address bytes (%s) invalid, no known prefix found." %
                (address.hex()))
        network, atype = found_info
        prefix = network.address_prefix(atype)

        # Separate checksum
        value = address[len(prefix):]
        address_checksum = value[-cls.CHECKSUM_SIZE:]
        address_data = value[:-cls.CHECKSUM_SIZE]

        # Construct object
        address_object = cls(prefix, address_data)

        # Validate checksum
        if address_checksum != address_object.checksum:
            raise AddressChecksumError(
                "%s != %s" % (address_object.checksum.hex(),
                              address_checksum.hex()))

        return address_object

    def encode(self) -> str:
        """Encodes the address using :py:mod:`base58`."""
        return b58encode(self.serialize())

    @classmethod
    def decode(cls, address: str) -> "Base58EncodableAddress":
        """Decodes the address using :py:mod:`base58`."""
        return cls.deserialize(b58decode(address))
