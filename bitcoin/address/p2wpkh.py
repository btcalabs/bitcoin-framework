"""P2WPKH address model."""

# Relative imports
from .bech32encodable import Bech32EncodableAddress
from .errors import AddressTypeError
from .model import PaymentAddress
from ..params import AddressTypes, BlockchainParams, get_default_network
from ..script import P2WPKHScriptPubKey


class P2WPKHAddress(Bech32EncodableAddress, PaymentAddress):
    """`P2WPKH`_ address model.

    .. _P2WPKH: https://github.com/libbitcoin/libbitcoin/wiki/\
    P2WPKH-Transactions

    `Pay-to-witness-public-key-hash`__ address.

    __ P2WPKH_

    Allows to serialize, deserialize, encode and decode P2WPKH addresses so the
    public key hash and the :py:class:`bitcoin.P2WPKHScriptPubKey`
    can be easily retrieved and set and checksum calculations are
    performed automatically.

    These addresses indicate that `native SegWit`__ must be used (via a
    :py:class:`bitcoin.P2WPKHScriptPubKey`) so it is spent by a
    :py:class:`bitcoin.P2WPKHScriptSig`).

    .. _native_segwit: https://bitcoincore.org/en/segwit_wallet_dev/\
    #native-pay-to-witness-public-key-hash-p2wpkh

    __ native_segwit_

    Attributes:
        _script_pubkey (P2WPKHScriptPubKey): Matching ``P2WPKHScriptPubKey``
    """
    __slots__ = "_script_pubkey"

    def __init__(self, script_pubkey: P2WPKHScriptPubKey,
                 network: BlockchainParams = None) -> None:
        """Initializes a new P2WPKH address.

        Args:
            script_pubkey: :py:class:`bitcoin.script.std.P2WPKHScriptPubKey` to
                           pay to
            network: network where the address belongs. Default network is
                     used (:py:meth:`bitcoin.params.get_default_network`)
                     if no network provided
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(script_pubkey, P2WPKHScriptPubKey)
        assert isinstance(network, BlockchainParams)

        # Initialize super
        super().__init__(AddressTypes.PKH, script_pubkey.data, 0, network)

        # Store ScriptPubKey
        self._script_pubkey = script_pubkey

    # Properties
    @property
    def data(self) -> bytes:
        """Same as :py:class:`P2WPKHAddress.script_pubkey.public_key_hash`."""
        return self._data

    @property
    def script_pubkey(self) -> P2WPKHScriptPubKey:
        """Returns the associated :py:class:`P2WPKHScriptPubKey`."""
        return self._script_pubkey

    # Serialize / deserialize
    @classmethod
    def decode(cls, address: str):
        """Decodes a P2WPKH address from a string into an address object.

        See also:
            :py:meth:`bitcoin.address.Bech32EncodableAddress.decode` as it
            is being used for the general string decoding and object
            creation. It may rise exceptions not listed here.

            :py:meth:`bitcoin.encoding.bech32decode` for `bech32` decoding
            details.

        Raises:
            AddressTypeError: decoded value is not a P2WPKH address.
        """
        address_model = Bech32EncodableAddress.decode(address)

        if address_model.type != AddressTypes.PKH:
            raise AddressTypeError(AddressTypes.PKH, address_model.type)

        script_pubkey = P2WPKHScriptPubKey(address_model.data)

        return cls(script_pubkey, address_model.network)
