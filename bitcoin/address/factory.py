"""Address factory module.

Allows to transform a string that is supposed to be an address to be decoded
without knowing its encoding.
"""
from typing import Union
from .model import Address, PaymentAddress
from .np2wpkh import NP2WPKHAddress
from .np2wsh import NP2WSHAddress
from .p2pkh import P2PKHAddress
from .p2sh import P2SHAddress
from .p2wpkh import P2WPKHAddress
from .p2wsh import P2WSHAddress
from .wif import WIFAddress
from ..crypto import ECDSAPrivateKey, ECDSAPublicKey
from ..standards import PaymentStandard, StdPaymentTypes, \
    StdPaymentImplementations, P2PKH, P2WPKH, NP2WPKH
from ..params import BlockchainParams, MAINNET, REGTEST, TESTNET
from ..script import P2PKHScriptPubKey, P2WPKHScriptPubKey, NP2WPKHScriptPubKey

MYPY = False
if MYPY:
    from typing import Type, Optional  # noqa: F401
    from ..script import StdP2PKHScriptPubKey  # noqa: F401

BASE58_ADDRESSES = NP2WPKHAddress, NP2WSHAddress, P2PKHAddress, P2SHAddress, \
                   WIFAddress
"""
    Sequence[Base58EncodableAddress]: Base58 address implementations.
"""
BECH32_ADDRESSES = P2WPKHAddress, P2WSHAddress
"""
    Sequence[Bech32EncodableAddress]: Bech32 address implementations.
"""


class AddressFactory:
    """Create addresses easily."""

    @classmethod
    def from_private_key(cls, private_key: Union[ECDSAPrivateKey, str],
                         payment_standard: Union[PaymentStandard, str],
                         blockchain: Union[BlockchainParams, None, str],
                         compressed: bool = None) -> PaymentAddress:
        """Creates an address given the private key that controls it.

        Args:
            private_key: Private key that will control the generated address
            payment_standard: Payment standard to use to pay.
            blockchain: Blockchain to spend the private key in.
            compressed: Whether if the public key derived and used to generate
                        the payment script will have compressed serialization.

        Raises:
            See :py:meth:`AddressFactory.from_public_key` for possible errors.

        Returns:
            Generated address
        """
        private_key_obj = ECDSAPrivateKey.from_hex(private_key) if \
            isinstance(private_key, str) else private_key
        return cls.from_public_key(private_key_obj.to_public(),
                                   payment_standard,
                                   blockchain, compressed)

    @classmethod
    def from_public_key(cls, public_key: Union[ECDSAPublicKey, str],
                        payment_standard: Union[PaymentStandard, str],
                        blockchain: Union[BlockchainParams, str, None],
                        compressed: bool = None) -> PaymentAddress:
        """Creates an address given a public key to pay to."""
        # Translations
        # TODO: Move to proper files (create EasyAddressFactory)
        public_key_obj = ECDSAPublicKey.from_hex(public_key) if \
            isinstance(public_key, str) else public_key
        if isinstance(payment_standard, str):
            payment_standard = payment_standard.upper()
            if payment_standard == "P2PKH":
                payment_standard_obj = P2PKH
            elif payment_standard == "P2WPKH":
                payment_standard_obj = P2WPKH
            elif payment_standard == "NP2WPKH":
                payment_standard_obj = NP2WPKH
            else:
                raise ValueError("Invalid payment standard")
        else:
            # TODO: Fix typing
            payment_standard_obj = payment_standard  # type: ignore
        if isinstance(blockchain, str):
            if blockchain == "mainnet":
                blockchain_obj = MAINNET  # type: Optional[BlockchainParams]
            elif blockchain == "testnet":
                blockchain_obj = TESTNET
            elif blockchain == "regtest":
                blockchain_obj = REGTEST
            else:
                raise ValueError("Network invalid")
        elif isinstance(blockchain, BlockchainParams):
            blockchain_obj = blockchain
        else:
            blockchain_obj = None

        # Convert compressed to specified argument
        if compressed is not None:
            p = public_key_obj.point
            public_key_obj = ECDSAPublicKey(p[0], p[1], compressed)

        # Check PKH as payment type
        if payment_standard_obj.payment_type != StdPaymentTypes.PKH:
            raise ValueError(
                "Private key to address requires a pubic key or public key "
                "hash payment type.")

        # Create script pubkey
        # TODO: Move code to ScriptFactory
        # TODO: Automate if / elif given a registry
        if payment_standard_obj.payment_implementation == \
                StdPaymentImplementations.LEGACY:
            script = P2PKHScriptPubKey  # type: Type[StdP2PKHScriptPubKey]
            address = P2PKHAddress  # type: Type[PaymentAddress]
        elif payment_standard_obj.payment_implementation == \
                StdPaymentImplementations.SEGWIT:
            script = P2WPKHScriptPubKey
            address = P2WPKHAddress
        else:
            script = NP2WPKHScriptPubKey
            address = NP2WPKHAddress
        script_pubkey = script.from_public_key(public_key_obj)
        # TODO: Fix when address.model.PaymentAddress has correct __init__
        address_guess = address(script_pubkey, blockchain_obj)  # type: ignore
        return address_guess

    @classmethod
    def decode(cls, address: str) -> Address:
        """Decodes the given address string into an address object.

        Guesses its codification and type by trying to decode every address
        implemented.

        Args:
            address: Address to decode

        Raises:
            ValueError: Cannot decode string to an address.

        Returns:
            Address: Address object matching.
        """
        guessed_address = None
        # Try bech32 first
        for address_class in BECH32_ADDRESSES:
            try:
                guessed_address = address_class.decode(address)  # type: ignore
            except ValueError:
                continue
            else:
                break
        if guessed_address is not None:
            return guessed_address
        # Try base58 after
        for address_class in BASE58_ADDRESSES:
            try:
                guessed_address = address_class.decode(address)  # type: ignore
            except ValueError:
                continue
            else:
                break
        if guessed_address is not None:
            return guessed_address
        else:
            raise ValueError("String \"%s\" does not match any known address"
                             % address)
