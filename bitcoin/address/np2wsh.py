"""Nested `P2SH-P2WSH`_ address module.

.. _p2sh-p2wsh: https://bitcoincore.org/en/segwit_wallet_dev/\
creation-of-p2sh-p2wsh-address
"""

# Relative imports
from .base58encodable import Base58EncodableAddress
from .model import PaymentAddress
from ..params import AddressTypes, BlockchainParams, get_default_network
from ..script import NP2WSHScriptPubKey


class NP2WSHAddress(Base58EncodableAddress, PaymentAddress):
    """Nested ```P2WSH`` in a ``P2SH`` address.

        Attributes:
            _script_pubkey (NP2WSHScriptPubKey): Matching ``ScriptPubKey``.
        """
    __slots__ = "_script_pubkey"

    def __init__(self, script_pubkey: NP2WSHScriptPubKey,
                 network: BlockchainParams = None) -> None:
        """Initializes a nested `P2SH-P2WSH`_ address.

        .. _p2sh-p2wsh: https://bitcoincore.org/en/segwit_wallet_dev/\
        creation-of-p2sh-p2wsh-address
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(script_pubkey, NP2WSHScriptPubKey)
        assert isinstance(network, BlockchainParams)

        # Initialize
        super().__init__(
            network.address_prefix(AddressTypes.SH),
            script_pubkey.data)

        # Store script pubkey
        self._script_pubkey = script_pubkey

    # Properties
    @property
    def data(self) -> bytes:
        """``P2SH`` script hash of the ``P2WSH`` ``ScriptPubKey``."""
        return self._data

    @property
    def script_pubkey(self) -> NP2WSHScriptPubKey:
        """Returns an associated ``NP2WSH`` ``ScriptPubKey``."""
        return self._script_pubkey

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, address: bytes) -> "NP2WSHAddress":
        """Deserializes some address bytes into a :py:class:`NP2WSHAddress`

        Warning:
            This will always raise a :py:class:`ValueError` as there's no way
            to obtain a *public key hash* from a :py:class:`NP2WSHAddress`
            because the address and the matching ScriptPubKey do not contain
            the *public key hash*, but its hash.
        """
        raise ValueError("A P2SH-P2WSH address cannot be deserialized as it "
                         "is alike to a regular P2SH. Use P2SH address "
                         "deserialization instead.")  # pragma: no cover")

    @classmethod
    def decode(cls, address: str) -> "NP2WSHAddress":
        """Decodes an address into a :py:class:`NP2WSHAddress`

        Warning:
            This will always raise a :py:class:`ValueError` as there's no way
            to obtain a *public key hash* from a :py:class:`NP2WSHAddress`
            because the address and the matching ScriptPubKey do not contain
            the *public key hash*, but its hash.
        """
        raise ValueError("A P2SH-P2WSH address cannot be decoded as it is "
                         "alike to a regular P2SH. Use P2SH address "
                         "deserialization instead.")  # pragma: no cover")

    # Magic methods
    def __repr__(self) -> str:
        """Returns the representation using the serialized ``ScriptPubKey``."""
        return "{}({}, {})".format(
            self.__class__.__name__,
            repr(self._script_pubkey),
            repr(self._network))
