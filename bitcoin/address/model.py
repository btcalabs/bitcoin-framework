"""Basic Bitcoin address model."""
# Libraries
from abc import ABCMeta, abstractmethod

# Relative imports
from ..interfaces import Encodable, Serializable
from ..params import AddressTypes, BlockchainParams, get_default_network
from ..script import StdScriptPubKey


class Address(Serializable, Encodable, metaclass=ABCMeta):
    """Bitcoin address model.

    Allows to store the main information contained in an address: the network
    and type of the address and the information contained inside.

    Depending on the address type (legacy or SegWit), the model may vary,
    mainly in its encoding.

    The model class abstracts the behaviour for all addresses (they contain a
    prefix indicating either the network and / or its type and some information
    transferred).

    Attributes:
        _network (BlockchainParams): Network the address belongs to
        _type (AddressTypes): Type of the address
        _data (bytes): Information the address is transferring.
                       The decoded address without prefix and checksum.
    """
    __slots__ = "_network", "_type", "_data"

    def __init__(self, atype: AddressTypes, data: bytes,
                 network: BlockchainParams = None) -> None:
        """Initializes an address.

        Args:
            atype: Address type.
                One of the :py:attr:`bitcoin.params.AddressTypes` enum
            data: Information carried by the address.
            network: Network the address belongs to
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(network, BlockchainParams)
        assert isinstance(atype, AddressTypes)
        assert isinstance(data, bytes)

        # Initializations
        self._network = network
        self._type = atype
        self._data = data

    # Properties
    @property
    def network(self) -> BlockchainParams:
        """Network the address belongs to."""
        return self._network

    @property
    def type(self) -> AddressTypes:
        """Type of the address."""
        return self._type

    @property
    def data(self) -> bytes:
        """Information the address is transferring."""
        return self._data

    # Magic methods
    def __str__(self) -> str:
        """Returns the encoded address."""
        return str(self.encode())

    def __repr__(self) -> str:
        """Returns the representation as the encoded address."""
        return "{}.decode(\"{}\")".format(
            self.__class__.__name__,
            str(self))

    def __eq__(self, other: object) -> bool:
        """Compares two addresses based on their encoding."""
        return self.encode() == other.encode() if isinstance(other, Address) \
            else super().__eq__(other)


class PaymentAddress(Address, metaclass=ABCMeta):
    """Address used to exchange payment information.

    Stores the matching standard ``ScriptPubKey`` the address represents.

    Attributes:
        _script_pubkey (StdScriptPubKey): Matching ``ScriptPubKey`` for the
                                          address.
    """
    __slots__ = ()

    # TODO: Common interface for all payment addresses
    def __inits__(self, script_pubkey: StdScriptPubKey,
                  blockchain: BlockchainParams) -> None:
        pass

    @property
    @abstractmethod
    def script_pubkey(self) -> StdScriptPubKey:
        """Associated standard ``ScriptPubKey``.

        Creates a new one each time, given the data transferred by the
        address.
        """
        pass  # pragma: no cover
