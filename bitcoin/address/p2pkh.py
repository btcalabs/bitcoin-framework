"""`P2PKH`_ address model.

.. _P2PKH: https://en.bitcoin.it/wiki/Transaction#Pay-to-PubkeyHash
"""

from .base58encodable import Base58EncodableAddress
# Relative imports
from .errors import AddressLengthError, AddressTypeError
from .model import PaymentAddress
from ..params import AddressTypes, BlockchainParams, PKH_SIZE, \
    get_default_network
from ..script import P2PKHScriptPubKey

# Constants
PREFIX_SIZE = 1
"""
    int: size in bytes of the P2PKH addresses' prefixes
"""
VALUE_SIZE = PKH_SIZE + Base58EncodableAddress.CHECKSUM_SIZE
"""
    int: size in bytes of addresses' value (key hash and checksum)
"""
ADDRESS_SIZE = PREFIX_SIZE + VALUE_SIZE
"""
    int: size in bytes of P2PKH addresses
"""


class P2PKHAddress(Base58EncodableAddress, PaymentAddress):
    """P2PKH_ address model.

    .. _P2PKH: https://en.bitcoin.it/wiki/Transaction#Pay-to-PubkeyHash

    `Pay-to-public-key-hash`__ address.

    __ P2PKH_

    Allows to serialize, deserialize, encode and decode P2PKH addresses so the
    public key hash and the :py:class:`bitcoin.script.std.P2PKHScriptPubKey`
    can be easily retrieved and set and checksum calculations are
    performed automatically
    """
    __slots__ = "_script_pubkey"

    # Constructors
    def __init__(self, script_pubkey: P2PKHScriptPubKey,
                 network: BlockchainParams = None) -> None:
        """Initializes a P2PKH address given a :py:class:`P2PKHScriptPubKey`.

        Args:
            script_pubkey: :py:class:`bitcoin.script.std.P2PKHScriptPubKey` to
                           pay to
            network: network where the address belongs. Default network is
                     used (:py:meth:`bitcoin.params.get_default_network`)
                     if no network provided
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(script_pubkey, P2PKHScriptPubKey)
        assert isinstance(network, BlockchainParams)

        # Initialize
        super().__init__(network.address_prefix(AddressTypes.PKH),
                         script_pubkey.data)

        # Store ScriptPubKey
        self._script_pubkey = script_pubkey

    # Properties
    @property
    def data(self) -> bytes:
        """Alias of :py:class:`P2PKHAddress.script_pubkey.public_key_hash`."""
        return self._data

    @property
    def script_pubkey(self) -> P2PKHScriptPubKey:
        """Returns the associated ``P2PKH`` ``ScriptPubKey``."""
        return self._script_pubkey

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, address: bytes) -> "P2PKHAddress":
        """Deserializes some address bytes into a :py:class:`P2PKHAddress`.

        Deserializes the given address as an array of bytes, guessing its
        prefix and saving its info (network and type, which must be ``P2PKH``).

        Also checks the checksum is valid.

        Args:
            address: Bytes object containing an address to deserialize

        See also:
            :py:meth:`bitcoin.address.Base58Encodable.deserialize` as it is
            being used to deserialize and create the base address object.
            It may raise exceptions not listed here.

        Raises:
            AddressLengthError: Invalid size in bytes for a P2PKH address.
            AddressTypeError: Address bytes are not a P2PKH address,
                              but they belong to another known valid
                              address type.

        Returns:
            New :py:class:`P2PKHAddress` containing the bytes information.
        """
        # Check size
        if len(address) != ADDRESS_SIZE:
            raise AddressLengthError

        # Basic deserialization
        address_model = Base58EncodableAddress.deserialize(address)

        # Check type is correct
        if address_model.type != AddressTypes.PKH:
            raise AddressTypeError(AddressTypes.PKH, address_model.type)

        return cls(
            P2PKHScriptPubKey(address_model.data),
            address_model.network)
