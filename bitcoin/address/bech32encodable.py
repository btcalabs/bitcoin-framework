"""`Bech32`_ encodable addresses for `native SegWit`__ ``ScriptPubKey`` s.

.. _bech32: https://en.bitcoin.it/wiki/Bech32
.. _native_segwit: https://bitcoincore.org/en/segwit_wallet_dev/\
    #advanced-designs

__ native_segwit_
"""

# Libraries
from typing import Optional

# Relative imports
from .errors import AddressTypeError
from .model import Address
from ..encoding import bech32encode, bech32decode
from ..params import BlockchainParams, BLOCKCHAINS, AddressTypes, \
    get_default_network, SH_SIZE_SEGWIT, PKH_SIZE
from ..script import StdSegWitScriptPubKey, WITNESS_VERSION_DEFAULT


# Exceptions
class AddressHRPError(ValueError):
    """The human redable part does not belong to any known network."""
    pass


def find_network_by_hrp(hrp: str) -> Optional[BlockchainParams]:
    """Given a bech32 address human readable part, returns its network.

    Loops all known networks and extracts the human readable parts and
    returns the first match.

    Returns:
        the network the address hrp belongs to or None if no network is found
    """
    network = [n for n in BLOCKCHAINS if n.address_hrp == hrp]
    return network[0] if len(network) > 0 else None


class Bech32EncodableAddress(Address):
    """Native `SegWit`__ address model (uses `bech32` encoding).

    .. _bech32: https://en.bitcoin.it/wiki/Bech32
    .. _native_segwit: https://bitcoincore.org/en/segwit_wallet_dev/\
    #advanced-designs

    __ native_segwit_
    """
    __slots__ = "_version"

    # Constructors
    def __init__(self,
                 atype: AddressTypes,
                 program: bytes,
                 version: int = WITNESS_VERSION_DEFAULT,
                 network: BlockchainParams = None) -> None:
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(atype, AddressTypes)
        assert isinstance(program, bytes)
        assert isinstance(network, BlockchainParams)

        # Checks
        StdSegWitScriptPubKey(program, version)

        # Initialize
        super().__init__(atype, program, network)

        # Store
        self._version = version

    # Serialize / deserialize
    def serialize(self) -> bytes:
        """`Bech32`_ addresses cannot be serialized into bytes.

        .. _bech32: https://en.bitcoin.it/wiki/Bech32

        Warning:
            This will raise a :py:exc:`NotImplementedError` as bech32
            addresses can't be serialized into bytes.
        """
        raise NotImplementedError  # pragma: no cover

    def hex(self) -> str:
        """`Bech32`_ addresses cannot be represented as hex.

        .. _bech32: https://en.bitcoin.it/wiki/Bech32

        Warning:
            This will raise a :py:exc:`NotImplementedError` as bech32
            addresses can't be serialized into bytes and thererfore can't be
            represented as hex.
        """
        raise NotImplementedError  # pragma: no cover

    @classmethod
    def deserialize(cls, data: bytes) -> "Bech32EncodableAddress":
        """`Bech32`_ addresses cannot be deserialized from bytes.

        .. _bech32: https://en.bitcoin.it/wiki/Bech32

        Warning:
            This will raise a :py:exc:`NotImplementedError` as bech32
            addresses can't be deserialized from bytes.
        """
        raise NotImplementedError  # pragma: no cover

    @classmethod
    def from_hex(cls, data: str) -> "Bech32EncodableAddress":
        """`Bech32`_ addresses cannot be represented as hex.

        .. _bech32: https://en.bitcoin.it/wiki/Bech32

        Warning:
            This will raise a :py:exc:`NotImplementedError` as bech32
            addresses can't be serialized into bytes and thererfore can't be
            represented as hex.
        """
        raise NotImplementedError  # pragma: no cover

    # Encode / decode
    def encode(self) -> str:
        """Encodes the address into a string using `bech32`_
        encoding.

        .. _bech32: https://en.bitcoin.it/wiki/Bech32
        """
        return bech32encode(self._network.address_hrp,
                            WITNESS_VERSION_DEFAULT,
                            self._data)

    @classmethod
    def decode(cls, address: str) -> "Bech32EncodableAddress":
        """Decodes the address from a Bech32 decoding.

        Raises:
            AddressHRPError if the address hrp is not known to any network
            AddressTypeError if type of the address cannot be determined
        """
        hrp, version, program = bech32decode(address)

        network = find_network_by_hrp(hrp)
        if network is None:
            raise AddressHRPError

        if len(program) == PKH_SIZE:
            atype = AddressTypes.PKH
        elif len(program) == SH_SIZE_SEGWIT:
            atype = AddressTypes.SH
        else:
            raise AddressTypeError((
                AddressTypes.PKH, AddressTypes.SH), None)

        return cls(atype, program, version, network)

    # Extra properties
    @property
    def version(self) -> int:
        """Witness program version."""
        return self._version

    @property
    def program(self) -> bytes:
        """Witness program. Same as :py:meth:`bitcoin.Address.data`."""
        return self._data
