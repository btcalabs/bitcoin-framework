"""P2SH address model.

Allows to transfer and extract redeem scripts' hashes using P2SH addresses.
Contains methods to calculate and add the proper checksum required by these
addresses

**Sources:**

- https://en.bitcoin.it/wiki/Pay_to_script_hash
- https://github.com/bitcoin/bips/blob/master/bip-0013.mediawiki
- http://www.soroushjp.com/2014/12/20/bitcoin-multisig-the-hard-way\
-understanding-raw-multisignature-bitcoin-transactions/
"""

# Libraries
from typing import Union

from .base58encodable import Base58EncodableAddress
# Relative imports
from .errors import AddressLengthError, AddressTypeError
from .model import PaymentAddress
from ..params import AddressTypes, BlockchainParams, SH_SIZE_LEGACY, \
    get_default_network
from ..script import NP2WPKHScriptPubKey, NP2WSHScriptPubKey, P2SHScriptPubKey

# Constants
PREFIX_SIZE = 1
"""
    int: Size in bytes of the P2SH prefixes
"""
VALUE_SIZE = SH_SIZE_LEGACY + Base58EncodableAddress.CHECKSUM_SIZE
"""
    int: Size in bytes for the address value (script hash and checksum)
"""
ADDRESS_SIZE = PREFIX_SIZE + VALUE_SIZE
"""
    int: Size in bytes of all P2SH addresses
"""


class P2SHAddress(Base58EncodableAddress, PaymentAddress):
    """`P2SH`_ address model.

    .. _p2sh: https://github.com/bitcoin/bips/blob/master/bip-0013.mediawiki

    `Pay-to-script-hash`_ address.

    .. _pay-to-script-hash: https://en.bitcoin.it/wiki/Pay_to_script_hash

    Allows to serialize, deserialize, encode and decode P2SH addresses
    so the *reedem script*'s hash can be easily set and retrieved to easily
    handle P2SH addresses.
    """

    # Constructors
    def __init__(self,
                 script_pubkey: Union[P2SHScriptPubKey, NP2WPKHScriptPubKey,
                                      NP2WSHScriptPubKey],
                 network: BlockchainParams = None) -> None:
        """Creates a P2SH address given a :py:class:`bitcoin.P2SHScriptPubKey`

        Args:
            script_pubkey: :py:class:`bitcoin.script.std.P2SHScriptPubKey` to
                           pay to
            network: Network where the address belongs. Default network is
                     used (:py:meth:`bitcoin.params.get_default_network`)
                     if no network provided
        """
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(script_pubkey, P2SHScriptPubKey) or \
            isinstance(script_pubkey, NP2WPKHScriptPubKey) or \
            isinstance(script_pubkey, NP2WSHScriptPubKey)
        assert isinstance(network, BlockchainParams)

        # Initialize super
        super().__init__(network.address_prefix(AddressTypes.SH),
                         script_pubkey.data)

    # Properties
    @property
    def data(self) -> bytes:
        """Alias of :py:meth:`P2SHScriptPubKey.script_pubkey.script_hash`."""
        return self._data

    @property
    def script_pubkey(self) -> P2SHScriptPubKey:
        """Returns an associated ``P2SH`` ``ScriptPubKey``."""
        return P2SHScriptPubKey(self._data)

    # Serialization
    @classmethod
    def deserialize(cls, address: bytes) -> "P2SHAddress":
        """Deserializes some address bytes into a :py:class:`P2SHAddress`.

        Deserializes the given address as an array of bytes, guessing its
        prefix and saving its info (network and type, which must be P2SH) and
        the redeem script's hash.

        Also checks the checksum is valid.

        Args:
            address: Bytes object containing an address to deserialize

        Raises:
            AddressLengthError: Size in bytes is not correct for a P2SH
                                address.
            AddressTypeError: Address bytes are not from a P2SH address
            AddressChecksumError: Contained checksum bytes are invalid.

        Returns:
            New :py:class:`P2SHAdress` containing the bytes information.
        """
        # Check size
        if len(address) != ADDRESS_SIZE:
            raise AddressLengthError

        # Basic deserialization
        address_object = Base58EncodableAddress.deserialize(address)

        # Check type
        if address_object.type != AddressTypes.SH:
            raise AddressTypeError(AddressTypes.SH, address_object.type)

        return cls(
            P2SHScriptPubKey(address_object.data),
            address_object.network)
