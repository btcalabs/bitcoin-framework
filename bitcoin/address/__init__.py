"""Retrieve and transfer information using Bitcoin addresses.

**Sources:**

- https://en.bitcoin.it/wiki/Address
- https://en.bitcoin.it/wiki/List_of_address_prefixes
"""

# Relative imports
from .errors import AddressTypeError, AddressLengthError, \
    AddressChecksumError, AddressPrefixError
from .model import Address, PaymentAddress
from .base58encodable import Base58EncodableAddress, find_prefix
from .bech32encodable import Bech32EncodableAddress, find_network_by_hrp, \
    AddressHRPError
from .factory import AddressFactory
from .np2wpkh import NP2WPKHAddress
from .np2wsh import NP2WSHAddress
from .p2pkh import P2PKHAddress
from .p2sh import P2SHAddress
from .p2wpkh import P2WPKHAddress
from .p2wsh import P2WSHAddress
from .wif import WIFAddress

# Exports
__all__ = [
    "AddressTypeError", "AddressLengthError", "AddressChecksumError",
    "AddressPrefixError", "Address", "P2PKHAddress", "P2SHAddress",
    "P2WPKHAddress", "P2WSHAddress", "find_prefix", "WIFAddress",
    "find_network_by_hrp", "NP2WPKHAddress", "NP2WSHAddress",
    "Base58EncodableAddress", "Bech32EncodableAddress", "AddressHRPError",
    "PaymentAddress", "AddressFactory"
]
