"""P2WSH address model."""

# Relative imports
from .bech32encodable import Bech32EncodableAddress
from .errors import AddressTypeError
from .model import PaymentAddress
from ..params import AddressTypes, BlockchainParams, get_default_network
from ..script import P2WSHScriptPubKey


class P2WSHAddress(Bech32EncodableAddress, PaymentAddress):
    """Models a P2WSH address.

    Saves the redeem script's hash as the value of the address.

    Attributes:
        _script_pubkey (P2WSHScriptPubKey): Matching ``ScriptPubKey``.
    """
    __slots__ = "_script_pubkey"

    def __init__(self, script_pubkey: P2WSHScriptPubKey,
                 network: BlockchainParams = None) -> None:
        """Initializes a new P2WSH address."""
        # Defaults
        network = get_default_network() if network is None else network

        # Type assertions
        assert isinstance(script_pubkey, P2WSHScriptPubKey)
        assert isinstance(network, BlockchainParams)

        # Initialize super
        super().__init__(AddressTypes.SH, script_pubkey.data, 0, network)

        # Store scriptpubkey
        self._script_pubkey = script_pubkey

    # Properties
    @property
    def data(self) -> bytes:
        """Same as :py:class:`P2WSHAddress.script_pubkey.script_hash`."""
        return self._data

    @property
    def script_pubkey(self) -> P2WSHScriptPubKey:
        """Returns the associated ``P2WSH`` ``ScriptPubKey``."""
        return self._script_pubkey

    # Serialize / Deserialize
    @classmethod
    def decode(cls, address: str):
        """Decodes a P2WSH address.

        Raises:
            AddressTypeError if it is not a P2WSH address.
        """
        address_model = Bech32EncodableAddress.decode(address)

        if address_model.type != AddressTypes.SH:
            raise AddressTypeError(AddressTypes.SH, address_model.type)

        pubkey_script = P2WSHScriptPubKey(address_model.data)

        return cls(pubkey_script, address_model.network)
