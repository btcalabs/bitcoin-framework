"""
Defines several data structures used in low-level bitcoin for carrying data
in the protocol as fields in transactions, blocks...

Modules defined here are only used inside the app to provide easy storage and
serialization, it's not inteded to use for the app user. This way, this classes
provided here should only be used to create data structure that will deal with
Python data types and may use this classes just to store them, knowing that
they will also provide serialization methods.

The following modules allow to deal with low-level data providing methods to
easily handle the data in fields and providing methods to transform them into
an array of bytes compatible with the Bitcoin standards definition and
vice-versa

Modules and classes are added as needed so maybe many possible fields may exist
in the Bitcoin protocol but not available here because it's not used anywhere.
"""
# Relative imports
from .general import IntField, S4BLEInt, U2BLEInt, U4BLEInt, U8BLEInt, \
    VarInt, S4BBEInt
from .model import Field, ScriptField
from .opcode import OP_0, OP_1, OP_10, OP_11, OP_12, OP_13, OP_14, OP_15, \
    OP_16, OP_2, OP_3, OP_4, OP_5, OP_6, OP_7, OP_8, OP_9, \
    OP_CHECKLOCKTIMEVERIFY, OP_CHECKMULTISIG, OP_CHECKSIG, OP_CLTV, OP_CMS, \
    OP_CODESEPARATOR, OP_CS, OP_DROP, OP_DUP, OP_ELSE, OP_ENDIF, OP_EQUAL, \
    OP_EQUALVERIFY, OP_EV, OP_FALSE, OP_HASH160, OP_HASH256, OP_IF, OP_N, \
    OP_OVER, OP_PUSHDATA1, OP_PUSHDATA2, OP_PUSHDATA4, OP_PUSHDATA_MAX, \
    OP_PUSHDATA_MAX_SIZE, OP_PUSHDATA_MAX_SIZE_CONSENSUS, OP_PUSHDATA_MIN, \
    OP_RIPEMD160, OP_SHA1, OP_SHA256, OP_TRUE, Opcode, get_op_code_n,\
    OP_PUSHDATA, OP_RETURN
from .script import ScriptData, ScriptNum

# Exports
__all__ = ["IntField", "S4BLEInt", "U2BLEInt", "U4BLEInt", "U8BLEInt",
           "VarInt", "S4BBEInt"]
__all__ += ["Field", "ScriptField"]
__all__ += ["OP_0", "OP_1", "OP_10", "OP_11", "OP_12", "OP_13", "OP_14",
            "OP_15", "OP_16", "OP_2", "OP_3", "OP_4", "OP_5", "OP_6", "OP_7",
            "OP_8", "OP_9", "OP_CHECKLOCKTIMEVERIFY", "OP_CHECKMULTISIG",
            "OP_CHECKSIG", "OP_CLTV", "OP_CMS", "OP_CODESEPARATOR", "OP_CS",
            "OP_DROP", "OP_DUP", "OP_ELSE", "OP_ENDIF", "OP_EQUAL",
            "OP_EQUALVERIFY", "OP_EV", "OP_FALSE", "OP_HASH160", "OP_HASH256",
            "OP_IF", "OP_N", "OP_OVER", "OP_PUSHDATA1", "OP_PUSHDATA2",
            "OP_PUSHDATA4", "OP_PUSHDATA_MAX", "OP_PUSHDATA_MAX_SIZE",
            "OP_PUSHDATA_MAX_SIZE_CONSENSUS", "OP_PUSHDATA_MIN",
            "OP_RIPEMD160", "OP_SHA1", "OP_SHA256", "OP_TRUE", "Opcode",
            "get_op_code_n", "OP_PUSHDATA", "OP_RETURN"]
__all__ += ["ScriptData", "ScriptNum"]
