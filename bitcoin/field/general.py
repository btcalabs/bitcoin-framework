"""Defines data fields used widely around the Bitcoin protocol standards

**Name conventions**

For convention purposes, we'll use special notation when naming classes.
These prefixes may be applied, in the following order:

- **Signed / unsigned prefix**:

  Whether the field can contain a signed or unsigned numbers:

  - ``U`` for unsigned
  - ``S`` for signed

- **Size prefix**:

  A number containing the size in bytes followed by a ``B`` indicating the
  number means the number of bytes the field takes.
      Example: ``U4B`` specifies an unsigned 4 byte field

  If the field has no fixed size, then prefix ``Var`` is used

  We keep usage of ``b`` to when we need to work with bits.

- **Endianness prefix**:

  If nothing is specified, big endian is used. Otherwise, the prefix ``LE``
  will be understood as little endian and ``BE`` for big endian.

  The endianness applies at byte level.

- **Data type**:

  The kind of data included in the field is specified this way:

  - ``Int``: any integer field, either positive or negative
  - ``Char``: any character or bytes-composed field
"""

# Libraries
import struct

# Relative imports
from .model import Field


# Integer types
class IntField(Field):
    """Model for every integer field.

    In fact, it also acts as a variable sized signed integer.

    When serializing, uses little endian as most Bitcoin fields do so. When
    deserializing it is interpreted as a signed little endian too.
    """
    SIGNED = True
    """
        bool: Whether negative numbers are allowed (signs).
    """
    ENDIANNESS = "little"
    """
        bool: Whether to represent the number as big or little endian.
              Can be either 'little' or 'big'
    """
    BYTES_SIZE = 0
    """
        int: Number in bytes of the field, if it is fixed-size.
             ``0`` or less to be variable sized.
    """
    BYTES_SIZE_MAX = 8
    """
        int: Maximum bytes to represent the integer, if variable sized.

        By default, 8 bytes as it is the larger integer field we have.
    """
    @classmethod
    def get_bytes_size_max(cls):
        """Maximum bytes to represent the integer.

        Checks if it has a fixed size or not.
        """
        return cls.BYTES_SIZE if cls.BYTES_SIZE > 0 else cls.BYTES_SIZE_MAX

    def __init__(self, number: int) -> None:
        """Initializes the field checking boundaries and sign.

        Args:
            number: Number to represent in the field

        Raises:
            ValueError: Number cannot be negative or is beyond boundaries
            (can't be represented within the maximum field size)
        """
        assert isinstance(number, int)

        # Signed check
        if not self.SIGNED and number < 0:
            raise ValueError("Negative numbers not allowed for this field "
                             "as the field is unsigned")

        # Boundaries check
        bits_size_max = self.get_bytes_size_max() * 8
        if self.SIGNED and not -(1 << (bits_size_max-1)) <= number <= \
                (1 << (bits_size_max-1))-1:
            raise ValueError("Number is too big or to small (negative) to be "
                             "represented using this field.")
        elif not self.SIGNED and number >= (1 << bits_size_max):
            raise ValueError("Number is too big to be represented using this "
                             "field.")

        super().__init__(number)

    def serialize(self) -> bytes:
        """Serializes a field using the fixed size and endianness.

        Returns:
            the field represented in :py:class:`bytes`
        """
        # Bytes required
        bytes_size = self.BYTES_SIZE if self.BYTES_SIZE > 0 \
            else (self._value.bit_length() + 7)//8

        # Negative numbers 2-complement
        value = self._value + (1 << (bytes_size*8)) if self._value < 0 \
            else self._value

        # Switch to bytes
        value_bytes = value.to_bytes(bytes_size, self.ENDIANNESS)

        # Add extra byte if number is positive but first bit is negative
        # and field is signed. Just will happen in dynamic sized fields.
        if self.BYTES_SIZE <= 0 < self._value and self.SIGNED:
            first_byte = value_bytes[0] if self.ENDIANNESS == "big" \
                else value_bytes[-1]
            if first_byte & 0x80 != 0:
                value_bytes = bytes(1) + value_bytes \
                    if self.ENDIANNESS == "big" \
                    else value_bytes + bytes(1)

        return value_bytes

    @classmethod
    def deserialize(cls, data: bytes) -> "Field":
        """Deserializes a field, if the :py:class:`byte` size is appropiate.

        Checks also that the size of the data passed is not larger than the
        maximum allowed if variable-sized, and that is the exact length if it
        is fixed-size.

        Raises:
            ValueError: Too many bytes :py:class:`passed` to deserialize,
                        not the exact bytes length passed if fixed-size field
                        or empty bytes.
        """
        # Check not empty
        if len(data) == 0:
            raise ValueError("A field can't be deserialized from an empty "
                             "bytes object.")
        # Check fixed size
        elif 0 < cls.BYTES_SIZE != len(data):
            raise ValueError("To deserialize this fields, exactly %d bytes "
                             "are needed." % cls.BYTES_SIZE)
        # Check not a larger size
        elif len(data) > cls.get_bytes_size_max():
            raise ValueError("Too many bytes to deserialize for this field.")

        # Get value
        value = int.from_bytes(data, cls.ENDIANNESS)

        # Negative numbers
        bits_length = len(data) * 8
        if cls.SIGNED and value & (1 << (bits_length-1)) != 0:
            value = value - (1 << bits_length)

        return cls(value)

    def __repr__(self) -> str:
        """String to create the field using its integer value."""
        return "{}({})".format(self.__class__.__name__, self._value)


class U2BLEInt(IntField):
    """Unsigned 2-byte integer field."""
    NAME = "Unsigned 2-byte integer little-endian field (uint16_t)"
    DESCRIPTION = "16 bits without 2-complement sign"
    SIGNED = False
    BYTES_SIZE = 2
    ENDIANNESS = "little"


class S4BLEInt(IntField):
    """Signed 4-byte integer field."""
    NAME = "Signed 4-byte integer little-endian field (int32_t)"
    DESCRIPTION = "32 bits with 2-complement sign"
    SIGNED = True
    BYTES_SIZE = 4
    ENDDIANNESS = "little"


class S4BBEInt(IntField):
    """Signed 4-byte big endian integer field."""
    NAME = "Signed 4-byte integer big-endian field (int32_t)"
    DESCRIPTION = "32 bits with 2-complement sign"
    SIGNED = True
    BYTES_SIZE = 4
    ENDIANNESS = "big"


class U4BLEInt(IntField):
    """Unsigned 4-byte integer field."""
    NAME = "Unsigned 4-byte integer little-endian field (uint32_t)"
    DESCRIPTION = "32 bits without 2-complement sign"
    SIGNED = False
    BYTES_SIZE = 4
    ENDDIANNESS = "little"


class U8BLEInt(IntField):
    """Unsigned 8-byte integer field."""
    NAME = "Unsigned 8-byte integer little-endian field (uint64_t)"
    DESCRIPTION = "64 bits without 2-complement sign"
    SIGNED = False
    BYTES_SIZE = 8
    ENDDIANNESS = "little"


class VarInt(IntField):
    """Bitcoin protocol specific variable-sized integer field.

    Note:
        This just accepts unsigned numbers, but ``VarInt`` name is used against
        ``UVarInt`` so it becomes clearer when using it.

    https://en.bitcoin.it/wiki/Protocol_documentation#Variable_length_integer
    """
    SIGNED = False
    ENDIANNESS = "big"
    BYTES_SIZE = 0
    BYTES_SIZE_MAX = 8
    BYTE_LIMIT = 0xFC
    """
        int: Last possible number to be represented as one byte.
    """
    NAME = "Bitcoin variable-length unsigned little-endian integer"
    DESCRIPTION = "Allows to save an integer with variable size (1 to 9 bytes)"

    @classmethod
    def translate_size(cls, first_byte: int) -> int:
        """Returns the size a variable integer is taking given the first byte.

        As the first byte indicates the size in a :py:class:`VarInt`, returns
        the size the integer is taking.

        The values that can return are then 1 if just occupies this byte, 3 if
        occupies 3 bytes, 5 or 9.

        See Also:
            :py:meth:`VarInt.serialize` to see how a :py:class:`VarInt`
            serialization and its size byte works.

        Args:
            first_byte: first byte as an integer of a variable sized integer
                        to calculate the full size of it. Big endian is used
                        to read this first byte (default).

        Returns:
            the size of the variable integer

        Raises:
            ValueError: if not a valid integer that fits in a byte passed:

                        ``(0 <= first_byte <= 255)``
        """
        # A byte must be between 0 - 255
        if not 0 <= first_byte <= 255:
            raise ValueError("Invalid value for the (first) byte in a VarInt.")

        # Calculate size
        return 2**(first_byte - cls.BYTE_LIMIT) + 1 \
            if first_byte > cls.BYTE_LIMIT else 1

    def __len__(self) -> int:
        """Returns the length of the serialized variable integer.

        Notes:
            Calculates it without serializing to improve speed.
        """
        if self._value <= self.BYTE_LIMIT:
            return 1
        elif self._value <= 0xFFFF:
            return 3
        elif self._value <= 0xFFFFFFFF:
            return 5
        else:
            return 9

    def serialize(self) -> bytes:
        """Serializes the integer using the minimum size possible.

        As specified in the `Bitcoin protocol`__

        .. _bp: https://en.bitcoin.it/wiki/Protocol_documentation\
        #Variable_length_integer

        __ _bp

        Note:

            **Format**

            If the integer is lower than ``0xFC``
            (:py:const:`VarInt.BYTE_LIMIT`), it serializes it into 1 unsigned
            byte. If not, the integer is saved the following way:

            ``[size][integer_value]``

            Where size:

            - ``0xFD`` (:py:attr:`VarInt.BYTE_LIMIT` ``+1``)
              To save an integer that fits in 2 bytes. 2 byte integers and 1
              byte integers between ``0xFD`` and ``0xFF`` included.

            - ``0xFE`` (:py:attr:`VarInt.BYTE_LIMIT` ``+2``)
              To save an integer that fits in 3 to 4 bytes

            - ``0xFF`` (:py:attr:`VarInt.BYTE_LIMIT` ``+3``)
              To save an integer that fits in 5 to 8 bytes

            And integer value is the integer value coded as 2, 4, 8 byte
            unsigned little-endian integer depending on the size specified.

        .. tip::
            Remember all this calculus are performed automatically once
            serializing, to use the :py:class:`VarInt`, just create it with
            an integer and the proper serialization will automatically be
            calculated.
        """
        if self._value <= self.BYTE_LIMIT:
            return struct.pack('<B', self._value)
        elif self._value <= 0xFFFF:
            return struct.pack('<BH', self.BYTE_LIMIT+1, self._value)
        elif self._value <= 0xFFFFFFFF:
            return struct.pack('<BL', self.BYTE_LIMIT+2, self._value)
        else:
            return struct.pack('<BQ', self.BYTE_LIMIT+3, self._value)

    @classmethod
    def deserialize(cls, data: bytes) -> "VarInt":
        """Deserializes a variable integer from the passed bytes.

        You can pass a bytes object with any size and it will be interpreted
        as a :py:class:`VarInt`, taking just the necessary bytes to obtain the
        value reading the first byte that indicates size, as specified in the
        :py:meth:`VarInt.serialize` format.

        Args:
            data: :py:class:`bytes` to interpret as a variable integer. Can be
                  any size but not empty. Just required bytes will be read.

        Returns:
            :py:class:`VarInt` object with the interpreted integer stored.

        Raises:
            ValueError: :py:class:`bytes` passed are empty.
        """
        assert isinstance(data, bytes)

        # Empty bytes don't mean anything
        if len(data) == 0:
            raise ValueError("Can't deserialize empty bytes as a VarInt.")

        size = cls.translate_size(data[0])
        value = -1
        if size == 1:
            value = data[0]
        elif size == 3:
            value = struct.unpack('<H', data[1:size])[0]
        elif size == 5:
            value = struct.unpack('<L', data[1:size])[0]
        elif size == 9:
            value = struct.unpack('<Q', data[1:size])[0]

        return cls(value)
