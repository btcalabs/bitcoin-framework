"""Opcodes that can be present in a Bitcoin transaction script.

Defines a base class to create opcodes in scripts and defines all the used
opcodes in the application.

.. note::
    Some opcodes may exist but they may not appear here yet because we have
    not needed yet. In the case an unknown opcode is deserialized,
    the generic :py:class:`Opcode` will be used if it is not implemented
    specifically.
"""
# Libraries
from abc import ABCMeta, abstractmethod
from typing import Union, Optional

# Relative imports
from .model import ScriptField


# Constants
_OPCODE_REGISTRY = {}
"""
    Dict[Union[str, int], _SpecificOpcode]: Registers the opcode value as an
    integer and the opcode class in order to be able to obtain the class
    corresponding to each opcode when deserializing.

    Also registers the ASM string and maps to the corresponding class.

    Note:
        Because when accessing a Python's :py:class:`bytes` object at a certain
        position, it will return an :py:class:`int`, the key to access the
        corresponding opcode class in the registry is an :py:class:`int`

        For instance ``bytes.fromhex("01")[0]`` returns ``1`` instead of
        ``b'\x01'``.
"""


class _OpcodeRegistry(ABCMeta):
    """Opcode registry to be able to deserialize opcodes.

    Every time an opcode is created and this metaclass is provided,
    the opcode class will be added to the registry so that its serialization
    maps to the opcode class.
    """
    def __new__(mcs, name, bases, class_dict):
        # Create class
        cls = super().__new__(mcs, name, bases, class_dict)

        # Register class
        _OPCODE_REGISTRY[cls.opcode()] = cls
        _OPCODE_REGISTRY[cls.asm()] = cls

        # Return class
        return cls


class Opcode(ScriptField):
    """Generic Bitcoin scripting system opcode."""

    def __init__(self, value: int) -> None:
        """Initializes a generic opcode given its serialized value.

        ..  Each subclass must implement its own ``__init__`` method,
            calling this class' :py:class:`Opcode.__init__` with the proper
            value so the opcode value gets set.

            For instance, for the opcode with value ``0x00``:

            .. highlight::

               Opcode.__init__(self, 0x00)

            This way a specific opcode can't be instantiated without arguments

            .. highlight::

               desired_opcode = OP_0()

            Don't call ``super().__init__`` instead of ``Opcode.__init__`` or
            an error will be raise in Python 3.5
            (``RutimeError: super(): empty __class__ cell``)

        Args:
            value: Opcode serialized value as an :py:class:`int`. When
                   specified as literal, use hexadecimal notation for better
                   understanding (ie: ``Opcode(0x00)``)
        """
        assert isinstance(value, int)

        # Check limits
        if not 0 <= value <= 255:
            raise ValueError("The opcode must fit in one byte. "
                             "Its codification must be then between "
                             "0 <= n <= 255.")

        super().__init__(value)

    def serialize(self) -> bytes:
        """Returns the opcode serialization.

        Returns:
            Serialized opcode **always as just one byte**, except for
            OP_PUSHDATA[1,2,4] that will also encode the number of bytes to
            push.
        """
        return bytes([self._value])

    @classmethod
    def deserialize(cls, data: Union[int, bytes]) -> "Opcode":
        """Deserializes an opcode given some data.

        Warning:
            More than 1 byte of data may be required if deserializing an
            :py:class:`OP_PUSHDATA1`, :py:class:`OP_PUSHDATA2`, or a
            :py:class:`OP_PUSHDATA4`

        Args:
            data: Opcode to deserialize. You can pass an arbitrary sized
                  :py:class:`bytes` object, except an empty one.

        Returns:
            Matching opcode object if found or this generic class
            :py:class:`Opcode` if not found in the registry.

        Raises:
            ValueError: :py:class:`bytes` passed's size is 0 or if passed
                        an :py:class:`int`, it's not between the possible
                        values of a byte (``0 <= value <= 255``)
        """
        # Type assertion
        assert isinstance(data, bytes) or isinstance(data, int)

        # Just >= 1 byte opcodes
        if isinstance(data, bytes) and len(data) <= 0:
            raise ValueError("Opcodes take 1 byte or more")

        # Take value
        opcode_value = data[0] if isinstance(data, bytes) else data

        # One byte OP_PUSHDATA
        if OP_PUSHDATA_MIN <= opcode_value <= OP_PUSHDATA_MAX:
            return OP_PUSHDATA(opcode_value)
        else:
            # Search in registry
            opcode_class = _OPCODE_REGISTRY.get(opcode_value, Opcode)
            # Known class
            if opcode_class != Opcode:
                return opcode_class()
            # Unknown, generic class
            else:
                return opcode_class(opcode_value)

    def opcode(self) -> int:
        """Returns the opcode value for this opcode."""
        return self.value

    @classmethod
    def asm(cls) -> str:
        """Returns the ASM opcode name.

        Note:
           For the generic, unknown opcode, prints ``OP_UNKNOWN``.
        """
        return "OP_UNKNOWN"

    @classmethod
    def from_asm(cls, asm: str) -> Optional["_SpecificOpcode"]:
        """Returns the opcode matching the specified ASM.

        Returns:
            Specific opcode with the given ASM or ``None`` if not opcode found.
        """
        assert isinstance(asm, str)

        opcode = _OPCODE_REGISTRY.get(asm, None)

        return opcode() if opcode is not None else opcode

    def __str__(self) -> str:
        """Prints the opcode ASM mnemonic name."""
        return self.asm()

    def __repr__(self) -> str:
        """Prints the opcode representation to recreate the opcode."""
        return "{}({})".format(
            self.__class__.__name__,
            hex(self._value) if len(hex(self._value)) % 2 == 0
            else "0x0" + hex(self._value)[2:])


class _SpecificOpcode(Opcode, metaclass=ABCMeta):
    """Specific opcode implementation.

    Automatically initializes the opcode once this class method gets
    implemented.
    """
    def __init__(self: "Opcode") -> None:
        """Initializes the opcode."""
        Opcode.__init__(self, self.opcode())

    @classmethod
    @abstractmethod
    def opcode(cls) -> int:
        """Returns the opcode value for this opcode."""
        return 0xff

    @classmethod
    def asm(cls) -> str:
        """Returns the opcode ASM mnemonic."""
        return cls.__name__

    def __str__(self) -> str:
        """Returns the opcode ASM mnemonic.

        Alias of :py:meth:`_SpecificOpcode.asm()` if no extra data is needed.
        """
        return self.__class__.__name__

    def __repr__(self) -> str:
        """Returns how to create the same opcode."""
        return self.__class__.__name__ + "()"


# Constants
# # Push data
OP_PUSHDATA_MIN = 0x01
"""
    int: When represented as a byte, all numbers from this value to
    :py:const:`bitcoin.field.opcode.OP_PUSHDATA_MAX`, the opcode means push
    this number of bytes into the stack.
"""
OP_PUSHDATA_MAX = 0x4b
"""
    int: When represented as a byte, all numbers from
    :py:const:`bitcoin.field.opcode.OP_PUSHDATA_MAX` to this value, the opcode
    means push this number of bytes into the stack. Therefore, if you want to
    push more than this number of bytes to the stack you have to use other
    opcodes, the ``OP_PUSHDATA[1-4]``
"""
OP_PUSHDATA_MAX_SIZE = 0xffffffff
"""
    int: Maximum amount of bytes that can be pushed into the stack, knowing
    that the maximum bytes to set to be pushed into the stack are 4, using
    :py:class:`OP_PUSHDATA4`. Therefore the technical maximum amount of bytes
    to push into the stack is `0xffffffff`.
"""
OP_PUSHDATA_MAX_SIZE_CONSENSUS = 520
"""
    int: Maximum amount of bytes that can be pushed into the stack as
    set by the network consensus.
"""
OP_N_MIN = 0
"""
    int: Minimum number whose opcode pushes that number to the stack
"""
OP_N_MAX = 16
"""
    int: Maximum number whose opcode pushes that number to the stack
"""
OP_N_BASE = 0x50
"""
    int: the base of OP_1 -> OP_N_MAX opcode number
        (OP_1=OP_N_BASE+1, OP_2 = OP_N_BASE+2, ...)
"""


# ==================
# OPCODE DEFINITIONS
# ==================
#
# Constants
#
class OP_0(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_0

    An empty array of bytes is pushed onto the stack.
    (This is not a no-op: an item is added to the stack.)
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x00


class OP_PUSHDATA(Opcode):
    """OP_PUSHDATA

    The amount of bytes to push into the stack is the same opcode itself.
    """

    def __init__(self, amount: int) -> None:
        """Initializes the opcode given the number of bytes to push.

        Note:
            The amount of bytes to push has to be between the range
            (:py:const:`bitcoin.field.opcode.OP_PUSHDATA_MIN` <= amount <=
            :py:const:`bitcoin.field.opcode.OP_PUSHDATA_MAX`) so it fits in
            just one opcode.

        Args:
            amount: Amount of bytes to push into the stack.

        Raises:
            ValueError: Amount does not fit in the valid range to be just
                        one opcode or negative or 0 amount.
        """
        # Type assertion
        assert isinstance(amount, int)

        # Check the amount can really be used as the opcode
        if not OP_PUSHDATA_MIN <= amount <= OP_PUSHDATA_MAX:
            raise ValueError("This amount of bytes to be pushed into the "
                             "stack cannot be used as an opcode. Try using"
                             "OP_PUSHDATA1 or bigger ones.")

        # Initialize opcode value (same as amount)
        super().__init__(amount)

    @property
    def amount(self) -> int:
        """Amount of bytes the opcode indicates must be pushed."""
        return self._value

    @classmethod
    def asm(cls) -> str:
        """Returns the opcode ASM."""
        return cls.__name__

    def __str__(self) -> str:
        """Shows the pushdata opcode in ASM format."""
        return "({}[{}])".format(self.__class__.__name__, self.amount)

    def __repr__(self) -> str:
        """Represents a new pushdata opcode creation."""
        return "{}({})".format(self.__class__.__name__, self.amount)


class OP_PUSHDATA1(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_PUSHDATA1

    The next byte contains the number of bytes to be pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x4c


class OP_PUSHDATA2(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_PUSHDATA2

    The next two bytes contain the number of bytes to be pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x4d


class OP_PUSHDATA4(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_PUSHDATA4

    The next four bytes contain the number of bytes to be pushed onto the
    stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x4e


class OP_1(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_1

    The number 1 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x51


class OP_2(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_2

    The number 2 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x52


class OP_3(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_3

    The number 3 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x53


class OP_4(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_4

    The number 4 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x54


class OP_5(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_5

    The number 5 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x55


class OP_6(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_6

    The number 6 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x56


class OP_7(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_7

    The number 7 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x57


class OP_8(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_8

    The number 8 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x58


class OP_9(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_9

    The number 9 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x59


class OP_10(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_10

    The number 10 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5a


class OP_11(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_11

    The number 11 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5b


class OP_12(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_12

    The number 12 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5c


class OP_13(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_13

    The number 13 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5d


class OP_14(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_14

    The number 14 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5e


class OP_15(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_15

    The number 15 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x5f


class OP_16(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_16

    The number 16 is pushed onto the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x60


OP_N = (OP_0, OP_1, OP_2, OP_3, OP_4, OP_5, OP_6, OP_7, OP_8, OP_9, OP_10,
        OP_11, OP_12, OP_13, OP_14, OP_15, OP_16)


# Methods
def get_op_code_n(n: int) -> Union[OP_0, OP_1, OP_2, OP_3, OP_4, OP_5, OP_6,
                                   OP_7, OP_8, OP_9, OP_10, OP_11, OP_12,
                                   OP_13, OP_14, OP_15, OP_16]:
    """Gets the opcode OP_N where n is the given number.

    Given an integer ``n``, returns the associated opcode that pushes that
    number into the stack.

    Args:
        n: Integer to get opcode that pushes that integer to the stack

    Returns:
        A new instance of the ``OP_N`` opcode requested.
    """
    if not OP_N_MIN <= n <= OP_N_MAX:
        raise ValueError("The number %d can't be pushed to the stack with a "
                         "single opcode")

    return OP_N[n]()


#
# Flow control
#
class OP_IF(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_IF

    If the top stack value is not False, the statements are executed. The top
    stack value is removed.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x63


class OP_ELSE(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_ELSE

    If the preceding OP_IF or OP_NOTIF or OP_ELSE was not executed then these
    statements are and if the preceding OP_IF or OP_NOTIF or OP_ELSE was
    executed then these statements are not.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x67


class OP_ENDIF(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_ENDIF

    Ends an if/else block. All blocks must end, or the transaction is invalid.
    An OP_ENDIF without OP_IF earlier is also invalid.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x68


class OP_RETURN(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_RETURN

    Marks transaction as invalid. A standard way of attaching extra data to
    transactions is to add a zero-value output with a scriptPubKey consisting
    of OP_RETURN followed by exactly one pushdata op. Such outputs are provably
    unspendable, reducing their cost to the network. Currently it is usually
    considered non-standard (though valid) for a transaction to have more than
    one OP_RETURN output or an OP_RETURN output with more than one pushdata op.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x6a


#
# Stack
#
class OP_DROP(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_DROP

    Removes the top stack item.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x75


class OP_DUP(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_DUP

    Duplicates the top stack item.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x76


class OP_OVER(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_OVER

    Copies the second-to-top stack item to the top.
    """

    @classmethod
    def opcode(cls) -> int:
        return 0x78


#
# Bitwise logic
#
class OP_EQUAL(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_EQUAL

    Returns 1 if the inputs are exactly equal, 0 otherwise.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x87


class OP_EQUALVERIFY(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_EQUALVERIFY

    Same as OP_EQUAL, but runs OP_VERIFY afterward.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0x88


#
# Crypto
#
class OP_RIPEMD160(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_RIPEMD160

    The input is hashed using RIPEMD-160.z
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xa6


class OP_SHA1(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_SHA1

    The input is hashed using SHA-1.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xa7


class OP_SHA256(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_SHA256

    The input is hashed using SHA-256.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xa8


class OP_HASH160(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_HASH160

    The input is hashed twice: first with SHA-256 and then with RIPEMD-160.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xa9


class OP_HASH256(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_HASH256

    The input is hashed two times with SHA-256.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xaa


class OP_CODESEPARATOR(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_CODESEPARATOR

    All of the signature checking words will only match
    signatures to the data after the most recently-executed OP_CODESEPARATOR.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xab


class OP_CHECKSIG(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_CHECKSIG

    The entire transaction's outputs, inputs, and script (from
    the most recently-executed OP_CODESEPARATOR to the end) are hashed. The
    signature used by OP_CHECKSIG must be a valid signature for this hash and
    public key. If it is, 1 is returned, 0 otherwise.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xac


class OP_CHECKMULTISIG(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_CHECKMULTISIG

    Compares the first signature against each public key until
    it finds an ECDSA match. Starting with the subsequent public key, it
    compares the second signature against each remaining public key until it
    finds an ECDSA match. The process is repeated until all signatures have
    been checked or not enough public keys remain to produce a successful
    result.

    All signatures need to match a public key. Because public keys are not
    checked again if they fail any signature comparison, signatures must be
    placed in the scriptSig using the same order as their corresponding
    public keys were placed in the scriptPubKey or 2 redeemScript. If all
    signatures are valid, 1 is returned, 0 otherwise.

    Due to a bug, one extra unused value is removed from the stack.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xae


#
# Locktime
#
class OP_CHECKLOCKTIMEVERIFY(_SpecificOpcode, metaclass=_OpcodeRegistry):
    """OP_CHECKLOCKTIMEVERIFY

    Marks transaction as invalid if the top stack item is
    greater than the transaction's nLockTime field, otherwise script evaluation
    continues as though an OP_NOP was executed.
    """
    @classmethod
    def opcode(cls) -> int:
        return 0xb1


#
# Aliases
#

OP_FALSE = OP_0
OP_TRUE = OP_1
OP_EV = OP_EQUALVERIFY
OP_CS = OP_CHECKSIG
OP_CMS = OP_CHECKMULTISIG
OP_CLTV = OP_CHECKLOCKTIMEVERIFY
