"""Defines fields that can appear in a script."""
# Libraries
from typing import Union

# Relative imports
from .general import IntField, U2BLEInt, U4BLEInt
from .model import ScriptField
from .opcode import OP_PUSHDATA, OP_PUSHDATA1, OP_PUSHDATA2, OP_PUSHDATA4, \
    OP_PUSHDATA_MAX, OP_PUSHDATA_MIN, Opcode, OP_0, OP_16, get_op_code_n
from ..interfaces import Serializable


class ScriptData(ScriptField):
    """Defines a field that will contain data to be placed in a script.

    A :py:class:`ScriptData` field allows to include data in a script that has
    to be pushed to the stack. It will automatically include the push data
    opcodes required so the data is correctly pushed into the stack.

    Attributes:
        _value (Union[bytes, Field]): Saved value must be either a bytes
                                      object or a :py:class:`Serializable`
                                      object so that it can be serialized.
        _opcode (OpcodePushData): Opcode necessary to push the data into the
                                  stack
    """
    __slots__ = "_opcode"

    def __init__(self, data: Union[Serializable, bytes]) -> None:
        """Initializes the field with the data passed

        Args:
            data: data to be placed in the stack.

        Raises:
            ValueError: The data to be pushed is empty or too big.
                        (:py:const:`bitcoin.field.opcode.OP_PUSHDATA_MAX_SIZE`)
        """
        # Type assertions
        assert isinstance(data, Serializable) or isinstance(data, bytes)

        # Initialize
        super().__init__(data)

        # Calculate opcodes and check lengths
        self._opcode = self.calculate_opcodes(
            data if isinstance(data, bytes) else data.serialize())

    @classmethod
    def calculate_opcodes(cls, data: bytes) -> Opcode:
        """Minimum opcode needed to push the data to the stack.

        Note:

            **How to choose the opcode to push data**

            The opcode to push the data depends on the data's length:

            If data length in bytes is

            ``>= OP_PUSHDATA_MIN (0x01) and <= OP_PUSHDATA_MAX (0x4b)``

            then serialization is:

            ``<number of data bytes to push><data to push>``

            Because the amount of data to push is itself an opcode.

            If not:

            Calculate how much bytes does the size of the data to be pushed
            occupies (if we want to push ``65535`` bytes of data, then we
            require two bytes to express ``65535`` bytes
            (``ceil(log_2(65535)/8) = 2``)

            Once we know how many bytes that the length of the data takes to be
            expressed, we have to use ``OP_PUSHDATA[1,2,4]`` depending if the
            amount of data bytes to be pushed can be represented in 1, 2 or
            4 bytes.

            After the opcode ``OP_PUSHDATA`` we've chosen, we add the size
            in bytes of the data as a little-endian unsigned integer. You can
            check each ``OP_PUSHDATA`` for more info in the
            :py:mod:`bitcoin.field.opcode` module.

            **Sources:**
            - https://en.bitcoin.it/wiki/Script
            - http://bitcoin.stackexchange.com/questions/2285

        Args:
            data: data to be pushed into the stack

        Returns:
            The smallest opcode to push that data into the stack (understanding
            that some opcodes append the number of bytes to push into the
            stack after them)

        Raises:
            ValueError: Data is empty or too big (see
                        :py:const:`bitcoin.field.opcode.OP_PUSHDATA_MAX_SIZE`)
        """
        # Data to be pushed's length
        data_length = len(data)

        # Empty data
        if data_length == 0:
            raise ValueError("No opcode exist to push empty data")
        # OP_0 -> OP_16
        elif len(data) == 1 \
                and OP_0().serialize() <= data <= OP_16().serialize():
            return get_op_code_n(int.from_bytes(data, "big"))

        # Minimum size in bytes to represent the previous variable as bytes
        data_length_size = (data_length.bit_length() + 7) // 8

        # Opcode selection
        # # Amount fits in one byte <= OP_PUSHDATA_MAX
        if OP_PUSHDATA_MIN <= data_length <= OP_PUSHDATA_MAX:
            return OP_PUSHDATA(data_length)
        elif data_length_size == 1:
            return OP_PUSHDATA1()
        # TODO(ccebrecos): Warn if too big for consensus, but technically OK.
        elif data_length_size == 2:
            return OP_PUSHDATA2()
        elif data_length_size <= 4:
            return OP_PUSHDATA4()
        else:
            raise ValueError("Data is too larger to be pushed. No opcode "
                             "exists to push so much data as a whole")

    def serialize(self) -> bytes:
        """Serializes the data saved with the necessary opcodes.

        Returns:
            Serialized bytes so the data stored can be pushed into the stack,
        adding the opcodes necessary for that to happen
        """
        # Opcode
        opcode = self.opcode
        opcode_data = opcode.serialize()
        opcode_size = bytes()

        # Amount
        if isinstance(opcode, OP_PUSHDATA1):
            opcode_size = bytes([len(self._value)])
        elif isinstance(opcode, OP_PUSHDATA2):
            opcode_size = U2BLEInt(len(self._value)).serialize()
        elif isinstance(opcode, OP_PUSHDATA4):
            opcode_size = U4BLEInt(len(self._value)).serialize()

        # Data
        if len(self.serialized_data) == 1 and \
                OP_0.opcode() <= opcode.opcode() <= OP_16.opcode():
            data_to_push = b''
        else:
            data_to_push = self.serialized_data

        return opcode_data + opcode_size + data_to_push

    @classmethod
    def deserialize(cls, data: bytes) -> "ScriptData":
        """Interprets an opcode to push data and some data.

        Given a bytes object that is known to contain as first byte a push data
        opcode, reads the opcode and extracts the appropiate data, storing it
        in the newly created :py:class:`ScriptData` instance.

        The length of the data can be different from the length of the
        actual data. The method will just save the data that the opcode says.
        The rest of data will be ignored

        Raise:
            ValueError: No valid push data opcode is found or a valid is found
                        but was not complete (missing some bytes of opcode or
                        data)
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Check valid size
        offset = 0
        if len(data) < 2:
            raise ValueError("Not enough data to be deserialized. "
                             "At least an opcode and 1 byte data is required")

        # Retrieve opcode
        opcode = Opcode.deserialize(data)
        offset += 1
        if not (isinstance(opcode, OP_PUSHDATA)
                or isinstance(opcode, OP_PUSHDATA1)
                or isinstance(opcode, OP_PUSHDATA2)
                or isinstance(opcode, OP_PUSHDATA4)):
            raise ValueError("Script data cannot be extracted as the data"
                             "passed does not contain an opcode to push data"
                             "in the first position")

        # Code bytes to push if needed
        if isinstance(opcode, OP_PUSHDATA1) and len(data[offset:]) < 1 \
            or isinstance(opcode, OP_PUSHDATA2) and len(data[offset:]) < 2 \
                or isinstance(opcode, OP_PUSHDATA4) and len(data[offset:]) < 4:
            raise ValueError("Script data amount cannot be extracted as not "
                             "enough data was provided.")

        # Decode amount of bytes to push
        amount = 0
        if isinstance(opcode, OP_PUSHDATA):
            amount = opcode.amount
        elif isinstance(opcode, OP_PUSHDATA1):
            amount = data[1]
            offset += 1
        elif isinstance(opcode, OP_PUSHDATA2):
            amount = U2BLEInt.deserialize(data[offset:offset+2]).value
            offset += 2
        elif isinstance(opcode, OP_PUSHDATA4):
            amount = U4BLEInt.deserialize(data[offset:offset+4]).value
            offset += 4

        # Check if enough data available
        if len(data[offset:]) < amount:
            raise ValueError("An opcode to push data has been found, but "
                             "there's not enough data to push afterwards")

        return cls(data[offset:offset+amount])

    @property
    def opcode(self) -> Opcode:
        """Opcode to place in order to be able to push data into the stack."""
        return self._opcode

    @property
    def data(self) -> bytes:
        """Returns the data this script data contains."""
        return self._value

    @property
    def serialized_data(self) -> bytes:
        """Returns the data to be placed, serialized as a bytes object."""
        return self._value if isinstance(self._value, bytes) else \
            self._value.serialize()

    def __str__(self):
        """Shows the push data opcodes and the data itself."""
        return " ".join((str(self.opcode), "<" + self.serialized_data.hex() +
                         ">"))

    def __repr__(self):
        """Represents a ScriptData using its serialization."""
        return "{}(bytes.fromhex(\"{}\"))".format(
            self.__class__.__name__,
            self.serialized_data.hex()
        )


class ScriptNum(IntField):
    """Integer that will be nested in a StackDataField.

    Defines a field containing an integer number that can be used inside a
    StackDataField to perform script operations

    According to:
    https://github.com/bitcoin/bitcoin/blob/master/src/script/script.h#L353-L371

    Its serialization is a little-endian signed integer with the minimum bytes
    possible. The signed / unsigned meaning is given by the first bit (sign,
    magnitude coding)
    """
    SIGNED = True
    ENDIANNESS = 'little'
    BYTES_SIZE = -1
    BYTES_SIZE_MAX = 8
    # TODO(davidlj): Warn if num is 0 -> 16. An OP_N could be used.
