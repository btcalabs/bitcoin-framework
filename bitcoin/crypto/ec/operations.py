"""Operations performable with ECDSA algorithm."""

# Relative imports
from .secp256k1 import DEFAULT_CURVE


def compute_y_from_x(x: int, positive_y: bool = True, curve=DEFAULT_CURVE) \
        -> int:
    """Computes the coordinate y of the Elliptic curve given related to X
    Args:
        x: coordinate of the point
        positive_y: False if the y wanted should be the positive root,
        True otherwise
        curve:

    Returns: the coordinate y
    """
    sign = 1 if positive_y else -1
    y2 = (pow(x, 3, curve.p) + 7) % curve.p
    y = (sign * pow(y2, (curve.p + 1) // 4, curve.p)) % curve.p

    return y
