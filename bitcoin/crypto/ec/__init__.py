"""Defines the needed for creating the structure of an Elliptic Curve."""

# Relative imports
from .curve import CurveParams
from .operations import compute_y_from_x
from .secp256k1 import Secp256k1, DEFAULT_CURVE


"""
    CurveParams: default ECDSA curve to use.
"""

# Exports
__all__ = ["CurveParams", "compute_y_from_x", "Secp256k1", "DEFAULT_CURVE"]
