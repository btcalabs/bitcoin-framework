"""."""

# Libraries
import math
import logging

# Relative imports
from ..ec import DEFAULT_CURVE
from ...interfaces import Serializable

# Constants
LOGGER = logging.getLogger(__name__)


# Methods
def int_to_bytes(n: int) -> bytes:
    return n.to_bytes(math.ceil(n.bit_length()/8), "big")


class ECDSASignature(Serializable):

    __slots__ = ["_R", "_S", "_V"]

    def __init__(self, r: int, s: int, v: int = -1) -> None:
        """Initializes a signature with its parameters."""
        # Value checks
        if not 1 <= r <= DEFAULT_CURVE.n-1:
            raise ValueError
        if not 1 <= s <= DEFAULT_CURVE.n-1:
            raise ValueError
        self._R = r
        self._S = s
        self._V = v

    def der_encode(self) -> bytes:
        """Canonical DER encoding (without v parameter)."""
        sequence_byte = b'\x30'
        r_bytes = int_to_bytes(self._R)
        if r_bytes.hex()[0] in "89abcdef":
            r_bytes = b'\x00' + r_bytes
        r_field = b'\x02' + int_to_bytes(len(r_bytes)) + r_bytes
        s_bytes = int_to_bytes(self._S)
        if s_bytes.hex()[0] in "89abcdef":
            s_bytes = b'\x00' + s_bytes
        s_field = b'\x02' + int_to_bytes(len(s_bytes)) + s_bytes
        sequence = r_field + s_field
        sequence_length = int_to_bytes(len(sequence))

        return sequence_byte + sequence_length + sequence

    # Properties
    @property
    def r(self):
        return self._R

    @property
    def s(self):
        return self._S

    def serialize(self) -> bytes:
        return self.der_encode()

    @classmethod
    def deserialize(cls, data: bytes) -> "ECDSASignature":
        """Only from DER format.

        Raises:
            ValueError: invalid length or malformed signature
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Checks
        if not 69 <= len(data) <= 71:  # Without the sighash
            # Although sizes even smaller than that are possible with
            # exponentially decreasing probability.
            if len(data) < 69:
                LOGGER.warning("Signature length is {}, thats weird "
                               "but technically possible".format(len(data)))
            else:
                raise ValueError

        # Prefix
        if not data[0] == 0x30:
            raise ValueError("Invalid DER serialization.")

        # Length
        total_length = data[1]

        if len(data[2:]) != total_length:
            raise ValueError("Missing some bytes; specified {}, reading: {}",
                             ".".format(total_length), len(data[2:]))

        # Retrieve R
        # Variable prefix
        if not data[2] == 0x02:
            raise ValueError("Invalid DER serialization.")
        r_length = data[3]
        offset = 4
        r = int.from_bytes(data[offset:offset+r_length], "big")

        # Retrieve S
        offset += r_length
        # Variable prefix
        if not data[offset] == 0x02:
            raise ValueError("Invalid DER serialization.")
        offset += 1
        s_length = data[offset]
        offset += 1
        s = int.from_bytes(data[offset:offset+s_length], "big")

        offset += s_length

        if offset != len(data):
            raise ValueError("Missing some bytes.")

        return cls(r, s)
