"""Provides an interface for the key objects and the private/public classes.

**Sources:**

- https://bitcoin.stackexchange.com/questions/76785/link-to-algorithmic\
behind-signatures-verification
"""

# Libraries
from typing import Tuple

# Relative imports
from .calculator import fast_multiply, fast_add, ecdsa_raw_sign, inv
from .signature import ECDSASignature
from ..ec import DEFAULT_CURVE, compute_y_from_x
from ...interfaces import Serializable

# Constants
PUBKEY_UNCOMPRESSED_PREFIX = b'\x04'
"""
    bytes: Prefix used by the public keys in uncompressed format.
"""
PUBKEY_UNCOMPRESSED_SIZE = 65
"""
    int: Size of the public keys in uncompressed format.
"""

PUBKEY_COMPRESSED_PREFIX_EVEN = b'\x02'
"""
    bytes: Prefix used by the public keys in compressed format when the ``y``
           coordinate is even.
"""

PUBKEY_COMPRESSED_PREFIX_ODD = b'\x03'
"""
    bytes: Prefix used by the public keys in compressed format when the ``y``
           coordinate is odd.
"""

PUBKEY_COMPRESSED_SIZE = 33
"""
    int: Size of the public keys in compressed format.
"""
PUBKEY_COMPRESSED_BY_DEFAULT = True
"""
    bool: Whether to use default public key compressed serialization format.
"""


class ECDSAPublicKey(Serializable):
    """ECDSA Public Key.

    In Bitcoin, a public key is a point of the ``secp256k1`` elliptic curve,
    that point is obtained by multiplying the private key and the curve
    generator.

    A public key can be expressed in two different ways, the most commonly
    used is the compressed format due to the space saving. Its format is
    marked with its own prefix.

    Its uncompressed format carries the two different coordinates of the
    point, meanwhile the compressed format only indicates de ``X`` coordinate
    so the ``Y`` coordinate can be easily computed every time its needed from
    the first one using the public key prefix.

    Attributes:
        _x (int): First coordinate of the point forming the public key.
        _y (int): Second coordinate of the point forming the public key.
        _compressed_by_default (bool): Flag indicating if its default
                                       serialization should be in compressed
                                       or in uncompressed form. Default
                                       value indicates compressed (``True``).
    """
    __slots__ = "_x", "_y", "_compressed_by_default"

    def __init__(self, x: int, y: int,
                 compressed: bool = PUBKEY_COMPRESSED_BY_DEFAULT) -> None:
        """Creates an ECDSAPublicKey object.

        Args:
            x: First coordinate of the point.
            y: Second coordinate of the point.
            compressed: Flag indicating if its serialization should be in
                        compressed format.
        """
        self._x, self._y = x, y
        self._compressed_by_default = compressed

    def verify_signature(self, msg: bytes, signature: ECDSASignature) -> bool:
        """Verifies if the signature is valid.

        A signature is considered valid if the passed message was signed with
        the associated private key of this public key.

        Args:
            msg: Presumably signed message.
            signature: Signature to verify related to the public key and to
                       the message.

        Returns:
            True if the signature can be verified, False otherwise.

        """
        # Calculations
        msg_num = int.from_bytes(msg, "big")
        w = inv(signature.s, DEFAULT_CURVE.n)
        u1 = fast_multiply(DEFAULT_CURVE.g, msg_num * w)
        u2 = fast_multiply(self.point, signature.r * w)
        p = fast_add(u1, u2)

        # final check
        return p[0] == signature.r

    @property
    def point(self) -> Tuple[int, int]:
        """Returns the two coordinates of the point representing the key."""
        return self._x, self._y

    @property
    def is_compressed_by_default(self) -> bool:
        """Indicates if its default serialization is compressed format."""
        return self._compressed_by_default

    @property
    def compressed(self) -> bytes:
        """Returns the public key in compressed format."""
        prefix = PUBKEY_COMPRESSED_PREFIX_ODD if self._y % 2 else \
            PUBKEY_COMPRESSED_PREFIX_EVEN

        return prefix + self._x.to_bytes(32, "big")

    @property
    def uncompressed(self) -> bytes:
        """Returns the public key in uncompressed format."""
        return PUBKEY_UNCOMPRESSED_PREFIX + self._x.to_bytes(32, "big") + \
            self._y.to_bytes(32, "big")

    def serialize(self) -> bytes:
        if self._compressed_by_default:
            return self.compressed
        else:
            return self.uncompressed

    @classmethod
    def deserialize(cls, pubkey: bytes) -> "ECDSAPublicKey":
        if PUBKEY_COMPRESSED_SIZE != len(pubkey) and len(pubkey) != \
                PUBKEY_UNCOMPRESSED_SIZE:
            raise ValueError("The pubkey has not a valid length.")

        prefix, x = pubkey[0], int.from_bytes(pubkey[1:33], "big")
        compressed = True

        # Uncompressed key
        if prefix == PUBKEY_UNCOMPRESSED_PREFIX[0]:
            if len(pubkey) != PUBKEY_UNCOMPRESSED_SIZE:
                raise ValueError("The pubkey has not a valid format.")
            y = int.from_bytes(pubkey[33:], "big")
            compressed = False

        # Compressed
        elif prefix in [PUBKEY_COMPRESSED_PREFIX_EVEN[0],
                        PUBKEY_COMPRESSED_PREFIX_ODD[0]]:
            if len(pubkey) != PUBKEY_COMPRESSED_SIZE:
                raise ValueError("The pubkey has not a valid format.")

            y = compute_y_from_x(x)

            if (prefix == PUBKEY_COMPRESSED_PREFIX_ODD[0] and y % 2 == 0) or \
                    (prefix == PUBKEY_COMPRESSED_PREFIX_EVEN[0] and y % 2):
                y = compute_y_from_x(x, False)

        # Not a valid prefix
        else:
            raise ValueError("The pubkey has not a valid format.")

        return cls(x, y, compressed)


class ECDSAPrivateKey(Serializable):
    """Bitcoin ECDSA private key

    In Bitcoin, a private key is a 256-bit number. Nearly every 256-bit number
    is a valid ECDSA private key. The range of valid private keys is governed
    by the ``secp256k1`` ECDSA standard used by Bitcoin.

    **Sources:**

    - https://en.bitcoin.it/wiki/Private_key

    Attributes:
        _value (int): Secret number, private key itself.
        _compressed_by_default (bool): Flag indicating if the associated
                                       public key should be in compressed
                                       format.
    """
    __slots__ = "_value", "_compressed_by_default"

    def __init__(self, value: int,
                 compressed: bool = PUBKEY_COMPRESSED_BY_DEFAULT) -> None:
        """Creates an ECDSAPrivateKey object.

        Args:
            value: Secret number that will conform the private key.
            compressed: Flag indicating if the associated public key should
                        be in compressed format.
        """
        if not 1 <= value <= DEFAULT_CURVE.n - 1:
            raise ValueError("The private key is not the valid range.")
        self._value = value
        self._compressed_by_default = compressed

    def to_public(self) -> ECDSAPublicKey:
        """Calculates and returns the associated public key to that exponent.

        Returns:
            The associated public key to that private key in the compression
            format the private key specified.
        """
        x, y = fast_multiply(DEFAULT_CURVE.g, self._value)
        return ECDSAPublicKey(x, y, self._compressed_by_default)

    def sign(self, msg: bytes) -> ECDSASignature:
        """Computes the signature of the data.

        Args:
            msg: :py:class:`bytes` that will be signed.

        Returns:
            :py:class:`ECDSASignature` object containing the signature.

        """
        return ECDSASignature(*ecdsa_raw_sign(msg, self._value))

    @property
    def value(self) -> int:
        """Returns the secret exponent of the private key."""
        return self._value

    def serialize(self) -> bytes:
        return self._value.to_bytes(32, "big")

    @classmethod
    def deserialize(cls, privkey: bytes) -> "ECDSAPrivateKey":
        return cls(int.from_bytes(privkey, "big"))
