"""Basic implementations from methods defined in BIP32."""


def ser32(i: int) -> bytes:
    """Serialize a 32-bit unsigned integer i as a 4-byte sequence, BE."""
    return int(i).to_bytes(4, "big")


def parse256(p: bytes) -> int:
    """Interprets a 32-byte sequence as a 256-bit number, BE."""
    return int.from_bytes(p, "big")
