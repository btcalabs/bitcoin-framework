"""Provides interfaces to create extended public/private keys.

Extended Public Keys are used to deterministically generate a collection of
Bitcoin addresses.


An extended key is just a base58 encoded serialization of a few pieces of data:
[ magic ][ depth ][ parent fingerprint ][ key index ][ chain code ][ key ]

An extended key is usually derived by "traversing" some path, meaning you would
start your derivation at some parent extended key, and consecutively derive
child keys with specific indexes until you finally derive the final extended
key in the path.
"""


# Libraries
from typing import TypeVar, Generic, Optional


# Relative imports
from .conversions import ser32, parse256
from ..ec import DEFAULT_CURVE
from ..hash import double_sha256, hmac_sha512, hash160, CHECKSUM_SIZE
from ..ecdsa.calculator import fast_add
from ..ecdsa.key import ECDSAPublicKey, ECDSAPrivateKey
from ...encoding import b58encode, b58decode
from ...interfaces import Serializable, Encodable
from ...params import BIP32_PUBKEY, BIP32_PRIVKEY, BlockchainParams, \
    AddressTypes, BLOCKCHAINS, get_default_network


# Constants
EXTENDED_KEY_LENGTH = 78
"""
    int: Size of a serialized extended key.
"""
EXTENDED_KEY_LENGTH_WITH_CHECKSUM = EXTENDED_KEY_LENGTH + CHECKSUM_SIZE
"""
    int: Total size of a serialized extended key including its checksum.
"""
HARDENED_THRESHOLD = 0x80000000  # 2^31
"""
    int: Threshold to consider a key as a hardened one (2**31)
         Less risk of parent private key leakage
         That value divides the space in two ranges:
         [0, 2**31-1] U [2**31, 2**32-1]
         non-hardened /     hardened
"""
DEFAULT_DEPTH = 0
"""
    int: Default depth level.
"""
DEFAULT_FINGERPRINT = b'\x00\x00\x00\x00'
"""
    bytes: Fingerprint default value.
"""
DEFAULT_CHILDNUMBER = b'\x00\x00\x00\x00'
"""
    bytes: Childnumber default value.
"""
MAX_DERIVATION_VALUE = 0xffffffff  # 2^32 - 1
"""
    int: Maximum value to derive a key.
"""

T = TypeVar("T", bound=Serializable)


class ExtendedECDSAKey(Generic[T], Serializable, Encodable):
    """

    Attributes:
        _depth (int):
        _fingerprint (bytes):
        _child_number (bytes):
        _chaincode (bytes):
        _key (T): The key itself.
        _version (bytes):
    """
    __slots__ = "_depth", "_fingerprint", "_child_number", "_chaincode", \
                "_key", "_network", "_version"

    def __init__(self, key: T, chaincode: bytes, depth: int = None,
                 fingerprint: bytes = None, childnumber: bytes = None,
                 network: BlockchainParams = None
                 ) -> None:
        """"""
        # Defaults
        self._depth = depth if depth is not None else DEFAULT_DEPTH
        self._fingerprint = fingerprint if fingerprint is not None else \
            DEFAULT_FINGERPRINT
        self._child_number = childnumber if childnumber is not None else \
            DEFAULT_CHILDNUMBER
        self._network = network if network is not None else \
            get_default_network()
        self._version = self._network.address_prefix(self.get_address_type())

        # Checks:
        if not 0 <= self._depth <= 0xff:
            raise ValueError(
                "The value of depth is not in the valid range ([0, 255]).")

        if not b'\x00\x00\x00\x00' <= self._fingerprint <= b'\xff\xff\xff\xff':
            raise ValueError("Invalid fingerprint.")

        self._key = key
        self._chaincode = chaincode

    def serialize(self) -> bytes:
        serialized_data = b''.join([
            self._version,
            bytes([self.depth]),
            self._fingerprint,
            self._child_number,
            self._chaincode,
            self.serialize_key()
        ])

        return serialized_data + self._checksum(serialized_data)

    @classmethod
    def deserialize(cls, data: bytes) -> "ExtendedECDSAKey":
        if len(data) != EXTENDED_KEY_LENGTH_WITH_CHECKSUM:
            raise ValueError("Not enough bytes provided.")
        # Parse data, fixed size for every field
        version = data[:4]
        depth = data[4]
        fingerprint = data[5:9]
        child_number = data[9:13]
        chaincode = data[13:45]
        key = cls.deserialize_key(data[45:78])  # type:ignore
        checksum_retrieved = data[78:]

        # Check version
        network = cls.get_network_by_version(version)
        if network is None:
            raise ValueError("Wrong type of address detected.")
        # Check deserialization, if checksum
        if not cls._checksum(data[:-4]) == checksum_retrieved:
            raise ValueError("Checksum does not match.")

        return cls(
            key, chaincode, depth,
            fingerprint, child_number, network)

    @classmethod
    def deserialize_key(cls, key: bytes) -> T:
        raise NotImplementedError

    def encode(self) -> str:
        """Encodes the address using :py:mod:`base58`."""
        return b58encode(self.serialize())

    @classmethod
    def decode(cls, address: str) -> "ExtendedECDSAKey":
        """Decodes the address using :py:mod:`base58`."""
        return cls.deserialize(b58decode(address))

    @classmethod
    def _checksum(cls, data: bytes) -> bytes:
        return double_sha256(data)[:CHECKSUM_SIZE]

    def is_master(self) -> bool:
        return self._depth == 0 and \
               self._fingerprint == DEFAULT_FINGERPRINT and \
               self._child_number == DEFAULT_CHILDNUMBER

    def serialize_key(self) -> bytes:
        """Serializes the key contained as specified by the protocol."""
        return self.key.serialize()

    @classmethod
    def get_address_type(cls) -> AddressTypes:
        raise NotImplementedError

    @classmethod
    def get_network_by_version(cls, version: bytes) -> \
            Optional[BlockchainParams]:
        atype = cls.get_address_type()

        for network in BLOCKCHAINS:
            if network.address_prefix(atype) == version:
                return network
        return None

    # Properties
    @property
    def version(self) -> bytes:
        return self._version

    @property
    def depth(self) -> int:
        return self._depth

    @property
    def fingerprint(self) -> bytes:
        return self._fingerprint

    @property
    def childnumber(self) -> bytes:
        return self._child_number

    @property
    def chaincode(self) -> bytes:
        return self._chaincode

    @property
    def key(self) -> T:
        return self._key


class ExtendedECDSAPublicKey(ExtendedECDSAKey[ECDSAPublicKey]):
    """
    """
    def __init__(self, pk: ECDSAPublicKey, *args, **kwargs) -> None:
        """"""
        super().__init__(pk, *args, **kwargs)

    def CKDpub(self, i: int, hardened: bool = False) -> \
            "ExtendedECDSAPublicKey":
        """

        Args:
            i:
            hardened:

        Returns:

        """
        if hardened:
            raise ValueError

        # HMAC-SHA512(Key = cpar, Data = serP(Kpar) || ser32(i))
        I = hmac_sha512(self._chaincode,  # noqa: E741
                        self._key.serialize()+ser32(i))
        # Split I into two 32-byte sequences, IL and IR
        Il, Ir = I[:32], I[32:]

        # In case parse256(IL) ≥ n or Ki is the point at infinity, the
        # resulting key is invalid, and one should proceed with the next value
        # for i
        Il_int = parse256(Il)
        if Il_int >= DEFAULT_CURVE.n:
            return self.CKDpub(i+1)
        else:
            # The returned child key Ki is point(parse256(IL)) + Kpar
            child_key_x, child_key_y = fast_add(
                self._key.point, ECDSAPrivateKey(Il_int).to_public().point)

        # The returned chain code ci is IR
        return ExtendedECDSAPublicKey(
            pk=ECDSAPublicKey(child_key_x, child_key_y),
            chaincode=Ir,
            depth=self._depth+1,
            fingerprint=hash160(self._key.serialize())[:4],
            childnumber=ser32(i),
            network=self._network
        )

    @classmethod
    def deserialize_key(cls, key: bytes) -> ECDSAPublicKey:
        return ECDSAPublicKey.deserialize(key)

    @classmethod
    def get_address_type(cls) -> AddressTypes:
        return BIP32_PUBKEY


class ExtendedECDSAPrivateKey(ExtendedECDSAKey[ECDSAPrivateKey]):
    """
    """
    def __init__(self, sk: ECDSAPrivateKey, *args, **kwargs) -> None:
        """"""
        super().__init__(sk, *args, **kwargs)

    @classmethod
    def from_seed(cls, seed: bytes, network: BlockchainParams = None) -> \
            "ExtendedECDSAPrivateKey":
        """

        Args:
            seed: Seed in order to generate the master private key.
            network: Network were the ExtendedECDSAPrivateKey is bounded.

        Returns:
            ExtendedECDSAPrivateKey object filled with the data.
        """
        # Calculate I = HMAC-SHA512(Key = "Bitcoin seed", Data = S)
        I = hmac_sha512("Bitcoin seed".encode('utf-8'), seed)  # noqa: E741
        # Split I into two 32-byte sequences, IL and IR.
        Il, Ir = I[:32], I[32:]
        # Use parse256(IL) as master secret key, and IR as master chain code.
        return cls(ECDSAPrivateKey(parse256(Il)), Ir, network=network)

    def CKDpriv(self, i: int, hardened: bool = False) -> \
            "ExtendedECDSAPrivateKey":
        """

        Args:
            i:
            hardened:

        Returns:

        """
        # Checks
        if not 0 <= i <= MAX_DERIVATION_VALUE:
            raise ValueError(
                "Invalid index; must be in the valid range: [0, 2**32-1].")

        # Not beyond the non-hardened range
        # Index on hardened range
        if HARDENED_THRESHOLD <= i <= MAX_DERIVATION_VALUE:
            hardened = True
        # Non-hardened range, but hardened value desired; increase index.
        elif hardened:
            i += HARDENED_THRESHOLD

        # Start Derivation
        if hardened:
            # If so (hardened child):
            # Data = 0x00 || ser256(kpar) || ser32(i))
            data = self.serialize_key()
        else:
            # Data = serP(point(kpar)) || ser32(i))
            data = self._key.to_public().serialize()

        data += ser32(i)

        # HMAC-SHA512(Key = cpar, Data = data)
        I = hmac_sha512(self._chaincode, data)  # noqa: E741

        # Split I into two 32-byte sequences, IL and IR
        Il, Ir = I[:32], I[32:]

        # In case parse256(IL) ≥ n or ki = 0, the resulting key is invalid,
        # and one should proceed with the next value for i. (Note: this has
        # probability lower than 1 in 2127.)
        Il_int = parse256(Il)
        if Il_int >= DEFAULT_CURVE.n:
            return self.CKDpriv(i + 1, hardened)
        else:
            # The returned child key ki is parse256(IL) + kpar (mod n)
            child_key = (parse256(Il) + self.key.value) % DEFAULT_CURVE.n

        # The returned chain code ci is IR
        return ExtendedECDSAPrivateKey(
            sk=ECDSAPrivateKey(child_key),
            chaincode=Ir,
            depth=self._depth + 1,
            fingerprint=hash160(self._key.to_public().serialize())[:4],
            childnumber=ser32(i),
            network=self._network
        )

    def to_public(self) -> ExtendedECDSAPublicKey:
        """Returns the Extended public key associated."""
        return ExtendedECDSAPublicKey(
            self._key.to_public(), self._chaincode, self._depth,
            self._fingerprint, self._child_number, self._network)

    def neuter(self) -> ExtendedECDSAPublicKey:
        """Alias of to_public following the BIP32 notation."""
        return self.to_public()

    def serialize_key(self) -> bytes:
        """"""
        return b'\x00' + self.key.serialize()

    @classmethod
    def deserialize_key(cls, key: bytes) -> ECDSAPrivateKey:
        return ECDSAPrivateKey.deserialize(key[1:])

    @classmethod
    def get_address_type(cls) -> AddressTypes:
        return BIP32_PRIVKEY
