"""
Defines functional models as interfaces to specify the functionality most
classes must provide in order to fulfill the puzzle-friendliness.
"""
# Libraries
from typing import Sequence, Dict, Any, Union
from abc import ABCMeta, abstractmethod


class Serializable(metaclass=ABCMeta):
    """Base interface for a serializable & deserializable class.

    Defines a model for a class that will be serializable, this means, will
    have methods to transform the class information into an array of bytes
    and to create a new class instance from an array of bytes.

    Those arrays of bytes will always have to be compatible when possible with
    the bytes specified in the Bitcoin protocol.

    Note:
        When we say an array of bytes we mean an instance of Python 3
        :py:class:`bytes` object.
    """
    __slots__ = ()

    @abstractmethod
    def serialize(self) -> bytes:
        """Serializes the object into a :py:class:`bytes` object.

        Serializes the contents of the current class into an array of bytes so
        the object can be represented as an array of bytes compatible with what
        the Bitcoin network protocol specifies.

        Returns:
            data of the class serialized in a bytes object
        """
        raise NotImplementedError

    def hex(self) -> str:
        """Returns the hexadecimal representation of the serialization."""
        return self.serialize().hex()

    @classmethod
    @abstractmethod
    def deserialize(cls, data: bytes) -> "Serializable":
        """Deserializes an array of bytes to create a new class instance.

        Deserializes the contents of the data passed to try make them fit into
        the class model to create a new class instance from this data. If the
        data has invalid length or invalid data, appropiate exceptions shall
        be raised by the implementations of this method.

        Note:
            The bytes format must be the same used in the serialization

        Warning:
            Please implement this method in a way that the bytes data can be
            larger than the strictly required size when dealing with variable
            sized fields, so it will help caller methods to detect size after
            calling deserialization.

        Args:
            data (bytes): a bytes object containing data to de-serialize

        Returns:
            an instance of the class filled with the data if succeeded

        Raises:
            ValueError: if data can't be fit into the model

        Note:
            Please implement subclasses of :py:class:`ValueError` if several
            exceptions may be raised in order to be able to detect which one
            was raised.
        """
        raise NotImplementedError

    @classmethod
    def from_hex(cls, data: str):
        """Deserializes an hex string to create a new class instance.

        Args:
            data: String in hexadecimal to build the object from.

        Returns:
            An instance of the class filled with the data if succeeded.
        """
        return cls.deserialize(bytes.fromhex(data))

    """
    .. automethod:: __len__
    """
    def __len__(self) -> int:
        """Length in bytes of the serialized object.

        Returns:
            number of bytes the serialized object takes
        """
        return len(self.serialize())

    def __eq__(self, other) -> bool:
        """Compares two objects based on their serializations.

        Compares if the object is equal to another object via their
        serialization if they are both :py:class:`Serializable` objects.

        Falls back to default Python comparison if the other object is not
        serializable.

        Returns:
            :py:keyword:`True` if the object is equal to the other
        """
        if isinstance(other, self.__class__):
            return self.serialize() == other.serialize()  # type: ignore
        else:
            return super().__eq__(other)

    def __repr__(self) -> str:
        """Reproduces the object using its hexadecimal representation."""
        return "{}.from_hex(\"{}\")".format(
            self.__class__.__name__,
            self.hex())

    def __bytes__(self) -> bytes:
        """Alias of :py:meth:`bitcoin.interfaces.Serializable.serialize`."""
        return self.serialize()  # pragma: no cover


class Encodable(metaclass=ABCMeta):
    """
    This interface defines that classes who inherit from it can be encoded into
    an object and decoded from an object. The difference between the previous
    class is that the serializable class is supposed to serialize to bytes
    objects and be deserialized from them. An encodable class when encoded
    contains enough information to then decode the output and obtain the same
    information but with the difference that when encoding, the result is not
    an array of bytes and is an object, most times a string object
    """

    @abstractmethod
    def encode(self):
        """
        Encodes the information of the class into an object so that this object
        can be then decoded into another object of this class with the same
        information

        Returns:
            object: object containing the encoded information of the class
        """
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def decode(cls, obj) -> "Encodable":
        """
        Decodes the object passed and tries to generate a new object of the
        class with the contents of the passed object

        Args:
            cls (class): class to decode the object into
            obj (object): object containing information to set class status

        Returns:
            cls(): an object with the status filled

        Raises:
            ValueError: if object passed can't be decoded
        """
        raise NotImplementedError


JSONType = Union[int, float, bool, str, Dict[str, Any], Sequence]
