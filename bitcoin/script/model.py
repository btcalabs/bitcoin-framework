"""
General purpose script, modelling all the data and the methods needed to
create a generalistic script.
"""

# Libraries
from collections import OrderedDict
from typing import List, Sequence, Union

# Relative imports
from ..field import OP_16, OP_PUSHDATA, OP_PUSHDATA1, OP_PUSHDATA2, \
    OP_PUSHDATA4, Opcode, ScriptData, ScriptField, VarInt
from ..interfaces import Serializable, JSONType

MYPY = False
if MYPY:
    from typing import Optional  # noqa: F401, pragma: no cover
    from ..tx import TxInputOutput  # noqa: F401, pragma: no cover


class Script(Serializable):
    """Defines how every script must look like and behave.

    Every script will have some internal values to build the script. When
    access to script's data is needed, the data property will call
    the method `_build`, that has to update a list of serializable items
    that contain opcodes and data that creates the script (attribute `_data`).

    Attributes:
        _items (List[ScriptField]): List of script items of the script (
                                    opcodes and data)
    """
    __slots__ = "_items",

    # Constructors
    def __init__(self, items: List[ScriptField] = None) -> None:
        """Initializes a script given the items to contain.

        Args:
            items: list of serializable items that conform the script
        """
        # Defaults
        items = items if items is not None else list()

        # Type checks
        assert isinstance(items, list) and \
            all(isinstance(item, ScriptField) for item in items), \
            "Script items items must be either an opcode or a ScriptData. " \
            "Remember to use parenthesis when declaring opcodes (OP_0() " \
            "instead of OP_0)"

        # Do save
        self._items = items

    @classmethod
    def from_asm(cls, asm: Union[str, List[str]]) -> "Script":
        """Given an ASM returns the matching script.

        Args:
            asm: List of ASM items or a :py:class:`str` containing an ASM
                 script with opcodes and data separed by spaces.
        """
        # Defaults
        asm = asm.split() if isinstance(asm, str) else asm

        # Type assertions
        assert isinstance(asm, list) and \
            all(isinstance(item, str) for item in asm)

        # Convert ASM
        items = list()  # type: List[ScriptField]
        for item in asm:
            # Guess opcode
            opcode = Opcode.from_asm(item)
            if opcode is not None:
                items.append(opcode)
            else:
                # Unknown opcode maybe
                if item.startswith("OP_"):
                    raise ValueError(
                        "Opcode %s has not been implemented yet. "
                        "Please do a PR in "
                        "https://github.com/BTCAssessors/bitcoin-framework "
                        "and we will be really grateful!"
                    )
                else:
                    # Push data
                    # TODO: Interpret [ALL],... in ASM of scriptsigs
                    try:
                        item_bytes = bytes.fromhex(item)
                    except ValueError as e:
                        raise ValueError("Unable to decode ASM data: " +
                                         str(e))
                    items.append(ScriptData(item_bytes))

        return cls(items)

    # Serialize / deserialize
    def serialize(self) -> bytes:
        """
        Serializes the contents of the current class into an array of bytes so
        the class can be represented as an array of bytes compatible with what
        the Bitcoin protocol specifies.

        Returns:
            bytes: data of the script serialized in a bytes object
        """
        return b''.join(x.serialize() for x in self._items)

    @classmethod
    def deserialize(cls, data: bytes) -> "Script":
        """Deserializes the bytes data object as a script."""
        # Type assertions
        assert isinstance(data, bytes)

        offset = 0
        items = list()  # type: List[ScriptField]

        while offset < len(data):
            # As maximum we need 5 bytes (biggest is OP_PUSHDATA4)
            opcode = Opcode.deserialize(data[offset:offset+5])
            # Push data
            if isinstance(opcode, OP_PUSHDATA) \
                    or isinstance(opcode, OP_PUSHDATA1) \
                    or isinstance(opcode, OP_PUSHDATA2) \
                    or isinstance(opcode, OP_PUSHDATA4):
                script_data = ScriptData.deserialize(data[offset:])
                items.append(script_data)
                offset += len(script_data)
            else:
                items.append(opcode)
                offset += 1

        return cls(items)

    # Other methods
    @staticmethod
    def _alike(items: Sequence[ScriptField],
               other_items: Sequence[ScriptField]):
        """Compares two sequences of script items to see if they are alike.

        Compares two sequences of script items and raises a
        :py:class`ValueError` if they are not alike with the reason they are
        not.

        Check :py:meth:`Script.alike` for more information about *alike*
        meaning.
        """
        # Same items length
        if len(items) != len(other_items):
            raise ValueError("Not the same items count.")

        # Same opcodes / script data format
        i = 0
        for item, other_item in zip(items, other_items):
            # Both items are opcodes
            if isinstance(item, Opcode) and \
                    isinstance(other_item, Opcode):
                # Must be same opcode
                if item.opcode() != other_item.opcode():
                    raise ValueError(
                        "Difference at element %d: Opcode is %d (%s), "
                        "expected %d (%s)." % (
                            i+1, item.opcode(),
                            item.asm(), other_item.opcode(),
                            other_item.asm()))
            # Both items are data
            elif isinstance(item, ScriptData) and \
                    isinstance(other_item, ScriptData):
                # Must be same data length
                if len(item.data) != len(other_item.data):
                    raise ValueError(
                        "Difference at element %d: Data pushed takes %d "
                        "bytes, expected %d bytes." % (
                            i+1, len(item.data), len(other_item.data)))
                # TODO(davidlj): Same opcodes to push data are used.
            # Some item is data and the other is opcode
            else:
                raise ValueError(
                    "Difference at element %d: found %s when %s "
                    "was expected" % (
                        i+1,
                        "opcode" if isinstance(item, Opcode) else "data",
                        "opcode" if isinstance(other_item, Opcode) else "data")
                )

            i += 1

    def alike(self, other_script: "Script") -> "Script":
        """Compares two scripts to see if they are alike.

        Note:
           To be alike means:

           - Same amount of items (:py:meth:`Script.count`) is found on both
             scripts.

           - For each item in both script items:

             - They are both the same type of item (Opcode, Data)
             - If one item is an opcode, the other item is the same opcode.
             - If one item is data, the other item has the same data length.

           For instance, the following scripts are alike:
           ``OP_HASH160 0123456789abcdef0123456789abcdef01234567 OP_EQUAL``

           ``OP_HASH160 0000000000000000000000000000000000000000 OP_EQUAL``

        Args:
            other_script: The other script to see if they are both alike.

        Raises:
            ValueError: They are not alike.
        """
        # Type assertions
        assert isinstance(other_script, Script)

        # Check if they are alike
        self._alike(self._items, other_script.items)

        return self

    def is_alike(self, other_script: "Script") -> bool:
        """Same as :py:meth:`Script.alike` but returns a boolean.

        See Also:
            :py:meth:`Script.alike` method's note to understand the *alike*
            meaning.

        Args:
            other_script: The other script to check if they are alike.

        Returns:
            ``True`` if they are alike.
        """
        # Type assertions
        assert isinstance(other_script, Script)

        # Check if they are alike
        try:
            self.alike(other_script)
        except ValueError:
            return False
        else:
            return True

    # Properties
    @property
    def items(self) -> List[ScriptField]:
        """Returns the script items (opcodes and data).

        Returns:
            list: Script items
        """
        return self._items

    @property
    def count(self) -> int:
        """Returns the number of items present in the ``Script``

        Equivalent to ``len(self.items)``
        """
        return len(self._items)

    def encode(self) -> JSONType:
        return OrderedDict((
            ("asm", str(self)),
            ("hex", self.hex())
        ))

    # Magic methods
    def __add__(self, other: "Script") -> "Script":
        """Adds the contents of this script to another script."""
        # Type assertions
        assert isinstance(other, Script)

        return self.__class__(self._items + other.items)

    def __str__(self) -> str:
        """Prints the script looping through its fields."""
        return " ".join([str(item) for item in self._items])

    def __repr__(self) -> str:
        """Represents the string using items __repr__."""
        return "{}([{}])".format(
            self.__class__.__name__,
            ", ".join([repr(item) for item in self._items])
        )


class Witness(Serializable):
    """Witness containing data to spend an input when using SegWit.

    Attributes:
        _items (List[bytes]): *Witness* data items
    """
    __slots__ = "_items",

    def __init__(self, items: List[bytes] = None) -> None:
        """Initializes a witness with the given items items.

        Args:
            items: Data items to store in the witness.
        """
        # Defaults
        items = list() if items is None else items

        # Type assertions
        assert isinstance(items, list) and \
            all(isinstance(item, bytes) for item in items)

        # Store items items
        self._items = items

    def serialize(self) -> bytes:
        """Returns the serialization of the witness and its items."""
        return VarInt(len(self._items)).serialize() + \
            b''.join(
                VarInt(len(item)).serialize() + item for item in self._items)

    @classmethod
    def deserialize(cls, data: bytes) -> "Witness":
        """Returns a witness given its serialization.

        Raises:
            ValueError: One of the length fields is invalid.
        """
        offset = 0

        # Items count
        items_count = VarInt.deserialize(data[offset:])
        offset += len(items_count)

        # Deserialize items
        items = list()
        for i in range(items_count.value):
            # Item size
            item_length = VarInt.deserialize(data[offset:])
            offset += len(item_length)

            # Store item
            items.append(data[offset:offset+item_length.value])
            offset += item_length.value

        # Initialize witness
        return cls(items)

    @property
    def items(self) -> List[bytes]:
        """Returns the data items contained in the witness."""
        return self._items

    @property
    def count(self) -> int:
        """Returns the number of data items in the witness."""
        return len(self._items)

    def encode(self) -> JSONType:
        return [i.hex() for i in self.items]

    def __str__(self) -> str:
        """Returns the witness as a string of hexadecimals."""
        return "[{}]".format("".join(item.hex() for item in self._items))

    def __repr__(self) -> str:
        """Represents the witness so it can be easily recreated again."""
        return "{}([{}])".format(self.__class__.__name__, ", ".join(
            "bytes.fromhex(\"{}\")".format(item.hex()) for item in
            self._items))

    def __add__(self, other) -> "Witness":
        """Appends a witness to this witness."""
        return Witness(self._items + other.items)


class TransactionScript(Script):
    """Script that belongs to a transaction input / output.

    Attributes:
        _parent (TxInputOutput): Parent transaction i/o it belongs to
    """
    __slots__ = "_parent",

    def __init__(self, items: List[ScriptField] = None) -> None:
        """Initializes an i/o script given the parent and items."""
        super().__init__(items)
        self._parent = None  # type: Optional[TxInputOutput]

    def __add__(self, other: "Script") -> "TransactionScript":
        """Adds another script items to this ``Script``.

        Keeps the link to the parent, if any.
        """
        # Type assertions
        assert isinstance(other, Script)

        # Add other items to its list
        self._items += other.items

        # Return itself with its list updated
        return self


class ScriptPubKey(TransactionScript):
    """Models a ``ScriptPubKey`` to set in a transaction output field.

    It contains a reference to the output the script is being included in.
    """

    def __init__(self, items: List[ScriptField] = None) -> None:
        """
        Initializes a scriptPubKey with the tx_output, address and items given.

        Args:
            items: Items of the ``ScriptPubKey``
        """
        super().__init__(items)

    @classmethod
    def deserialize(cls, data: bytes) -> "ScriptPubKey":
        """Deserializes the ``ScriptPubKey`` data."""
        return cls(super().deserialize(data).items)


class ScriptSig(TransactionScript):
    """Models a ``ScriptSig`` to set in a transaction input field.

    It contains a reference to the input the script is related to.

    Attributes:
        _witness (Witness): Witness if the input uses SegWit.
    """
    def __init__(self, items: List[ScriptField] = None,
                 witness_items: List[bytes] = None) -> None:
        """Initializes a ``ScriptSig`` given its items and witness.

        Creates an empty ``ScriptSig`` if no items is provided. If just
        script items is provided, the *witness* will be empty.
        """
        # Initialize
        super().__init__(items)

        # Store witness
        self._witness = Witness(witness_items) if witness_items is not None \
            else Witness()

    @classmethod
    def deserialize(cls, data: bytes) -> "ScriptSig":
        """Deserializes the data as a ``ScriptSig``

        Will just deserialize the data, not the witness.
        """
        return cls(super().deserialize(data).items)

    @property
    def witness(self) -> "Witness":
        """Returns the witness.

        Empty :py:class:`Script` if no witness is used.
        """
        return self._witness

    @witness.setter
    def witness(self, new_witness: "Witness"):
        """Changes the witness"""
        self._witness.items[:] = new_witness.items

    @property
    def has_witness(self) -> bool:
        """Returns whether it has a witness or not.

        Checks the length of it for that reason.
        """
        return self._witness.count > 0

    @property
    def is_standard(self) -> bool:
        """Returns ``True`` if the ``ScriptSig`` is considered standard.

        To be standard, just OP_0, opcodes to push data, OP_1NEGATE,
        OP_1 and OP_[1-16] have to be included in the script's data.

        Tip:
           This is implemented in Bitcoin Core's ``CScript::isPushOnly``

           https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/script/\
           .cpp#L237-L252
        """
        return all(
            isinstance(item, ScriptData) or item.value <= OP_16.opcode()
            for item in self._items)

    def __add__(self, other: "Script") -> "ScriptSig":
        """Appends another script to the ``ScriptSig``."""
        # Type assertions
        assert isinstance(other, Script)

        # Do sum
        if isinstance(other, ScriptSig):
            # Sum witness
            self._witness += other.witness

        # Sum items to itself
        self._items += other.items

        return self
