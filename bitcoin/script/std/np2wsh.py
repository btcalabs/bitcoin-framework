"""Nested ``P2WSH`` in a ``P2SH`` ``ScriptPubKey`` and ``ScriptSig``."""
# Libraries
from typing import List

# Relative imports
from .errors import StdSHLengthError
from .p2sh import P2SHScriptPubKey, P2SHScriptSig, StdP2SHScriptPubKey
from .p2wsh import P2WSHScriptPubKey
from .pubkey import StdScriptPubKeyRegistry
from .sig import StdP2SHScriptSig, StdScriptSigRegistry
from ..model import Script
from ...field import ScriptField, ScriptData
from ...params import SH_SIZE_SEGWIT
from ...standards import PaymentStandard, NP2WSH


class NP2WSHScriptSig(StdP2SHScriptSig, metaclass=StdScriptSigRegistry):
    """Nested ``P2WSH`` in a ``P2SH`` ``ScriptSig``."""

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return NP2WSH

    def _create_items(self) -> List[ScriptData]:
        """Creates the items to store in the witness."""
        redeem_script = P2WSHScriptPubKey.from_script(self.redeem_script)
        return P2SHScriptSig(redeem_script).items  # type: ignore

    def _create_witness_items(self) -> List[bytes]:
        """Creates the witness items of the P2SH ScriptSig."""
        return super()._create_witness_items()

    def script_pubkey(self) -> "NP2WSHScriptPubKey":
        """Returns a matching :py:class:`NP2WSHScriptPubKey`."""
        return NP2WSHScriptPubKey.from_script(self._redeem_script)

    def dummy_sign(self) -> bytes:
        """
        32-bytes program hash
        1-byte push-data program hash
        1-byte OP_0
        1-byte push-data OP_0
        --> TOTAL = 35 bytes.
        """
        return bytes(35)


class NP2WSHScriptPubKey(StdP2SHScriptPubKey,
                         metaclass=StdScriptPubKeyRegistry):
    """Nested ``P2WSH`` in a ``P2SH`` ``ScriptPubKey``."""

    # Constructors
    def __init__(self, script_hash: bytes) -> None:
        """Initializes a ``P2WSH`` in a ``P2SH``.

        Checks the given *redeem script*'s hash has a valid size.

        Args:
            script_hash: *Redeem script*'s hash to pay to.

        Raises:
            StdSHLengthError: *Script's hash* size is invalid.
        """
        # Checks
        if len(script_hash) != SH_SIZE_SEGWIT:
            raise StdSHLengthError

        # Initialize
        super().__init__(script_hash)

    @classmethod
    def from_script(cls, script: Script) -> "NP2WSHScriptPubKey":
        """Creates a ``P2SH-P2WSH`` given the *redeem script*."""
        return cls(cls.hash_script(script))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return NP2WSH

    @classmethod
    def hash_script(cls, script: Script) -> bytes:
        """Returns the hash of a *redeem script* to build a ``P2SH-P2WSH``."""
        return P2WSHScriptPubKey.hash_script(script)

    @classmethod
    def _create_items(cls, script_hash: bytes) -> List[ScriptField]:
        """Creates the items for a ``P2SH-P2WSH`` ``Script``."""
        return P2SHScriptPubKey.from_script(
            P2WSHScriptPubKey(script_hash)).items

    # Properties
    @property
    def script_hash(self) -> bytes:
        """Returns the *redeem script*'s hash is paying to."""
        return self._data

    @property
    def data(self) -> bytes:
        """Information this ``Script`` is paying to. (``P2SH`` script hash)."""
        return P2SHScriptPubKey.from_script(
            P2WSHScriptPubKey(self._data)).script_hash

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard nested ``P2SH-P2WSH`` ``ScriptPubKey``.

        Warning:
           Because it is impossible to distinguish a ``P2SH-P2WSH`` from a
           regular ``P2SH`` because they are :py:meth:`bitcoin.Script.alike`,
           a ``P2SH-P2WSH`` will not be able to be deserialized.

           Use :py:meth:`bitcoin.script.std.p2sh.P2SHScriptPubKey.deserialize`
           instead.

        Raises:
            ValueError: As it cannot be distinguished from a regular ``P2SH``
        """
        raise ValueError("A P2SH-P2WSH ScriptPubKey cannot be deserialized "
                         "as it is alike to a regular P2SH. Use P2SH "
                         "deserialization instead.")
