"""Standard script's models."""

# Libraries
import logging
from abc import ABCMeta, abstractmethod
from typing import List, Type
from typing import Tuple, Sequence, MutableMapping  # noqa: F401

from .errors import StdPKHLengthError
# Relative imports
from .model import StdPaymentScript
from ..model import Script, ScriptPubKey
from ...crypto import ECDSAPublicKey, ripemd160_sha256
from ...field import get_op_code_n, ScriptData
from ...field.model import ScriptField
from ...params import PKH_SIZE, SH_SIZE_SEGWIT
from ...standards import StdPaymentTypes, PaymentStandard

# Constants
LOGGER = logging.getLogger(__name__)
"""
    logger for that file.
"""
WITNESS_VERSION_MIN = 0
"""
    int: Minimum SegWit witness version.
"""
WITNESS_VERSION_MAX = 16
"""
    int: Maximum SegWit witness version.
"""
WITNESS_VERSION_DEFAULT = 0
"""
    int: Default version for SegWit witness programs.

    Currently (20180914) just version 0 exists.

**Sources:**

- https://github.com/bitcoin/bips/blob/master/bip-0141.mediawiki\
#witness-program
"""


# Exceptions
class StdSegWitScriptPubKeyVersionError(ValueError):
    """Invalid witness version for a SegWit ``ScriptPubKey``."""
    pass


class StdSegWitScriptPubKeyProgramError(ValueError):
    """Invalid witness program for the given witness version."""
    pass


# Classes
class StdScriptPubKey(ScriptPubKey, StdPaymentScript, metaclass=ABCMeta):
    """Generic standard ``ScriptPubKey``.

    Attributes:
        _data (bytes): Information the standard ``ScriptPubKey`` is paying to.
    """
    __slots__ = "_data"

    def __init__(self, data: bytes, items: List[ScriptField] = None) -> None:
        """Initializes the StdScriptPubKey value."""
        # Type assertions
        assert isinstance(data, bytes)

        # Initialize parent
        super().__init__(items)

        # Store data
        self._data = data

    @classmethod
    def deserialize(cls, data: bytes) -> "StdScriptPubKey":
        """Deserializes a standard ``ScriptPubKey``.

        Returns the concrete implementation of the ``ScriptPubKey`` that
        matches the data given data. For instance, if the
        :py:paramref:`StdScriptPubKey.deserialize.data` argument matches a
        :py:class:`bitcoin.script.std.p2wpkh.P2WPKHScriptPubKey`, an instance
        of that class will be returned instead of an instance of a
        :py:class:`StdScriptPubKey`.

        Raises:
            ValueError: Data is empty.
            ValueError: Data does not match any of the known standard
            ``ScriptPubKey``s
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Empty data not valid
        if len(data) == 0:
            raise ValueError("Data to be deserialized is empty.")

        # Loop known standard ScriptPubKeys
        std_scriptpubkey = None
        for std_scriptpubkey_class in \
                StdScriptPubKeyRegistry._STD_SCRIPTPUBKEY_REGISTRY:
            try:
                std_scriptpubkey = std_scriptpubkey_class.deserialize(data)
            except ValueError:
                pass
            else:
                break

        # No one found
        if std_scriptpubkey is None:
            raise ValueError("The data to be deserialized does not seem to "
                             "be any known standard ScriptPubKey.")
        else:
            return std_scriptpubkey

    @property
    def data(self) -> bytes:
        """Main information that the script pays to.

        For instance, the public key hash for a ``P2PKH`` or the *redeem
        script* hash in a *P2SH*
        """
        return self._data

    def __repr__(self) -> str:
        """Creates the standard ``ScriptPubKey`` using its main data."""
        return "{}({})".format(
            self.__class__.__name__,
            self._data)


class StdP2SHScriptPubKey(StdScriptPubKey, metaclass=ABCMeta):
    """Generic standard ``P2PKH`` ``ScriptPubKey``.

    Implementations:

    - :py:class:`bitcoin.script.std.p2sh.P2SHScriptPubKey`
      Legacy ``P2SH``

    - :py:class:`bitcoin.script.std.p2wsh.P2WSHScriptPubKey`
      Native SegWit ``P2WSH``

    - :py:class:`bitcoin.script.std.np2wsh.NP2WSHScriptPubKey`
      Nested SegWit ``P2SH-P2WSH``
    """
    __slots__ = ()  # type: Tuple

    # TODO(davidlj): typing hint for `tx_output`
    @abstractmethod
    def __init__(self, script_hash: bytes) -> None:
        """Initializes a standard ``P2SH`` ``ScriptPubKey``.

        .. Size check must be implemented by the concrete classes
        """
        # Initialize
        super().__init__(script_hash, self._create_items(script_hash))

    @classmethod
    @abstractmethod
    def _create_items(cls, script_hash: bytes) -> List[ScriptField]:
        """Creates the script items for this ``P2SH`` ``ScriptPubKey``

        Args:
            script_hash: *Redeem script*'s hash to pay to.

        Returns:
            ``P2SH`` ``ScriptPubKey`` items

        Raises:
            StdScriptPubKeySHLengthError: Invalid *redeem script*'s hash length
        """
        pass  # pragma: no cover

    @classmethod
    @abstractmethod
    def from_script(cls, script: Script) -> "StdP2SHScriptPubKey":
        """Creates a ``P2SH`` ``ScriptPubKey`` given a *redeem script*.

        Args:
            script: Redeem script to build a ``P2SH`` ``ScriptPubKey`` from.

        Returns:
            New ``P2SH`` ``ScriptPubKey`` that pays to the *redeem script*'s
            hash.
        """
        pass  # pragma: no cover

    @classmethod
    @abstractmethod
    def hash_script(cls, script: Script) -> bytes:
        """Calculates a *redeem script*'s hash.

        .. Each subclass must use the suitable hash algorithm.
        """
        pass  # pragma: no cover

    @property
    def script_hash(self) -> bytes:
        """Returns the *redeem script*'s hash this ``ScriptPubKey`` pays to."""
        return self._data

    @property
    def data(self):
        """Alias of :py:attr:`StdP2SHScriptPubKey.script_hash`."""
        return super().data


class StdP2PKHScriptPubKey(StdScriptPubKey, metaclass=ABCMeta):
    """Generic standard ``P2PKH`` ``ScriptPubKey``.

    Implementations:

    - :py:class:`bitcoin.script.std.p2pkh.P2PKHScriptPubKey`
      Legacy ``P2PKH``

    - :py:class:`bitcoin.script.std.p2wpkh.P2WPKHScriptPubKey`
      Native SegWit ``P2WPKH``

    - :py:class:`bitcoin.script.std.np2wpkh.NP2WPKHScriptPubKey`
      Nested SegWit ``P2SH-P2WPKH``
    """
    __slots__ = ()  # type: Tuple

    def __init__(self, public_key_hash: bytes) -> None:
        """Initializes a ``P2PKH`` ``ScriptPubKey``.

        Raises:
            StdP2PKHLengthError: Public key's hash has an invalid length
        """
        # Type assertions
        assert isinstance(public_key_hash, bytes)

        # Checks
        if len(public_key_hash) != PKH_SIZE:
            raise StdPKHLengthError

        # Initialize
        super().__init__(public_key_hash,
                         self._create_items(public_key_hash))

    @classmethod
    @abstractmethod
    def _create_items(cls, public_key_hash: bytes) -> List[ScriptField]:
        """Creates the script items for this ``P2PKH`` ``ScriptPubKey``

        Args:
            public_key_hash: Public key's hash to pay to.

        Returns:
            ``P2PKH`` ``ScriptPubKey`` items
        """
        pass  # pragma: no cover

    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard ``P2PKH`` ``ScriptPubKey``.

        Specific methods must detect the P2PKH opcodes and data and pick the
        public key hash from it.

        Raises:
            ValueError: Data is not a valid ``P2PKH`` ``ScriptPubKey``.
        """
        pass  # pragma: no cover

    @classmethod
    @abstractmethod
    def from_public_key(cls, public_key: ECDSAPublicKey) -> \
            "StdP2PKHScriptPubKey":
        """Creates a ``P2PKH`` ``ScriptPubKey`` given a *public key*."""
        pass  # pragma: no cover

    @classmethod
    def hash_public_key(cls, public_key: ECDSAPublicKey) -> bytes:
        """Calculates a *public key*'s hash."""
        return ripemd160_sha256(public_key.serialize())

    @property
    def public_key_hash(self) -> bytes:
        """Returns the *public key*'s hash this ``ScriptPubKey`` pays to."""
        return self._data

    @property
    def data(self):
        """Alias of :py:attr:`StdP2PKHScriptPubKey.public_key_hash`."""
        return super().data


class StdSegWitScriptPubKey:
    """Standard ``SegWit`` ``ScriptPubKey``.

    Helps constructing a standard ``SegWit`` ``ScriptPubKey``.

    Attributes:
        _version (int): Version of the witness program.
        _program (bytes): Witness program to pay to.
    """
    __slots__ = "_version", "_program"

    def __init__(self, program: bytes,
                 version: int = WITNESS_VERSION_DEFAULT) -> None:
        """Initializes a ``StdSegWitScriptPubKey``.

        Checks the program and version are valid.

        Raises:
            StdSegWitScriptPubKeyVersionError
            StdSegWitScriptPubKeyInvalidProgramError
        """
        # Type assertions
        assert isinstance(program, bytes)
        assert isinstance(version, int)

        # Checks
        if version < 0 or version > 16:
            raise StdSegWitScriptPubKeyVersionError
        if version == 0 and len(program) != SH_SIZE_SEGWIT \
                and len(program) != PKH_SIZE:
            raise StdSegWitScriptPubKeyProgramError(
                "Invalid program for version 0 witness scriptPubKey")
        if version != 0:
            LOGGER.warning("Version should be 0: it's the only one defined.")

        # Assignments
        self._version = WITNESS_VERSION_DEFAULT
        self._program = program

    @property
    def version(self) -> int:
        """Returns the witness program version."""
        return self._version

    @property
    def program(self) -> bytes:
        """Returns the witness program."""
        return self._program

    def create_items(self) -> List[ScriptField]:
        """Creates the script items with the the program and version.

        This will be always in the following format:

        ``[version][program]``

        Where ``version`` is an ``OP_N`` with ``N`` being the version number
        used and program is a :py:class:`bitcoin.ScriptData` that pushes the
        witness program to the stack.

        Currently, version is always
        :py:const:`bitcoin.WITNESS_VERSION_DEFAULT`
        """
        return [get_op_code_n(self._version), ScriptData(self._program)]


# Registry
class StdScriptPubKeyRegistry(ABCMeta):
    """Registers standard ScriptPubKey availables."""

    _STD_SCRIPTPUBKEY_REGISTRY = []  # type: Sequence[StdScriptPubKey]
    """
        Iterable[StdScriptPubKey]: \
            All standard ``ScriptPubKey`` implementations.
    """
    _STD_SCRIPTPUBKEY_TYPE_MAP = {
        StdPaymentTypes.PKH.name: StdP2PKHScriptPubKey,
        StdPaymentTypes.SH.name: StdP2SHScriptPubKey
    }
    """
        Mapping[PaymentType, Type[StdScriptPubKey]]: \
            Map a standard payment type to its interface
    """
    _STD_SCRIPTPUBKEY_MAP = \
        dict()  # type: MutableMapping[str, Type[StdScriptPubKey]]
    """
        Mapping[PaymentStandard, Type[StdScriptPubKey]]: \
            Map a standard script pubkey to its standard.
    """

    def __new__(mcs, name, bases, namespace, **kwargs):
        """Creates the class and registers it."""
        # Create the class
        cls = super().__new__(mcs, name, bases, namespace, **kwargs)

        # Append to registries
        # # Global registry
        cls._STD_SCRIPTPUBKEY_REGISTRY.append(cls)
        # # Standard registry
        mcs.register_standard_scriptpubkey(cls)
        return cls

    @classmethod
    def register_standard_scriptpubkey(
            mcs, script_pubkey_class: Type[StdScriptPubKey]) -> None:
        """Registers the given script pubkey class."""
        # Variables
        payment_standard = script_pubkey_class.get_payment_standard()
        # Assert proper interface
        script_pubkey_interface = \
            mcs._STD_SCRIPTPUBKEY_TYPE_MAP[payment_standard.payment_type.name]
        if not issubclass(script_pubkey_class, script_pubkey_interface):
            raise ValueError("Proper ScriptPubKey payment type interface not "
                             "implemented")
        # Assert does not exist
        if str(payment_standard) in mcs._STD_SCRIPTPUBKEY_MAP:
            raise ValueError(
                "A StdP2PKHScriptPubKey has already been registered"
                "for this payment standard")
        # Register
        mcs._STD_SCRIPTPUBKEY_MAP[str(payment_standard)] = script_pubkey_class

    @classmethod
    def by_standard(mcs, standard: PaymentStandard) -> Type[StdScriptPubKey]:
        return mcs._STD_SCRIPTPUBKEY_MAP[str(standard)]
