"""Models a NullData Output."""

# Relative imports
from ..model import ScriptPubKey
from ...field import OP_RETURN, ScriptData


# Constants
MAX_OP_RETURN_RELAY = 83
"""
    int: Maximum size for the NullDataScriptPubKey:
             80 bytes of data,
             +1 for OP_RETURN
             +2 for the pushdata opcodes
"""
MAX_OP_RETURN_DATA = 80
"""
    int: maximum size of data to be carried by a TxOutput.
"""


class NullDataScriptPubKey(ScriptPubKey):
    """Defines which its main purpose is to carry data.

    Attributes:
        _data: Data to be placed into the output.
    """
    __slots__ = "_data",

    def __init__(self, data: str):
        """Creates an Script that pays nobody and carries data.

        Args:
            data: String (data) to put into the Output.
        """
        assert len(data) <= MAX_OP_RETURN_DATA

        self._data = data
        super().__init__([OP_RETURN(), ScriptData(data.encode("utf-8"))])

    @property
    def data(self) -> str:
        return self._data
