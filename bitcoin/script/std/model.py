"""Standard scripts model."""
from abc import ABCMeta, abstractmethod
from ...standards import PaymentStandard


class StdPaymentScript(metaclass=ABCMeta):
    """A standard payment script provides the standard being implemented."""

    @classmethod
    @abstractmethod
    def get_payment_standard(cls) -> PaymentStandard:
        """Payment standard being implemented by the class."""
        pass

    @property
    def payment_standard(self) -> PaymentStandard:
        """Payment standard of the script."""
        return self.__class__.get_payment_standard()
