"""Legacy ``P2PKH`` ``ScriptPubKey`` and ``ScriptSig``."""
# Libraries
from typing import List

# Relative imports
from .pubkey import Script, StdP2PKHScriptPubKey, StdScriptPubKeyRegistry
from .sig import StdP2PKHScriptSig, StdScriptSigRegistry
from ...crypto import ECDSAPrivateKey, ECDSAPublicKey
from ...field import OP_CHECKSIG, OP_DUP, OP_EQUALVERIFY, OP_HASH160, \
    ScriptData
from ...field.model import ScriptField
from ...params import PKH_SIZE
from ...standards import P2PKH, PaymentStandard
from ...tx import DEFAULT_HASHTYPE, HashType, TxOutput


class P2PKHScriptSig(StdP2PKHScriptSig, metaclass=StdScriptSigRegistry):
    """Legacy ``P2PKH`` ``ScriptSig``.

    Allows to spend funds sent to a :py:class:`P2PKHScriptPubKey`, by revealing
    the public key whose hash is the one placed in the
    :py:class:`P2PKHScriptPubKey` and placing a signature made with its
    associated private key.
    """

    def sign(self, private_key: ECDSAPrivateKey, utxo: TxOutput = None,
             hashtype: HashType = DEFAULT_HASHTYPE) -> "P2PKHScriptSig":
        """Places a signature in the script given a secret key.

        .. attention::
            Before using this method, check that this
            :py:class:`P2PKHScriptSig` has been assigned to a
            :py:class:`bitcoin.TxInput` and that input has been assigned to a
            transaction (:py:class:`bitcoin.Tx`).

            Otherwise, data to create the signature will be missing as this
            data comes from the transaction this :py:class:`P2PKHScriptSig`
            belongs to.

        Note:
            When providing the :py:paramref:`P2PKHScriptSig.sign.utxo`
            argument (a :py:class:`bitcoin.TxOutput`) referring to the
            unspent transaction output this ``ScriptSig`` is spending, just the
            :py:class:`P2PKHScriptPubKey` of the output is required to be
            filled as the value is not used when creating the signature.

            You can set value to 0, for instance.

            Check :py:class:`bitcoin.LegacySignatureData` for more information
            about how the message and its hash to be signed is created.

        Note:
            If no :py:paramref:`P2PKHScriptSig.sign.utxo` is provided,
            the :py:paramref:`P2PKHScriptSig.sign.utxo` will be
            automatically calculated using the private key, but beware that
            then it cannot be checked if the private key used to sign is the
            proper one to spend that :py:paramref:`P2PKHScriptSig.sign.utxo`
            as it is being created dynamically.

        Args:
            private_key: Private ECDSA key to create the signature
            utxo: Previous unspent transaction output being spent.
                  Depending on the type, it must be fully or partially filled.
                  It may be omitted and the
            hashtype: Hashtype to use to create signature

        Returns:
            Same :py:class:`P2PKHScriptSig` instance, now signed.

        Raises:
            ValueError: No transaction linked to this ``ScriptSig``
        """
        # Generate UTXO dynamically
        if utxo is None:
            self._public_key = private_key.to_public()
            utxo = TxOutput(0, self.script_pubkey())

        # Initialize
        super().sign(private_key, utxo, hashtype)
        return self

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2PKH

    def _create_items(self) -> List[ScriptData]:
        """Creates the P2SH scriptSig data items.

        Its the pay script elements with the redeem script pushed to the stack.
        """
        return super()._create_items()

    def _create_witness_items(self) -> List[bytes]:
        """Creates the P2SH ScriptSig witness items."""
        return []

    def script_pubkey(self) -> "P2PKHScriptPubKey":
        """Returns a matching :py:class:`P2PKHScriptPubKey`."""
        if self._public_key is None:
            raise ValueError("Sign first to know matching script pubkey.")

        return P2PKHScriptPubKey.from_public_key(self._public_key)


class P2PKHScriptPubKey(StdP2PKHScriptPubKey,
                        metaclass=StdScriptPubKeyRegistry):
    """Legacy ``P2PKH`` ``ScriptPubKey``"""

    # Constructors
    @classmethod
    def from_public_key(cls, public_key: ECDSAPublicKey) \
            -> "P2PKHScriptPubKey":
        return cls(cls.hash_public_key(public_key))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2PKH

    @classmethod
    def _create_items(cls, public_key_hash: bytes) -> List[ScriptField]:
        """Returns the legacy ``P2PKH`` script items given the public key hash.

        Args:
            public_key_hash: Public key hash to put in the script items.
        """
        return [OP_DUP(), OP_HASH160(), ScriptData(public_key_hash),
                OP_EQUALVERIFY(), OP_CHECKSIG()]

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard legacy ``P2PKH`` ``ScriptPubKey``.

        Raises:
            ValueError: Format of the ``ScriptPubKey`` does not match a legacy
                        ``P2PKH`` (not the same opcodes and data)
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Deserialize items
        given_items = Script.deserialize(data).items

        # Create a null script
        real_items = cls._create_items(bytes(PKH_SIZE))

        # Compare them
        try:
            cls._alike(real_items, given_items)
        except ValueError as e:
            raise ValueError("Not a P2PKH ScriptPubKey to deserialize: " +
                             str(e))

        # Extract public key hash
        return cls(given_items[2].value)
