"""Nested ``P2WPKH`` in a ``P2SH`` ``ScriptPubKey`` and ``ScriptSig``."""
# Libraries
from typing import List, Tuple  # noqa: F401

# Relative imports
from bitcoin.script.std.sig import StdScriptSigRegistry
from .p2sh import P2SHScriptPubKey, P2SHScriptSig
from .p2wpkh import P2WPKHScriptPubKey, StdP2PKHScriptSig
from .pubkey import ECDSAPublicKey, StdP2PKHScriptPubKey, \
    StdScriptPubKeyRegistry
from ...field import ScriptData, ScriptField
from ...standards import PaymentStandard, NP2WPKH


class NP2WPKHScriptSig(StdP2PKHScriptSig, metaclass=StdScriptSigRegistry):
    """Nested ``P2WPKH`` in a ``P2SH`` ``ScriptSig``.

    Allows to spend funds sent to a :py:class:`P2WPKHScriptPubKey`, by
    revealing the public key whose hash is the one placed in the
    :py:class:`P2WPKHScriptPubKey` and placing a signature made with its
    associated private key.

    The public key and signature are placed in the
    :py:class:`NP2WPKHScriptSig.witness` and the *P2SH* *redeem script* is
    placed in the script's :py:attr:`NP2WPKHScriptSig.data`.

    The *redeem script* is the native :py:class:`bitcoin.P2WPKHScriptPubKey`.
    """

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return NP2WPKH

    def _create_items(self) -> List[ScriptData]:
        """Creates the P2SH scriptSig data items.

        Its the pay script elements with the redeem script pushed to the stack.
        """
        redeem_script = P2WPKHScriptPubKey(
            self.script_pubkey().public_key_hash)
        unlock_script = P2SHScriptSig(redeem_script)
        return unlock_script.items  # type: ignore

    def _create_witness_items(self) -> List[bytes]:
        """Creates the P2SH ScriptSig witness items."""
        return super()._create_witness_items()

    def _store_items(self) -> None:
        """Stores the P2WPKH witness and the P2SH unlocking script."""
        # Store witness
        super()._store_items()

        # MyPy ensure public key is not None
        if self._public_key is None:
            return  # pragma: no cover

        # Redeem script
        script = P2WPKHScriptPubKey.from_public_key(self._public_key)
        # Store unlocking P2SH
        self._items[:] = P2SHScriptSig(script).items

    def script_pubkey(self) -> "NP2WPKHScriptPubKey":
        """Returns a matching :py:class:`NP2WPKHScriptPubKey`."""
        if self._public_key is None:
            raise ValueError("Sign first to know matching script pubkey.")
        return NP2WPKHScriptPubKey.from_public_key(self._public_key)

    def dummy_sign(self) -> bytes:
        """
        20-bytes program hash
        1-byte push-data program hash
        1-byte OP_0
        1-byte push-data OP_0
        --> TOTAL = 23 bytes.
        """
        return bytes(23)


class NP2WPKHScriptPubKey(StdP2PKHScriptPubKey,
                          metaclass=StdScriptPubKeyRegistry):
    """Nested ``P2WPKH`` in a ``P2SH`` ``ScriptPubKey``."""
    __slots__ = ()  # type: Tuple

    # Constructors
    @classmethod
    def from_public_key(cls, public_key: ECDSAPublicKey) -> \
            "NP2WPKHScriptPubKey":
        """Builds a P2PKHScriptPubKey given a *public key*."""
        # Uncompressed public keys are not valid in SegWit
        if not public_key.is_compressed_by_default:
            raise ValueError("The public key must have default compressed "
                             "serialization")
        return cls(cls.hash_public_key(public_key))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return NP2WPKH

    @classmethod
    def _create_items(cls, public_key_hash: bytes) -> List[ScriptField]:
        """Creates the items of the script given the public key hash."""
        return P2SHScriptPubKey._create_items(
            P2SHScriptPubKey.hash_script(
                P2WPKHScriptPubKey(public_key_hash)))

    @property
    def data(self) -> bytes:
        """Data this ``ScriptPubKey`` pays to."""
        return P2SHScriptPubKey.from_script(
            P2WPKHScriptPubKey(self.public_key_hash)).script_hash

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard nested ``P2SH-P2WPKH`` ``ScriptPubKey``.

        Warning:
           Because it is impossible to distinguish a ``P2SH-P2WPKH`` from a
           regular ``P2SH`` because they are :py:meth:`bitcoin.Script.alike`,
           a ``P2SH-P2WPKH`` will not be able to be deserialized.

           Use :py:meth:`bitcoin.script.std.p2sh.P2SHScriptPubKey.deserialize`
           instead.

        Raises:
            ValueError: As it cannot be distinguished from a regular ``P2SH``
        """
        raise ValueError("A P2SH-P2WPKH ScriptPubKey cannot be deserialized "
                         "as it is alike to a regular P2SH. Use P2SH "
                         "deserialization instead.")
