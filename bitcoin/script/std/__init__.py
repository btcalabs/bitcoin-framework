"""Standard ``ScriptPubKey`` & ``ScriptSig`` module.

Implements standard P2SH and P2PKH ``ScriptPubKey`` and ``ScriptSig`` pairs,
with different serializations:

- **Legacy standard scripts**

  - :py:mod:`bitcoin.script.std.p2pkh`
  - :py:mod:`bitcoin.script.std.p2sh`

- **SegWit standard scripts (native witness)**

  - :py:mod:`bitcoin.script.std.p2wpkh`
  - :py:mod:`bitcoin.script.std.p2wsh`

- **Nested SegWit standard scripts (Legacy P2sh + witness)**

  - :py:mod:`bitcoin.script.std.np2wpkh`
  - :py:mod:`bitcoin.script.std.np2wsh`

**Sources:**

- https://bitcoincore.org/en/segwit_wallet_dev/
"""
# Relative imports
from .errors import StdPKHLengthError, StdSHLengthError
from .pubkey import StdScriptPubKey, StdP2PKHScriptPubKey, \
    StdP2SHScriptPubKey, StdSegWitScriptPubKey, WITNESS_VERSION_DEFAULT, \
    StdSegWitScriptPubKeyVersionError, StdSegWitScriptPubKeyProgramError
from .model import StdPaymentScript
from .np2wpkh import NP2WPKHScriptPubKey, NP2WPKHScriptSig
from .np2wsh import NP2WSHScriptPubKey, NP2WSHScriptSig
from .nulldata import NullDataScriptPubKey
from .p2pkh import P2PKHScriptPubKey, P2PKHScriptSig
from .p2sh import P2SHScriptPubKey, P2SHScriptSig
from .p2wpkh import P2WPKHScriptPubKey, P2WPKHScriptSig
from .p2wsh import P2WSHScriptPubKey, P2WSHScriptSig
from .sig import StdScriptSig, StdP2PKHScriptSig, StdP2SHScriptSig

# Exports
__all__ = [
    "StdPKHLengthError", "StdSHLengthError", "StdScriptPubKey",
    "StdP2PKHScriptPubKey", "StdP2SHScriptPubKey",
    "StdSegWitScriptPubKey", "WITNESS_VERSION_DEFAULT",
    "StdSegWitScriptPubKeyVersionError",
    "StdSegWitScriptPubKeyProgramError", "StdPaymentScript",
    "NP2WPKHScriptPubKey", "NP2WPKHScriptSig", "NP2WSHScriptPubKey",
    "NP2WSHScriptSig", "P2SHScriptPubKey", "P2SHScriptSig",
    "P2PKHScriptPubKey", "P2PKHScriptSig", "P2WPKHScriptPubKey",
    "P2WPKHScriptSig", "P2WSHScriptPubKey", "P2WSHScriptSig",
    "StdScriptSig", "StdP2PKHScriptSig", "StdP2SHScriptSig",
    "NullDataScriptPubKey"
]
