"""Standard ``ScriptSig`` definitions."""
# Libraries
from abc import ABCMeta, abstractmethod
from typing import List, Optional, Type
from typing import Sequence, MutableMapping  # noqa: F401

# Relative imports
from .model import StdPaymentScript
from .pubkey import StdScriptPubKey
from ..model import Script, ScriptSig
from ...crypto import ECDSAPrivateKey, ECDSAPublicKey, ECDSASignature
from ...field import ScriptData
from ...standards import StdPaymentTypes, PaymentStandard
from ...tx import DEFAULT_HASHTYPE, HashType, LegacySignatureData, \
    SegWitSignatureData, SignatureData, TxOutput


class StdScriptSig(ScriptSig, StdPaymentScript, metaclass=ABCMeta):
    """Defines a base to use for all standard :py:class:`ScriptSig`."""

    # Common methods
    def _store_items(self) -> None:
        """Stores the P2SH ScriptSig items and witness items."""
        self._items[:] = self._create_items()
        self._witness.items[:] = self._create_witness_items()

    @classmethod
    def signature_data_creator(cls) -> Type[SignatureData]:
        """Returns the class responsible of creating the signature messsage."""
        return SegWitSignatureData if cls.get_payment_standard().uses_segwit \
            else LegacySignatureData

    # Template specifics
    @abstractmethod
    def _create_items(self) -> List[ScriptData]:
        """Delegate creating the script items into specific implementations.

        This method creates the script data so subclasses can behave exactly
        like the model but having different scripts.
        """
        pass

    @abstractmethod
    def _create_witness_items(self) -> List[bytes]:
        """Delegates filling the witness into specific implementations."""
        pass

    @abstractmethod
    def script_pubkey(self) -> StdScriptPubKey:
        """Returns a standard ``ScriptPubKey`` matching object."""
        pass

    @abstractmethod
    def dummy_sign(self) -> bytes:
        """Returns as bytes as the signed script would occupy."""
        pass


class StdP2PKHScriptSig(StdScriptSig, metaclass=ABCMeta):
    """Pay to a public key hash ``ScriptSig``.

    Creates an empty pay to public key hash script. It can be filled later
    using the :py:meth:`StdPKHScriptSig.sign` method, that will automatically
    place the public key and the signature given the private key to calculate
    the signature.

    **Sources**:

    - https://en.bitcoin.it/wiki/Transaction#Pay-to-PubkeyHash

    Attributes:
        _signature (ECDSASignature): ECDSA signature to authorize the spend.
        _public_key (ECDSAPublicKey): Public key whose private key generated
                                      the signature
        _hashtype (HashTypes):
    """
    __slots__ = "_signature", "_hashtype", "_public_key"

    # Constructors
    def __init__(self) -> None:
        """Initializes an empty P2PKH ``ScriptSig``."""
        # Initialize
        super().__init__()

        # Save empty attributes
        self._signature = None  # type: Optional[ECDSASignature]
        self._hashtype = None  # type: Optional[HashType]
        self._public_key = None  # type: Optional[ECDSAPublicKey]

    # Methods
    def fill(self, signature: ECDSASignature, hashtype: HashType,
             public_key: ECDSAPublicKey) -> "StdP2PKHScriptSig":
        """Puts signature, hashtype and public key into the ``ScriptSig``."""
        # Type assertions
        assert isinstance(signature, ECDSASignature)
        assert isinstance(hashtype, HashType)
        assert isinstance(public_key, ECDSAPublicKey)

        # Store
        self._signature = signature
        self._hashtype = hashtype
        self._public_key = public_key

        # Store script and witness
        self._store_items()

        return self

    def sign(self, private_key: ECDSAPrivateKey, utxo: TxOutput,
             hashtype: HashType = DEFAULT_HASHTYPE) -> "StdP2PKHScriptSig":
        """Places a signature in the script given a secret key.

        Creates a signature with the private key given to make spendable this
        :py:class:`StdPKHScriptSig`.

        .. attention:
            Before using this method, check that this
            :py:class:`StdPKHScriptSig` has been assigned to a
            :py:class:`bitcoin.TxInput` and that input has been assigned to a
            transaction.

            Otherwise, we don't have data to create the signature as this
            data comes from the transaction this :py:class:`StdPKHScriptSig`
            belongs to.

        Warning:
            Some exceptions may be risen by :py:class:`bitcoin.SignatureData`

            In order to sign, make sure the :py:class:`bitcoin.TxOutput` is
            filled with the required data, that depends if using legacy or
            SegWit P2PKHs because of the :py:class:`bitcoin.SignatureData`
            used to create the signable message.

        Note:
            If no :py:paramref:`StdPKHScriptSig.sign.utxo` is provided,
            the :py:paramref:`StdPKHScriptSig.sign.utxo` will be
            automatically calculated using the private key, but beware that
            then it cannot be checked if the private key used to sign is the
            proper one to spend that :py:paramref:`P2PKHScriptSig.sign.utxo`
            as it is being created dynamically.

        Args:
            private_key: Private ECDSA key to create the signature
            utxo: Previous unspent transaction output being spent.
                  Depending on the type, it must be fully or partially filled.
                  It may be omitted and the
            hashtype: Hashtype to use to create signature

        Returns:
            the same :py:class:`StdPKHScriptSig`, now signed.

        Raises:
            ValueError: no transaction linked to this ``ScriptSig``
        """
        # Type assertions
        assert isinstance(private_key, ECDSAPrivateKey)
        assert isinstance(utxo, TxOutput) or utxo is None
        assert isinstance(hashtype, HashType)

        # Assert can be signed: a transaction must be linked
        if self._parent is None or self._parent._tx is None:
            raise ValueError("This StdPKHScriptSig has no related "
                             "transaction, therefore it cannot be signed.")

        # Retrieve public key
        public_key = private_key.to_public()

        # Check if UTXO matches private key
        if utxo.script != self.script_pubkey():
            raise ValueError("Private key specified cannot spend the UTXO "
                             "passed.")

        # Create signable message
        tx_input = self._parent
        tx_input_index = tx_input._tx.inputs.index(tx_input)
        signature_data = self.signature_data_creator()(
            tx_input._tx, tx_input_index, utxo, hashtype)

        # Create signature
        signature = private_key.sign(signature_data.signable_msg())

        # Store them
        self.fill(signature, hashtype, public_key)

        return self

    # Template specifics
    @abstractmethod
    def _create_items(self) -> List[ScriptData]:
        """Creates the script items.

        Raises:
            ValueError: Not signed yet.
        """
        if self._signature is None or self._hashtype is None or \
                self._public_key is None:
            raise ValueError("Not been filled yet.")

        return [
            ScriptData(
                self._signature.serialize() + self._hashtype.serialize(True)),
            ScriptData(self._public_key.serialize())]

    @abstractmethod
    def _create_witness_items(self) -> List[bytes]:
        """Creates the witness items.

        Raises:
            ValueError: Not signed yet.
        """
        if self._signature is None or self._hashtype is None or \
                self._public_key is None:
            raise ValueError("Not been filled yet.")

        return [
            self._signature.serialize() + self._hashtype.serialize(True),
            self._public_key.serialize()]

    # Properties
    @property
    def signature(self) -> Optional[ECDSASignature]:
        """Returns the signature if it has been signed or ``None``."""
        return self._signature

    @property
    def public_key(self) -> Optional[ECDSAPublicKey]:
        """Returns the public key if has been signed or ``None``."""
        return self._public_key

    @property
    def hashtype(self) -> Optional[HashType]:
        """Returns the hashtype used to sign or None if has not been signed."""
        return self._hashtype

    @property
    def is_signed(self) -> bool:
        """Returns ``True`` if has been signed."""
        return self._signature is not None

    def dummy_sign(self) -> bytes:
        """
        ~71 bytes of ECDSASignature
         33 bytes of ECDSAPublicKey
         02 bytes of push data for each element
        """
        return bytes(106)


class StdP2SHScriptSig(StdScriptSig, metaclass=ABCMeta):
    """Pay to a script hash ``ScriptSig``.

    Stores the redeem script it's being payed to, and the optional script
    containing the data the script needs to be spent (``pay_script``).

    Base for the explicit implementations:

    - :py:class:`bitcoin.P2SHScriptSig`
    - :py:class:`bitcoin.P2WSHScriptSig`
    - :py:class:`bitcoin.NP2WSHScriptSig.

    Attributes:
        _pay_items (List[bytes]):
            Data to redeem the specified redeem script
        _redeem_script (Script): Redeemed script
    """
    __slots__ = "_pay_items", "_redeem_script"

    # Constructors
    def __init__(self, redeem_script: Script, pay_items: List[bytes] = None) \
            -> None:
        """Initializes a pay-to-script-hash ``ScriptSig``.

        Args:
            redeem_script: Redeem script to unlock the ``P2SH``.
            pay_items: (Optional)
                        Data items needed by the redeem script to be
                        executed successfully.
        Raises:
            ValueError: Redeem script cannot be empty.
        """
        # Pay script is empty by default
        pay_items = pay_items if pay_items is not None else list()

        # Type assertions
        assert isinstance(redeem_script, Script)
        assert isinstance(pay_items, list) and \
            all(isinstance(i, bytes) for i in pay_items)

        # Redeem script must not be empty
        if len(redeem_script) == 0:
            raise ValueError("Redeem script cannot be empty.")

        # Stores the pay and redeem scripts
        self._pay_items = pay_items
        self._redeem_script = redeem_script

        # Initialize
        super().__init__(None, None)

        # Store
        self._store_items()

    # Template specifics
    @abstractmethod
    def _create_items(self) -> List[ScriptData]:
        """Creates the P2SH scriptSig data items.

        Its the pay script elements with the redeem script pushed to the stack.
        """
        return [ScriptData(i) for i in self._pay_items] + [
            ScriptData(self._redeem_script.serialize())]

    @abstractmethod
    def _create_witness_items(self) -> List[bytes]:
        """Creates the P2SH ScriptSig witness items."""
        return self.pay_items + [self._redeem_script.serialize()]

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes) -> "StdP2SHScriptSig":
        """Deserializes a standard P2SH ``ScriptSig``

        Detects the last :py:class:`bitcoin.ScriptData` as the redeem script
        and the rest as the pay script.

        Raises:
            ValueError: No redeem script found as the last item is not an
                        :py:class:`bitcoin.ScriptData`
        """
        # Deserialize as a normal script
        script = ScriptSig.deserialize(data)

        # Check is standard ScriptSig
        if not script.is_standard:
            raise ValueError("Not a standard ScriptSig. Just data allowed.")

        # Retrieve pay items
        pay_items = [item.serialized_data if isinstance(item, ScriptData)
                     else item.serialize() for item in script.items[:-1]]

        # Retrieve the redeem script (last ScriptData)
        last_item = script.items[-1]
        if isinstance(last_item, ScriptData):
            redeem_script = Script.deserialize(last_item.data)
        else:
            raise ValueError("Last item of the StdP2SHScriptSig is not data"
                             "so the redeem script cannot be detected.")

        return cls(redeem_script, pay_items)

    # Properties
    @property
    def redeem_script(self) -> Script:
        """Returns the redeem script matching the P2SH."""
        return self._redeem_script

    @property
    def pay_items(self) -> List[bytes]:
        """Returns the data needed to execute the redeem script."""
        return self._pay_items

    # Magics
    def __repr__(self) -> str:
        """Representation string using its pay data and redeem script."""
        return "{}({})".format(self.__class__.__name__,
                               repr(self._redeem_script)) \
            if len(self._pay_items) == 0 else \
            "{}({}, {})".format(self.__class__.__name__,
                                repr(self._redeem_script),
                                repr(self._pay_items))


class StdScriptSigRegistry(ABCMeta):
    """Registers standard ScriptSig availables."""

    _STD_SCRIPTSIG_REGISTRY = []  # type: Sequence[StdScriptSig]
    """
        Iterable[StdScriptPubKey]: \
            All standard ``ScriptPubKey`` implementations.
    """
    _STD_SCRIPTSIG_TYPE_MAP = {
        StdPaymentTypes.PKH.name: StdP2PKHScriptSig,
        StdPaymentTypes.SH.name: StdP2SHScriptSig
    }
    """
        Mapping[PaymentType, Type[StdScriptSig]]: \
            Map a standard payment type to its interface
    """
    _STD_SCRIPTSIG_MAP = \
        dict()  # type: MutableMapping[str, Type[StdScriptSig]]
    """
        Mapping[PaymentStandard, Type[StdScriptSig]]: \
            Map a standard script pubkey to its standard.
    """

    def __new__(mcs, name, bases, namespace, **kwargs):
        """Creates the class and registers it."""
        # Create the class
        cls = super().__new__(mcs, name, bases, namespace, **kwargs)

        # Append to registries
        # # Global registry
        cls._STD_SCRIPTSIG_REGISTRY.append(cls)
        # # Standard registry
        mcs.register_standard_scriptsig(cls)
        return cls

    @classmethod
    def register_standard_scriptsig(
            mcs, script_sig_class: Type[StdScriptSig]) -> None:
        """Registers the given script pubkey class."""
        # Variables
        payment_standard = script_sig_class.get_payment_standard()
        # Assert proper interface
        script_sig_interface = \
            mcs._STD_SCRIPTSIG_TYPE_MAP[payment_standard.payment_type.name]
        if not issubclass(script_sig_class, script_sig_interface):
            raise ValueError("Proper ScriptSig payment type interface not "
                             "implemented")
        # Assert does not exist
        if str(payment_standard) in mcs._STD_SCRIPTSIG_MAP:
            raise ValueError(
                "A StdScriptSig has already been registered"
                "for this payment standard")
        # Register
        mcs._STD_SCRIPTSIG_MAP[str(payment_standard)] = script_sig_class

    @classmethod
    def by_standard(mcs, standard: PaymentStandard) -> Type[StdScriptSig]:
        return mcs._STD_SCRIPTSIG_MAP[str(standard)]
