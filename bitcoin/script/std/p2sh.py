"""Legacy ``P2SH`` ``ScriptPubKey`` and ``ScriptSig`ValueError`."""

# Libraries
from typing import List

# Relative imports
from .errors import StdSHLengthError
from .pubkey import StdP2SHScriptPubKey, StdScriptPubKeyRegistry
from .sig import StdP2SHScriptSig, StdScriptSigRegistry
from ...crypto import ripemd160_sha256
from ...field import OP_EQUAL, OP_HASH160, ScriptData
from ...field.model import ScriptField
from ...params import SH_SIZE_LEGACY
from ...standards import PaymentStandard, P2SH
from ...script import Script


class P2SHScriptSig(StdP2SHScriptSig, metaclass=StdScriptSigRegistry):
    """Legacy ``P2SH`` ``ScriptSig``.

    Note:
       Behaves exactly as the :py:class:`bitcoin.StdSHScriptSig`
    """
    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2SH

    def _create_items(self) -> List[ScriptData]:
        """Creates the items to store in the witness."""
        return super()._create_items()

    def _create_witness_items(self) -> List[bytes]:
        """Creates the witness items of the P2SH ScriptSig."""
        return []

    # Properties
    def script_pubkey(self) -> "P2SHScriptPubKey":
        """Returns a matching :py:class:`P2SHScriptPubKey`."""
        return P2SHScriptPubKey.from_script(self._redeem_script)

    def dummy_sign(self) -> bytes:
        # TODO: real estimation ?
        return bytes()


class P2SHScriptPubKey(StdP2SHScriptPubKey,
                       metaclass=StdScriptPubKeyRegistry):
    """Legacy ``P2SH`` ``ScriptPubKey``."""

    # Constructors
    def __init__(self, script_hash: bytes) -> None:
        """Initializes a ``P2SH`` ``ScriptPubKey``.

        Checks the given *script's hash* size.

        Args:
            script_hash: *Redeem script*'s hash to pay to.

        Raises:
            StdSHLengthError: *Redeem script*'s hash size is invalid
        """
        # Type assertions
        assert isinstance(script_hash, bytes)

        # Checks
        if len(script_hash) != SH_SIZE_LEGACY:
            raise StdSHLengthError

        # Initialize
        super().__init__(script_hash)

    @classmethod
    def from_script(cls, script: Script) -> "P2SHScriptPubKey":
        return cls(cls.hash_script(script))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2SH

    @classmethod
    def _create_items(cls, script_hash: bytes) -> List[ScriptField]:
        """Returns the ``P2SH`` script items given a *redeem script*'s hash."""
        return [OP_HASH160(), ScriptData(script_hash), OP_EQUAL()]

    @classmethod
    def hash_script(cls, script: Script) -> bytes:
        """Returns the hash of a *redeem script* to be used in a P2SHSP."""
        return ripemd160_sha256(script.serialize())

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard legacy ``P2SH`` ``ScriptPubKey``.

        Raises:
            ValueError: Format of the ``ScriptPubKey`` does not match a legacy
                        ``P2SH`` (not the same opcodes and data)
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Deserialize items
        given_items = Script.deserialize(data).items

        # Create a null script
        real_items = cls._create_items(bytes(SH_SIZE_LEGACY))

        # Compare them
        try:
            cls._alike(real_items, given_items)
        except ValueError as e:
            raise ValueError("Not a P2SH ScriptPubKey to deserialize: " +
                             str(e))

        # Extract script hash
        return cls(given_items[1].value)
