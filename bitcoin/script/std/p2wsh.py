"""Native ``P2WSH`` ``ScriptPubKey`` and ``ScriptSig``."""
# Libraries
from typing import List

# Relative imports
from .errors import StdSHLengthError
from .pubkey import StdP2SHScriptPubKey, StdSegWitScriptPubKey, \
    WITNESS_VERSION_DEFAULT, StdScriptPubKeyRegistry
from .sig import StdP2SHScriptSig, StdScriptSigRegistry
from ...field.model import ScriptField
from ...field.script import ScriptData
from ..model import Script
from ...crypto import sha256
from ...params import SH_SIZE_SEGWIT
from ...standards import PaymentStandard, P2WSH


class P2WSHScriptSig(StdP2SHScriptSig, metaclass=StdScriptSigRegistry):
    """Native ``P2WSH`` ``ScriptSig``."""

    # Constructors
    def __init__(self, redeem_script, pay_script: List[bytes] = None) -> None:
        """Initializes a native ``P2WSH`` ``ScriptSig``.

        Args:
            redeem_script: Redeem script to unlock the ``P2SH``.
            pay_script: (Optional)
                        Data needed by the redeem script to be
                        executed, if any. Otherwise it will be an empty script.
        """
        super().__init__(redeem_script, pay_script)

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2WSH

    def _create_items(self) -> List[ScriptData]:
        """Creates the items to store in the witness."""
        return []

    def _create_witness_items(self) -> List[bytes]:
        """Creates the witness items of the P2SH ScriptSig."""
        return super()._create_witness_items()

    def script_pubkey(self) -> StdP2SHScriptPubKey:
        """Returns a matching :py:class:`P2WSHScriptPubKey`."""
        return P2WSHScriptPubKey.from_script(self._redeem_script)

    def dummy_sign(self) -> bytes:
        # TODO: real estimation ?
        return bytes()


class P2WSHScriptPubKey(StdP2SHScriptPubKey,
                        metaclass=StdScriptPubKeyRegistry):
    """Native ``P2WSH`` ``ScriptPubKey``."""
    __slots__ = "_segwit_script"

    # Constructors
    def __init__(self, script_hash: bytes) -> None:
        """Initializes a ``P2WSH``.

        Checks the given *redeem script*'s hash has a valid size.

        Args:
            script_hash: *Redeem script*'s hash to pay to.

        Raises:
            StdSHLengthError: *Script's hash* size is invalid.
        """
        # Checks
        if len(script_hash) != SH_SIZE_SEGWIT:
            raise StdSHLengthError

        # Initialize
        StdP2SHScriptPubKey.__init__(self, script_hash)

        self._segwit_script = StdSegWitScriptPubKey(script_hash, 0)

    @classmethod
    def from_script(cls, script: Script,
                    version: int = WITNESS_VERSION_DEFAULT) -> \
            "P2WSHScriptPubKey":
        """Builds a P2SHScriptPubKey given a *redeem script*."""
        return cls(cls.hash_script(script))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2WSH

    @classmethod
    def hash_script(cls, script: Script) -> bytes:
        """Computes the hash of a *redeem script* for a ``P2WSH``."""
        return sha256(script.serialize())

    @classmethod
    def _create_items(cls, script_hash: bytes) -> List[ScriptField]:
        """Creates items with the help of standard SegWit ``ScriptPubKey``."""
        return StdSegWitScriptPubKey(script_hash).create_items()

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard legacy ``P2WSH`` ``ScriptPubKey``.

        Raises:
            ValueError: Format of the ``ScriptPubKey`` does not match a legacy
                        ``P2WSH`` (not the same opcodes and data)
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Deserialize items
        given_items = Script.deserialize(data).items

        # Create a null script
        real_items = cls._create_items(bytes(SH_SIZE_SEGWIT))

        # Compare them
        try:
            cls._alike(real_items, given_items)
        except ValueError as e:
            raise ValueError("Not a P2WSH ScriptPubKey to deserialize: " +
                             str(e))

        # Extract redeem script hash
        return cls(given_items[1].value)

    # Properties
    @property
    def version(self) -> int:
        return self._segwit_script.version

    @property
    def program(self) -> bytes:
        return self._segwit_script.program
