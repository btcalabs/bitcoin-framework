"""Exceptions that can occur around the package."""


class StdSHLengthError(ValueError):
    """*Redeem script*'s hash length in a ``P2SH`` is not valid."""
    pass


class StdPKHLengthError(ValueError):
    """Public key's hash length in a ``P2PKH`` is not valid."""
    pass
