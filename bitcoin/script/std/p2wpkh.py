"""Native ``P2WPKH`` ``ScriptPubKey`` and ``ScriptSig``."""
# Libraries
from typing import List

# Relative imports
from .pubkey import StdP2PKHScriptPubKey, StdSegWitScriptPubKey, \
    StdScriptPubKeyRegistry
from .sig import StdP2PKHScriptSig, StdScriptSigRegistry
from ..model import Script
from ...crypto import ECDSAPrivateKey, ECDSAPublicKey
from ...field import ScriptData, ScriptField
from ...params import PKH_SIZE
from ...standards import PaymentStandard, P2WPKH
from ...tx import DEFAULT_HASHTYPE, HashType, TxOutput


class P2WPKHScriptSig(StdP2PKHScriptSig, metaclass=StdScriptSigRegistry):
    """Native ``P2WPKK`` ``ScriptSig``.

    Allows to spend funds sent to a :py:class:`P2WPKHScriptPubKey`, by
    revealing the public key whose hash is the one placed in the
    :py:class:`P2WPKHScriptPubKey` and placing a signature made with its
    associated private key.

    As it uses *SegWit*, all data is placed in the
    :py:attr:`P2WPKHScriptSig.witness`.
    """

    def sign(self, private_key: ECDSAPrivateKey, utxo: TxOutput,
             hashtype: HashType = DEFAULT_HASHTYPE) -> "P2WPKHScriptSig":
        """Places a signature in the script's witness given a secret key.

        .. attention::
            Before using this method, check that this
            :py:class:`P2WPKHScriptSig` object has been assigned to a
            :py:class:`bitcoin.TxInput` and that input has been assigned to a
            transaction (:py:class:`bitcoin.Tx`).

            Otherwise, data to create the signature will be missing as this
            data comes from the transaction this :py:class:`P2WPKHScriptSig`
            belongs to.

        Warning:
            Make sure the :py:class:`bitcoin.TxOutput`
            :py:paramref:`P2WPKHScriptSig.sign.utxo` argument which makes
            reference to the unspent transaction output being spent
            contains the proper :py:class:`P2WPKHScriptPubKey` **and also the
            UTXO's exact value as the signature also signs that value**.

            Check :py:class:`bitcoin.SegWitSignatureData` for more
            information about how the message and its hash to be signed is
            created.

        Args:
            private_key: Private ECDSA key to create the signature
            utxo: Previous unspent transaction output being spent.
            hashtype: Hashtype to use to create signature

        Returns:
            The same :py:class:`P2WPKHScriptSig`, now signed.

        Raises:
            ValueError: no transaction linked to this ``ScriptSig``
        """
        super().sign(private_key, utxo, hashtype)
        return self

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2WPKH

    def _create_items(self) -> List[ScriptData]:
        """Creates the P2SH scriptSig data items.

        Its the pay script elements with the redeem script pushed to the stack.
        """
        return []

    def _create_witness_items(self) -> List[bytes]:
        """Creates the P2SH ScriptSig witness items."""
        return super()._create_witness_items()

    def script_pubkey(self) -> "P2WPKHScriptPubKey":
        """Returns a matching :py:class:`P2WPKHScriptPubKey`."""
        if self._public_key is None:
            raise ValueError("Sign first to know matching ScriptPubKey.")

        return P2WPKHScriptPubKey.from_public_key(self._public_key)


class P2WPKHScriptPubKey(StdP2PKHScriptPubKey,
                         metaclass=StdScriptPubKeyRegistry):
    """Native ``P2WPKH`` ``ScriptPubKey``."""
    __slots__ = "_segwit_script",

    # Constructors
    def __init__(self, public_key_hash: bytes) -> None:
        """Initializes a ``P2WPKH`` ``ScriptPubKey``."""
        StdP2PKHScriptPubKey.__init__(self, public_key_hash)
        self._segwit_script = StdSegWitScriptPubKey(public_key_hash, 0)

    @classmethod
    def from_public_key(cls, public_key: ECDSAPublicKey,
                        tx_output=None) -> "P2WPKHScriptPubKey":
        """Builds a P2PKHScriptPubKey given a *public key*.

        Raises:
            ValueError: The public key is not compressed.
        """
        # Uncompressed public keys are not valid in SegWit
        if not public_key.is_compressed_by_default:
            raise ValueError("The public key must have default compressed "
                             "serialization")
        return cls(cls.hash_public_key(public_key))

    # Template specifics
    @classmethod
    def get_payment_standard(cls) -> PaymentStandard:
        return P2WPKH

    @classmethod
    def _create_items(cls, public_key_hash: bytes) -> List[ScriptField]:
        """Creates items with the help of standard SegWit ``ScriptPubKey``."""
        return StdSegWitScriptPubKey(public_key_hash).create_items()

    # Serialize / deserialize
    @classmethod
    def deserialize(cls, data: bytes):
        """Deserializes a standard native ``P2WPKH`` ``ScriptPubKey``.

        Raises:
            ValueError: Format of the ``ScriptPubKey`` does not match a native
                        ``P2WPKH`` (not the same opcodes and data)
        """
        # Type assertions
        assert isinstance(data, bytes)

        # Deserialize items
        given_items = Script.deserialize(data).items

        # Create a null script
        real_items = cls._create_items(bytes(PKH_SIZE))

        # Compare them
        try:
            cls._alike(real_items, given_items)
        except ValueError as e:
            raise ValueError("Not a P2WPKH ScriptPubKey to deserialize: " +
                             str(e))

        # Extract public key hash
        return cls(given_items[1].value)

    # Properties
    @property
    def version(self) -> int:
        return self._segwit_script.version

    @property
    def program(self) -> bytes:
        return self._segwit_script.program
