"""
The aim of this class is to provide an abstract contract in order to
automatize the creation of scripts that will be part of a transaction.
"""

# Libraries
from enum import Enum, unique
from typing import Union

# Relative imports
from ..address import P2SHAddress, NP2WSHAddress, P2WSHAddress
from ..script import Script, P2SHScriptPubKey, NP2WSHScriptPubKey, \
    P2WSHScriptPubKey, P2SHScriptSig, NP2WSHScriptSig, P2WSHScriptSig


# Enumerations
@unique
class TxTypes(Enum):
    """
    Defines the types a Bitcoin Transaction can be.
    """
    LEGACY = 0
    NESTED = 1
    SEGWIT = 2


class Contract:
    __slots__ = "_redeem_script", "_pay_script"

    def __init__(self, redeem_script: Script, pay_script: Script = None) \
            -> None:
        # Type assertions
        assert isinstance(redeem_script, Script)
        # Assignments
        self._redeem_script = redeem_script
        self._pay_script = pay_script

    def fund_address(self, tx_type: TxTypes) -> \
            Union[P2SHAddress, NP2WSHAddress, P2WSHAddress]:
        # Select class
        if tx_type == TxTypes.LEGACY:
            clazz = P2SHAddress  # type: ignore
        elif tx_type == TxTypes.NESTED:
            clazz = NP2WSHAddress  # type: ignore
        elif tx_type == TxTypes.SEGWIT:
            clazz = P2WSHAddress  # type: ignore

        return clazz.from_redeem_script(self._redeem_script)  # type: ignore

    def fund_script(self, tx_type: TxTypes) -> \
            Union[P2SHScriptPubKey, NP2WSHScriptPubKey, P2WSHScriptPubKey]:
        # Select class
        if tx_type == TxTypes.LEGACY:
            clazz = P2SHScriptPubKey  # type: ignore
        elif tx_type == TxTypes.NESTED:
            clazz = NP2WSHScriptPubKey  # type: ignore
        elif tx_type == TxTypes.SEGWIT:
            clazz = P2WSHScriptPubKey  # type: ignore

        return clazz(self.fund_address(tx_type))  # type: ignore

    def spend_script(self, tx_type: TxTypes) -> \
            Union[P2SHScriptSig, NP2WSHScriptSig, P2WSHScriptSig]:
        if self._pay_script is None:
            raise ValueError("You must fill the payscript first in order to "
                             "spend that contract.")
        # Select class
        if tx_type == TxTypes.LEGACY:
            clazz = P2SHScriptSig  # type: ignore
        elif tx_type == TxTypes.NESTED:
            clazz = NP2WSHScriptSig  # type: ignore
        elif tx_type == TxTypes.SEGWIT:
            clazz = P2WSHScriptSig  # type: ignore

        return clazz(self._redeem_script, self._pay_script)  # type: ignore

    @property
    def redeem_script(self) -> Script:
        return self._redeem_script

    @property
    def pay_script(self) -> Script:
        return self._pay_script  # type: ignore
