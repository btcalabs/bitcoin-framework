"""Provides an interface to create m-of-n multi-signature contracts."""

# Libraries
from typing import List

# Relative imports
from .contract import Contract
from ..crypto import ECDSAPublicKey
from ..field import OP_0, OP_CMS, ScriptData, get_op_code_n
from ..script import Script

# Typing compliant imports
from ..field import ScriptField  # NOQA: F401


class MultisigContract(Contract):
    """Defines the structure for an m-of-n Multisig Contract.

    Attributes:
        _public_keys (List[ECDSAPublicKey]): Public keys that will be placed
                                             in the contract in order to
                                             specify who can spend the funds
                                             locked by the current contract.
        _m (int): Number of the minimum signatures needed to spend.
        _n (int): Total of keys provided.
        _signatures (List[bytes]): ECDSASignatures to spend that contract.
    """

    __slots__ = "_public_keys", "_m", "_n", "_signatures"

    def __init__(self, pubkeys: List[ECDSAPublicKey], m: int = -1) -> None:
        # Type assertions
        assert isinstance(pubkeys, list)
        assert all([isinstance(pubkey, ECDSAPublicKey) for pubkey in pubkeys])
        assert isinstance(m, int)

        # Defaults
        if m == -1:
            m = len(pubkeys)

        # Checks
        if len(pubkeys) == 0:
            raise ValueError("Not enough public keys.")
        if len(pubkeys) > m:
            raise ValueError("The m value has to be <= n.")

        # Assignments
        self._public_keys = pubkeys
        self._m = m
        self._n = len(pubkeys)
        self._signatures = list()  # type: ignore

        # Calculations
        redeem_script_items = list()  # type: List[ScriptField]
        redeem_script_items += \
            [ScriptData(pubkey) for pubkey in self._public_keys]
        redeem_script_items += [get_op_code_n(self._n), OP_CMS()]
        redeem_script = Script(redeem_script_items)

        super().__init__(redeem_script)

    @property
    def signatures(self) -> List[bytes]:
        return self._signatures

    @signatures.setter
    def signatures(self, signatures: List[bytes]) -> None:
        if len(signatures) < self._m:
            raise ValueError("Not enough signatures provided")
        self._signatures = signatures
        script = [OP_0()]  # type: ignore
        script += [ScriptData(sig) for sig in signatures]  # type: ignore
        self._pay_script = script  # type: ignore
