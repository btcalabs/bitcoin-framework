"""Provides an interface to create time-locked contracts."""

# Libraries
import logging

# Relative imports
from .contract import Contract
from ..field import ScriptData, ScriptNum, OP_CLTV, OP_DROP
from ..script import Script

# Constants
LOGGER = logging.getLogger(__name__)
"""
    logger for that file.
"""


class TimeLockedContract(Contract):
    """Locks a smart contract in a time period.

    Models a smart contract that makes a redeem script only spendable after
    a certain time.

    Attributes:
        _locktime (int)
        _post_contract (Contract)
    """

    __slots__ = "_locktime", "_post_contract"

    def __init__(self, locktime: int,
                 post_contract: Contract = Contract(Script())) -> None:
        # Type assertions
        assert isinstance(locktime, int)
        assert isinstance(post_contract, Script)

        # Checks
        if locktime < 0:
            raise ValueError
        # TODO: time format checks (UNIX timestamp or number of blocks)

        # Warnings
        if len(post_contract.count) == 0:
            LOGGER.warning("Anyone could spend that UTXO after the locktime "
                           "expires, be careful.")

        # Assignments
        self._locktime = locktime
        self._post_contract = post_contract

        # Derive redeem script
        base_redeem_script = Script([
            ScriptData(ScriptNum(self._locktime)), OP_CLTV(), OP_DROP()
        ])
        redeem_script = base_redeem_script + post_contract.redeem_script

        super().__init__(redeem_script, post_contract.pay_script)

    def pay_script(self) -> Script:
        return self._post_contract.pay_script  # type: ignore
