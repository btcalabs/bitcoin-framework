"""Provides an interface to create hash-locked contracts.

**Sources:**

- https://en.bitcoin.it/wiki/Hashlock
- https://en.bitcoin.it/wiki/Script#Transaction_puzzle
"""

# Relative imports
from .contract import Contract
from ..crypto import ripemd160, sha1, sha256, ripemd160_sha256, double_sha256
from ..field import OP_RIPEMD160, OP_SHA1, OP_SHA256, OP_HASH160, OP_HASH256, \
    OP_EQUAL, ScriptData
from ..script import Script


hash_functions = {
    # pre-image --> RIPEMD160 --> hash
    'ripemd160': (ripemd160, OP_RIPEMD160),
    # pre-image --> SHA1 --> hash
    'sha1': (sha1, OP_SHA1),
    # pre-image --> OP_SHA256 --> hash
    'sha256': (sha256, OP_SHA256),
    # pre-image --> SHA256 --> RIPEMD160 --> hash
    'hash160': (ripemd160_sha256, OP_HASH160),
    # pre-image --> SHA256 --> SHA256 --> hash
    'hash256': (double_sha256, OP_HASH256)
}


class HashLockedContract(Contract):
    """."""
    __slots__ = "_preimage",

    def __init__(self, preimage: str, hash_func: str = 'hash256') -> None:
        # Type assertions
        assert isinstance(preimage, str)
        assert isinstance(hash_func, str)
        assert hash_func in ['ripemd160', 'sha1', 'sha256', 'hash160',
                             'hash256']

        # Calculations
        preimage_bytes = preimage.encode('utf-8')

        func, opcode = hash_functions[hash_func]
        hashed = func(preimage_bytes)

        # Derive scripts
        redeem_script = Script([
            opcode(), ScriptData(hashed), OP_EQUAL()  # type: ignore
        ])
        pay_script = Script([ScriptData(preimage_bytes)])

        super().__init__(redeem_script, pay_script)
