"""Custom enum type module."""
# Libraries
from enum import Enum


class REnum(Enum):
    """Representable enum.

    Improves python :py:class:`enum.Enum` as the representation can be used
    to create the same enum item when the representation string is evaluated.
    """

    def __repr__(self) -> str:
        """Enumeration item representation.

        Can be evaluated to reproduce the same item.
        """
        return "%s.%s" % (self.__class__.__name__, self.name)
