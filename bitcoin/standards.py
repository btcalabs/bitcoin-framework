"""Payment standards definitions."""
from enum import unique
from typing import Sequence
from typing import MutableMapping  # noqa: F401

from .enum import REnum


@unique
class StdPaymentTypes(REnum):
    """Types of standard payment ``ScriptPubKey`` and ``ScriptSig``.

    Note:
        The values of the enumeration items do not mean anything. We have
        assigned them starting by 0 and in order of appearance of the
        payment type.
    """
    PK = 0
    PKH = 1
    SH = 2


@unique
class StdPaymentImplementations(REnum):
    """Types of standard payments implementations.

    The same conceptual standard payment type can be implemented using legacy,
    SegWit or nested SegWit. This aims to provide and define which
    implementation is using a standard ``ScriptPubKey`` or ``ScriptSig`` to
    send / spend the funds.

    Note:
        Items values are assigned by order of appearance.
    """
    LEGACY = 0
    NESTED = 1
    SEGWIT = 2


class PaymentStandard:
    """A payment standard is the tuple of a payment type and implementation."""
    __slots__ = "_payment_type", "_payment_implementation"

    # Constructors
    def __init__(self, payment_type: StdPaymentTypes,
                 payment_implementation: StdPaymentImplementations) -> None:
        """Initializes a payment standard given its type and implementation."""
        # Type assertions
        assert isinstance(payment_type, StdPaymentTypes)
        assert isinstance(payment_implementation, StdPaymentImplementations)

        # Assignments
        self._payment_type = payment_type
        self._payment_implementation = payment_implementation

    @property
    def payment_type(self) -> StdPaymentTypes:
        """Returns the payment standard type."""
        return self._payment_type

    @property
    def payment_implementation(self) -> StdPaymentImplementations:
        """Returns the payment standard implementation."""
        return self._payment_implementation

    @property
    def uses_segwit(self) -> bool:
        """Whether the payment standard uses Segregated Witness.

        Either if it is native or nested Segregated Witness.
        """
        return \
            self.payment_implementation == StdPaymentImplementations.SEGWIT \
            or \
            self.payment_implementation == StdPaymentImplementations.NESTED

    def __str__(self) -> str:
        """Name of the payment standard.

        Format of the payment standards:
            (N)P2(W)[TYPE]

        All uppercase. Where:
            (N): N if it is a nested SegWit, empty if it's not.
            (W): If it is a SegWit implementation (either nested or native)
            [TYPE]: PKH or SH depending on the payment type

        For instance:
            NP2WPKH: Pay to a public key hash, via nested SegWit.
            P2WPKH: Pay to a public key  hash, via native SegWit.
        """
        return "%sP2%s%s" % (
            "N" if self.payment_implementation ==
            StdPaymentImplementations.NESTED else "",
            "W" if self.uses_segwit else "",
            self.payment_type.name.upper())

    def __eq__(self, other) -> bool:
        """Compares two payment standards.

        They are the same if have the same payment type and implementation.
        """
        return self.payment_type == other.payment_type and \
            self.payment_implementation == other.payment_implementation \
            if isinstance(other, self.__class__) else \
            super().__eq__(other)

    def __repr__(self) -> str:
        """Representation of the payment standard."""
        return "%s(%s, %s)" % (self.__class__.__name__,
                               repr(self.payment_type),
                               repr(self.payment_implementation))


class KnownPaymentStandard(PaymentStandard):
    """Known payment standard are registered and can only exist one."""

    _STANDARDS_MAP = dict()  # type: MutableMapping[str, PaymentStandard]
    """
        Map[str, KnownPaymentStandard]: Maps each payment standard name
        to its singleton object.
    """

    def __init__(self, payment_type: StdPaymentTypes,
                 payment_implementation: StdPaymentImplementations) -> None:
        """Initializes a known payment standard."""
        # Init
        super().__init__(payment_type, payment_implementation)

        # Validate
        if payment_type == StdPaymentTypes.PK and \
                payment_implementation != StdPaymentImplementations.LEGACY:
            raise ValueError("The pay to public key has just one (legacy) "
                             "implementation")

        # Registration
        name = str(self)
        if name in self._STANDARDS_MAP:
            # Already registered
            raise ValueError("Standard %s has already been registered" % name)
        else:
            self._STANDARDS_MAP[name] = self

    @classmethod
    def from_string(cls, name: str):
        """Returns the payment standard object given it's name."""
        return cls._STANDARDS_MAP[name]

    @classmethod
    def get_payment_standards(cls) -> Sequence[PaymentStandard]:
        """Returns the payment standards registered."""
        return tuple(cls._STANDARDS_MAP.values())

    def __repr__(self) -> str:
        """Returns the payment standard representation.

        Returns the global pointer.
        """
        return str(self)


def get_payment_standard(
        payment_type: StdPaymentTypes,
        payment_implementation: StdPaymentImplementations) -> PaymentStandard:
    """Returns the payment standard given the type and implementation.

    Args:
        payment_type: Payment type of the standard
        payment_implementation: Payment implementation of the standard

    Raises:
        ValueError: Payment type and implementation are not a valid payment
                    standard.

    Returns:
        Matching payment standard for the type and implementation.
    """
    # Find a payment standard with the given arguments
    payment_standards = KnownPaymentStandard.get_payment_standards()
    for payment_standard in payment_standards:
        if payment_standard.payment_type == payment_type \
                and payment_standard.payment_implementation == \
                payment_implementation:
            return payment_standard

    # Nothing found
    raise ValueError(
        "No payment standard has been found for this type and implementation")


# Implemented standards
P2PK = KnownPaymentStandard(
    StdPaymentTypes.PK,
    StdPaymentImplementations.LEGACY)
P2PKH = KnownPaymentStandard(
    StdPaymentTypes.PKH,
    StdPaymentImplementations.LEGACY)
NP2WPKH = KnownPaymentStandard(
    StdPaymentTypes.PKH,
    StdPaymentImplementations.NESTED)
P2WPKH = KnownPaymentStandard(
    StdPaymentTypes.PKH,
    StdPaymentImplementations.SEGWIT)
P2SH = KnownPaymentStandard(
    StdPaymentTypes.SH,
    StdPaymentImplementations.LEGACY)
NP2WSH = KnownPaymentStandard(
    StdPaymentTypes.SH,
    StdPaymentImplementations.NESTED)
P2WSH = KnownPaymentStandard(
    StdPaymentTypes.SH,
    StdPaymentImplementations.SEGWIT)
