"""Defines bitcoin currency units and equivalences.

Provides also conversion methods to switch between units.
"""
# Libraries
from decimal import Decimal, Context
from typing import Union

# Constants
SATOSHIS_IN_A_BITCOIN = 100000000
"""
    int: Amount of satoshis in 1 bitcoin
"""
BITCOINS_MAX_DECIMALS = str(SATOSHIS_IN_A_BITCOIN).count("0")
"""
    int: Decimal places a bitcoin unit must have as maximum due to being
         a satoshi the smallest representable unit.
"""
BITCOINS_MAX = 21000000
"""
    int: Maximum bitcoins that can be converted to satoshis units.
"""
SATOSHIS_MAX = BITCOINS_MAX * SATOSHIS_IN_A_BITCOIN
"""
    int: Maximum satoshis that can be converted to bitcoins units.
"""
BITCOINS_MAX_STRING_LENGTH = \
    len("%d.%s" % (BITCOINS_MAX, "0" * BITCOINS_MAX_DECIMALS))
"""
    int: Length of the maximum bitcoins that can be converted to satoshis,
         expressed as a string in the longest possible format.
"""
UNITS_CONTEXT = Context(Emin=-BITCOINS_MAX_DECIMALS, Emax=0, )
"""
"""
SATOSHIS_IN_A_BIT = 100
"""
    int: Amount of satoshis in 1 bit
"""
BITS_MAX_DECIMALS = str(SATOSHIS_IN_A_BIT).count("0")
"""
    int: Decimal places a bit unit must have as maximum due to being a
    satoshi the smallest representable unit.
"""
SATOSHIS_IN_A_mBITCOIN = 100000
"""
    int: Amount of satoshis in 1 milli-bitcoin
"""
mBITCOINS_MAX_DECIMALS = str(SATOSHIS_IN_A_mBITCOIN).count("0")
"""
    int: Decimal places a milli-bitcoin unit must have as maximum due to being
         a satoshi the smallest representable unit.
"""


# Exceptions
class UnitsPrecisionError(ValueError):
    """Too much precision used in the value. Cannot fragment so much."""
    pass


class UnitsFormatError(ValueError):
    """Format of the units specified is invalid."""
    pass


class UnitsNegativeError(ValueError):
    """Units cannot be negative."""
    pass


# Methods
def btc_to_satoshi(bitcoins: Union[int, str, Decimal, float]) -> int:
    """Converts the given amount of bitcoins to satoshis.

    You can use either an amount of bitcoins as an :py:class:`int`,
    :py:class:`str` or :py:class:`float` and you will be returned the number
    of satoshis as an :py:class:`int`

    .. danger::
        **Avoid using** :py:class:`float` **to represent bitcoin units
        if it's not for learning, non-production purposes.**

        Read the warning below for more information.

    Note:
        **String format**

        Like a Python float literal, use decimal digits with a dot ``.`` as the
        decimal separator. For instance:

        ``0.12345678``

        **Beware**:

        - Do not use more than :py:const:`bitcoin.units.BITCOINS_MAX_DECIMALS`
          decimal places or it will raise an error as satoshis are the
          smallest unit possible and can't be fragmented.

          (1 bitcoin is :py:const:`bitcoin.units.SATOSHIS_IN_A_BITCOIN`
          satoshis)

    Warning:
        When using :py:class:`float`, some bugs may occur as representing
        :py:class:`float` numbers in binary has `issues and limitations`__ so
        the actual satoshis amount obtained may not be exact because of them.

        .. _float-issues: https://docs.python.org/3/tutorial/floatingpoint.html

        __ `float-issues`_

        So when using bitcoins as units, use either an :py:class:`int` if
        no decimals are required or a :py:class:`str` following the format
        specified above to avoid the bugs with floating numbers binary
        representations that may cause bugs.

    Args:
        bitcoins: Amount of bitcoins to convert to satoshis

    Returns:
        Amount of satoshis corresponding to the bitcoins amount passed

    Raises:
        UnitsPrecisionError: Too much precision used in decimal places
        UnitsFormatError: Used a :py:class:`str` to specify the bitcoins
    """
    # Type assertion
    assert isinstance(bitcoins, int) or isinstance(bitcoins, str) or \
        isinstance(bitcoins, Decimal) or isinstance(bitcoins, float)

    # Conversion depending on type
    if isinstance(bitcoins, int):
        # INTEGER conversion
        satoshis = bitcoins * SATOSHIS_IN_A_BITCOIN
    elif isinstance(bitcoins, str):
        # STRING conversion
        # # Format check
        if len(bitcoins) > BITCOINS_MAX_STRING_LENGTH or \
            not all([c in "0123456789." for c in bitcoins]) or \
                not bitcoins.count(".") in (0, 1):
            raise UnitsFormatError(
                "Amount of bitcoins invalid: %s. Make sure all characters "
                "are from 0 to 9 and just one dot (.) is present, if any. "
                % bitcoins)

        # # Convert to numbers
        whole_decimal = bitcoins.split(".")
        if len(whole_decimal) == 1:
            # Just decimal specified
            satoshis = btc_to_satoshi(int(whole_decimal[0]))
        else:
            whole, decimal = whole_decimal
            # Too much decimals
            if len(decimal) > BITCOINS_MAX_DECIMALS:
                raise UnitsPrecisionError(
                    "Too much precision. Use maximum %d decimal digits."
                    % BITCOINS_MAX_DECIMALS)
            # Convert
            satoshis = int(whole) * SATOSHIS_IN_A_BITCOIN + \
                int(decimal.ljust(BITCOINS_MAX_DECIMALS, "0"))
    else:
        # FLOAT conversion
        if isinstance(bitcoins, float):
            bitcoins = Decimal(str(bitcoins))

        # DECIMAL conversion
        # # Too much precision
        if bitcoins.as_tuple().exponent < -BITCOINS_MAX_DECIMALS:
            raise UnitsPrecisionError(
                "Too much precision, maximum %d digits" %
                BITCOINS_MAX_DECIMALS)

        # # Convert as a regular string
        satoshis = int(bitcoins.shift(BITCOINS_MAX_DECIMALS))

    return satoshis


def satoshi_to_btc(satoshis: int) -> str:
    """Converts an amount of satoshis to bitcoins.

    Note:
        The function returns a :py:class:`str` with the same format as the
        valid format in :py:meth:`btc_to_satoshi`.

        The string format will be the most reduced as posible.

        For instance, if the result is a tenth of a bitcoin, it will be
        expressed as ``0.1`` instead of ``0.10000000``, or a whole bitcoin
        will be expressed as just ``1``

    See also:
        To understand why a :py:class:`str` is used to return the bitcoin
        units read the warning note in the method :py:meth:`btc_to_satoshi`
        about using :py:class:`float` to represent units.

    Args:
        satoshis: Amount of satoshis to convert to bitcoins.

    Returns:
        Amount of bitcoins corresponding to the satoshis passed as a
        :py:class:`str` with the format specified in :py:meth:`btc_to_satoshi`.
    """
    assert isinstance(satoshis, int)

    # Fill with left zeros
    bitcoins_padded = str(satoshis).rjust(BITCOINS_MAX_DECIMALS, "0")

    # Take units and decimals
    bitcoin_units = bitcoins_padded[:-BITCOINS_MAX_DECIMALS]
    bitcoin_decimals = bitcoins_padded[-BITCOINS_MAX_DECIMALS:]

    # Insert zero as unit if not present
    bitcoin_units = "0" if len(bitcoin_units) == 0 else bitcoin_units

    # Insert dot
    bitcoins = "%s.%s" % (bitcoin_units, bitcoin_decimals)

    # Reduced form
    bitcoins = bitcoins.rstrip("0").rstrip(".")
    bitcoins = bitcoins if len(bitcoins) != 0 else "0"

    return bitcoins


def satoshi_to_bit(satoshis: int) -> str:
    """Converts an amount of satoshis to bits."""
    assert isinstance(satoshis, int)

    # Fill with left zeros
    bits_padded = str(satoshis).rjust(BITS_MAX_DECIMALS, "0")

    # Take units and decimals
    bits_units = bits_padded[:-BITS_MAX_DECIMALS]
    bits_decimanls = bits_padded[-BITS_MAX_DECIMALS:]

    # Insert zero as unit if not present
    bits_units = "0" if len(bits_units) == 0 else bits_units

    # Insert dot
    bits = "%s.%s" % (bits_units, bits_decimanls)

    # Reduced form
    bits = bits.rstrip("0").rstrip(".")

    return bits


def satoshi_to_mbitcoin(satoshis: int) -> str:
    """Converts an amount of satoshis to milli bitcoins."""
    assert isinstance(satoshis, int)

    # Fill with left zeros
    mbtc_padded = str(satoshis).rjust(mBITCOINS_MAX_DECIMALS, "0")

    # Take units and decimals
    mbtc_units = mbtc_padded[:-mBITCOINS_MAX_DECIMALS]
    mbtc_decimanls = mbtc_padded[-mBITCOINS_MAX_DECIMALS:]

    # Insert zero as unit if not present
    mbtc_units = "0" if len(mbtc_units) == 0 else mbtc_units

    # Insert dot
    mbtc = "%s.%s" % (mbtc_units, mbtc_decimanls)

    # Reduced form
    mbtc = mbtc.rstrip("0").rstrip(".")

    return mbtc
