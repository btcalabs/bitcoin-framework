
# Libraries
from abc import abstractmethod, ABCMeta
from enum import Enum, unique

# Relative imports
from ..field import VarInt
from ..interfaces import Serializable


def serialize_item(item: bytes) -> bytes:
    return VarInt(len(item)).serialize() + item


@unique
class PSBTScope(Enum):
    GLOBAL = 0
    INPUT = 1
    OUTPUT = 2


class PSBTItem(Serializable, metaclass=ABCMeta):
    """Mapping Between PSBT key:value"""
    __slots__ = "_key_data", "_value",

    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        self._value = value
        self._key_data = key_data if key_data is not None else bytes()

    @classmethod
    @abstractmethod
    def get_type(cls) -> int:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def get_scope(cls) -> PSBTScope:
        raise NotImplementedError

    def serialize(self) -> bytes:
        key, value = bytes([self.key_type]) + self._key_data, self._value

        return serialize_item(key) + serialize_item(value)

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBTItem":
        offset = 0
        # Read key length
        key_length = VarInt.deserialize(data[offset:])
        offset += len(key_length)

        # Read key
        key = data[offset:offset+key_length.value]
        # Separate key-type & key-data
        key_type, key_data = key[0], key[1:]

        # Deserializing the valid class ?
        if key_type != cls.get_type():
            raise ValueError("Deserializing the wrong class.")

        offset += key_length.value

        # Read value
        value_length = VarInt.deserialize(data[offset:])
        offset += len(value_length)

        value = data[offset:offset+value_length.value]

        if not key_data:
            return cls(value)
        return cls(value, key_data)

    @property
    def key_type(self) -> int:
        return self.get_type()

    @property
    def key(self) -> bytes:
        return bytes([self.key_type]) + self._key_data

    @property
    def key_data(self) -> bytes:
        return self._key_data

    @property
    def value(self) -> bytes:
        """Returns the key value.

        Returns:
            The value of the key.
        """
        return self._value


class NoParamPSBTItem(PSBTItem, metaclass=ABCMeta):
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        if key_data is not None:
            raise ValueError("That key takes no extra data.")
        super().__init__(value)
