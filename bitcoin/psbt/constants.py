"""Defines the PSBT constants and key-types."""

# Relative imports
from .model import PSBTItem, NoParamPSBTItem, PSBTScope
from .registry import PSBTRegistry
from ..crypto import ECDSAPublicKey, ECDSASignature
from ..tx import Tx, TxOutput, HashType
from ..script import ScriptSig, ScriptPubKey


# Constants
MAGIC_BYTES = b'psbt'
"""
    bytes: Magic bytes which are ASCII for psbt.
         This integer should be serialized in most significant byte order.
"""
HEADER_SEPARATOR = b'\xff'
"""
    bytes: Separator between the header and the data carried by the PSBT.
"""
DATA_SEPARATOR = b'\x00'
"""
    bytes: Separator between scopes on the PSBT serialization.
"""


#
# GLOBAL TYPES
#
class PSBT_GLOBAL_UNSIGNED_TX(NoParamPSBTItem, metaclass=PSBTRegistry):
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            Tx.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Tx.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x00

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.GLOBAL

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_GLOBAL_UNSIGNED_TX":
        item = super().deserialize(data)
        return cls(item.value)


#
# PER-INPUT TYPES
#
class PSBT_IN_NON_WITNESS_UTXO(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Non-Witness UTXO."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            Tx.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Tx.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x00

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_NON_WITNESS_UTXO":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_WITNESS_UTXO(NoParamPSBTItem, metaclass=PSBTRegistry):
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            TxOutput.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid TxOutput.")

        super().__init__(value, key_data)

    """Witness UTXO."""
    @classmethod
    def get_type(cls) -> int:
        return 0x01

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_WITNESS_UTXO":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_PARTIAL_SIG(PSBTItem, metaclass=PSBTRegistry):
    """Partial Signature."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        # Checks
        if key_data is None:
            raise ValueError("Public key expected.")
        # Check signature validity
        try:
            # Remove the sighash first
            ECDSASignature.deserialize(value[:-1])
        except ValueError:
            raise ValueError("Not a valid Signature.")
        # Check public key validity
        try:
            ECDSAPublicKey.deserialize(key_data)
        except ValueError:
            raise ValueError("Not a valid PublicKey.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x02

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_PARTIAL_SIG":
        item = super().deserialize(data)
        return cls(item.value, item.key_data)


class PSBT_IN_SIGHASH_TYPE(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Sighash Type."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            HashType.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid HashType.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x03

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_SIGHASH_TYPE":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_REDEEM_SCRIPT(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Redeem Script."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptSig.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x04

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_REDEEM_SCRIPT":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_WITNESS_SCRIPT(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Witness Script."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptSig.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x05

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_WITNESS_SCRIPT":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_BIP32_DERIVATION(PSBTItem, metaclass=PSBTRegistry):
    """BIP 32 Derivation Path."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        # Checks
        if key_data is None:
            raise ValueError("Public key expected.")
        # Check key validity
        try:
            ECDSAPublicKey.deserialize(key_data)
        except ValueError:
            raise ValueError("Not a valid PublicKey.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x06

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_BIP32_DERIVATION":
        item = super().deserialize(data)
        return cls(item.value, item.key_data)


class PSBT_IN_FINAL_SCRIPTSIG(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Finalized scriptSig."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptSig.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x07

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_FINAL_SCRIPTSIG":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_IN_FINAL_SCRIPTWITNESS(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Finalized scriptWitness."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptSig.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x08

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.INPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_IN_FINAL_SCRIPTWITNESS":
        item = super().deserialize(data)
        return cls(item.value)


#
# PER-OUTPUT TYPES
#
class PSBT_OUT_REDEEM_SCRIPT(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Redeem Script."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptPubKey.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x00

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.OUTPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_OUT_REDEEM_SCRIPT":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_OUT_WITNESS_SCRIPT(NoParamPSBTItem, metaclass=PSBTRegistry):
    """Witness Script."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        try:
            ScriptPubKey.deserialize(value)
        except ValueError:
            raise ValueError("Not a valid Script.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x01

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.OUTPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_OUT_WITNESS_SCRIPT":
        item = super().deserialize(data)
        return cls(item.value)


class PSBT_OUT_BIP32_DERIVATION(PSBTItem, metaclass=PSBTRegistry):
    """BIP 32 Derivation Path."""
    def __init__(self, value: bytes, key_data: bytes = None) -> None:
        # Checks
        if key_data is None:
            raise ValueError("Public key expected.")
        # Check key validity
        try:
            ECDSAPublicKey.deserialize(key_data)
        except ValueError:
            raise ValueError("Not a valid PublicKey.")

        super().__init__(value, key_data)

    @classmethod
    def get_type(cls) -> int:
        return 0x02

    @classmethod
    def get_scope(cls) -> PSBTScope:
        return PSBTScope.OUTPUT

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT_OUT_BIP32_DERIVATION":
        item = super().deserialize(data)
        return cls(item.value, item.key_data)
