"""Defines a Registry for the PSBT constants"""

# Libraries
from abc import ABCMeta
from typing import Type

# Relative imports
from .model import PSBTScope, PSBTItem


class PSBTRegistry(ABCMeta):
    """
    Attributes:
        _registry(dict): Mapping containing all the constants and its int
                         representation.
    """
    _registry = {scope: {} for scope in PSBTScope}  # type: dict

    def __new__(mcs, *args, **kwargs):
        cls = super().__new__(mcs, *args, **kwargs)

        mcs._registry[cls.get_scope()][cls.get_type()] = cls

        return cls

    @classmethod
    def get_item(mcs, scope: PSBTScope, key_type: int) -> Type[PSBTItem]:
        return mcs._registry[scope][key_type]
