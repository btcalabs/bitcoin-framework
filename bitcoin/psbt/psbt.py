"""Basic implementation of PSBT, defined in BIP174:
- https://github.com/bitcoin/bips/blob/master/bip-0174.mediawiki

The Partially Signed Bitcoin Transaction (PSBT) format consists of key-value
maps. Each key-value pair must have a unique key within its scope; duplicates
are not allowed.

    {0x70736274}|{0xff}|{global key-value map}|
    {input key-value map}|...|{input key-value map}|
    {output key-value map}|...|{output key-value map}|

"""
# Libraries
from typing import List  # noqa: F401
from base64 import b64encode, b64decode

# Relative imports
from .constants import MAGIC_BYTES, HEADER_SEPARATOR, DATA_SEPARATOR, \
    PSBT_GLOBAL_UNSIGNED_TX
from .model import PSBTItem, PSBTScope
from .registry import PSBTRegistry
from ..interfaces import Serializable
from ..tx import Tx


class PSBT(Serializable):
    __slots__ = "_global", "_inputs", "_outputs", "_data"

    def __init__(self, pglobal=None, inputs=None, outputs=None) -> None:
        # Type assertions
        assert isinstance(pglobal, list) or pglobal is None
        assert isinstance(inputs, list) or inputs is None
        assert isinstance(outputs, list) or outputs is None
        # Defaults
        self._global = pglobal if pglobal is not None else []
        self._inputs = inputs if inputs is not None else []
        self._outputs = outputs if outputs is not None else []

        self._data = {
            "global": self._global,
            "inputs": self._inputs,
            "outputs": self._outputs
        }

    def serialize(self) -> bytes:
        """Serializes the PSBT into a bytes object."""
        header_serialization = MAGIC_BYTES + HEADER_SEPARATOR

        # Serialize data
        data_serialization = b''

        for item in self._global:
            data_serialization += item.serialize()
        data_serialization += DATA_SEPARATOR

        for inp in self._inputs:
            for item in inp:
                data_serialization += item.serialize()
            data_serialization += DATA_SEPARATOR

        for out in self._outputs:
            for item in out:
                data_serialization += item.serialize()
            data_serialization += DATA_SEPARATOR

        return b''.join([header_serialization, data_serialization])

    @classmethod
    def deserialize(cls, data: bytes) -> "PSBT":
        """Construcs a PSBT from its bytes serialization."""
        if not len(data):
            raise ValueError("Not enough bytes provided.")
        total_length = len(data)

        offset = 0
        if data[offset:offset+4] != MAGIC_BYTES:
            raise ValueError("This is not a valid PSBT; missing magic bytes.")
        offset += 4
        if data[offset:offset+1] != HEADER_SEPARATOR:
            raise ValueError("This is not a valid PSBT; missing header "
                             "separator.")
        offset += 1

        # Empty PSBT
        if data[offset:offset+1] == DATA_SEPARATOR:
            return cls()

        # Globals
        item_class = PSBTRegistry.get_item(PSBTScope.GLOBAL, data[offset+1])
        glob = item_class.deserialize(data[offset:])
        offset += len(glob)
        if data[offset:offset+1] != DATA_SEPARATOR:
            raise ValueError("Bad deserialization.")
        offset += 1  # DATA SEPARATOR
        tx = Tx.deserialize(glob.value)

        # Inputs
        inp = [[] for _ in tx.inputs]  # type: List

        for l in inp:
            while offset < total_length and \
                    data[offset:offset+1] != DATA_SEPARATOR:
                item_class = PSBTRegistry.get_item(
                    PSBTScope.INPUT, data[offset+1])
                item = item_class.deserialize(data[offset:])
                l.append(item)
                offset += len(item)
            offset += 1  # DATA SEPARATOR

        # Outputs
        out = [[] for _ in tx.outputs]  # type: List
        for l in out:
            while offset < total_length and \
                    data[offset:offset+1] != DATA_SEPARATOR:
                item_class = PSBTRegistry.get_item(
                    PSBTScope.OUTPUT, data[offset+1])
                item = item_class.deserialize(data[offset:])
                l.append(item)
                offset += len(item)
            offset += 1  # DATA SEPARATOR

        if total_length != offset:
            raise ValueError("Missing {} byte(s)".format(total_length-offset))

        return PSBT([glob], inp, out)

    def _validate(self) -> bool:
        """Validates the PSBT object."""
        # Missing unique PSBT_GLOBAL_UNSIGNED TX
        if len(self._global) != 1 or \
                not isinstance(self._global[0], PSBT_GLOBAL_UNSIGNED_TX):
            return False
        # Pending to improve or delegate
        return True

    def add_global(self, item: PSBTItem) -> None:
        """Adds the given item to the global scope."""
        if item.get_scope() != PSBTScope.GLOBAL:
            raise ValueError("Wrong scope item. That item should be in "
                             "{}.".format(item.get_scope()))
        if len(self._global):
            raise ValueError("The current psbt already has a global defined.")

        tx = Tx.deserialize(item.value)
        for i in range(len(tx.inputs)):
            self._inputs.append([])
        for i in range(len(tx.outputs)):
            self._outputs.append([])

        self._global.append(item)

    def add_input(self, idx:  int, item: PSBTItem) -> None:
        """Adds an item to the input specified."""
        if not len(self._global):
            raise ValueError("Please, fulfill first the global scope.")
        if item.get_scope() != PSBTScope.INPUT:
            raise ValueError("Wrong scope item. That item should be in "
                             "{}.".format(item.get_scope()))
        if idx > len(self._inputs) - 1:
            raise ValueError("Not enough inputs on the current associated Tx.")
        if item.get_type() in [i.get_type() for i in self._inputs[idx]]:
            raise ValueError("Repeated key for the input {}".format(idx))

        # Add input
        self._inputs[idx].append(item)

    def add_output(self, idx: int, item: PSBTItem) -> None:
        """Adds an item to the output specified."""
        if not len(self._global):
            raise ValueError("Please, fulfill first the global scope.")
        if item.get_scope() != PSBTScope.OUTPUT:
            raise ValueError("Wrong scope item. That item should be in "
                             "{}.".format(item.get_scope()))
        if idx > len(self._outputs) - 1:
            raise ValueError("Not enough outputs on the current associated "
                             "Tx.")
        if item.get_type() in [i.get_type() for i in self._outputs[idx]]:
            raise ValueError("Repeated key for the output {}".format(idx))

        # Add output
        self._outputs[idx].append(item)

    @property
    def data(self) -> dict:
        """Returns all the data contained by the PSBT."""
        return self._data

    def b64(self) -> str:
        """Returns the PSBT coded in Base64."""
        return b64encode(self.serialize()).decode("utf-8")

    @classmethod
    def from_b64(cls, b64_encoded: str) -> "PSBT":
        """Constructs a PSBT object from its Base64 serialization."""
        return cls.deserialize(b64decode(b64_encoded))
