"""Bitcoin *blockchains* parameters and enumerations / constants."""
# Libraries
from .enum import REnum
from enum import unique
from typing import Union, Tuple
from typing import Optional  # noqa: F401


# REnumerations
@unique
class StdPaymentTypes(REnum):
    """Types of standard payment ``ScriptPubKey`` and ``ScriptSig``.

    Note:
        The values of the enumeration items do not mean anything. We have
        assigned them starting by 0 and in order of appearance of the
        payment type.
    """
    PK = 0
    PKH = 1
    SH = 2


@unique
class StdPaymentImplementations(REnum):
    """Types of standard payments implementations.

    The same conceptual standard payment type can be implemented using legacy,
    SegWit or nested SegWit. This aims to provide and define which
    implementation is using a standard ``ScriptPubKey`` or ``ScriptSig`` to
    send / spend the funds.

    Note:
        Items values are assigned by order of appearance.
    """
    LEGACY = 0
    NESTED = 1
    SEGWIT = 2


class PaymentStandard:
    """A payment standard is the tuple of a payment type and implementation."""
    __slots__ = "_payment_type", "_payment_implementation"

    def __init__(self, payment_type: StdPaymentTypes,
                 payment_implementation: StdPaymentImplementations) -> None:
        """Initializes a payment standard given its type and implementation."""
        assert isinstance(payment_type, StdPaymentTypes)
        assert isinstance(payment_implementation, StdPaymentImplementations)

        self._payment_type = payment_type
        self._payment_implementation = payment_implementation

    @property
    def payment_type(self) -> StdPaymentTypes:
        """Returns the payment standard type."""
        return self._payment_type

    @property
    def payment_implementation(self) -> StdPaymentImplementations:
        """Returns the payment standard implementation."""
        return self._payment_implementation

    @property
    def uses_segwit(self) -> bool:
        """Whether the payment standard uses Segregated Witness.

        Either if it is native or nested Segregated Witness.
        """
        return \
            self.payment_implementation == StdPaymentImplementations.SEGWIT \
            or \
            self.payment_implementation == StdPaymentImplementations.NESTED

    def __str__(self) -> str:
        """Name of the payment standard.

        Format of the payment standards:
            (N)P2(W)[TYPE]

        All uppercase. Where:
            (N): N if it is a nested SegWit, empty if it's not.
            (W): If it is a SegWit implementation (either nested or native)
            [TYPE]: PKH or SH depending on the payment type

        For instance:
            NP2WPKH: Pay to a public key hash, via nested SegWit.
            P2WPKH: Pay to a public key  hash, via native SegWit.
        """
        return "%sP2%s%s" % (
            "N" if self.payment_implementation ==
            StdPaymentImplementations.NESTED else "",
            "W" if self.uses_segwit else "",
            self.payment_type.name.upper())

    def __eq__(self, other) -> bool:
        """Compares two payment standards.

        They are the same if have the same payment type and implementation.
        """
        return self.payment_type == other.payment_type and \
            self.payment_implementation == other.payment_implementation \
            if isinstance(other, self.__class__) else \
            super().__eq__(other)

    def __repr__(self) -> str:
        """Representation of the payment standard."""
        return "%s(%s, %s)" % (self.__class__.__name__,
                               repr(self.payment_type),
                               repr(self.payment_implementation))


@unique
class StdPayment(REnum):
    """Standard payment methods available."""
    P2PK = PaymentStandard(StdPaymentTypes.PK,
                           StdPaymentImplementations.LEGACY)
    P2PKH = PaymentStandard(StdPaymentTypes.PKH,
                            StdPaymentImplementations.LEGACY)
    NP2WPKH = PaymentStandard(StdPaymentTypes.PKH,
                              StdPaymentImplementations.NESTED)
    P2WPKH = PaymentStandard(StdPaymentTypes.PKH,
                             StdPaymentImplementations.SEGWIT)
    P2SH = PaymentStandard(StdPaymentTypes.SH,
                           StdPaymentImplementations.LEGACY)
    NP2WSH = PaymentStandard(StdPaymentTypes.SH,
                             StdPaymentImplementations.NESTED)
    P2WSH = PaymentStandard(StdPaymentTypes.SH,
                            StdPaymentImplementations.SEGWIT)


@unique
class AddressTypes(REnum):
    """Defines the types a Bitcoin address can be.

    Note:
        The values of the enumeration items do not mean anything but they
        are the same as :py:class:`bitcoin.StandardPaymentTypes` for payment
        address items to mantain logic betweeen them.

        The other address types that are not for payment addresses are
        assigned in order of appearance in time and in the `Bitcoin Core source
        code`__.

        .. _mainnet-params: https://github.com/bitcoin/bitcoin/blob/v0.16.2/\
        src/chainparams.cpp#L140-L144

        __ mainnet-params_
    """
    PK = StdPaymentTypes.PK.value
    PKH = StdPaymentTypes.PKH.value
    SH = StdPaymentTypes.SH.value
    WIF = StdPaymentTypes.SH.value + 1
    BIP32_PUBKEY = StdPaymentTypes.SH.value + 2
    BIP32_PRIVKEY = StdPaymentTypes.SH.value + 3


# Constants
# # From enumerations
P2PK = StdPayment.P2PK.value
P2PKH = StdPayment.P2PKH.value
P2WPKH = StdPayment.P2WPKH.value
NP2WPKH = StdPayment.NP2WPKH.value
P2SH = StdPayment.P2SH.value
NP2WSH = StdPayment.NP2WSH.value
P2WSH = StdPayment.P2WSH.value
# # From addresses
WIF = AddressTypes.WIF
BIP32_PRIVKEY = AddressTypes.BIP32_PRIVKEY
BIP32_PUBKEY = AddressTypes.BIP32_PUBKEY

# # `base58` addresses prefixes
_DEFAULT_BLOCKCHAIN = None  # type: Optional[BlockchainParams]
"""
    NetworkParams: Default network parameters to use if no network has been
                   specified. Retrieve it and set it with the proper methods
                   (:py:meth:`bitcoin.get_default_network`,
                   :py:meth:`bitcoin.set_default_network`)
"""

_MAINNET_PREFIXES = (
    int(0).to_bytes(1, "big"),  # P2PKH
    int(5).to_bytes(1, "big"),  # P2SH
    int(128).to_bytes(1, "big"),  # WIF
    b'\x04\x88\xB2\x1E',  # BIP32_PUBKEY
    b'\x04\x88\xAD\xE4'  # BIP32_PRIVKEY
)
"""
    Tuple[bytes, ...]: `mainnet` address prefixes, as specified in

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L140-L144
"""

_TESTNET_PREFIXES = (
    int(111).to_bytes(1, "big"),  # P2PKH
    int(196).to_bytes(1, "big"),  # P2SH
    int(239).to_bytes(1, "big"),  # WIF
    b'\x04\x35\x87\xCF',  # BIP32_PUBKEY
    b'\x04\x35\x83\x94'  # BIP32_PRIVKEY
)
"""
    Tuple[bytes, ...]: `testnet` address prefixes, as specified in

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L242-L246
"""

_REGTEST_PREFIXES = (
    int(111).to_bytes(1, "big"),  # P2PKH
    int(196).to_bytes(1, "big"),  # P2SH
    int(239).to_bytes(1, "big"),  # WIF
    b'\x04\x35\x87\xCF',  # BIP32_PUBKEY
    b'\x04\x35\x83\x94'  # BIP32_PRIVKEY
)
"""
    Tuple[bytes, ...]: `regtest` address prefixes, as specified in

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L340-L344
"""
# # `bech32` address prefixes
_MAINNET_HRP = "bc"
"""
    str: `mainnet` human readable part for `bech32` addresses.

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L146
"""

_TESTNET_HRP = "tb"
"""
    str: `testnet` human readable part for `bech32` addresses.

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L248
"""
_REGTEST_HRP = "bcrt"
"""
    str: `regtest` human readable part for `bech32` addresses.

https://github.com/bitcoin/bitcoin/blob/v0.16.2/src/chainparams.cpp#L346
"""

# # Payment hashes size
PKH_SIZE = 20
"""
    int: Size in bytes for a public key hash used in P2PKH payments.

Note:
   The size is determined by the size of the result of the ``ripemd_160`` hash
   function, as a public key hash is calculated this way:

   >>> ripemd160(sha256(public_key_serialization))
"""
SH_SIZE_LEGACY = 20
"""
    int: Size in bytes for the *script hash* in legacy P2SH payments.

Note:
   This size is determined by the size of the result of the ``ripemd_160``
   and ``sha256`` hash functions, as the *redeem script*'s hash is calculated
   the following way:

   >>> ripemd160(sha256(redeem_script_serialization))
"""
SH_SIZE_SEGWIT = 32
"""
    int: Size in bytes for the *script hash* in SegWit P2SH payments.

Note:
   This size is determined by the size of the result of the ``sha256`` hash
   function, as the redeem script hash in a segwit version 0 program as defined
   in `BIP 141`_ is the following one:

   .. _`BIP 141`: https://github.com/bitcoin/bips/blob/master/\
   bip-0141.mediawiki#witness-program

   >>> sha256(sha256(redeem_script))
"""


# Errors
class NoDefaultBlockchainError(ValueError):
    """No default blockchain has been specified yet and one is needed."""
    pass


# Network parameters
class BlockchainParams:
    """Parameters dependent on the *blockchain*.

    Attributes:
        _address_prefixes (list): Prefixes for ``base58`` addresses.
        _address_hrp (str): Human readable part for ``bech32`` addresses.
        _version (int): Version of the network (like `testnet1`, ...).
    """
    __slots__ = "_name", "_address_prefixes", "_address_hrp", "_version"

    def __init__(self, name: str, address_prefixes: Tuple[bytes, ...],
                 address_hrp: str, version: int) -> None:
        """Initializes a blockchain params object."""
        # Type assertions
        assert isinstance(name, str)
        assert all(isinstance(i, bytes) for i in address_prefixes) \
            and isinstance(address_prefixes, tuple)
        assert isinstance(address_hrp, str)
        assert isinstance(version, int)

        # Checks
        # TODO: Apply integrity checks

        # Initialize
        self._name = name
        self._address_prefixes = address_prefixes
        self._address_hrp = address_hrp
        self._version = version

    def address_prefix(self, address_type: Union[StdPaymentTypes,
                                                 AddressTypes]) -> bytes:
        """Returns the `base58` legacy address prefix.

        Args:
            address_type: Type of address to get the prefix.

        Returns:
            Prefix as a :py:class:`bytes` object.

        Raises:
            ValueError: ``P2PK`` does not have any address associated.
        """
        assert isinstance(address_type, AddressTypes) or \
            isinstance(address_type, StdPaymentTypes)

        if address_type == StdPaymentTypes.PK \
                or address_type == AddressTypes.PK:
            raise ValueError("P2PK cannot be represented with a legacy "
                             "address.")

        return self._address_prefixes[address_type.value - 1]

    @property
    def address_prefixes(self) -> Tuple[bytes, ...]:
        """Returns the ``base58`` legacy address prefixes.

        Format of the returned :py:class:`tuple`:

        ``(P2PKH, P2SH, WIF, BIP32_PUBKEY, BIP32_PRIVKEY)``

        Where each item is the prefix for that kind of address as a
        :py:class:`bytes` object.
        """
        return self._address_prefixes

    @property
    def address_hrp(self) -> str:
        """Human readable part of `bech32` addresses in this network."""
        return self._address_hrp

    @property
    def name(self) -> str:
        """Name of the *blockchain* params."""
        return self._name

    @property
    def version(self) -> int:
        """Network's version."""
        return self._version

    def __str__(self) -> str:
        """*Blockchain* name."""
        return self._name

    def __eq__(self, other) -> bool:
        """Checks its params to check if they are equal."""
        return other.address_prefixes == self.address_prefixes and \
            other.address_hrp == self.address_hrp and \
            other.version == self.version \
            if isinstance(other, self.__class__) else \
            super().__eq__(other)

    def __repr__(self):
        """*Blockchain* parameters accessor."""
        return "{}({}, {}, {}, {})".format(
            BlockchainParams.__name__,
            repr(self._name),
            repr(self._address_prefixes),
            repr(self._address_hrp),
            repr(self._version)
        )


# Methods
def get_payment_standard(payment_type: StdPaymentTypes,
                         payment_implementation: StdPaymentImplementations) \
        -> PaymentStandard:
    """Returns the payment standard given the type and implementation.

    Args:
        payment_type: Payment type of the standard
        payment_implementation: Payment implementation of the standard

    Raises:
        ValueError: Payment type and implementation are not a valid payment
                    standard.

    Returns:
        Matching payment standard for the type and implementation.
    """
    # Find a payment standard with the given arguments
    for payment_standard in StdPayment:
        standard = payment_standard.value
        if standard.payment_type == payment_type \
                and standard.payment_implementation == payment_implementation:
            return standard

    # Nothing found
    raise ValueError(
        "No payment standard has been found for this type and implementation")


def get_address_type_from_index(index: int) -> AddressTypes:
    """Returns an address type given its index value.

    Each address type has a numeric index assigned in order to create lists
    of prefixes for each address type. This method returns the address type
    given the index value.

    Args:
        index: Value of the address type enum

    Returns:
        AddressType: Address type item

    Raises:
        ValueError Index does not belong to any address type.
    """
    try:
        return AddressTypes(index)
    except ValueError:
        raise ValueError("Value %d does not belong to any address type" %
                         index)


def set_default_network(network: BlockchainParams) -> None:
    """Sets the default *blockchain* to use when no one is specified.

    This will affect all subsequent calls to the library.

    Args:
        network (BlockchainParams): Network to use
    """
    assert isinstance(network, BlockchainParams)

    global _DEFAULT_BLOCKCHAIN
    _DEFAULT_BLOCKCHAIN = network


def get_default_network() -> BlockchainParams:
    """Default *blockchain* to be used when no one is specified.

    Will raise an error if no default *blockchain* has been set yet.

    Returns:
        BlockchainParams: Default selected *blockchain*
    """
    global _DEFAULT_BLOCKCHAIN
    if _DEFAULT_BLOCKCHAIN is None:
        raise NoDefaultBlockchainError(
            "No default blockchain has been selected yet. "
            "Please specify one or choose a default one.")
    return _DEFAULT_BLOCKCHAIN


# Derived constants
class KnownBlockchainParams(BlockchainParams):
    """Blockchain parameters that are global to the framework."""

    def __init__(self, *args, **kwargs) -> None:
        """Initializes and stores the blockchain params as a global.

        If the name already exists, does not store it as a global.

        Raises:
            ValueError: Blockchain params with that name already stored.
        """
        # Correct name
        name = self.global_name(kwargs["name"]) if "name" in kwargs else \
            self.global_name(args[0])

        # Store in globals if not one exists.
        if globals().get(name, None) is None:
            globals()[name] = self
        else:
            raise ValueError("Blockchain params %s already exists" % name)

        # Initialize as a normal object
        super().__init__(*args, **kwargs)

    @staticmethod
    def global_name(name):
        """Converts the blockchain parameters name to global constant name."""
        return name.upper()

    def __repr__(self) -> str:
        """Returns the blockchain params representation as a global."""
        return str(self.global_name(self._name))


# # Networks
MAINNET = KnownBlockchainParams(
    "mainnet",
    _MAINNET_PREFIXES,
    _MAINNET_HRP,
    1
)
"""
    BlockchainParams: Parameters of the `mainnet` Bitcoin *blockchain*.
"""
TESTNET = KnownBlockchainParams(
    "testnet",
    _TESTNET_PREFIXES,
    _TESTNET_HRP,
    3
)
"""
    BlockchainParams: Parameters of the `testnet3` Bitcoin *blockchain*.
"""
REGTEST = KnownBlockchainParams(
    "regtest",
    _REGTEST_PREFIXES,
    _REGTEST_HRP,
    3
)
"""
    BlockchainParams: Parameters of the `regtest3` Bitcoin *blockchain*.
"""
BLOCKCHAINS = MAINNET, TESTNET, REGTEST
"""
    Tuple[BlockchainParams, ...]: Tuple of available *blockchain* params.
"""
