# Bitcoin Framework
[![PyPI](https://img.shields.io/pypi/v/bitcoin-framework.svg)](https://pypi.org/project/bitcoin-framework/) [![PyPI - Python Version](https://img.shields.io/pypi/pyversions/bitcoin-framework.svg)](https://pypi.org/project/bitcoin-framework/) [![PyPI - Status](https://img.shields.io/pypi/status/bitcoin-framework.svg)](https://pypi.org/project/bitcoin-framework/) [![ReadTheDocs](https://readthedocs.org/projects/bitcoin-framework/badge/?version=latest)](https://bitcoin-framework.readthedocs.io/en/latest/) [![Build Status](https://travis-ci.com/BTCAssessors/bitcoin-framework.svg?branch=master)](https://travis-ci.com/BTCAssessors/bitcoin-framework) [![Coverage Status](https://coveralls.io/repos/github/BTCAssessors/bitcoin-framework/badge.svg)](https://coveralls.io/github/BTCAssessors/bitcoin-framework) 
[![Bitcoin Framework Logo](./docs/_static/logo_gradient.png)](https://pypi.org/project/bitcoin-framework/)

Python Bitcoin framework to create transactions with smart contracts based
on puzzle-friendliness and OOP

**\<\> with ♥ in [Barcelona Tech City](https://barcelonatechcity.com/), Barcelona for [BTC Assessors](https://btcassessors.com) by [@ccebrecos](https://gitlab.com/ccebrecos) & [@davidlj95](https://gitlab.com/davidlj95)**

[![BTC Assessors](https://i.imgur.com/7nzUvR0.png)](https://www.btcassessors.com)
