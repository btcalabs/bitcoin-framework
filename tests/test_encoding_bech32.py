"""Tests the methods to encode/decode to/from bech32.

**Sources:**

- http://gobittest.appspot.com/Address
"""

# Relative imports
from .model import JSONTestModel

# Methods to test
from bitcoin import bech32encode, bech32decode


class TestEncodingBech32Valid(JSONTestModel):

    def test_encode(self):
        """Tests the bech32 encoding."""
        for test in self._data:
            with self.subTest(test['address']):
                # Test variables
                hrp = test['hrp']
                version = test['version']
                program = test['program']
                address = test['address']
                # Test conversions
                program = bytes.fromhex(program)
                # Create object
                guessed = bech32encode(hrp, version, program)
                # Test
                self.assertEqual(address, guessed)

    def test_decode(self):
        """Tests the bech32 decoding."""
        for test in self._data:
            with self.subTest(test['address']):
                # Test variables
                hrp = test['hrp']
                version = test['version']
                program = test['program']
                address = test['address']
                # Test conversions
                program = bytes.fromhex(program)
                # Create object
                hrp_guess, version_guess, program_guess = bech32decode(address)
                # Test
                self.assertEqual(hrp, hrp_guess)
                self.assertEqual(version, version_guess)
                self.assertEqual(program, program_guess)


class TestEncodingBech32Invalid(JSONTestModel):
    def test_encode(self):
        """Tests the bech32 encoding with invalid test cases."""
        for test in self._data['encoding']:
            with self.subTest(test['error']):
                # Test variables
                hrp = test['hrp']
                version = test['version']
                program = test['program']
                # Test conversions
                program = bytes.fromhex(program)
                with self.assertRaises(ValueError):
                    bech32encode(hrp, version, program)

    def test_decode(self):
        """Tests the bech32 decoding with invalid test cases."""
        for test in self._data['decoding']:
            with self.subTest(test['address']):
                # Test variables
                address = test['address']
                error = self.convert_error(test['error'])
                with self.assertRaises(error):
                    bech32decode(address)
