"""Tests for the Extended-ECDSAPublicKey and Extended-ECDSAPrivateKey.

Test generated with:
- https://iancoleman.io/bip39/
- http://bip32.org/

Notation used:
    msk = master secret key
    mpk = master public key
    dmpk = derived mpk
"""

# Relative imports
from .model import JSONTestModel

from bitcoin import WIFAddress, MAINNET

# Classes to test
from bitcoin import ExtendedECDSAPublicKey, ExtendedECDSAPrivateKey


class TestCryptoExtendedKeyValid(JSONTestModel):

    def test_private_serialization(self):
        msk = ExtendedECDSAPrivateKey.decode(self._data["private"]["extended"])

        self.assertEqual(
            msk.depth,
            self._data["private"]["depth"])
        self.assertEqual(
            msk.fingerprint.hex(),
            self._data["private"]["fingerprint"])
        self.assertEqual(
            int(msk.childnumber.hex()),
            self._data["private"]["childnumber"])
        self.assertEqual(
            msk.chaincode.hex(),
            self._data["private"]["chaincode"])
        self.assertEqual(
            WIFAddress(msk.key, network=MAINNET).encode(),
            self._data["private"]["key"])
        self.assertEqual(msk.encode(),
                         self._data["private"]["extended"])
        self.assertEqual(msk.is_master(), self._data["private"]["master"])

    def test_public_serialization(self):
        mpk = ExtendedECDSAPublicKey.decode(self._data["public"]["extended"])

        self.assertEqual(
            mpk.depth,
            self._data["public"]["depth"])
        self.assertEqual(
            mpk.fingerprint.hex(),
            self._data["public"]["fingerprint"])
        self.assertEqual(
            int(mpk.childnumber.hex()),
            self._data["public"]["childnumber"])
        self.assertEqual(
            mpk.chaincode.hex(),
            self._data["public"]["chaincode"])
        self.assertEqual(
            mpk.key.hex(),
            self._data["public"]["key"])

        self.assertEqual(mpk.encode(),
                         self._data["public"]["extended"])
        self.assertEqual(mpk.is_master(), self._data["public"]["master"])

    def test_private_derivation(self):
        msk = ExtendedECDSAPrivateKey.decode(self._data["private"]["extended"])

        for i in range(len(self._data["private"]["derivations"])):
            sk = msk.CKDpriv(i).key
            self.assertEqual(
                WIFAddress(sk, network=MAINNET).encode(),
                self._data["private"]["derivations"][i])

    def test_public_derivation(self):
        mpk = ExtendedECDSAPublicKey.decode(self._data["public"]["extended"])

        for i in range(len(self._data["public"]["derivations"])):
            pk = mpk.CKDpub(i).key
            self.assertEqual(
                pk.hex(),
                self._data["public"]["derivations"][i])

        with self.assertRaises(ValueError):
            mpk.CKDpub(0, True)

    def test_private_to_public(self):
        msk = ExtendedECDSAPrivateKey.decode(self._data["private"]["extended"])
        mpk = ExtendedECDSAPublicKey.decode(self._data["public"]["extended"])

        dmpk = msk.neuter()
        self.assertEqual(dmpk, mpk)

        self.assertEqual(len(self._data["private"]["derivations"]),
                         len(self._data["public"]["derivations"]))

        for i in range(len(self._data["private"]["derivations"])):
            self.assertEqual(
                msk.CKDpriv(i).key.to_public(),
                mpk.CKDpub(i).key
            )


class TestCryptoExtendedKeyInvalid(JSONTestModel):
    def test_serialize(self):
        for addr in self._data["public"]:
            with self.assertRaises(ValueError):
                ExtendedECDSAPublicKey.decode(addr)

        for addr in self._data["private"]:
            with self.assertRaises(ValueError):
                ExtendedECDSAPublicKey.decode(addr)
