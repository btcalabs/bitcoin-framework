"""Tests the script model file with some silly tests."""

# Libraries
import unittest
from .model import JSONTestModel

# Relative imports
from bitcoin import Script, TransactionScript, TxInputOutput, ScriptPubKey, \
    ScriptSig, TxInput, OP_0, OP_1, ScriptData, Witness, VarInt, OP_DUP


class MockTxInputOutput(TxInputOutput):
    def serialize(self):
        pass

    @classmethod
    def deserialize(cls, data: bytes):
        pass


class TestScriptValid(JSONTestModel):
    """Tests the ``Script`` with valid test cases.

    Test cases format are:
        [["list","of","script","asm","items"], "serialization"]
    """
    def test_empty_script(self):
        """Tests an empty script construction."""
        script = Script()
        self.assertEqual(list(), script.items)
        self.assertEqual(bytes(), script.serialize())
        self.assertIsInstance(script.__str__(), str)

    def test_serialize(self):
        """Tests serializing some scripts.

        Also tests from_asm as is used to construct the real script.
        """
        for asm_items, serialization in self._data:
            with self.subTest(serialization):
                # Test variables
                script = Script.from_asm(asm_items)

                # Serialize
                self.assertEqual(script.hex(), serialization)

                # Test with a string instead of a list
                script = Script.from_asm(" ".join(asm_items))
                self.assertEqual(script.hex(), serialization)

                # Test str
                self.assertIsInstance(str(script), str)

                # Test repr
                self.assertEqual(serialization, self.eval(repr(script)).hex())

                # Test count
                self.assertEqual(len(asm_items), script.count)

    def test_deserialize(self):
        """Tests deserializing some scripts."""
        for asm_items, serialization in self._data:
            with self.subTest(serialization):
                # Test variables
                real_serialization = bytes.fromhex(serialization)

                # Deserialize and check
                self.assertEqual(
                    real_serialization,
                    Script.deserialize(real_serialization).serialize()
                )

    def test_alike(self):
        """Tests two scripts are alike."""
        # Are alike
        script_a = Script([OP_DUP(), ScriptData(bytes(2))])
        script_b = Script([OP_DUP(), ScriptData(b'\xff\xff')])
        self.assertIsNotNone(script_a.alike(script_b))
        self.assertTrue(script_a.is_alike(script_b))

        # Are not alike
        script_b = Script([OP_DUP()])
        with self.assertRaises(ValueError):
            script_a.alike(script_b)
        self.assertFalse(script_a.is_alike(script_b))

    def test_add(self):
        """Tests adding two scripts."""
        scripts = Script([OP_0(), ScriptData(b'\x00'*5)]), \
            Script([OP_1(), ScriptData(b'\xff'*6)])
        joined = scripts[0].hex() + scripts[1].hex()
        self.assertEqual(joined, (scripts[0] + scripts[1]).hex())
        joined = scripts[1].hex() + scripts[0].hex()
        self.assertEqual(joined, (scripts[1] + scripts[0]).hex())


class TestScriptInvalid(unittest.TestCase):
    """Tests the ``Script`` model with invalid test cases."""
    def test_from_asm(self):
        """Tests script from ASM fails if unknown ASM or bad data."""
        for asm in ("OP_SATOSHI", "gg", "abc"):
            with self.subTest(asm):
                with self.assertRaises(ValueError):
                    Script.from_asm(asm)


class TestWitnessValid(JSONTestModel):
    """Tests the ``Witness`` class with valid test cases.

    Test cases format:
        [["list","of","hex","witness","items"], "witness_serialization"]
    """

    def test_serialize(self):
        """Creates and serializes valid witnesses."""
        for witness_items, serialization in self._data:
            with self.subTest(serialization):
                # Test variables
                witness_items = [bytes.fromhex(i) for i in witness_items]

                # Create and serialize
                witness = Witness(witness_items)
                self.assertEqual(serialization, witness.hex())
                self.assertEqual(serialization, eval(repr(witness)).hex())

    def test_deserialize(self):
        """Deserializes and checks properties of a witness."""
        for witness_items, serialization in self._data:
            with self.subTest(serialization):
                # Deserialize and check
                witness = Witness.deserialize(bytes.fromhex(serialization))

                # Data items
                for i in range(len(witness_items)):
                    real_item = witness_items[i]
                    guessed_item = witness.items[i]
                    self.assertEqual(real_item, guessed_item.hex())

                # String
                self.assertIsInstance(str(witness), str)

    def test_add(self):
        """Tests adding two witnesses."""
        witnesses = Witness([b'\x02'*2, b'\x03'*3]), \
            Witness([b'\x04'*4, b'\x05'*5])
        joined = VarInt(4).hex() + witnesses[0].serialize()[1:].hex() + \
            witnesses[1].serialize()[1:].hex()
        self.assertEqual(joined, (witnesses[0] + witnesses[1]).hex())
        joined = VarInt(4).hex() + witnesses[1].serialize()[1:].hex() + \
            witnesses[0].serialize()[1:].hex()
        self.assertEqual(joined, (witnesses[1] + witnesses[0]).hex())


class TestTransactionScript(unittest.TestCase):
    """Tests the ``TransactionScript`` with some silly tests."""

    def test_empty_transaction_script(self):
        """Check no parent and no data for an empty ``TransactionScript``"""
        txscript = TransactionScript()
        self.assertEqual(None, txscript._parent)
        self.assertEqual(list(), txscript.items)

    def test_add(self):
        """Tests adding two TransactionScripts"""
        # Test variables
        first = TransactionScript([OP_0()])
        tx_out = MockTxInputOutput(first)
        second = TransactionScript([OP_1()])

        # Join first and second scripts
        joined, link = "0051", tx_out
        script = first + second
        self.assertEqual(joined, script.hex())
        self.assertEqual(link, script._parent)

        # Join second and first scripts
        first = TransactionScript([OP_0()])
        second = TransactionScript([OP_1()])
        joined, link = "5100", None
        script = second + first
        self.assertEqual(joined, script.hex())
        self.assertEqual(link, script._parent)

        # Join another script without parent
        first = TransactionScript([OP_0()])
        tx_out = MockTxInputOutput(first)
        joined, link = "0000", tx_out
        script = first + Script([OP_0()])
        self.assertEqual(joined, script.hex())
        self.assertEqual(link, script._parent)


class TestScriptPubKey(unittest.TestCase):
    """Tests the ``ScriptPubKey`` with some silly tests."""

    def test_empty_pubkey_script(self):
        """Checks the parent property for an empty script."""
        txscript = ScriptPubKey()
        self.assertEqual(None, txscript._parent)
        self.assertEqual(list(), txscript.items)


class TestScriptSigValid(unittest.TestCase):
    """Tests the ``ScriptSig`` with some valid tests."""

    def test_empty(self):
        """Checks the parent property for an empty script."""
        script = ScriptSig()
        self.assertEqual(None, script._parent)
        self.assertEqual(list(), script.items)

    def test_is_standard(self):
        """Tests if the ScriptSig is standard."""
        self.assertEqual(False, ScriptSig([OP_DUP()]).is_standard)

    def test_add(self):
        """Tests adding two ScriptSigs."""
        first = ScriptSig([ScriptData(b'\x02'*2)])
        second = ScriptSig([ScriptData(b'\x04'*4)])
        tx_in = TxInput(bytes(32), 0, first)
        joined, witness, parent = first.hex() + second.hex(), \
            first.witness + second.witness, tx_in
        script = first + second
        self.assertEqual(joined, script.hex())
        self.assertEqual(witness, script.witness)
        self.assertEqual(parent, script._parent)

        first = ScriptSig([ScriptData(b'\x02' * 2)])
        second = ScriptSig([ScriptData(b'\x04' * 4)])
        joined, witness, parent = second.hex() + first.hex(), \
            second.witness + first.witness, None
        script = second + first
        self.assertEqual(joined, script.hex())
        self.assertEqual(witness, script.witness)
        self.assertEqual(parent, script._parent)

        first = ScriptSig([ScriptData(b'\x02' * 2)])
        second = Script([ScriptData(b'\x04' * 4)])
        tx_in = TxInput(bytes(32), 0, first)
        joined, witness, parent = first.hex() + second.hex(), first.witness,\
            tx_in
        script = first + second
        self.assertEqual(joined, script.hex())
        self.assertEqual(witness, script.witness)
        self.assertEqual(parent, script._parent)
