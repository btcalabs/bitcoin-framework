"""Tests in high level the crypto package."""

# Libraries
import unittest
import random

# Relative imports
from bitcoin.crypto.ec import compute_y_from_x
from bitcoin.crypto.ecdsa.calculator import inv, jacobian_double, \
    jacobian_add, jacobian_multiply, fast_add, from_jacobian  # ecdsa_raw_sign


class TestCrypto(unittest.TestCase):
    """Test for the crypto package."""

    TEST_CASES = [
        (int("619c335025c7f4012e556c2a58b2506e30b8511b53ade95ea316fd8c3286feb"
             "9", 16),
         bytes.fromhex("c37af31116d1b27caf68aae9e3ac82f1477929014d5b917657d0e"
                       "b49478cb670"),
         "304402203609e17b84f6a7d30c80bfa610b5b4542f32a8a0d5447a12fb1366d7f01"
         "cc44a0220573a954c4518331561406f90300e8f3358f51928d43c212a8caed02de6"
         "7eebee")
    ]
    """
    Test case for the crypto module, to check if the signatures are properly
    calculated, its format is as follows:
        (private key, hash to sign, signature in DER)
    """

    TEST_COMPUTE_Y = [
        ("04"
         "c2d6ba78450225c577d370c03d90f86e03c983ffd602056ece211c7b33917866"
         "4d970262d859142d445803c9fec8d40f270059a28352b8e2b799d21b77ced6f8",
         True),
        ("04"
         "7b399437f98a6a4f1770986a25aeaf13ca57df8170dfc68ad6e1a4a97c29385a"
         "ad8a9767ffded87c7b449a7402eb4eb87d0b813d194f727ffd2669d42d137b15",
         True)
    ]
    """
    format:
        (uncompressed public key (04+x+y), positive_y)
    """

    TEST_INV = [
        (3, 5, 2),
        (0, 17, 0),
        (314, 1317, 734)
    ]
    """
    format:
        (num, module, inv_num)
    """

    def test_signature(self):
        """Tests the signature function and its DER encoding."""
        """
        i = 0
        for privkey, msg, signature in self.TEST_CASES:
            with self.subTest(i=i):
                guessed_signature = der_encode_sig(
                    *ecdsa_raw_sign(msg, privkey))
                self.assertEqual(signature, guessed_signature.hex())
            i += 1
        """
        # TODO: fix this
        return

    def test_compute_y(self):
        """."""
        for uncompressed, positive_y in self.TEST_COMPUTE_Y:
            uncompressed_bytes = bytes.fromhex(uncompressed)
            x = int.from_bytes(uncompressed_bytes[1:33], "big")
            y = int.from_bytes(uncompressed_bytes[33:], "big")

            y_guessed = compute_y_from_x(x, positive_y)

            self.assertEqual(y, y_guessed)

    def test_inv(self):
        """Tests the Extended Euclidean Algorithm."""
        i = 0
        for num, mod, inv_num in self.TEST_INV:
            with self.subTest(i=i):
                inv_guessed = inv(num, mod)
                self.assertEqual(inv_num, inv_guessed)
            i += 1

    def test_jacobian_double_defaults(self):
        """."""
        p = (random.randint(1, 9), 0, random.randint(1, 9))
        res = jacobian_double(p)
        self.assertEqual((0, 0, 0), res)

    def test_jacobian_add_defaults(self):
        w, x, y, z = 1, 2, 3, 4
        # not p[1]
        p, q = (x, 0, z), (x, y, z)
        res = jacobian_add(p, q)
        self.assertEqual(q, res)
        # not q[1]
        p, q = (x, y, z), (x, 0, z)
        res = jacobian_add(p, q)
        self.assertEqual(p, res)
        # U1 == U2 and S1 != S2
        p, q = (x, y, z), (x, w, z)
        res = jacobian_add(p, q)
        self.assertEqual((0, 0, 1), res)
        # U1 == U2 and S1 == S2
        p, q = (x, y, z), (x, y, z)
        res = jacobian_add(p, q)
        self.assertEqual(jacobian_double(p), res)

    def test_jacobian_multiply_defaults(self):
        x, y, z = 1, 2, 3
        # p[1] == 0
        p, n = (x, 0, z), random.randint(1, 9)
        res = jacobian_multiply(p, n)
        self.assertEqual((0, 0, 1), res)
        # n == 0
        p, n = (x, y, z), 0
        res = jacobian_multiply(p, n)
        self.assertEqual((0, 0, 1), res)
        # n == curve.n
        p = (x, y, z)
        n = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141
        res = jacobian_multiply(p, n)
        self.assertEqual(jacobian_multiply(p, 0), res)
        # n > curve.n
        p = (x, y, z)
        n = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364143
        res = jacobian_multiply(p, n)
        self.assertEqual(jacobian_multiply(p, 2), res)
        # n < 0
        p, n = (x, y, z), -1
        m = 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364140
        res = jacobian_multiply(p, n)
        self.assertEqual(jacobian_multiply(p, m), res)

    def test_fast_add(self):
        p, q = (2, 3), (4, 5)
        p_j, q_j = (2, 3, 1), (4, 5, 1)

        res = fast_add(p, q)
        res_j = jacobian_add(p_j, q_j)

        self.assertEqual(res, from_jacobian(res_j))
