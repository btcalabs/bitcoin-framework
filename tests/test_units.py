"""Tests units submodule."""
# Relative imports
from .model import JSONTestModel

# Module to test
from bitcoin import btc_to_satoshi, satoshi_to_btc, satoshi_to_bit, \
    satoshi_to_mbitcoin


class TestUnitsValid(JSONTestModel):
    """Test the bitcoin units submodule with valid test cases.

    Test cases are formatted as a list of tuples in the following format:

    ``(bitcoins_str, bitcoins_float, bitcoins_int, satoshis_int)``

    If no ``bitcoins_int`` can be used because it has decimals, use ``null``
    """
    def test_btc_to_satoshi(self):
        """Tests the bitcoins to satoshis units conversion function."""
        for bitcoins_str, bitcoins_float, bitcoins_int, mbtc_str, bits_str, \
                satoshis in self._data:
            with self.subTest(
                    "bitcoins: %s, satoshis: %d" % (bitcoins_str, satoshis)):
                # Using a string
                self.assertEqual(satoshis, btc_to_satoshi(bitcoins_str))
                # Using a float
                self.assertEqual(satoshis, btc_to_satoshi(bitcoins_float))
                # Using an int (if possible)
                if bitcoins_int is not None:
                    self.assertEqual(satoshis, btc_to_satoshi(bitcoins_int))

    def test_satoshi_to_btc(self):
        """Tests the satoshi to bitcoins units conversion function."""
        for bitcoins_str, bitcoins_float, bitcoins_int, mbtc_str, bits_str, \
                satoshis in self._data:
            with self.subTest(
                    "bitcoins: %s, satoshis: %d" % (bitcoins_str, satoshis)):
                # Using str (the only return type)
                self.assertEqual(bitcoins_str, satoshi_to_btc(satoshis))

    def test_satoshi_to_bits(self):
        for bitcoins_str, bitcoins_float, bitcoins_int, mbtc_str, bits_str, \
                satoshis in self._data:
            with self.subTest(
                    "bits: %s, satoshis: %d" % (bits_str, satoshis)):
                # Using str (the only return type)
                self.assertEqual(bits_str, satoshi_to_bit(satoshis))

    def test_satoshi_to_mbitcoin(self):
        for bitcoins_str, bitcoins_float, bitcoins_int, mbtc_str, bits_str, \
                satoshis in self._data:
            with self.subTest(
                    "bits: %s, satoshis: %d" % (mbtc_str, satoshis)):
                # Using str (the only return type)
                self.assertEqual(mbtc_str, satoshi_to_mbitcoin(satoshis))


class TestUnitsInvalid(JSONTestModel):
    """Tests the bitcoin units submodule with invalid test cases.

    Test cases are formatted as a list of tuples in the following format:

    ``(bitcoins, exception)``

    Just the method :py:meth:`bitcoin.btc_to_satoshi` will be tested as is the
    only that raises errors.
    """
    def test_btc_to_satoshi(self):
        for bitcoins, error_str in self._data:
            with self.subTest("bitcoins: %s, error: %s"
                              % (bitcoins, error_str)):
                error = self.convert_error(error_str)
                with self.assertRaises(error):
                    btc_to_satoshi(bitcoins)
