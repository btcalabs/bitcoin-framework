"""
Tests the field general file with some silly tests
"""
# Classes to test
from bitcoin import ScriptData, ScriptNum

# Relative imports
from .model import JSONTestModel


class TestFieldScriptDataValid(JSONTestModel):
    """Test the ScriptData script's field with valid tests cases.

    Test cases format is the following:

    ``(data, data_repeat, pushdata_opcodes)``

    Where ``repeat`` means how many times to repeat previous field (data).

    Data to serialize is constructed using:
        data * data_repeat

    The serialization is constructed using
        pushdata_opcodes + data * data_repeat
    """

    @classmethod
    def get_test_vars(cls, test_case):
        """Returns the test variables as a tuple given a test case.

        Tuple in the following format:
            (data, serialization)
        """
        # Get vars
        data, data_repeat, opcodes = test_case

        # Convert
        data = bytes.fromhex(data)
        opcodes = bytes.fromhex(opcodes)

        # Repeat and merge
        return data * data_repeat, opcodes + data * data_repeat

    @classmethod
    def get_test_name(cls, test_case):
        """Returns the name of the specific test case."""
        return "ScriptData({}*{})".format(*test_case[:2])

    def test_serialize(self):
        """Tests valid ScriptData serializations and eval."""
        for test_case in self._data:
            data, serialization = self.get_test_vars(test_case)
            with self.subTest(self.get_test_name(test_case)):
                script_data = ScriptData(data)
                self.assertEqual(serialization.hex(),
                                 script_data.hex())
                self.assertEqual(script_data, eval(repr(script_data)))

    def test_deserialize(self):
        """Test valid ScriptData deserializations and properties."""
        for test_case in self._data:
            data, serialization = self.get_test_vars(test_case)
            with self.subTest(self.get_test_name(test_case)):
                script_data = ScriptData.deserialize(serialization)
                self.assertEqual(data.hex(), script_data.value.hex())
                self.assertEqual(data.hex(), script_data.data.hex())
                self.assertEqual("{} <{}>".format(
                    str(script_data.opcode), data.hex()), str(script_data))


class TestFieldScriptDataInvalid(JSONTestModel):
    """Tests the ScriptData's field with invalid test cases.

    JSON test cases are for deserialize only. Format is:
        (serialization, error)
    """

    def test_serialize(self):
        """Test invalid ScriptData serializations."""
        try:
            test_cases = (b'', bytes(4294967296))
        except MemoryError:
            return

        for data in test_cases:
            with self.subTest(data[:32].hex()):
                with self.assertRaises(ValueError):
                    ScriptData(data)

    def test_deserialize(self):
        """Test invalid ScriptData deserializations."""
        for serialization, error in self._data:
            with self.subTest(serialization):
                serialization = bytes.fromhex(serialization)
                error = self.convert_error(error)
                with self.assertRaises(error):
                    ScriptData.deserialize(serialization)


class TestFieldScriptNumValid(JSONTestModel):
    """Tests the ScriptNum field.

    Format of the test case is:
        (number, serialization)
    """

    def test_serialize(self):
        """Tests valid ScriptNum serializations."""
        for number, serialization in self._data:
            with self.subTest(str(number)):
                script_num = ScriptNum(number)
                self.assertEqual(serialization, script_num.hex())

    def test_deserialize(self):
        """Tests valid ScriptNum deserializations."""
        for number, serialization in self._data:
            with self.subTest(str(number)):
                # Test variables
                serialization = bytes.fromhex(serialization)

                # Do test
                script_num = ScriptNum.deserialize(serialization)
                self.assertEqual(number, script_num.value)
