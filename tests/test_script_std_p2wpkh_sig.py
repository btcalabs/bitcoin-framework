"""Tests the P2WPKHScriptSig."""
# TODO(ccebrecos): test signature and full data script

# Libraries
import unittest

from bitcoin import P2WPKHScriptSig


class TestP2WPKHScriptSig(unittest.TestCase):
    """Tests the :py:class:`P2WPKHScriptSig`."""

    def test_empty(self):
        """Tests the serialization and properties for an empty
        P2WPKHScriptSig."""
        script = P2WPKHScriptSig()
        self.assertEqual(0, len(script))
        self.assertEqual(list(), script.data)
        self.assertEqual(False, script.is_signed)
        self.assertEqual(None, script.signature)
        self.assertEqual(None, script.public_key)

    def test_serialize(self):
        ...
