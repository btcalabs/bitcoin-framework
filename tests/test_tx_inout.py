"""Tests the transaction inputs and outputs linking features.

This features allow the transaction inputs and outputs to have a reference
to the transaction they belong to that can by dynamically updated so both
the transaction and the input / output are notified about the change.

Also controls a reference to the script they contain so the script can know
they input or output they belong to. The reference can by dynamically
updated in the same way as the parent transaction link.
"""
# Libraries
import unittest

from bitcoin import TxInputOutput, Tx, TransactionScript


class TestTxInputOutput(unittest.TestCase):
    """Tests the ``TxInputOutput`` reference handlers.

    We use a dummy transaction and script for this purpose.
    """
    class MockTxInputOutput(TxInputOutput):
        def serialize(self) -> bytes:
            return bytes

        def deserialize(self, data: bytes) -> "MockTxInputOutput":  # noqa:F821
            return self

    # Test references
    # 1. Transaction references
    def test_tx_reference_set(self):
        """Tests setting the parent _tx_ reference works."""
        # Create tx and inout
        tx = Tx()
        inout = self.MockTxInputOutput(TransactionScript())
        # Assign parent tx
        inout.tx = tx
        # Test assignment
        self.assertEqual(tx, inout.tx)

    # 2. Script reference handling
    def test_script_reference_set(self):
        """Tests setting the contained _script_ reference works."""
        return
        # Create tx and script
        inout = self.MockTxInputOutput()
        script = TransactionScript()
        # Assign contained script
        inout.script = script
        # Test assignment
        self.assertEqual(script, inout.script)

    def test_script_reference_unset(self):
        """Tests unsetting the contained _script_ updates its parent."""
        def inout_script():
            """Creates the test case elements"""
            # Create tx and script
            new_inout = self.MockTxInputOutput(TransactionScript())
            new_script = TransactionScript()
            # Assign contained script
            new_inout.script = new_script
            return new_inout, new_script

        # Unassign script and test
        for new_assignment in [None, TransactionScript()]:
            # Generate tx and script (with script belonging to inout)
            inout, script = inout_script()
            if new_assignment is not None:
                # Assign to new object
                inout.script = new_assignment
                # Assert is in the new assignment if it is a new script
                self.assertEqual(new_assignment, inout.script)
                self.assertIsNone(script._parent)

    def test_assignment(self):
        # Test variables
        script = TransactionScript()
        inout_prev = self.MockTxInputOutput(script)  # noqa: F841

        with self.assertRaises(ValueError):
            self.MockTxInputOutput(script)
