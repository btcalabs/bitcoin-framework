"""Tests if the signature data of a witness signature is done properly.

**Sources:**
- https://github.com/bitcoin/bips/blob/master/bip-0143.mediawiki
"""

# Libraries
import unittest

# Classes to test
from bitcoin import ECDSAPublicKey, ECDSASignature, HashType, HashTypeValue, \
    ScriptPubKey, SegWitSignatureData, Tx, TxInput, TxOutput, btc_to_satoshi

# Relative imports
from .model import JSONTestModel


class TestWitnessSignatureDataByMessage(unittest.TestCase):
    """Tests the SegWit signature data serializer.

    Tests with cases where we know the signature message to be signed and not
    just its hash so we can debug where is failing exactly.

    **Sources:**
    - https://github.com/bitcoin/bips/blob/master/bip-0143.mediawiki
    """
    TEST_CASES = [(
        "0100000002fff7f7881a8099afa6940d42d1e7f6362bec38171ea3edf433541db4e"
        "4ad969f0000000000eeffffffef51e1b804cc89d182d279655c3aa89e815b1b309f"
        "e287d9b2b55d57b90ec68a0100000000ffffffff02202cb206000000001976a9148"
        "280b37df378db99f66f85c95a783a76ac7a6d5988ac9093510d000000001976a914"
        "3bde42dbee7e4dbe6a21b2d50ce2f0167faa815988ac11000000",
        1,
        TxOutput(btc_to_satoshi(6), ScriptPubKey.deserialize(bytes.fromhex(
            "00141d0f172a0ecb48aee1be1f2687d2963ae33f71a1"))),
        HashType(HashTypeValue.ALL),
        "0100000096b827c8483d4e9b96712b6713a7b68d6e8003a781feba36c31143470b4"
        "efd3752b0a642eea2fb7ae638c36f6252b6750293dbe574a806984b8e4d8548339a"
        "3bef51e1b804cc89d182d279655c3aa89e815b1b309fe287d9b2b55d57b90ec68a0"
        "10000001976a9141d0f172a0ecb48aee1be1f2687d2963ae33f71a188ac0046c323"
        "00000000ffffffff863ef3e1a92afbfdb97f31ad0fc7683ee943e9abcf2501590ff"
        "8f6551f47e5e51100000001000000",
        "c37af31116d1b27caf68aae9e3ac82f1477929014d5b917657d0eb49478cb670"
    )]
    """
        tuple: Test cases in the following format:.
               ["rawtx", input_to_sign_index, utxo, hashtype, "msg_hex",
               "hash_hex"]
    """

    @classmethod
    def compare_results(cls, valid: bytes, guess: bytes):
        """Compares two signature datas side by side."""
        print()
        str_format = "{:^64} | {:^64}"

        def print_vals(offset, length):
            print(str_format.format(
                valid[offset:offset + length].hex(),
                guess[offset:offset + length].hex()
            ))

        print(str_format.format("REAL", "GUESS"))
        lengths = [4, 32, 32, 32, 4, 26, 8, 4, 32, 4, 4]
        offset = 0
        for length in lengths:
            print_vals(offset, length)
            offset += length

    def test_msg(self):
        """Tests that signature message data is created properly."""
        for rawtx, input_n, utxo, hashtype, msg, msg_hash in self.TEST_CASES:
            # Transaction variables
            tx = Tx.deserialize(bytes.fromhex(rawtx))
            data_creator = SegWitSignatureData(tx, input_n, utxo, hashtype)
            self.assertEqual(msg, data_creator.msg().hex())
            self.assertEqual(msg_hash, data_creator.signable_msg().hex())

    def test_invalids(self):
        utxo = TxOutput(0, ScriptPubKey())
        tx = Tx(
            inputs=[TxInput(bytes(32), 0)],
            outputs=[TxOutput(12345, ScriptPubKey())]
        )
        with self.assertRaises(ValueError):
            SegWitSignatureData(tx, 0, utxo)

        utxo = TxOutput(12345, ScriptPubKey())
        with self.assertRaises(ValueError):
            SegWitSignatureData(tx, 0, utxo).msg()


class TestTxSignaturedataWitness(JSONTestModel):
    """Tests SegWit signature messages serializations (BIP 173).

    Uses a signature and a public key to determine if the produced hash that is
    being signed is correct.

    Tests cases are in the following format:
        ["public_key_list", "rawtx_signed", "utxo_script_list",
        utxo_satoshis_value_list]

        All the items in the list are in the same order they appear in the
        transactions.
    Just tests the first signature found in P2PK, P2PKH signed transactions.
    """

    def test_signable_msg(self):
        """Tests the signature in a transaction is valid."""
        test_n = 0
        for public_key_list, rawtx_signed, utxo_script_list, \
                utxo_satoshis_list, hashtype_asm in self._data:
            # Test variables
            tx = Tx.from_hex(rawtx_signed)

            # Loop inputs
            for i in range(len(tx.inputs)):
                # Test variables
                public_key = ECDSAPublicKey.from_hex(public_key_list[i])
                utxo = TxOutput(
                    btc_to_satoshi(utxo_satoshis_list[i]),
                    ScriptPubKey.from_hex(utxo_script_list[i])
                )

                with self.subTest("Test[{}].inputs[{}]:{}]".format(
                        test_n, i, hashtype_asm)):
                    # Take input
                    signature_hashtype = tx.inputs[i].script.witness.items[0]

                    # Split signature and hashtype
                    signature, hashtype = signature_hashtype[:-1], \
                        signature_hashtype[-1]
                    signature = ECDSASignature.deserialize(signature)
                    hashtype = HashType.from_integer(hashtype)

                    # Check hashtype extracted
                    self.assertEqual(hashtype_asm, str(hashtype))

                    # Create signature data
                    signature_creator = SegWitSignatureData(
                        tx, i, utxo, hashtype)
                    msg = signature_creator.signable_msg()

                    # Check verifies
                    self.assertTrue(
                        public_key.verify_signature(msg, signature))
            # Next test
            test_n += 1
