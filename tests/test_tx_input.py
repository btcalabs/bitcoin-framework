"""Tests the transaction input model."""

# Libraries
from unittest import TestCase

# Classes to test
from bitcoin import ScriptSig, TxInput, TxOutpoint, Witness

# Relative imports
from .model import JSONTestModel


class TestTxInputValid(JSONTestModel):

    def test_serialize(self):
        for test in self._data:
            # Test variables
            utxo_id = test["prev_txid"]
            utxo_n = test["prev_outn"]
            script = test["script"]
            sequence = test["sequence"]
            serialization = test["serialization"]

            # Test conversions
            utxo_id = bytes.fromhex(utxo_id)
            script = ScriptSig.deserialize(bytes.fromhex(script))

            # Create object
            txin = TxInput(utxo_id, utxo_n, script, sequence)

            # Test serialize
            self.assertEqual(serialization, txin.hex())

    def test_deserialize(self):
        for test in self._data:
            # Test variables
            utxo_id = test["prev_txid"]
            utxo_n = test["prev_outn"]
            script = test["script"]
            sequence = test["sequence"]
            serialization = test["serialization"]

            # Test conversions
            utxo_id = bytes.fromhex(utxo_id)
            script = ScriptSig.deserialize(bytes.fromhex(script))
            serialization = bytes.fromhex(serialization)

            # Create object
            txin = TxInput.deserialize(serialization)

            # Test deserialize
            self.assertEqual(utxo_id, txin.utxo_id)
            self.assertEqual(utxo_n, txin.utxo_n)
            self.assertEqual(script, txin.script)
            self.assertEqual(sequence, txin.sequence)

    def test_witness(self):
        utxo_id = bytes(32)
        utxo_n = 0

        # With a witness script
        wss = ScriptSig([], [bytes(20)])
        txin = TxInput(utxo_id, utxo_n, wss)
        self.assertEqual(True, txin.has_witness)
        self.assertEqual(wss.witness, txin.witness)

        # Without a witness script
        ss = ScriptSig()
        witness = Witness()
        txin = TxInput(utxo_id, utxo_n, ss)
        self.assertEqual(False, txin.has_witness)
        self.assertEqual(witness, txin.witness)

    def test_sequence(self):
        utxo_id = bytes(32)
        utxo_n = 0
        new_sequence = 4294967294

        txin = TxInput(utxo_id, utxo_n)
        txin.sequence = new_sequence

        self.assertEqual(new_sequence, txin.sequence)


class TestTxOutpoint(TestCase):
    def test_properties(self):
        utxo_id = bytes(32)
        utxo_n = 0

        outpoint_hex = "0000000000000000000000000000000000000000000000000000" \
                       "00000000000000000000"

        txin = TxInput(utxo_id, utxo_n)
        self.assertEqual(txin.outpoint.hex(), outpoint_hex)

    def test_invalid(self):
        # Invalid length for a Transaction ID, sha256 is 32 bytes long
        with self.assertRaises(ValueError):
            TxOutpoint(bytes(31), 0)

        # Negative index
        with self.assertRaises(ValueError):
            TxOutpoint(bytes(32), -3)

        # Not enough bytes to deserialize; needed: 36
        with self.assertRaises(ValueError):
            TxOutpoint.deserialize(bytes(35))


class TestTxInputInvalid(TestCase):
    def test_deserialize(self):
        # Invalid byte length (minimum)
        with self.assertRaises(ValueError):
            TxInput.deserialize(bytes(40))
