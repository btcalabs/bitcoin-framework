"""Defines a model for unittest test cases able to handle JSON data."""

# Libraries
from collections import OrderedDict
import json
import os
import re
import unittest
from typing import Union, Type

# Testing attributes
import bitcoin
from bitcoin import MAINNET, TESTNET, REGTEST, BlockchainParams, \
    StdSHLengthError, StdPKHLengthError, AddressLengthError, \
    AddressTypeError, AddressPrefixError, AddressChecksumError, \
    AddressHRPError, StdSegWitScriptPubKeyProgramError, \
    StdSegWitScriptPubKeyVersionError, UnitsFormatError, UnitsPrecisionError, \
    DecodingAlphabetError, DecodingChecksumError, DecodingLengthError, \
    DecodingUnknownError, Bech32VersionError
# TODO: Dynamically import it

# Constants
_PACKAGE_ATTRIBUTES = {name: getattr(bitcoin, name) for name in dir(bitcoin)}
"""
    Dict[str, object]: Map of all available attributes in the ``bitcoin``
    package by its attribute name.
"""


class TestModel(unittest.TestCase):
    """Extended `unittest` with added features to test the framework."""
    ERRORS = ValueError, StdSHLengthError, StdPKHLengthError, \
        AddressLengthError, AddressTypeError, AddressPrefixError, \
        AddressChecksumError, AddressHRPError, \
        StdSegWitScriptPubKeyVersionError, StdSegWitScriptPubKeyProgramError, \
        UnitsFormatError, UnitsPrecisionError, DecodingAlphabetError, \
        DecodingChecksumError, DecodingLengthError, DecodingUnknownError, \
        Bech32VersionError
    """
        List[ValueError]: errors that can occur around the app and are being
        tested.
    """
    ERRORS_NAMES = [e.__name__ for e in ERRORS]
    """
        List[str]: list of names of the errors in the previous constant.
    """

    @classmethod
    def convert_network(cls, network: str) -> BlockchainParams:
        """Converts a string to a network object."""
        if network == "mainnet":
            network_object = MAINNET
        elif network == "testnet":
            network_object = TESTNET
        elif network == "regtest":
            network_object = REGTEST
        else:
            raise ValueError("Invalid network \"%s\"" % network)
        return network_object

    @classmethod
    def convert_error(cls, error: str) -> Exception:
        """Converts an error name string into an error class.

        Uses the defined list of accepted errors in the :py:attr:ERRORS
        constant.
        """
        return cls.ERRORS[cls.ERRORS_NAMES.index(error)]

    @classmethod
    def eval(cls, code: str) -> Union[object, Type[object]]:
        """Evaluates the code and returns its evaluation result.

        Makes sure that everything in ``bitcoin`` package is imported.
        Useful to test `__repr__` methods.
        """
        return eval(code, _PACKAGE_ATTRIBUTES)


class JSONTestModel(TestModel):
    """Models a ``unittest`` with test cases loaded from a JSON file.

    Attributes:
        _data (dict): data as a dictionary obtained from the JSON file
    """
    JSON_FOLDER = os.path.join(".", "tests", "data")
    """
        str: folder where JSON data files are present
    """
    JSON_FILE = None
    """
        str: file where JSON data to be loaded is found
    """
    TEST_PREFIX = "Test"
    """
        str: prefix every test case class will have to calculate automagically
    the JSON file to load
    """

    @classmethod
    def camel_to_snake(cls, name):
        """Converts CamelCase to snake_case notation.

        Helps getting the JSON file through the class name.
        """
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    @classmethod
    def json_file(cls):
        """Returns the JSON file name to be loaded.

        Uses as a default the name of the class in snake case plus the ".json"
        extension and without the prefix :py:attr:`TEST_PREFIX`.

        If not, the :py:attr:`JSON_FILE` class constant will be used
        """
        if cls.JSON_FILE is not None:
            # Defined filename
            filename = cls.JSON_FILE
        else:
            # Obtain from its name
            filename = cls.__name__
            # # Replace "Test" prefix
            if filename.startswith(cls.TEST_PREFIX):
                filename = filename[len(cls.TEST_PREFIX):]
            # # Convert to snake case
            filename = cls.camel_to_snake(filename)
            # # Add extension
            filename = filename + ".json"

        return filename

    def setUp(self):
        """Loads the JSON file to the :py:attr:`_data` attribute."""
        with open(os.path.join(self.JSON_FOLDER, self.json_file()), "r") as f:
            self._data = json.load(f, object_pairs_hook=OrderedDict)
