"""Tests WIF addresses."""
from .model import JSONTestModel

from bitcoin import WIFAddress, ECDSAPrivateKey


class TestAddressWIF(JSONTestModel):
    """Tests WIF adresses.

    Test format is:
        [privkey_n, "network", "prefix_hex", "checksum_hex", "pubkey_hex",
        compressed, "type_byte_hex", "address_encoded"]
    """
    def test_encode(self):
        """Test encoding WIF addresses."""
        for privkey, network, prefix, checksum, pubkey, compressed, \
                type_byte, address_encoded in self._data:
            # Test variables
            privkey = ECDSAPrivateKey(privkey)
            network = self.convert_network(network)

            # Test serialize
            self.assertEqual(
                address_encoded,
                WIFAddress(privkey, compressed, network).encode())

    def test_decode(self):
        """Tests decoding WIF addresses."""
        for privkey, network, prefix, checksum, pubkey, compressed, \
                type_byte, address_encoded in self._data:
            # Test variables
            privkey = ECDSAPrivateKey(privkey)
            network = self.convert_network(network)
            type_byte = bytes.fromhex(type_byte)

            # Test deserialize
            address = WIFAddress.decode(address_encoded)

            # # Test properties
            self.assertEqual(network, address.network)
            self.assertEqual(prefix, address.prefix.hex())
            self.assertEqual(checksum, address.checksum.hex())
            self.assertEqual(privkey, address.private_key)
            self.assertEqual(type_byte, address.type_byte)
            self.assertEqual(
                privkey.hex() + type_byte.hex(),
                address.data.hex())
            self.assertEqual(pubkey, address.public_key.hex())
            self.assertEqual(compressed, address.is_compressed)

            # # Test encoding
            self.assertEqual(address_encoded, address.encode())
