
# Libraries
import unittest

from .model import JSONTestModel

# Classes to test
from bitcoin import ECDSASignature, ECDSAPublicKey


class TestCryptoECDSASignatureValid(JSONTestModel):

    def test_serialize(self):
        i = 0
        for test in self._data:
            with self.subTest(i=i):
                serialization = test["der_serialized"]
                r, s = test["R"], test["S"]
                sig = ECDSASignature(r, s)
                self.assertEqual(serialization, sig.hex())

    def test_deserialize(self):
        i = 0
        for test in self._data:
            with self.subTest(i=i):
                serialization = test["der_serialized"]
                r, s = test["R"], test["S"]
                sig = ECDSASignature.from_hex(serialization)
                self.assertEqual(r, sig.r)
                self.assertEqual(s, sig.s)


class TestCryptoECDSASignatureInvalid(JSONTestModel):

    def test_deserialize(self):
        i = 0
        for test in self._data["deserialize"]:
            with self.subTest(i=i):
                with self.assertRaises(self.convert_error(test["error"])):
                    ECDSASignature.from_hex(test["der_serialized"])

    def test_serialize(self):
        i = 0
        for test in self._data["serialize"]:
            with self.subTest(i=i):
                with self.assertRaises(self.convert_error(test["error"])):
                    ECDSASignature(test["R"], test["S"])


class TestCryptoECDSASignatureVerification(unittest.TestCase):

    SIGNATURE_VERIFY = [
        ("71c9cd9b2869b9c70b01b1f0360c148f42dee72297db312638df136f43311f23",
         "30450220487fb382c4974de3f7d834c1b617fe15860828c7f96454490edd6d8915"
         "56dcc9022100baf95feb48f845d5bfc9882eb6aeefa1bc3790e39f59eaa46ff7f1"
         "5ae626c53e",
         "02a9781d66b61fb5a7ef00ac5ad5bc6ffc78be7b44a566e3c87870e1079368df4c",
         True),
        ("71c9cd9b2869b9c70b01b1f0360c148f42dee72297db312638df136f43311f23",
         "30450220487fb382c4974de3f7d834c1b617fe15860828c7f96454490edd6d8915"
         "56dcc9022100baf95feb48f845d5bfc9882eb6aeefa1bc3790e39f59eaa46ff7f1"
         "5ae626c53e",
         "02c2d6ba78450225c577d370c03d90f86e03c983ffd602056ece211c7b33917866",
         False),
    ]
    """
        (msg to sign, signature, public key, expected result)
    """

    def test_verify_signature(self):
        i = 0
        for msg, signature, public, expected in self.SIGNATURE_VERIFY:
            with self.subTest(i=i):
                pubkey = ECDSAPublicKey.deserialize(bytes.fromhex(public))
                sig = ECDSASignature.deserialize(bytes.fromhex(signature))
                res = pubkey.verify_signature(bytes.fromhex(msg), sig)
                self.assertEqual(expected, res)
            i += 1
