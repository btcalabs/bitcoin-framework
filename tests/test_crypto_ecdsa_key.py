"""Tests for the ECDSAPublicKey and ECDSAPrivateKey."""

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import ECDSAPublicKey, ECDSAPrivateKey


class TestCryptoECDSAKeyValid(JSONTestModel):
    """Tests the ECDSA*Key classes with test cases with valid values."""
    def test_public_deserialize(self):
        """Tests the deserialization of the ECDSAPublicKey class."""
        for test in self._data['public']:
            with self.subTest(test['compressed']):
                # Test variables
                x = test['x']
                y = test['y']
                compressed = test['compressed']
                uncompressed = test['uncompressed']
                # Test conversions
                uncompressed = bytes.fromhex(uncompressed)
                compressed = bytes.fromhex(compressed)

                # Create object
                # from uncompresed
                pubkey = ECDSAPublicKey.deserialize(uncompressed)
                # Checks
                self.assertEqual(uncompressed, pubkey.uncompressed)
                self.assertEqual(compressed, pubkey.compressed)
                self.assertEqual((x, y), pubkey.point)

                # from compresed
                pubkey = ECDSAPublicKey.deserialize(compressed)
                # Checks
                self.assertEqual(uncompressed, pubkey.uncompressed)
                self.assertEqual(compressed, pubkey.compressed)
                self.assertEqual((x, y), pubkey.point)

    def test_public_serialize(self):
        """Tests the serialization of the ECDSAPublicKey class."""
        for test in self._data['public']:
            with self.subTest(test['compressed']):
                # Test variables
                x = test['x']
                y = test['y']
                compressed = test['compressed']
                uncompressed = test['uncompressed']
                # Test conversions
                uncompressed = bytes.fromhex(uncompressed)
                compressed = bytes.fromhex(compressed)

                pubkey_uncompressed = ECDSAPublicKey(x, y, False)
                pubkey_compressed = ECDSAPublicKey(x, y)

                # Checks
                self.assertEqual(uncompressed, pubkey_uncompressed.serialize())
                self.assertEqual(False,
                                 pubkey_uncompressed.is_compressed_by_default)
                self.assertEqual(compressed, pubkey_compressed.serialize())
                self.assertEqual(True,
                                 pubkey_compressed.is_compressed_by_default)

    def test_private_deserialize(self):
        """Tests the deserialization of the ECDSAPrivateKey class."""
        for test in self._data['private']:
            with self.subTest(test['serialized']):
                # Test variables
                serialized = test['serialized']
                exponent = test['exponent']

                # Test conversions
                serialized = bytes.fromhex(serialized)

                # Create object
                sk = ECDSAPrivateKey.deserialize(serialized)
                # Checks
                self.assertEqual(exponent, sk.value)

    def test_private_serialize(self):
        """Tests the serialization of the ECDSAPrivateKey class."""
        for test in self._data['private']:
            with self.subTest(test['serialized']):
                # Test variables
                serialized = test['serialized']
                exponent = test['exponent']

                # Test conversions
                serialized = bytes.fromhex(serialized)

                # Create object
                sk = ECDSAPrivateKey(exponent)
                # Checks
                self.assertEqual(serialized, sk.serialize())

    def test_private_to_public(self):
        """Tests ECDSAPublicKey retrieval from ECDSAPrivateKey."""
        for test in self._data['private']:
            with self.subTest(test['serialized']):
                # Test variables
                exponent = test['exponent']
                public = test['public']

                # Create object
                sk = ECDSAPrivateKey(exponent)
                pk = sk.to_public()
                # Checks
                self.assertEqual(public, pk.hex())

    def test_private_sign(self):
        """Tests the signature creation from ECDSAPrivateKey."""
        for test in self._data['signature']:
            with self.subTest(test['signature']):
                # Test variables
                message = test['message']
                signature = test['signature']
                exponent = test['exponent']

                # Test conversions
                message = bytes.fromhex(message)

                sk = ECDSAPrivateKey(exponent)
                signature_guessed = sk.sign(message)
                self.assertEqual(signature, signature_guessed.hex())


class TestCryptoECDSAKeyInvalid(JSONTestModel):
    """Tests the ECDSA*Key classes with test cases with invalid values."""
    def test_public_deserialize(self):
        """Tests the ECDSAPublicKey deserialization with invalid test cases."""
        for test in self._data['public']:
            with self.subTest(test):
                with self.assertRaises(ValueError):
                    ECDSAPublicKey.deserialize(bytes.fromhex(test))

    def test_private_creation(self):
        """Tests the ECDSAPrivateKey creation with invalid test cases."""
        for test in self._data['private']:
            with self.subTest(test):
                with self.assertRaises(ValueError):
                    ECDSAPrivateKey(test)
