"""Tests the Field model methods."""
# Libraries
import unittest

# Relative imports
from bitcoin.field import model


class TestFieldModel(unittest.TestCase):

    def test_value_getter(self):
        """Tests the getter for the value property."""
        value_to_put = 'abc'
        var = model.Field(value_to_put)
        self.assertEqual(value_to_put, var.value)

    def test_value_setter(self):
        """Tests the setter (and getter) for the value property."""
        value_to_put = 'abc'
        var = model.Field()
        var.value = value_to_put
        self.assertEqual(value_to_put, var.value)
