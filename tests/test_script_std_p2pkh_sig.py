"""Tests the P2PKHScriptSig."""

# Libraries
import unittest

# Relative imports
from bitcoin import P2PKHScriptSig


class TestScriptStdP2PKHSig(unittest.TestCase):
    """Test for the ``P2PKHScriptSig``."""

    TEST_CASES = [
    ]
    """
    tests for P2PKHScriptSig, its format is as follows:
        (address, script serialization)
    """

    def test_empty(self):
        """Tests the serialization and properties for a P2PKHScriptPSig."""
        script = P2PKHScriptSig()
        self.assertEqual(False, script.is_signed)
        self.assertEqual(None, script.signature)
        self.assertEqual(None, script.public_key)
        self.assertEqual(None, script.hashtype)
        self.assertEqual(0, len(script))
        self.assertIsInstance(script.__str__(), str)
        self.assertEqual(bytes(), script.serialize())

    def test_signature(self):
        # TODO: test the signature
        pass
