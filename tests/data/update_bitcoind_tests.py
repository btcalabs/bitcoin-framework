# Updates bitcoind's test cases files
#
# Uses raw.githubusercontent.com to retrieve the test files

# Libraries
import http.client

# Constants
GITHUB_RAW_HOST = "raw.githubusercontent.com"
TEST_PREFIX = "bitcoind_"
BITCOIND_REPO = "bitcoin", "bitcoin"
BITCOIND_BRANCH = "master"
BITCOIND_TESTS_FOLDER = "/".join(("src", "test", "data"))
BITCOIND_TESTS = "sighash.json",


# Methods
def get_test_url(test):
    return "/" + "/".join(("/".join(BITCOIND_REPO), BITCOIND_BRANCH,
                          BITCOIND_TESTS_FOLDER, test))


def update_tests():
    connection = http.client.HTTPSConnection(GITHUB_RAW_HOST)
    for test in BITCOIND_TESTS:
        test_url = get_test_url(test)
        connection.request("GET", test_url)
        with open(TEST_PREFIX+test, "w") as f:
            f.write(connection.getresponse().read().decode())
            f.close()
    connection.close()


if __name__ == "__main__":
    update_tests()
