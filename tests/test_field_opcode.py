"""Tests the opcode fields module."""
# Libraries
import inspect
from unittest import TestCase

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import Opcode, OP_N, get_op_code_n, OP_PUSHDATA
from bitcoin.field import opcode as opcode_module
from bitcoin.field.opcode import *  # NOQA: F401, F403

# Constants
OPCODES = [o for n, o in inspect.getmembers(opcode_module)
           if inspect.isclass(o)
           and issubclass(o, opcode_module._SpecificOpcode)
           and not o == opcode_module._SpecificOpcode]
"""
    list: dynamically import all opcodes fields
"""
OPCODES_BY_NAME = {o.__name__: o for o in OPCODES}
OPCODES_BY_NAME[OP_PUSHDATA.__name__] = OP_PUSHDATA
"""
    dict: match each opcode class by its name
"""


class TestFieldOpcode(JSONTestModel):
    """Tests all the opcode fields and main module methods.

    Basically tests that each opcode value is the proper one as the Bitcoin
    protocol states.

    The JSON used to check the opcode codification values is directly obtained
    from the ``opcodetype`` enum in Bitcoin Core ``script.h``

    https://github.com/bitcoin/bitcoin/blob/master/src/script/script.h
    """
    def opcode_value(self, opcode_asm):
        """Returns the real opcode value given an opcode ASM.

        Resolves links in the enum.
        """
        opcode_value = self._data[opcode_asm]
        return int.from_bytes(bytes.fromhex(opcode_value[2:]), "big") \
            if opcode_value.startswith("0x") \
            else self.opcode_value(opcode_value)

    def test_exist(self):
        """Tests the available opcodes ASM exist with the specified names."""
        for opcode_class in OPCODES:
            with self.subTest(opcode_class.asm()):
                self.assertIn(opcode_class.asm(), self._data)

    def test_serialize(self):
        """Tests the available opcodes values are the correct ones."""
        for opcode_class in OPCODES:
            with self.subTest(opcode_class.asm()):
                # Check if fixed opcode
                self.assertEqual(self.opcode_value(opcode_class.asm()),
                                 opcode_class.opcode())

    def test_get_opcode_n(self):
        """Tests the get_opcode_n module method."""
        for i in range(len(OP_N)):
            with self.subTest("OP_%d" % i):
                op_n = OP_N[i]
                self.assertEqual(op_n(), get_op_code_n(i))

    def test_generic(self):
        """Tests the generic Opcode does not change behaviour."""
        opcode = Opcode(0xff)
        self.assertEqual(0xff, opcode.opcode())
        self.assertEqual("OP_UNKNOWN", opcode.asm())
        self.assertEqual("OP_UNKNOWN", str(opcode))
        self.assertEqual(opcode, eval(repr(opcode)))

    def test_specific(self):
        """Tests the specific opcode class behaves as expected."""
        mock_opcode = type("MockSpecificOpcode",
                           (opcode_module._SpecificOpcode,),
                           {"opcode": lambda:
                            opcode_module._SpecificOpcode.opcode()})
        self.assertEqual(0xff, mock_opcode.opcode())


class TestFieldOpcodeInvalid(TestCase):
    """Tests errors in the opcode module and Opcode class."""
    def test_opcode_init(self):
        """Tests opcode init with an invalid values.

        Integers that do not fit in a byte.
        """
        for data in (0x100, -1):
            with self.subTest(str(data)):
                with self.assertRaises(ValueError):
                    Opcode(data)

    def test_opcode_deserialize(self):
        """Tests opcode deserialization with invalid values.

        - Deserializing just OP_PUSHDATA[1,2,4] without any further data.
        - Deserializing empty bytes.
        """
        for data in (0x100, -1, bytes()):
            with self.subTest(str(data)):
                with self.assertRaises(ValueError):
                    Opcode.deserialize(data)

    def test_get_opcode_n(self):
        """Tests the get_opcode_n method with invalid n."""
        for n in (-1, 17):
            with self.subTest(str(n)):
                with self.assertRaises(ValueError):
                    get_op_code_n(n)


class TestFieldOpPushDataValid(TestCase):
    """Tests valid test cases with OP_PUSHDATA classes."""
    def test_serialize(self):
        """Tests constructing and serializing valid pushdata opcodes."""
        for amount in (0x01, 0x4b):
            with self.subTest("OP_PUSHDATA([])".format(amount)):
                opcode = OP_PUSHDATA(amount)
                self.assertEqual(amount, opcode.serialize()[0])
                self.assertEqual(opcode, eval(repr(opcode)))

    def test_deserialize(self):
        """Tests deserializing valid pushdata opcodes."""
        for amount in (0x01, 0x4b, b'\x01', b'\x4b'):
            with self.subTest("OP_PUSHDATA[{}]".format(amount)):
                # Do test
                opcode = OP_PUSHDATA.deserialize(amount)
                amount = amount[0] if isinstance(amount, bytes) else amount
                self.assertEqual(amount, opcode.amount)
                self.assertEqual("OP_PUSHDATA", opcode.asm())
                self.assertEqual("({}[{}])".format(OP_PUSHDATA.__name__,
                                                   amount), str(opcode))
                self.assertEqual(opcode, eval(repr(opcode)))


class TestOpPushDataInvalid(TestCase):
    """Tests invalid test cases with OP_PUSHDATA."""
    def test_serialize(self):
        """Tests serializing invalid pushdata opcodes."""
        for amount in (0x00, 0x4c):
            with self.subTest("OP_PUSHDATA[{}]".format(amount)):
                with self.assertRaises(ValueError):
                    OP_PUSHDATA(amount).serialize()
