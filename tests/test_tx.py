"""
Tests the Transaction model and methods
"""

# Libraries
import unittest

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import Tx, TxInput, TxOutput, ScriptSig, ScriptPubKey, \
    btc_to_satoshi, Witness
from bitcoin.tx.model import TRANSACTION_SIZE_MIN


class TestTx(unittest.TestCase):
    """Manually test the transaction class."""
    EMPTY_TX = {
        "serialization": "02000000000000000000",
        "txid":
            "4ebd325a4b394cff8c57e8317ccf5a8d0e2bdf1b8526f8aad6c8e43d8240621a"
    }
    """
        dict: Empty transaction test case
    """

    def test_empty(self):
        """Tests de default values for an empty initialized transaction."""
        tx = Tx()
        # Testing default properties
        self.assertEqual(2, tx.version)
        self.assertEqual(0, len(tx.inputs))
        self.assertEqual(0, len(tx.outputs))
        self.assertEqual(0, tx.locktime)
        self.assertEqual(self.EMPTY_TX["txid"], tx.id.hex())

    # VERSION
    def test_version_property(self):
        """Test the version setter and getter."""
        tx = Tx(version=2)
        # Default version expected
        self.assertEqual(2, tx.version)
        tx.version = 3
        self.assertEqual(3, tx.version)

    # INPUTS
    def test_inputs_by_constructor(self):
        """Tests inputs are constructed properly."""
        in0 = TxInput(bytes(32), 0)
        in1 = TxInput(b'\xff'*32, 0)
        inputs_list = (in0, in1)
        # Tx creation
        tx = Tx(inputs=inputs_list)
        # Default inputs
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)
        # Check relation between inputs and tx
        for inp in inputs_list:
            self.assertEqual(tx, inp._tx)

    def test_inputs_by_property(self):
        """Tests changing transaction inputs works."""
        in0 = TxInput(bytes(32), 0)
        in1 = TxInput(b'\xff'*32, 0)
        inputs_list = (in0, in1)
        # Tx creation
        tx = Tx()
        tx.inputs = inputs_list
        # Default inputs
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)
        # Check relation between inputs and tx
        for inp in inputs_list:
            self.assertEqual(tx, inp._tx)

    def test_inputs_add(self):
        """Tests adding inputs works."""
        in0 = TxInput(bytes(32), 0)
        in1 = TxInput(b'\xff'*32, 0)
        # Tx creation
        tx = Tx(inputs=[in0])
        inputs_list = [in0]
        # Checks
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)
        # Add method
        tx.add_input(in1)
        inputs_list.append(in1)
        # Checks
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)

    def test_inputs_remove(self):
        """Tests adding inputs works."""
        in0 = TxInput(bytes(32), 0)
        in1 = TxInput(b'\xff'*32, 0)
        # Tx creation
        tx = Tx(inputs=[in0, in1])
        inputs_list = [in0, in1]
        # Checks
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)
        # Remove method
        tx.remove_input(in1)
        inputs_list.remove(in1)
        # Checks
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)
        # Check again
        tx.remove_input(in1)
        self.assertEqual(len(inputs_list), len(tx.inputs))
        for a, b in zip(inputs_list, tx.inputs):
            self.assertEqual(a, b)

    def test_inputs_reassignment(self):
        """Tests reassigning inputs works."""
        in0 = TxInput(bytes.fromhex("00"*32), 0)
        in1 = TxInput(bytes.fromhex("11"*32), 0)
        # Tx creation
        tx = Tx(inputs=[in0])
        self.assertEqual(in0, tx.inputs[0])
        self.assertEqual(tx, in0._tx)
        # Add method
        tx.inputs = [in1]
        self.assertEqual(in1, tx.inputs[0])
        self.assertEqual(tx, in1._tx)
        self.assertIsNone(in0._tx)

    def test_inputs_error(self):
        """Tests cannot assign same input into more txs."""
        # Tx creation
        tx = Tx(inputs=[TxInput(bytes(32), 0)])
        # Add method
        with self.assertRaises(ValueError):
            Tx(inputs=[tx.inputs[0]])

    # OUTPUTS
    def test_outputs_by_constructor(self):
        """Tests outputs are constructed properly."""
        out0 = TxOutput(0, ScriptPubKey())
        out1 = TxOutput(1, ScriptPubKey())
        outputs_list = (out0, out1)
        # Tx creation
        tx = Tx(outputs=outputs_list)
        # Default outputs
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)
        # Check relation between inputs and tx
        for out in outputs_list:
            self.assertEqual(tx, out._tx)

    def test_outputs_by_property(self):
        """Tests outputs are set properly."""
        out0 = TxOutput(0, ScriptPubKey())
        out1 = TxOutput(1, ScriptPubKey())
        outputs_list = [out0, out1]
        # Tx creation
        tx = Tx()
        tx.outputs = outputs_list
        # Default inputs
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)
        # Check relation between inputs and tx
        for out in outputs_list:
            self.assertEqual(tx, out._tx)

    def test_outputs_add(self):
        """Tests outputs are added properly."""
        out0 = TxOutput(0, ScriptPubKey())
        out1 = TxOutput(1, ScriptPubKey())
        # Tx creation
        tx = Tx(outputs=[out0])
        outputs_list = [out0]
        # Checks
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)
        # Add method
        tx.add_output(out1)
        outputs_list.append(out1)
        # Checks
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)

    def test_outputs_remove(self):
        """Tests adding outputs works."""
        out0 = TxOutput(0, ScriptPubKey())
        out1 = TxOutput(1, ScriptPubKey())
        # Tx creation
        tx = Tx(outputs=[out0, out1])
        outputs_list = [out0, out1]
        # Checks
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)
        # Remove method
        tx.remove_output(out1)
        outputs_list.remove(out1)
        # Checks
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)
        # Check again
        tx.remove_output(out1)
        self.assertEqual(len(outputs_list), len(tx.outputs))
        for a, b in zip(outputs_list, tx.outputs):
            self.assertEqual(a, b)

    def test_outputs_reassignment(self):
        """Tests outputs are reassigned properly."""
        out0 = TxOutput(0, ScriptPubKey())
        out1 = TxOutput(1, ScriptPubKey())
        # Tx creation
        tx = Tx(outputs=[out0])
        self.assertEqual(out0, tx.outputs[0])
        self.assertEqual(tx, out0._tx)
        # Add method
        tx.outputs = [out1]
        self.assertEqual(out1, tx.outputs[0])
        self.assertEqual(tx, out1._tx)
        self.assertIsNone(out0._tx)

    def test_outputs_error(self):
        """Tests cannot assign same input into more txs."""
        # Tx creation
        tx = Tx(outputs=[TxOutput(0, ScriptPubKey())])
        # Add method
        with self.assertRaises(ValueError):
            Tx(outputs=[tx.outputs[0]])

    # LOCKTIME
    def test_locktime_property(self):
        """Test the locktime setter and getter."""
        tx = Tx(locktime=1000)
        # Default version expected
        self.assertEqual(1000, tx.locktime)
        tx.locktime = 50000
        self.assertEqual(50000, tx.locktime)


class TestTxValid(JSONTestModel):
    """Tests the transaction model.

    The format of the test case is
    {
        "src": "str" // (Optional) Source of the test case
        "bitcoin-cli": "str" // (Optional)  Args to create the tx using
                            // ``bitcoin-cli``
        "serialization": "hex" // Hex serialization of the transaction
        "tx": {} // JSON object containing the tx in the same format as
                 // Bitcoin Core `bitcoin-cli createrawtransaction`
    }
    """
    def test_deserialize(self):
        """Tests deserializing a bunch of legacy & SegWit valid txs."""
        for test in self._data:
            with self.subTest(test['tx']['hash']):
                # Test variables
                real_tx = test["tx"]

                # Create object
                guess_tx = Tx.from_hex(test['serialization'])

                # Checks
                # # Version
                self.assertEqual(real_tx['version'], guess_tx.version)

                # # Inputs
                self.assertEqual(len(real_tx['vin']), len(guess_tx.inputs))
                for i in range(len(real_tx['vin'])):
                    # Assignments
                    real_inp = real_tx['vin'][i]
                    guess_inp = guess_tx.inputs[i]
                    # Checks
                    self.assertEqual(real_inp['txid'], guess_inp.utxo_id.hex())
                    self.assertEqual(real_inp['vout'], guess_inp.utxo_n)
                    self.assertEqual(
                        real_inp['scriptSig']['hex'], guess_inp.script.hex())
                    self.assertEqual(real_inp['sequence'], guess_inp.sequence)

                # # Outputs
                self.assertEqual(len(real_tx['vout']), len(guess_tx.outputs))
                for i in range(len(real_tx['vout'])):
                    # Assignments
                    real_out = real_tx['vout'][i]
                    guess_out = guess_tx.outputs[i]
                    # Checks
                    self.assertEqual(str(real_out['value']), guess_out.btc)
                    self.assertEqual(
                        real_out['scriptPubKey']['hex'],
                        guess_out.script.hex())

                # # Locktime
                self.assertEqual(real_tx['locktime'], guess_tx.locktime)

                # # Has witness
                has_witness = any([
                    "txinwitness" in tx_in.keys()
                    and len(tx_in["txinwitness"]) != 0
                    for tx_in in real_tx["vin"]])
                self.assertEqual(has_witness, guess_tx.has_witness)

                # # ID and hash (wtxid if SegWit)
                self.assertEqual(real_tx['txid'], guess_tx.id.hex())
                self.assertEqual(real_tx['hash'], guess_tx.hash.hex())

                # # Transaction representation works
                self.assertIsInstance(str(guess_tx), str)

    def test_serialize(self):
        """Tests serializing a bunch of legacy & SegWit transactions."""
        for test in self._data:
            with self.subTest(test["tx"]["hash"]):
                # Test variables
                real_tx = test["tx"]
                inputs = [
                    TxInput(
                        bytes.fromhex(tx_in["txid"]),
                        tx_in["vout"],
                        ScriptSig.deserialize(
                            bytes.fromhex(tx_in["scriptSig"]["hex"])),
                        tx_in["sequence"]
                    ) for tx_in in real_tx["vin"]]
                witnesses = [
                    Witness(
                        [bytes.fromhex(item) for item in tx_in["txinwitness"]]
                    ) if "txinwitness" in tx_in else Witness()
                    for tx_in in real_tx["vin"]]
                for i in range(len(inputs)):
                    inputs[i].script.witness = witnesses[i]
                outputs = [
                    TxOutput(
                        btc_to_satoshi(tx_out["value"]),
                        ScriptPubKey.deserialize(
                            bytes.fromhex(tx_out["scriptPubKey"]["hex"]))
                    ) for tx_out in real_tx["vout"]]

                # Create object
                guess_tx = Tx(real_tx["version"], inputs, outputs,
                              real_tx["locktime"])

                # Check serialization
                self.assertEqual(test["serialization"], guess_tx.hex())
                self.assertEqual(guess_tx, eval(repr(guess_tx)))

    def test_weight(self):
        """Tests the weight and vsize properties"""
        for test in self._data:
            with self.subTest(test["tx"]["hash"]):
                tx = Tx.from_hex(test["serialization"])

                self.assertEqual(tx.weight, test["weight"])
                self.assertEqual(tx.vsize, test["vsize"])


class TestTxInvalid(unittest.TestCase):
    """Tests legacy and SegWit transactions using invalid test cases."""
    def test_deserialize(self):
        """Tests deserializing non deserializable transactions."""
        for serialization in (
                bytes(TRANSACTION_SIZE_MIN-1),
                bytes.fromhex("02000000000200000000"),
                bytes.fromhex("0200000000000000000000")):
            with self.assertRaises(ValueError):
                Tx.deserialize(serialization)
