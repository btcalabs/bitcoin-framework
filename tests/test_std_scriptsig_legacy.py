"""Tests the legacy ``ScriptSigs``."""

# Classes to test
from bitcoin import P2SHScriptSig, Script

# Relative imports
from .model import JSONTestModel


class TestStdScriptsigLegacyValid(JSONTestModel):
    """Tests legacy ``ScriptSigs`` using valid test cases.

    Test cases are formatted depending on the type of ScriptSig. Each type
    contains a list of different type tests.

    If type is "P2SH", the following fields are required:
        {
            "redeem_script": "hex_serialization",
            "pay_script": "hex_serialization",
            "serialization": "hex_serialization"
        }
    """

    def test_legacy_p2sh_serialize(self):
        """Creates and serializes legacy standard P2SH ScriptSigs.

        Test case is in the following format:
        ["redeem_script_hex", "pay_script_hex", "serialization_hex"]
        """
        i = 0
        for redeem_script, pay_script, serialization in self._data["P2SH"]:
            # P2SH
            with self.subTest("P2SH[{}]".format(i)):
                # Test variables
                redeem_script = Script.deserialize(
                    bytes.fromhex(redeem_script))
                pay_script = Script.deserialize(
                    bytes.fromhex(pay_script))

                # Construct P2SH
                script = P2SHScriptSig(redeem_script, pay_script)

                # Serialize
                self.assertEqual(serialization, script.hex())

            i += 1

    def test_legacy_p2sh_deserialize(self):
        """Creates and deserializes legacy standard P2SH ScriptSigs.

        Test case is in the following format:
        ["redeem_script_hex", "pay_script_hex", "serialization_hex"]
        """
        i = 0
        for redeem_script, pay_script, serialization in self._data["P2SH"]:
            # P2SH
            with self.subTest("P2SH[{}]".format(i)):
                # Construct P2SH
                script = P2SHScriptSig.deserialize(
                    bytes.fromhex(serialization))

                # Serialize
                # TODO: check properties
                self.assertEqual(serialization, script.hex())

            i += 1
