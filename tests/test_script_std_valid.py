"""Tests standard scripts without testing signatures.

Also tests the respective addresses that encode the ``ScriptPubKey`` s.

Modules tested:

- bitcoin.address.base58encodable
- bitcoin.address.bech32encodable
- bitcoin.address.p2pkh
- bitcoin.address.p2sh
- bitcoin.address.p2wpkh
- bitcoin.address.p2wsh
- bitcoin.address.np2wpkh
- bitcoin.address.np2wsh
- bitcoin.script.std.p2pkh
- bitcoin.script.std.p2sh
- bitcoin.script.std.p2wpkh
- bitcoin.script.std.p2wsh
- bitcoin.script.std.np2wpkh
- bitcoin.script.std.np2wsh
"""
# Libraries
from typing import Type
from unittest import TestCase

# Classes to test
from bitcoin import Address, AddressTypes, Base58EncodableAddress, \
    Bech32EncodableAddress, ECDSAPublicKey, Script, \
    StdPaymentImplementations, StdPaymentTypes, StdScriptPubKey, \
    StdScriptSig, NP2WPKHScriptPubKey, NP2WPKHAddress, NP2WSHAddress, \
    ECDSASignature, HashType, NP2WPKHScriptSig, StdP2PKHScriptSig, \
    LegacySignatureData, SegWitSignatureData

# Relative imports
from .model import JSONTestModel


class GenericTestScriptStdValid(JSONTestModel):
    """Tests valid standard scripts.

    This includes ``ScriptPubKey`` s, ``ScriptSig``s (without signing them
    explicitly) and the addresses representing the ``ScriptPubKey``s.

    Tests are formatted the following way:
        ["std_type", pubkey_data, address_data]

    Where "std_type" is:
        - "P2PKH" for legacy
        - "P2WPKH" for witness
        - "NP2WPKH" for nested witness (P2SH-P2PKH)

    Where ``pubkey_data`` is:
        ["public_key_hex", "public_key_hash_hex", "script_pubkey_hex"]

    Where ``address_data`` is:
        ["network_str", "prefix_hex", "checksum_hex", "encoded_address"]
    """

    @classmethod
    def get_payment_class(cls, std_type, class_type):
        """Returns a class given the standard type and type of class.

        Must be one of the serialization types and one of the following
        strings ["Address", "ScriptPubKey", "ScriptSig"]
        """
        class_name = std_type + class_type
        try:
            return cls.eval(std_type + class_type)
        except Exception as e:
            raise ValueError("{} for {} ({}) not found: {}".format(
                class_type, std_type, class_name, str(e)))

    @classmethod
    def get_address_class(cls, std_type) -> Type[Address]:
        return cls.get_payment_class(std_type, "Address")

    @classmethod
    def get_script_pubkey_class(cls, std_type) -> Type[StdScriptPubKey]:
        return cls.get_payment_class(std_type, "ScriptPubKey")

    @classmethod
    def get_script_sig_class(cls, std_type) -> Type[StdScriptSig]:
        return cls.get_payment_class(std_type, "ScriptSig")

    @staticmethod
    def get_payment_implementation(name: str) -> StdPaymentImplementations:
        # Nesteds start with "NP2W"
        if name.startswith("NP2W"):
            return StdPaymentImplementations.NESTED
        # SegWit native start with "P2W"
        elif name.startswith("P2W"):
            return StdPaymentImplementations.SEGWIT
        # Legacy starts with just "P2"
        elif name.startswith("P2"):
            return StdPaymentImplementations.LEGACY
        # Not known
        else:
            raise ValueError("%s is not an implementation" % name)

    @classmethod
    def get_test_name(cls, std_type, data_type, test_id):
        return "Test%s%s: %s" % (std_type, data_type, test_id)


class TestScriptStdP2pkhValid(GenericTestScriptStdValid):
    """Tests valid P2PKH & NP2WPKH & P2WPKH standard scripts and addresses.

    Tests are formatted the following way:
        ["std_type", pubkey_data, address_data]

    Where "std_type" is:
        - "P2PKH" for legacy
        - "P2WPKH" for native witness
        - "NP2WPKH" for nested witness (P2SH-P2WPKH)

    Where ``scriptsig_data```is:
        [pay_items, serializations]

        And ``pay_items`` is:
            ["pay_item_0_hex", "pay_item_1_hex", ...]

        And ``serializations`` is:
            ["scriptsig_serialization_hex", "witness_serialization_hex"]

    Where ``pubkey_data`` is:
        ["public_key_hex", "public_key_hash_hex", "script_pubkey_hex"]

    Where ``address_data`` is:
        ["network_str", address_extras, "encoded_address"]

    Where ``address_extras`` is:
        ["prefix_hex", "checksum_hex"]  # for base58 non-SegWit
        [version]                       # for bech32 native SegWit

    Sources:
    - https://api.blockcypher.com/v1/btc/main/txs/\
    d2247e77eac92f65e5cac03050aa1d48d62beb56ee4dc13172e991b46a77d04b
    - https://api.blockcypher.com/v1/btc/test3/txs/\
    0a96280769b1e11d44b589140e0a025796ee2b7d06c57bc315fdaa38dfeb1923
    """
    def test_script_pubkey(self):
        """Tests standard P2PKH ScriptPubKeys."""
        for std_type, _, pubkey_data, address_data in self._data:
            with self.subTest(self.get_test_name(
                    std_type, "StdScriptPubKey", pubkey_data[2])):
                # Test variables
                public_key, public_key_hash, script_pubkey_hex = pubkey_data
                public_key = ECDSAPublicKey.from_hex(public_key)

                # Test serialization
                # # Test hashes properly
                pubkey_class = self.get_script_pubkey_class(std_type)
                self.assertEqual(
                    public_key_hash,
                    pubkey_class.hash_public_key(public_key).hex())

                # # Test creating using public key
                self.assertEqual(
                    script_pubkey_hex,
                    pubkey_class.from_public_key(public_key).hex())

                # # Test creating using public key hash
                self.assertEqual(
                    script_pubkey_hex,
                    pubkey_class(bytes.fromhex(public_key_hash)).hex())

                # Test deserialization
                if issubclass(pubkey_class, NP2WPKHScriptPubKey):
                    with self.assertRaises(ValueError):
                        pubkey_class.from_hex(script_pubkey_hex)
                    # Test properties
                    script_pubkey = pubkey_class(
                        bytes.fromhex(public_key_hash))
                    self.assertEqual(
                        script_pubkey_hex,
                        self.eval(repr(script_pubkey)).hex())
                    payment_standard = script_pubkey.payment_standard
                    self.assertEqual(
                        self.get_payment_implementation(std_type),
                        payment_standard.payment_implementation
                    )
                    continue
                script_pubkey_class = self.get_script_pubkey_class(std_type)

                # # Using specific
                script_pubkey = script_pubkey_class.from_hex(script_pubkey_hex)
                self.assertEqual(script_pubkey_hex,
                                 script_pubkey.hex())

                # # Using generic
                script_pubkey = StdScriptPubKey.from_hex(script_pubkey_hex)

                # # Test properties
                # # # Payment standard
                payment_standard = script_pubkey.payment_standard
                self.assertEqual(
                    payment_standard.payment_type,
                    StdPaymentTypes.PKH)
                self.assertEqual(
                    payment_standard.payment_implementation,
                    self.get_payment_implementation(std_type))
                self.assertEqual(
                    payment_standard.uses_segwit,
                    self.get_payment_implementation(std_type) !=
                    StdPaymentImplementations.LEGACY)
                self.assertEqual(
                    public_key_hash,
                    script_pubkey.public_key_hash.hex())
                self.assertEqual(
                    public_key_hash,
                    script_pubkey.data.hex())
                # # # Witness-only
                if payment_standard.uses_segwit:
                    self.assertEqual(
                        0,
                        script_pubkey.version)
                    self.assertEqual(
                        public_key_hash,
                        script_pubkey.program.hex())

    def test_address(self):
        """Tests standard P2PKH addresses."""
        for std_type, _, pubkey_data, address_data in self._data:
            with self.subTest(
                    self.get_test_name(
                        std_type, "Address", address_data[2])):
                # Test variables
                network, address_extras, address_encoded = address_data
                _, pubkey_hash, script_pubkey_hex = pubkey_data
                network = self.convert_network(network)
                address_class = self.get_address_class(std_type)
                pubkey_class = self.get_script_pubkey_class(std_type)

                # ScriptPubKey
                # # Deserialize if not nested
                if issubclass(address_class, NP2WPKHAddress):
                    script_pubkey = pubkey_class(bytes.fromhex(pubkey_hash))
                else:
                    script_pubkey = pubkey_class.from_hex(script_pubkey_hex)

                # Test encoding
                # # By script pubkey
                address = address_class(script_pubkey, network)
                self.assertEqual(address_encoded, address.encode())

                # # (base58 only) By type and network
                if isinstance(address, Base58EncodableAddress) and \
                        not isinstance(address, NP2WPKHAddress):
                    address = Base58EncodableAddress.from_type_and_network(
                        AddressTypes.PKH, script_pubkey.data, network)
                    self.assertEqual(address_encoded, address.encode())

                # Test decoding
                # # Nested can't be decoded
                if issubclass(address_class, NP2WPKHAddress):
                    with self.assertRaises(ValueError):
                        address_class.decode(address_encoded)
                    address = address_class(script_pubkey, network)
                    self.assertEqual(
                        script_pubkey.data.hex(),
                        address.data.hex())
                    self.assertEqual(
                        script_pubkey_hex,
                        address.script_pubkey.hex())
                    self.assertEqual(
                        address.encode(),
                        self.eval(repr(address)).encode())
                    continue

                # # Decode
                address = address_class.decode(address_encoded)
                self.assertEqual(address_encoded, address.encode())

                # # Test properties
                self.assertEqual(address_encoded, str(address))
                self.assertEqual(address, self.eval(repr(address)))
                self.assertEqual(pubkey_hash, address.data.hex())
                self.assertEqual(
                    script_pubkey_hex,
                    address.script_pubkey.hex())

                # # (base58 only) Checksum and prefix
                if isinstance(address, Base58EncodableAddress):
                    prefix, checksum = address_extras
                    self.assertEqual(checksum, address.checksum.hex())
                    self.assertEqual(prefix, address.prefix.hex())
                # # (bech32 only) Version and program
                if isinstance(address, Bech32EncodableAddress):
                    version = address_extras[0]
                    self.assertEqual(version, address.version)
                    self.assertEqual(pubkey_hash, address.program.hex())

    def test_script_sig(self):
        """Tests P2PKH ScriptSigs."""
        for std_type, scriptsig_data, pubkey_data, _ in self._data:
            with self.subTest(self.get_test_name(std_type, "ScriptSig", _[2])):
                # Test variables
                sig_class = self.get_script_sig_class(std_type)
                scriptsig_items, serializations = scriptsig_data
                sig_hex, ht_int, pubkey_hex = scriptsig_items
                sig = ECDSASignature.from_hex(sig_hex)
                ht = HashType.from_integer(ht_int)
                pubkey = ECDSAPublicKey.from_hex(pubkey_hex)
                scriptsig_hex, witness_hex = serializations
                __, ___, script_pubkey_hex = pubkey_data

                # Test serialization
                # # Construct unsigned
                scriptsig = sig_class()
                # # # Not signed
                self.assertFalse(scriptsig.is_signed)
                # # # No script pubkey derived
                with self.assertRaises(ValueError):
                    scriptsig.script_pubkey()
                # # Apply signature
                scriptsig.fill(sig, ht, pubkey)
                self.assertTrue(scriptsig.is_signed)

                # # Test script items
                self.assertEqual(scriptsig_hex, scriptsig.hex())
                self.assertEqual(witness_hex, scriptsig.witness.hex())

                # # Test properties
                self.assertEqual(sig_hex, scriptsig.signature.hex())
                self.assertEqual(pubkey_hex, scriptsig.public_key.hex())
                self.assertEqual(
                    ht_int,
                    int.from_bytes(scriptsig.hashtype.serialize(), "little"))
                payment_standard = scriptsig.payment_standard
                self.assertEqual(
                    StdPaymentTypes.PKH,
                    payment_standard.payment_type)
                self.assertEqual(
                    self.get_payment_implementation(std_type),
                    payment_standard.payment_implementation)
                self.assertEqual(
                    self.get_payment_implementation(std_type) !=
                    StdPaymentImplementations.LEGACY,
                    payment_standard.uses_segwit)
                self.assertEqual(
                    SegWitSignatureData if payment_standard.uses_segwit else
                    LegacySignatureData,
                    scriptsig.signature_data_creator())
                self.assertEqual(
                    script_pubkey_hex,
                    scriptsig.script_pubkey().hex())


class TestScriptStdP2shValid(GenericTestScriptStdValid):
    """Tests valid P2SH & NP2WSH & P2WSH standard scripts and addresses.

    Tests are formatted the following way:
        ["std_type", scriptsig_data, redeem_script_data, address_data]

    Where "std_type" is:
        - "P2SH" for legacy
        - "P2WSH" for native witness
        - "NP2WSH" for nested witness (P2SH-P2WSH)

    Where ``scriptsig_data```is:
        [pay_items, serializations]

        And ``pay_items`` is:
            ["pay_item_0_hex", "pay_item_1_hex", ...]

        And ``serializations`` is:
            ["scriptsig_serialization_hex", "witness_serialization_hex"]

    Where ``redeem_script_data`` is:
        ["redeem_script_hex", "redeem_script_hash_hex", "script_pubkey_hex"]

    Where ``address_data`` is:
        ["network_str", address_extras, "encoded_address"]

    Where ``address_extras`` is:
        ["prefix_hex", "checksum_hex"]  # for base58 non-SegWit
        [version]                       # for bech32 native SegWit

    Sources:

    - https://www.soroushjp.com/2014/12/20/\
    bitcoin-multisig-the-hard-way-understanding-raw-multisignature-bitcoin-\
    transactions/
    - https://api.blockcypher.com/v1/btc/main/txs/\
    49de9ea077feee38d4952023fbe9cbdbb9020675667c9637ea0f1a456cc40030
    - https://github.com/bitcoin/bips/blob/master/bip-0143.mediawiki#P2SHP2WSH
    """
    def test_script_pubkey(self):
        """Tests standard P2SH ScriptPubKeys."""
        for std_type, ss_data, redeem_script_data, address_data in self._data:
            with self.subTest(self.get_test_name(
                    std_type, "StdScriptPubKey", redeem_script_data[2])):
                # Test variables
                redeem_script, redeem_script_hash, script_pubkey_hex = \
                    redeem_script_data
                redeem_script = Script.from_hex(redeem_script)
                pubkey_class = self.get_script_pubkey_class(std_type)

                # Test serialization
                # # Test hashes properly
                self.assertEqual(
                    redeem_script_hash,
                    pubkey_class.hash_script(redeem_script).hex())

                # # Test creating using script
                self.assertEqual(
                    script_pubkey_hex,
                    pubkey_class.from_script(redeem_script).hex())

                # # Test creating using script hash
                self.assertEqual(
                    script_pubkey_hex,
                    pubkey_class(bytes.fromhex(redeem_script_hash)).hex())

                # Test deserialization
                if self.get_payment_implementation(std_type) == \
                        StdPaymentImplementations.NESTED:
                    with self.assertRaises(ValueError):
                        pubkey_class.from_hex(script_pubkey_hex)
                    # Test properties
                    script_pubkey = pubkey_class(
                        bytes.fromhex(redeem_script_hash))
                    self.assertEqual(
                        script_pubkey_hex,
                        self.eval(repr(script_pubkey)).hex())
                    payment_standard = script_pubkey.payment_standard
                    self.assertEqual(
                        self.get_payment_implementation(std_type),
                        payment_standard.payment_implementation)
                    self.assertEqual(
                        redeem_script_hash,
                        script_pubkey.script_hash.hex())
                    continue

                # # Using specific
                script_pubkey = pubkey_class.from_hex(script_pubkey_hex)
                self.assertEqual(script_pubkey_hex,
                                 script_pubkey.hex())

                # # Using generic
                script_pubkey = StdScriptPubKey.from_hex(script_pubkey_hex)

                # # Test properties
                payment_standard = script_pubkey.payment_standard
                self.assertEqual(
                    payment_standard.payment_type,
                    StdPaymentTypes.SH)
                self.assertEqual(
                    payment_standard.payment_implementation,
                    self.get_payment_implementation(std_type))
                self.assertEqual(
                    redeem_script_hash,
                    script_pubkey.script_hash.hex())
                self.assertEqual(
                    redeem_script_hash,
                    script_pubkey.data.hex())
                if payment_standard.uses_segwit:
                    self.assertEqual(
                        0,
                        script_pubkey.version)
                    self.assertEqual(
                        redeem_script_hash,
                        script_pubkey.program.hex())

    def test_address(self):
        """Tests standard P2SH addresses."""
        for std_type, ss_data, redeem_script_data, address_data in self._data:
            with self.subTest(
                    self.get_test_name(
                        std_type, "Address", address_data[2])):
                # Test variables
                network, address_extras, address_encoded = address_data
                _, script_hash, script_pubkey_hex = redeem_script_data
                network = self.convert_network(network)
                pubkey_class = self.get_script_pubkey_class(std_type)
                address_class = self.get_address_class(std_type)
                script_pubkey = pubkey_class(bytes.fromhex(script_hash))

                # Test encoding
                # # By script pubkey
                address = address_class(script_pubkey, network)
                self.assertEqual(address_encoded, address.encode())

                # # (base58 only) By type and network
                if isinstance(address, Base58EncodableAddress):
                    address = Base58EncodableAddress.from_type_and_network(
                        AddressTypes.SH, script_pubkey.data, network)
                    self.assertEqual(address_encoded, address.encode())

                # Test decoding
                # # Nested can't be decoded
                if issubclass(address_class, NP2WSHAddress):
                    with self.assertRaises(ValueError):
                        address_class.decode(address_encoded)
                    # Test properties now
                    address = address_class(script_pubkey, network)
                    self.assertEqual(
                        script_pubkey.data.hex(),
                        address.data.hex())
                    self.assertEqual(
                        script_pubkey_hex,
                        address.script_pubkey.hex())
                    self.assertEqual(
                        address.encode(),
                        self.eval(repr(address)).encode())
                    continue

                # # Normal ones
                address = address_class.decode(address_encoded)
                self.assertEqual(address_encoded, address.encode())

                # # Test properties
                self.assertEqual(address_encoded, str(address))
                self.assertEqual(address, self.eval(repr(address)))
                self.assertEqual(script_hash, address.data.hex())
                self.assertEqual(
                    script_pubkey_hex,
                    address.script_pubkey.hex())

                # # (base58 only) Checksum and prefix
                if isinstance(address, Base58EncodableAddress):
                    prefix, checksum = address_extras
                    self.assertEqual(checksum, address.checksum.hex())
                    self.assertEqual(prefix, address.prefix.hex())
                # # (bech32 only)
                if isinstance(address, Bech32EncodableAddress):
                    version = address_extras[0]
                    self.assertEqual(version, address.version)
                    self.assertEqual(script_hash, address.program.hex())

    def test_script_sig(self):
        """Tests standard P2SH ScriptSigs."""
        for std_type, scriptsig_data, redeem_script_data, _ in self._data:
            with self.subTest(self.get_test_name(std_type, "ScriptSig", _[2])):
                # Test variables
                redeem_script_hex, redeem_hash_hex, script_pubkey_hex = \
                    redeem_script_data
                redeem_script = Script.from_hex(redeem_script_hex)
                pay_items, serializations = scriptsig_data
                pay_items = [bytes.fromhex(i) for i in pay_items]
                scriptsig_hex, witness_hex = serializations
                scriptsig_class = self.get_script_sig_class(std_type)

                # Test properties
                def test_properties(scriptsig):
                    self.assertEqual(
                        redeem_script_hex,
                        scriptsig.redeem_script.hex())
                    self.assertEqual(
                        pay_items,
                        scriptsig.pay_items)
                    payment_standard = scriptsig.payment_standard
                    self.assertEqual(
                        StdPaymentTypes.SH,
                        payment_standard.payment_type)
                    self.assertEqual(
                        self.get_payment_implementation(std_type),
                        payment_standard.payment_implementation)
                    self.assertEqual(
                        script_pubkey_hex,
                        scriptsig.script_pubkey().hex())
                    scriptsig_copy = self.eval(repr(scriptsig))
                    self.assertEqual(
                        scriptsig_hex,
                        scriptsig_copy.hex())
                    self.assertEqual(
                        witness_hex,
                        scriptsig_copy.witness.hex())

                # Test serialization
                scriptsig = scriptsig_class(redeem_script, pay_items)
                # # Test properties
                test_properties(scriptsig)
                # # Same serialization
                self.assertEqual(
                    scriptsig_hex,
                    scriptsig.hex())
                # # Same witness
                self.assertEqual(
                    witness_hex,
                    scriptsig.witness.hex())


class TestScriptStdP2pkhInvalid(TestCase):
    """Tests invalid actions on P2PKH ScriptSigs."""
    def test_unsigned_access(self):
        """Tests P2PKH properties when unsigned."""
        with self.assertRaises(ValueError):
            StdP2PKHScriptSig._create_items(NP2WPKHScriptSig())
        with self.assertRaises(ValueError):
            NP2WPKHScriptSig()._create_witness_items()
        with self.assertRaises(ValueError):
            NP2WPKHScriptSig()._store_items()
