"""Tests StdScript registries."""
from unittest import TestCase

from bitcoin import NP2WPKHScriptPubKey, NP2WPKHScriptSig, \
    NP2WSHScriptPubKey, NP2WSHScriptSig, P2PKHScriptPubKey, P2PKHScriptSig, \
    P2SHScriptPubKey, P2SHScriptSig, P2WPKHScriptPubKey, P2WPKHScriptSig, \
    P2WSHScriptPubKey, P2WSHScriptSig
from bitcoin.script.std.pubkey import StdScriptPubKeyRegistry
from bitcoin.script.std.sig import StdScriptSigRegistry
from bitcoin.standards import KnownPaymentStandard, P2PK


class TestScriptRegistry(TestCase):
    __slots__ = "_standards"

    _STANDARD_SCRIPT_PUBKEYS = {
        "P2PKH": P2PKHScriptPubKey,
        "P2WPKH": P2WPKHScriptPubKey,
        "NP2WPKH": NP2WPKHScriptPubKey,
        "P2SH": P2SHScriptPubKey,
        "P2WSH": P2WSHScriptPubKey,
        "NP2WSH": NP2WSHScriptPubKey
    }

    _STANDARD_SCRIPT_SIGS = {
        "P2PKH": P2PKHScriptSig,
        "P2WPKH": P2WPKHScriptSig,
        "NP2WPKH": NP2WPKHScriptSig,
        "P2SH": P2SHScriptSig,
        "P2WSH": P2WSHScriptSig,
        "NP2WSH": NP2WSHScriptSig
    }

    def setUp(self):
        self._standards = KnownPaymentStandard.get_payment_standards()

    def test_script_pubkey_registry(self):
        for standard in self._standards:
            if standard == P2PK:
                continue
            script_pubkey = StdScriptPubKeyRegistry.by_standard(standard)
            self.assertEqual(self._STANDARD_SCRIPT_PUBKEYS[str(standard)],
                             script_pubkey)

    def test_script_sig_registry(self):
        for standard in self._standards:
            if standard == P2PK:
                continue
            script_sig = StdScriptSigRegistry.by_standard(standard)
            self.assertEqual(self._STANDARD_SCRIPT_SIGS[str(standard)],
                             script_sig)
