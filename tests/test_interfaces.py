"""Tests the Bitcoin module defined interfaces."""

# Libraries
import unittest

from bitcoin.interfaces import Serializable, Encodable


class TestInterfaces(unittest.TestCase):
    """Tests the defined interfaces behave as defined."""
    HEX = "0123456789ABCDEF".lower()
    """
        str: some hex string
    """
    BYTES = b'\x01\x23\x45\x67\x89\xab\xcd\xef'
    """
        bytes: bytes object with same value as the :py:attr:`HEX` string
    """

    def test_serializable(self):
        """Tests serializable interface."""
        class MockSerializable(Serializable):
            def serialize(self):
                return super().serialize()

            @classmethod
            def deserialize(cls, data: bytes):
                return Serializable.deserialize(data)

        class NotSoMockSerializable(Serializable):
            def __init__(self, value):
                self.value = value

            def serialize(self):
                return self.value

            @classmethod
            def deserialize(cls, data: bytes):
                return cls(data)

        with self.assertRaises(NotImplementedError):
            MockSerializable().serialize()

        with self.assertRaises(NotImplementedError):
            MockSerializable.deserialize(bytes())

        self.assertEqual(NotSoMockSerializable(self.BYTES).hex(),
                         self.HEX)

    def test_encodable(self):
        """Tests encodable interface."""
        class MockEncodable(Encodable):
            def encode(self):
                return super().encode()

            @classmethod
            def decode(cls, data: bytes):
                return Encodable.decode(data)

        with self.assertRaises(NotImplementedError):
            MockEncodable().encode()

        with self.assertRaises(NotImplementedError):
            MockEncodable.decode("something")
