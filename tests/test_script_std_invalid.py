"""Tests invalid standard scripts and addresses.

Modules tested:

- bitcoin.address.base58encodable
- bitcoin.address.p2pkh
- bitcoin.address.p2sh
- bitcoin.address.wif
- bitcoin.script.std.p2pkh
- bitcoin.script.std.p2sh

**Sources:**

- http://gobittest.appspot.com/Address
- https://www.soroushjp.com/2014/12/20/bitcoin-multisig-the-hard-way\
-understanding-raw-multisignature-bitcoin-transactions/
- https://btcassessors.github.io/brainwallet/
- https://web.archive.org/web/20150707020924/https://brainwallet.org/
"""

from .model import JSONTestModel


class TestScriptStdInvalid(JSONTestModel):
    """Tests invalid cases of standard ``Script`` s and addresses."""

    def test_init(self):
        """Tests creating invalid standard objects.

        Test format is:
            [class, args, error]
        """
        for clazz, args, error in self._data["init"]:
            with self.subTest("{}({})".format(clazz, args)):
                # Test variables
                clazz = self.eval(clazz)
                args = [self.eval(arg) for arg in args]
                error = self.eval(error)

                # Test error
                with self.assertRaises(error):
                    clazz(*args)

    def test_decode(self):
        """Tests decoding invalid standard objects

        Test format is:
            [class, arg, error]
        """
        for clazz, arg, error in self._data["decode"]:
            with self.subTest("{}.decode({})".format(clazz, arg)):
                # Test variables
                clazz = self.eval(clazz)
                error = self.eval(error)

                # Test decode
                with self.assertRaises(error):
                    clazz.decode(arg)

    def test_deserialize(self):
        """Tests deserializing invalid standard objects.

        Test format is:
            [class, arg, error]
        Where ``arg`` is some hex string.
        """
        for clazz, arg, error in self._data["deserialize"]:
            with self.subTest("{}.deserialize({})".format(clazz, arg)):
                # Test variables
                clazz = self.eval(clazz)
                error = self.eval(error)

                # Test decode
                with self.assertRaises(error):
                    clazz.from_hex(arg)
