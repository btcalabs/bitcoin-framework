"""Tests the address factory."""
from .model import JSONTestModel

from bitcoin import AddressFactory


class TestAddressFactory(JSONTestModel):
    def test_derivations(self):
        for private_key, network, compressed, standards in \
                self._data["derivations"]:
            for standard, address in standards.items():
                # Invalid
                if address is None:
                    with self.assertRaises(ValueError):
                        AddressFactory.from_private_key(
                            private_key, standard, network, compressed)
                # Valid
                else:
                    guess_address = AddressFactory.from_private_key(
                        private_key, standard, network, compressed)
                    self.assertEqual(address, guess_address.encode())

    def test_decode(self):
        # Valid
        for address in self._data["decode"]:
            self.assertEqual(address, AddressFactory.decode(address).encode())
        # Invalid
        with self.assertRaises(ValueError):
            AddressFactory.decode("NotAValidAddress")
