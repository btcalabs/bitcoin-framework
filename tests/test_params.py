"""Tests the Bitcoin params module."""

# Libraries
import unittest

from bitcoin import AddressTypes, BlockchainParams, MAINNET, \
    NoDefaultBlockchainError, REGTEST, TESTNET, get_address_type_from_index, \
    get_default_network, set_default_network
from bitcoin.params import KnownBlockchainParams
from .model import TestModel


class TestBlockchainParams(TestModel):
    BLOCKCHAINS = [
        (MAINNET, 1, "mainnet"),
        (TESTNET, 3, "testnet"),
        (REGTEST, 3, "regtest")
    ]
    """
        list: list of test cases using tuples with the following format:
            (network, network_version, network_name)
    """

    def test_version(self):
        """Tests the version property of defined :py:attr:`BLOCKCHAINS`."""
        for network, version, name in self.BLOCKCHAINS:
            with self.subTest(name):
                self.assertEqual(version, network.version)

    def test_str(self):
        """Tests the __str__ magic method of defined :py:attr:`BLOCKCHAINS`."""
        for network, version, name in self.BLOCKCHAINS:
            with self.subTest(name):
                self.assertEqual(name, str(network))
                self.assertEqual(name, network.name)

    def test_repr(self):
        """Tests the repr of BlockchainParams."""
        for network, version, name in self.BLOCKCHAINS:
            with self.subTest(name):
                # Known blockchains
                self.assertEqual(repr(network), name.upper())
                # Other blockchains
                self.assertEqual(
                    network,
                    self.eval(BlockchainParams.__repr__(network)))
                # No 2 known blockchains
                rep = BlockchainParams.__repr__(network)
                rep = rep.replace(BlockchainParams.__name__,
                                  KnownBlockchainParams.__name__)
                with self.assertRaises(ValueError):
                    self.eval(rep)

    def test_address_prefix(self):
        for network, version, name in self.BLOCKCHAINS:
            with self.subTest(name):
                with self.assertRaises(ValueError):
                    network.address_prefix(AddressTypes.PK)


class TestParamsMethods(unittest.TestCase):
    """Bitcoin params module methods tests."""
    INVALID_ADDRESS_TYPE_INDEX = len(AddressTypes) + 1
    """
        int: invalid address type index
    """

    def test_address_type_from_index(self):
        with self.assertRaises(ValueError):
            get_address_type_from_index(self.INVALID_ADDRESS_TYPE_INDEX)

    def test_default_network(self):
        """Tests setting the default network."""
        with self.assertRaises(NoDefaultBlockchainError):
            get_default_network()
        set_default_network(MAINNET)
        self.assertEqual(MAINNET, get_default_network())
