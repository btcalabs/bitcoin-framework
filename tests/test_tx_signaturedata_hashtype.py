"""."""

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import HashType, HashTypeFlag
from bitcoin.tx.signaturedata.hashtype import HASHTYPE_MAP


class TestHashtype(JSONTestModel):
    """
    [short serialization, long serialization (default), ASM, value]
    """

    def test_deserialize(self):
        for short_serialization, serialization, string, val in self._data:
            with self.subTest("{}:{}".format(short_serialization, val)):
                # Creation
                ht = HashType.from_hex(serialization)

                # Checks
                self.assertEqual(ht.hashtype.value, val)
                self.assertEqual(ht.serialize().hex(), serialization)
                self.assertEqual(ht.serialize(True).hex(), short_serialization)
                self.assertEqual(string, str(ht))
                self.assertEqual(ht, self.eval(repr(ht)))

    def test_serialize(self):
        for short_serialization, serialization, string, val in self._data:
            with self.subTest("{}:{}".format(short_serialization, val)):
                # Test variables, flags as a tuple
                flags = tuple()
                if '|ANYONECANPAY' in string:
                    flags = (HashTypeFlag.ANYONECANPAY, )
                htvalue = HASHTYPE_MAP[val]

                ht = HashType(htvalue, flags)

                # Checks
                self.assertEqual(ht.hashtype.value, val)
                self.assertEqual(ht.flags, flags)

                # Test variables, flags as HashTypeFlags
                flags = None
                if '|ANYONECANPAY' in string:
                    flags = HashTypeFlag.ANYONECANPAY
                htvalue = HASHTYPE_MAP[val]

                ht = HashType(htvalue, flags)

                # Checks
                if flags is None:
                    self.assertEqual(ht.flags, tuple())
                else:
                    self.assertEqual(ht.flags, (flags, ))

                # Common checks
                self.assertEqual(ht.serialize().hex(), serialization)
                self.assertEqual(ht.serialize(True).hex(), short_serialization)
                self.assertEqual(string, str(ht))
                self.assertEqual(ht, self.eval(repr(ht)))

    def test_frominteger_invalid(self):
        for value in [4294967296, -4294967295]:
            with self.assertRaises(ValueError):
                HashType.from_integer(value)
