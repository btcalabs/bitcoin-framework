"""Tests the psbt module.

Source:
 - https://docs.google.com/document/d/1BUTC3dh89kaxJbjUUD-nb8WLZ4SBvd15ZdgMlZrx
 0f4/edit
"""

# Relative imports
from .model import JSONTestModel

from bitcoin import HashTypeValue, HashType, PSBT, PSBT_GLOBAL_UNSIGNED_TX, \
    PSBT_IN_SIGHASH_TYPE, PSBT_OUT_BIP32_DERIVATION


class TestPSBT(JSONTestModel):

    def test_serialization(self):
        self.maxDiff = None
        for psbt in self._data["serialization"]:
            try:
                p = PSBT.from_hex(psbt)
                self.assertEqual(p.hex(), psbt)
            except ValueError as e:
                self.fail("An error occurred; {}".format(e))

    def test_base64(self):
        """Tests the to/from base64 creation."""
        self.maxDiff = None
        for psbt in self._data["base64"]:
            self.assertEqual(PSBT.from_hex(psbt["hex"]).b64(), psbt["base64"])
            self.assertEqual(PSBT.from_b64(psbt["base64"]).hex(), psbt["hex"])

    def test_creation(self):
        """Tests the creation of a PSBT from scratch step by step."""
        # Variables
        self.maxDiff = None
        psbt = PSBT()
        global_item = PSBT_GLOBAL_UNSIGNED_TX(
            bytes.fromhex("020000000258e87a21b56daf0c23be8e7070456c336f7cbaa"
                          "5c8757924f545887bb2abdd750000000000ffffffff838d04"
                          "27d0ec650a68aa46bb0b098aea4422c071b2ca78352a07795"
                          "9d07cea1d0100000000ffffffff0270aaf008000000001600"
                          "14d85c2b71d0060b09c9886aeb815e50991dda124d00e1f50"
                          "50000000016001400aea9a2e5f0f876a588df5546e8742d1d"
                          "87008f00000000"))
        input_item = PSBT_IN_SIGHASH_TYPE(
            HashType(HashTypeValue.ALL).serialize())
        output_item = PSBT_OUT_BIP32_DERIVATION(
            bytes.fromhex("d90c6a4f000000800000008004000080"),
            bytes.fromhex("03a9a4c37f5996d3aa25dbac6b570af0650394492942460b3"
                          "54753ed9eeca58771")
            )
        # Check property
        self.assertEqual(input_item.key.hex(), "03")
        self.assertEqual(input_item.key_data.hex(), "")
        self.assertEqual(input_item.value.hex(), "01000000")

        # Empty PSBT
        self.assertEqual(psbt.hex(), "70736274ff00")
        self.assertEqual(psbt.data, {"global": [],
                                     "inputs": [],
                                     "outputs": []})

        # Add input/output before the addition of a global
        with self.assertRaises(ValueError):
            psbt.add_input(0, input_item)
        with self.assertRaises(ValueError):
            psbt.add_output(0, output_item)

        # Wrong scope
        with self.assertRaises(ValueError):
            psbt.add_global(input_item)
        # Add a global
        psbt.add_global(global_item)

        # Try to add another global
        with self.assertRaises(ValueError):
            psbt.add_global(global_item)

        # Check partial serialization
        self.assertEqual(
            psbt.hex(), "70736274ff01009a020000000258e87a21b56daf0c23be8e707"
                        "0456c336f7cbaa5c8757924f545887bb2abdd750000000000ff"
                        "ffffff838d0427d0ec650a68aa46bb0b098aea4422c071b2ca7"
                        "8352a077959d07cea1d0100000000ffffffff0270aaf0080000"
                        "0000160014d85c2b71d0060b09c9886aeb815e50991dda124d0"
                        "0e1f5050000000016001400aea9a2e5f0f876a588df5546e874"
                        "2d1d87008f000000000000000000")
        # Add an item related to the first input
        psbt.add_input(0, input_item)
        # Try to repeat key
        with self.assertRaises(ValueError):
            psbt.add_input(0, input_item)
        # Add an item related to the first output
        psbt.add_output(0, output_item)
        # Try to repeat key
        with self.assertRaises(ValueError):
            psbt.add_output(0, output_item)
        # Add an item related to the second output
        psbt.add_output(1, PSBT_OUT_BIP32_DERIVATION(
            bytes.fromhex("d90c6a4f000000800000008005000080"),
            bytes.fromhex("027f6399757d2eff55a136ad02c684b1838b6556e5f1b6b34"
                          "282a94b6b50051096")
        ))
        # Check partial serialization
        self.assertEqual(
            psbt.hex(), "70736274ff01009a020000000258e87a21b56daf0c23be8e707"
                        "0456c336f7cbaa5c8757924f545887bb2abdd750000000000ff"
                        "ffffff838d0427d0ec650a68aa46bb0b098aea4422c071b2ca7"
                        "8352a077959d07cea1d0100000000ffffffff0270aaf0080000"
                        "0000160014d85c2b71d0060b09c9886aeb815e50991dda124d0"
                        "0e1f5050000000016001400aea9a2e5f0f876a588df5546e874"
                        "2d1d87008f0000000000"
                        "0103040100000000"
                        "00"
                        "220203a9a4c37f5996d3aa25dbac6b570af0650394492942460"
                        "b354753ed9eeca5877110d90c6a4f0000008000000080040000"
                        "8000"
                        "2202027f6399757d2eff55a136ad02c684b1838b6556e5f1b6b"
                        "34282a94b6b5005109610d90c6a4f0000008000000080050000"
                        "8000")

        # Try to add an input in output scope and vice versa
        with self.assertRaises(ValueError):
            psbt.add_input(1, output_item)
        with self.assertRaises(ValueError):
            psbt.add_output(0, input_item)

        # Input / output overflow, not enough !
        with self.assertRaises(ValueError):
            psbt.add_input(3, input_item)
        with self.assertRaises(ValueError):
            psbt.add_output(2, output_item)

    def test_validate(self):
        """Tests the private method _validate, should be more exhaustive."""
        # Pending to improve checks
        psbt = PSBT()
        self.assertFalse(psbt._validate())
        psbt.add_global(
            PSBT_GLOBAL_UNSIGNED_TX(
                bytes.fromhex(
                    "020000000258e87a21b56daf0c23be8e7070456c336f7cbaa"
                    "5c8757924f545887bb2abdd750000000000ffffffff838d04"
                    "27d0ec650a68aa46bb0b098aea4422c071b2ca78352a07795"
                    "9d07cea1d0100000000ffffffff0270aaf008000000001600"
                    "14d85c2b71d0060b09c9886aeb815e50991dda124d00e1f50"
                    "50000000016001400aea9a2e5f0f876a588df5546e8742d1d"
                    "87008f00000000"))
        )
        self.assertTrue(psbt._validate())

    def test_psbt_item(self):
        # Wrong item deserialization
        with self.assertRaises(ValueError):
            PSBT_GLOBAL_UNSIGNED_TX.deserialize(
                PSBT_IN_SIGHASH_TYPE(
                    HashType(HashTypeValue.ALL).serialize()).serialize()
            )


class TestPSBTInvalid(JSONTestModel):

    def test_deserialization(self):
        for psbt in self._data:
            with self.assertRaises(ValueError):
                PSBT.from_hex(psbt)
