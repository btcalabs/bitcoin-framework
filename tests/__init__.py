"""
Interesting source:

https://github.com/bitcoin/bitcoin/blob/master/src/test/data/tx_valid.json
"""
# Libraries
import unittest

# Relative imports
from tests import test_standards, test_script_registry
from . import test_crypto, test_crypto_ecdsa_signature, test_crypto_ecdsa_key,\
    test_encoding_base58, test_encoding_bech32, test_field_general, \
    test_field_model, test_field_opcode, test_field_script, test_interfaces, \
    test_params, test_script_model, test_psbt, test_script_std_invalid, \
    test_tx_inout, test_tx_input, test_tx, test_tx_output, test_units, \
    test_tx_signaturedata_legacy, test_tx_signaturedata_witness, \
    test_bitcoind_sighash, test_tx_signaturedata_hashtype, \
    test_script_std_valid, test_address_wif, test_renum, \
    test_address_factory, test_crypto_extended_key

# Constants
TESTS_ORDER = [
    # No dependencies
    [test_interfaces, test_units, test_params, test_encoding_base58,
     test_encoding_bech32, test_crypto, test_renum],
    # Requires interfaces
    [test_crypto_ecdsa_key, test_crypto_ecdsa_signature, test_script_model,
     test_tx_inout, test_field_model, test_standards],
    # Requires field model
    [test_field_general, test_field_opcode, test_field_script,
     test_script_registry],
    # Requires fields
    [test_script_std_valid, test_script_std_invalid, test_address_wif,
     test_tx_signaturedata_hashtype],
    # Requires scripts
    [test_tx_input, test_tx_output, test_address_factory],
    # Requires tx inputs and outputs
    [test_tx],
    # Signatures serializations
    [test_tx_signaturedata_legacy, test_tx_signaturedata_witness,
     test_bitcoind_sighash],
    # Complex structures
    [test_psbt, test_crypto_extended_key]
]
"""
    list: defines the order in which tests must be executed.

    Each list item defines a stage to run some tests. This way, tests that
    depend on the proper implementations of other resources won't be
    executed til everything needed to run them has been succesfully tested.

    Each list item is a sublist containing the tests to run in that stage.
    They can be run in parallel.
"""


def load_tests(loader, standard_tests, pattern):
    """Coses"""
    package_tests = unittest.TestSuite()
    for test_stage in TESTS_ORDER:
        stage_test_suite = unittest.TestSuite()
        for test_module in test_stage:
            stage_test_suite.addTests(loader.loadTestsFromModule(test_module))
        package_tests.addTest(stage_test_suite)
    return package_tests
