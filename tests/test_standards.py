"""Tests the standards module."""
from bitcoin import StdPaymentImplementations, \
    StdPaymentTypes, get_payment_standard
from bitcoin.standards import KnownPaymentStandard
from .model import TestModel


class TestPaymentStandard(TestModel):
    __slots__ = "_standards"

    _STANDARD_NAMES = {
        StdPaymentTypes.PK: {
            StdPaymentImplementations.LEGACY: "P2PK"
        },
        StdPaymentTypes.PKH: {
            StdPaymentImplementations.LEGACY: "P2PKH",
            StdPaymentImplementations.NESTED: "NP2WPKH",
            StdPaymentImplementations.SEGWIT: "P2WPKH",
        },
        StdPaymentTypes.SH: {
            StdPaymentImplementations.LEGACY: "P2SH",
            StdPaymentImplementations.NESTED: "NP2WSH",
            StdPaymentImplementations.SEGWIT: "P2WSH",
        }
    }

    def setUp(self):
        self._standards = KnownPaymentStandard.get_payment_standards()

    def test_str(self):
        """Test standard payment names do match."""
        for standard in self._standards:
            name = self._STANDARD_NAMES[standard.payment_type][
                standard.payment_implementation]
            self.assertEqual(name, str(standard))

    def test_repr(self):
        """Test standard payments do repr properly"""
        for standard in self._standards:
            self.assertEqual(standard, self.eval(repr(standard)))

    def test_get(self):
        """Test getting standard payments method"""
        # Valid
        for standard in self._standards:
            self.assertEqual(standard, get_payment_standard(
                standard.payment_type,
                standard.payment_implementation))
        # Test invalid
        with self.assertRaises(ValueError):
            get_payment_standard(StdPaymentTypes.PK,
                                 StdPaymentImplementations.SEGWIT)
        with self.assertRaises(ValueError):
            get_payment_standard(StdPaymentTypes.PK,
                                 StdPaymentImplementations.NESTED)
