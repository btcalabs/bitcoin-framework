"""Tests creating signature message hashes using bitcoind test cases.

Test cases available in bitcoind's source:
https://github.com/bitcoin/bitcoin/blob/v0.16.3/src/test/data/sighash.json
"""

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import LegacySignatureData, Tx, TxOutput, ScriptPubKey, \
    HashType, S4BLEInt, double_sha256


class TestBitcoindSighash(JSONTestModel):
    """Tests Tx signature message hashes using bitcoind test cases."""
    def test_signaturedata_msg(self):
        for raw_tx, script, idx, hashtype_int, msg in self._data[1:]:
            with self.subTest("{},{}:{}".format(raw_tx[0:32], idx,
                                                hashtype_int)):
                # Test variables
                tx = Tx.from_hex(raw_tx)
                utxo = TxOutput(0, ScriptPubKey.from_hex(script))
                hashtype = HashType.from_integer(hashtype_int)

                signature_creator = LegacySignatureData(
                    tx, idx, utxo, hashtype)

                guess_msg = signature_creator.msg()[:-4]
                guess_msg = guess_msg + S4BLEInt(hashtype_int).serialize()
                guess_res = double_sha256(guess_msg)[::-1]

                self.assertEqual(msg, guess_res.hex())
