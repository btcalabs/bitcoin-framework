"""Tests signature hashes are produced properly in Legacy signature.

Modules tested:

- bitcoin.tx.signaturedata.legacy

**Sources:**

- https://klmoney.wordpress.com/bitcoin-dissecting-transactions-part-2-
building-a-transaction-by-hand/
"""

# Libraries
import unittest

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import HashType, HashTypeValue, LegacySignatureData, \
    ScriptPubKey, ScriptSig, Tx, TxInput, TxOutput, ECDSAPublicKey, \
    ECDSASignature


class TestLegacySignatureData(unittest.TestCase):
    """Tests the signature message serialization for legacy transactions."""
    MSG_CASES = [
        (
            "0100000001416e9b4555180aaa0c417067a46607bc58c96f0131b2f41f7d0fb"
            "665eab03a7e0000000000ffffffff01204e0000000000001976a914e81d742e"
            "2c3c7acd4c29de090fc2c4d4120b2bf888ac00000000",
            "76a91499b1ebcfc11a13df5161aba8160460fe1601d54188ac",
            HashType(HashTypeValue.ALL),
            "0100000001416e9b4555180aaa0c417067a46607bc58c96f0131b2f41f7d0fb6"
            "65eab03a7e000000001976a91499b1ebcfc11a13df5161aba8160460fe1601d5"
            "4188acffffffff01204e0000000000001976a914e81d742e2c3c7acd4c29de09"
            "0fc2c4d4120b2bf888ac0000000001000000",
            "456f9e1b6184d770f1a240da9a3c4458e55b6b4ba2244dd21404db30b3131b94"
        )
    ]
    """
        str: List of test cases where we know the signature message before
             it is hashed so we can debug better.

             Test cases format:
             ["rawtx", "utxo_script", hashtype", "msg", "msg_hash"]
    """

    def test_msg(self):
        """Tests the message to be signed is created properly."""
        for rawtx, utxo_script, hashtype, msg, msg_hash in self.MSG_CASES:
            with self.subTest(msg_hash):
                # Test variabbles
                tx = Tx.deserialize(bytes.fromhex(rawtx))
                utxo = TxOutput(0, ScriptPubKey.deserialize(bytes.fromhex(
                    utxo_script)))

                sd = LegacySignatureData(tx, 0, utxo, hashtype)

                # Checks
                self.assertEqual(msg, sd.msg().hex())
                self.assertEqual(msg_hash, sd.hex())

    def test_invalid_inputs(self):
        utxo = TxOutput(100, ScriptPubKey())
        hashtype = HashType(HashTypeValue.ALL)

        # Without inputs to sign
        with self.assertRaises(AssertionError):
            LegacySignatureData(Tx(), 0, utxo, hashtype)

        # Input to sign does not exist
        with self.assertRaises(AssertionError):
            LegacySignatureData(
                Tx(inputs=[TxInput(bytes(32), 0)]), 1, utxo,
                hashtype)

    def test_invalid_tx(self):
        # Variables creation
        utxo = TxOutput(0, ScriptPubKey())
        prev_tx = Tx(outputs=[utxo])
        signing_tx = Tx(
            inputs=[TxInput(prev_tx.id, 0, ScriptSig())]
        )

        # Fake variables creation
        fake_utxo = TxOutput(12345, ScriptPubKey())
        fake_prev_tx = Tx(outputs=[fake_utxo])  # noqa: F841

        # Check, wrong tx
        with self.assertRaises(ValueError):
            LegacySignatureData(signing_tx, 0, fake_utxo).msg()

        signing_tx = Tx(
            inputs=[TxInput(prev_tx.id, 1, ScriptSig())]
        )

        # Check, wrong index
        with self.assertRaises(ValueError):
            LegacySignatureData(signing_tx, 0, utxo).msg()


class TestTxSignaturedataLegacy(JSONTestModel):
    """Tests legacy signature messages serializations.

    Uses a signature and a public key to determine if the produced hash that is
    being signed is correct.

    Tests cases are in the following format:
        ["public_key", "rawtx_signed", "utxo_script", "hashtype_asm"]

    Hastype is considered to be equal for all the inputs.
    """
    def test_signable_msg(self):
        test_n = 0
        for public_key, rawtx_signed, utxo_script, hashtype_asm in self._data:
            # Test variables
            tx = Tx.from_hex(rawtx_signed)
            public_key = ECDSAPublicKey.from_hex(public_key)
            utxo = TxOutput(0, ScriptPubKey.from_hex(utxo_script))

            for i in range(len(tx.inputs)):
                with self.subTest("{},{}:{}".format(test_n, i,
                                                    hashtype_asm)):
                    # Take input
                    signature_hashtype = tx.inputs[i].script.items[0].value

                    # Split signature and hashtype
                    signature, hashtype = signature_hashtype[:-1], \
                        signature_hashtype[-1]

                    signature = ECDSASignature.deserialize(signature)
                    hashtype = HashType.from_integer(hashtype)

                    # Check hashtype extracted
                    self.assertEqual(hashtype_asm, str(hashtype))

                    # Create signature data
                    signature_creator = LegacySignatureData(
                        tx, i, utxo, hashtype)
                    msg = signature_creator.signable_msg()

                    # Check verifies
                    self.assertTrue(
                        public_key.verify_signature(msg, signature))

            test_n += 1
