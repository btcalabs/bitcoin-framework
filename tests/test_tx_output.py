"""Tests a Transaction Output."""

# Libraries
from unittest import TestCase

# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import TxOutput, ScriptPubKey


class TestTxOutputValid(JSONTestModel):
    """Tests the TxOutput class with test cases with valid values."""
    def test_serialize(self):
        for test in self._data:
            # Test variables
            satoshis = test["satoshis"]
            script_serialization = test["script"]
            tx_output_serialization = test["serialization"]

            # Test conversions
            script_serialization = bytes.fromhex(script_serialization)
            script = ScriptPubKey.deserialize(script_serialization)
            tx_output_serialization = bytes.fromhex(tx_output_serialization)

            # Create object
            tx_output = TxOutput(satoshis, script)

            # Test serialize
            self.assertEqual(tx_output_serialization, tx_output.serialize())

    def test_deserialize(self):
        for test in self._data:
            # Test variables
            satoshis = test["satoshis"]
            script_serialization = test["script"]
            tx_output_serialization = test["serialization"]

            # Test conversions
            script_serialization = bytes.fromhex(script_serialization)
            script = ScriptPubKey.deserialize(script_serialization)
            tx_output_serialization = bytes.fromhex(tx_output_serialization)

            # Create object
            tx_output = TxOutput(satoshis, script)

            # Test deserialize
            guess_tx_output = TxOutput.deserialize(tx_output_serialization)
            self.assertEqual(tx_output.value, guess_tx_output.value)
            self.assertEqual(tx_output.btc, guess_tx_output.btc)
            self.assertEqual(tx_output.script, guess_tx_output.script)


class TestTxOutputInvalid(TestCase):
    """Tests the TxOutput class with test cases with invalid values."""
    def test_init(self):
        # Invalid satoshis value
        with self.assertRaises(ValueError):
            TxOutput(-1, ScriptPubKey())

    def test_deserialize(self):
        # Invalid byte length (minimum)
        with self.assertRaises(ValueError):
            TxOutput.deserialize(bytes(8))
