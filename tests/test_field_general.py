"""Tests the general fields."""
# Relative imports
from .model import JSONTestModel

# Classes to test
from bitcoin import U2BLEInt, S4BLEInt, U4BLEInt, U8BLEInt, VarInt, IntField

# Constants
FIELD_CLASSES = [U2BLEInt, S4BLEInt, U4BLEInt, U8BLEInt, VarInt, IntField]
"""
    list: list of field classes being tested
"""
FIELD_CLASSES_BY_NAME = {c.__name__: c for c in FIELD_CLASSES}
"""
    dict: maps each field to test using by its name so the class to test
          is easily accessible using its name.
"""


def get_test_name(value, name):
    """Returns the test name given the test value and sign."""
    value_str = value.hex() if isinstance(value, bytes) \
        else hex(value) if isinstance(value, int) \
        else str(value)
    return "{}: {}".format(name, value_str)


class TestFieldGeneralValid(JSONTestModel):
    """Tests field.general classes with valid test cases.

    Test cases are split according to the field (and its representing class)
    to test. Each field has its list of test cases. Each test case is in
    the following format:
        [field_length, sign, field_value_hex, field_serialization]

    Hex value is in big endian. In unsigned fields, please set sign to ``null``
    """

    @classmethod
    def get_test_vars(cls, test, field_class):
        """Given a single test case, extract the information and returns it.

        Args:
            test: test tuple to extract info
            field_class: field class being tested

        Returns:
            Tuple containing: (length, sign, value, serialization)
        """
        length, sign, value_hex, serialized = test
        value = bytes.fromhex(value_hex)

        value = int.from_bytes(value, "big")
        value = sign * value if sign is not None else value
        return length, sign, value, serialized

    def test_serialize(self):
        """Tests the field classes serialization."""
        for field_name, field_tests in self._data.items():
            field_class = FIELD_CLASSES_BY_NAME[field_name]
            for test in field_tests:
                length, sign, value, serialized = self.get_test_vars(
                    test, field_class)
                with self.subTest(get_test_name(value, field_name)):
                    field = field_class(value)
                    self.assertEqual(serialized, field.hex())
                    self.assertEqual(length, len(field))
                    self.assertEqual(field, eval(repr(field)))

    def test_deserialize(self):
        """Tests the field classes deserialization."""
        for field_name, field_tests in self._data.items():
            field_class = FIELD_CLASSES_BY_NAME[field_name]
            for test in field_tests:
                length, sign, value, serialized = self.get_test_vars(
                    test, field_class)
                with self.subTest(get_test_name(value, field_name)):
                    field = field_class.deserialize(bytes.fromhex(serialized))
                    self.assertEqual(value, field.value)
                    self.assertEqual(length, len(field))


class TestFieldGeneralInvalid(JSONTestModel):
    """Tests field.general classes with invalid test cases.

    Test cases are split depending if the errors occur while serializing or
    while deserializing. In each operation ("serialize" and "deserialize"),
    test cases are in a list inside each field class name.
    """

    def test_serialize(self):
        """Tests general fields with invalid test cases in serializations.

        Tests have the following format:
            (sign, value_hex, error)

        Where value_hex is the value in hexadecimal in big endian.
        """
        for field_name, tests in self._data["serialize"].items():
            field_class = FIELD_CLASSES_BY_NAME[field_name]
            for sign, value_hex, error_str in tests:
                with self.subTest(get_test_name(value_hex, field_name)):
                    # Test variables
                    value = sign * int.from_bytes(
                        bytes.fromhex(value_hex), "big")
                    error = self.convert_error(error_str)
                    with self.assertRaises(error):
                        field_class(value)

    def test_deserialize(self):
        """Tests general fields with invalid test cases in deserialization.

        Tests have the following format:
            (serialization, error)
        """
        for field_name, tests in self._data["deserialize"].items():
            field_class = FIELD_CLASSES_BY_NAME[field_name]
            # Test each case
            for serialization, error_str in tests:
                with self.subTest(get_test_name(serialization, field_name)):
                    # Test variables
                    serialization = bytes.fromhex(serialization)
                    error = self.convert_error(error_str)

                    with self.assertRaises(error):
                        field_class.deserialize(serialization)

    def test_deserialize_empty(self):
        """Test deserializing general fields with empty bytes."""
        # Prepare field classes. VarLEChar is not needed.
        classes = FIELD_CLASSES.copy()

        for field_class in classes:
            with self.assertRaises(ValueError):
                field_class.deserialize(b'')

    def test_varint_translate_size(self):
        """Tests VarInt field translate size does not take incorrect nums."""
        for n in [-1, 256]:
            with self.assertRaises(ValueError):
                VarInt.translate_size(n)
