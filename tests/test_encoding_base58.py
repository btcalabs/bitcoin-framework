"""
Tests the methods to encode/decode to/from base58.

src: http://gobittest.appspot.com/Address
"""

# Relative imports
from .model import JSONTestModel

# Methods to test
from bitcoin import b58encode, b58decode


class TestEncodingBase58Valid(JSONTestModel):

    def test_encode(self):
        """Tests the base58 encoding."""
        for test in self._data:
            with self.subTest(test['address']):
                # Test variables
                address = test['address']
                encoded = test['encoded']
                # Test conversions
                address = bytes.fromhex(address)
                # Create object
                guessed = b58encode(address)
                # Test
                self.assertEqual(encoded, guessed)

    def test_decode(self):
        """Tests the base58 decoding."""
        for test in self._data:
            with self.subTest(test['address']):
                # Test variables
                address = test['address']
                encoded = test['encoded']
                # Test conversions
                address = bytes.fromhex(address)
                # Create object
                guessed = b58decode(encoded)
                # Test
                self.assertEqual(address, guessed)


class TestEncodingBase58Invalid(JSONTestModel):
    def test_decode(self):
        """Tests the base58 decoding with invalid test cases."""
        for test in self._data['decoding']:
            with self.subTest(test['encoded']):
                # Test variables
                encoded = test['encoded']
                error = self.convert_error(test['error'])
                with self.assertRaises(error):
                    b58decode(encoded)
