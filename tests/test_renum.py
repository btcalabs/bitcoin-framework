"""Tests the representable enum."""
from .model import TestModel

from bitcoin.enum import REnum


class TestEnum(REnum):
    KEY = "value"


class TestREnum(TestModel):

    def test_repr(self):
        self.assertEqual(TestEnum.KEY, eval(repr(TestEnum.KEY)))
