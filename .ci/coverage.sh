#!/bin/sh
# Runs the coverage tester 
#
# If second argument is "html", generates and opens an HTML report
# in the browser

# Coverage test
coverage run --source bitcoin -m unittest discover tests -t ..

# Coverage report
if [ "$1" = "html" ]; then
	# Generate report
	coverage html
	report="./htmlcov/index.html"
	# Open report
	uname_str=$(uname -s)
	case $uname_str in
		CYGWIN*) cygstart "$report";;
		Linux*) xdg-open "$report";;
	esac
fi
