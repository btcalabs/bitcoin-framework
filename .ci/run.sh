#!/bin/sh
# Runs the specificated tests
# Specify the test to run using the $TEST_SUITE environment variable
# If no variable $TEST_SUITE is found, or if it is empty, all tests will be
# run
# 
# Available tests: "tests", "typing", "style"
EXIT_CODE=0

# Python unittests
if [ -z "$TEST_SUITE" ] || [ "$TEST_SUITE" = "tests" ]; then
	python -m unittest discover tests -t ..
	EXIT_CODE=$(($EXIT_CODE|$?))
fi

# Python typing check
if [ -z "$TEST_SUITE" ] || [ "$TEST_SUITE" = "typing" ]; then
	python -m mypy bitcoin
	EXIT_CODE=$(($EXIT_CODE|$?))
fi

# Python style check
if [ -z "$TEST_SUITE" ] || [ "$TEST_SUITE" = "style" ]; then
	python -m flake8 bitcoin tests
	EXIT_CODE=$(($EXIT_CODE|$?))
fi

# Exit code
exit ${EXIT_CODE}
