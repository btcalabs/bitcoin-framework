"""
Calculates a WIF address given the prefix and private key. Shows all
intermediary steps. This script is for educational purposes

Requires:
    bitcoin-framework

Usage:
    python wif_address.py "prefix in hex" "private key in hex" "compressed"

Requires Python 3. Replace `python` with `python3` to use it in Debian-like  
systems.

More information:
    https://en.bitcoin.it/wiki/Wallet_import_format
"""
# Libraries
import sys
import os
import hashlib

# Change directory (if bitcoin-framework not yet installed)
# so we can dynamically load it from its sources
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from bitcoin import b58encode as base58_encode

# Arguments
if len(sys.argv) < 3:
    print("Usage: python %s \"address_prefix_hex\" \"private key in hex\"" \
          % (sys.argv[0]) + "\"compressed\"\n")
    print("Where compressed means if the public key derived from the private "
          "key in the WIF address must be represented in a compressed form "
          "or not. Default is uncompressed")
    print(
        "Prefixes should be \"80\" for `mainnet` WIF addresses and \"EF\" "
        "for `testnet` WIF addresses")
    sys.exit(1)

# Constants
KNOWN_PREFIXES = [b"\x80", b"\xEF"]
"""
    list: list of known prefixes (`mainnet` and `testnet` respectively)
"""
COMPRESSED_BYTE = b'\x01'
"""
    bytes: byte to add to the end of the serialization to indicate the 
    derived public key from the private key contained in the WIF address 
    must be represented in a compressed form.
"""


# Helpers
def hash(algorithm, value):
    hash_object = hashlib.new(algorithm)
    hash_object.update(value)
    return hash_object.digest()


def sha256(value):
    return hash("sha256", value)


# Validation
print("STEP 0. VALIDATING ARGUMENTS")
print("============================")
if len(sys.argv) < 4:
    sys.argv.append("False")
prefix_hex, private_key_hex, compressed_str = sys.argv[1:4]
print("prefix_hex = \"%s\"" % prefix_hex)
print("private_key_hex = \"%s\"" % private_key_hex)
print("compressed_str = \"%s\"" % compressed_str)

print('')

# # Transform to bytes
print("Transforming")
print("------------")
print("Transforming from hexadecimal to bytes...")

try:
    print("prefix = bytes.fromhex(prefix_hex)")
    prefix = bytes.fromhex(prefix_hex)
except Exception:
    print("Unable to transform prefix from hexadecimal to bytes")
    sys.exit(1)

try:
    print("private_key = bytes.fromhex(private_key_hex)")
    private_key = bytes.fromhex(private_key_hex)
except Exception:
    print("Unable to transform redeem script from hexadecimal to bytes")
    sys.exit(1)

try:
    print("compressed = compressed_str in s ['true', '1', 't', 'y', 'yes', "
          "'yeah', 'yup', 'certainly', 'uh-huh']")
    compressed = compressed_str.lower() in ['true', '1', 't', 'y', 'yes',
                                            'yeah', 'yup', 'certainly',
                                            'uh-huh']
    print("compressed = %s" % str(compressed))
except Exception:
    print("Unable to convert the compressed argument to a boolean")
    sys.exit(1)


print("done")
print("")

# # Check them
print("Basic integrity checks")
print("----------------------")

# # # Known prefix
print("Checking prefix...", end='')
if prefix not in KNOWN_PREFIXES:
    print("WARNING! The prefix passed is not known.")
else:
    print("done")

print("")
print("Validation ended")
print("")

# Address generation
# # Checksum
print("STEP 1. Checksum generation")
print("===========================")
if compressed:
    print("checksum = sha256(sha256(prefix + private_key + %s))[:4]" % (
        COMPRESSED_BYTE))
    checksum = sha256(sha256(prefix + private_key + COMPRESSED_BYTE))[:4]
else:
    print("checksum = sha256(sha256(prefix + private_key))[:4]")
    checksum = sha256(sha256(prefix + private_key))[:4]
print("checksum = 0x%s" % checksum.hex().upper())
print("")

# # Address
print("STEP 2. Address generation")
print("==========================")
if compressed:
    print("address = base58_encode(prefix + private_key + %s + checksum)" % (
        COMPRESSED_BYTE))
    address = base58_encode(prefix + private_key + COMPRESSED_BYTE + checksum)
else:
    print("address = base58_encode(prefix + private_key + checksum)")
    address = base58_encode(prefix + private_key + checksum)
print("address = %s" % address)
print("")

print("You're done!")
print("Checkout more cool things at https://www.btcassessors.com")
