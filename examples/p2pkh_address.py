"""
Calculates a P2PKH address given the prefix and public key hash. Shows all
intermediary steps. This script is for educational purposes

Requires:
    bitcoin-framework

Usage:
    python p2pkh_address.py "prefix in hex" "public_key_hash_in_hex"

Requires Python 3. Replace `python` with `python3` to use it in Debian-like  
systems.
"""
# Libraries
import sys
import os
import hashlib

# Change directory (if bitcoin-framework not yet installed)
# so we can dynamically load it from its sources
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from bitcoin import b58encode as base58_encode

# Arguments
if len(sys.argv) < 3:
    print("Usage: python %s \"address_prefix_hex\" \"public_key_hash_hex\"" %
          sys.argv[0])
    print(
        "Prefixes should be \"00\" for `mainnet` P2SH addresses and \"6F\" "
        "for `testnet` P2SH addresses")
    sys.exit(1)

# Constants
KNOWN_PREFIXES = [b"\x00", b"\x6F"]
"""
    list: list of known prefixes (`mainnet` and `testnet` respectively)
"""
PKH_LENGTH = 20
"""
    int: expected length from a public key hash
"""


# Helpers
def hash(algorithm, value):
    hash_object = hashlib.new(algorithm)
    hash_object.update(value)
    return hash_object.digest()


def sha256(value):
    return hash("sha256", value)


def ripemd160(value):
    return hash("ripemd", value)


# Validation
print("STEP 0. VALIDATING ARGUMENTS")
print("============================")
prefix_hex, public_key_hash_hex = sys.argv[1:3]
print("prefix_hex = \"%s\"" % prefix_hex)
print("public_key_hash_hex = \"%s\"" % public_key_hash_hex)
print('')

# # Transform to bytes
print("Transforming")
print("------------")
print("Transforming from hexadecimal to bytes...")

try:
    print("prefix = bytes.fromhex(prefix_hex)")
    prefix = bytes.fromhex(prefix_hex)
except Exception:
    print("Unable to transform prefix from hexadecimal to bytes")
    sys.exit(1)

try:
    print("public_key_hash = bytes.fromhex(public_key_hash_hex)")
    public_key_hash = bytes.fromhex(public_key_hash_hex)
except Exception:
    print("Unable to transform public key hash from hexadecimal to bytes")
    sys.exit(1)

print("done")
print("")

# # Check them
print("Basic integrity checks")
print("----------------------")

# # # Known prefix
print("Checking prefix...", end='')
print("")
if prefix not in KNOWN_PREFIXES:
    print("WARNING: The prefix passed is not known.")

# # # Public key hash length
if len(public_key_hash) != PKH_LENGTH:
    print("WARNING: Public key hash has not proper length. "
          "Expected %d bytes." % PKH_LENGTH)

print("done")

print("")
print("Validation ended")
print("")

# Address generation
# # Checksum
print("STEP 1. Checksum generation")
print("===========================")
print("checksum = sha256(sha256(prefix + public_key_hash))[:4]")
checksum = sha256(sha256(prefix + public_key_hash))[:4]
print("checksum = 0x%s" % checksum.hex().upper())
print("")

# # Address
print("STEP 3. Address generation")
print("==========================")
print("address = base58_encode(prefix + public_key_hash + checksum)")
address = base58_encode(prefix + public_key_hash + checksum)
print("address = %s" % address)
print("")


print("You're done!")
print("Checkout more cool things at https://www.btcassessors.com")
