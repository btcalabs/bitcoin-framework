"""
Decodes a raw transaction given in hex

Shows all intermediary steps. This script is for educational purposes

Usage:
    python decode_tx.py "transaction bytes in hex"

Or if you want to check a transaction by its `txid` in the specified network:

    python decode_tx.py "transaction id in hex" ["network"]

Available networks defined in API_NETWORKS constant

Requires Python 3. Replace `python` with `python3` to use it in Debian-like  
systems.

More information:
    https://en.bitcoin.it/wiki/Protocol_documentation
"""
# Libraries
import collections
import json
import sys

# Constants
INDENT_SPACES = 2
"""
    int: number spaces to use for output indentation
"""
SEGWIT_MARKER_FLAG = b'\x00\x01'
"""
    bytes: segwit transactions marker and flag when using legacy serialization
"""
API_URL = "https://api.blockcypher.com/v1/%s/txs"
"""
    str: API URL to get transactions from. Currently BlockCypher's.
         Must be formatted with the network to query
"""
API_NETWORKS = ["btc/main", "btc/test3", "doge/main", "ltc/main",
                "dash/main", "bcy/test"]
"""
    list: networks the API allows to query
"""
API_SHORT_NETWORKS = ["mainnet", "testnet"]
"""
    list: short network names to be used in arguments
"""
API_DEFAULT_NETWORK = API_SHORT_NETWORKS[0]
"""
    str: default network to use to query the API if not specified
"""

# Arguments
if len(sys.argv) < 2:
    print("Usage: python %s \"transaction bytes in hex\"" % sys.argv[0])
    print("       python %s \"mainnet tx id in hex\" [\"network\"]" %
        sys.argv[0])
    print("Bitcoin networks available: %s. Default is \"%s\"" % (
        API_SHORT_NETWORKS, API_DEFAULT_NETWORK))
    print("All networks available: %s" % API_NETWORKS)
    sys.exit(1)

# Validation
tx_hex = sys.argv[1]
try:
    tx_bytes = bytes.fromhex(tx_hex)
except Exception:
    print("Unable to transform txid or tx to bytes from hexadecimal")
    sys.exit(1)

# API
if len(tx_hex) == 64:
    print("/* Detected txid, using BlockCypher's API to query it */")
    # network
    # # take argument or default
    if len(sys.argv) > 2:
        network = sys.argv[2]
    else:
        network = API_DEFAULT_NETWORK
    # expand short name
    if network == "mainnet":
        network = API_NETWORKS[0]
    elif network == "testnet":
        network = API_NETWORKS[1]
    # validate
    if network not in API_NETWORKS:
        print("Network %s not valid" % network)
        sys.exit(1)
    import urllib.request
    url = API_URL % network + "/" + tx_hex + "?limit=50&includeHex=true"
    tx_bytes = bytes.fromhex(
        json.loads(urllib.request.urlopen(url).read())["hex"])

# Decoding
# # Init
tx = collections.OrderedDict()
index = 0

# # 1. Version
tx["version"] = tx_bytes[index:index + 4].hex()
index += 4

# # 2. SegWit marker and fla g?
marker_flag = tx_bytes[index:index+2] # Warning: fixed marker and flag!
is_segwit = False
if marker_flag == SEGWIT_MARKER_FLAG:
    is_segwit = True
    tx["marker"] = bytes([tx_bytes[index]]).hex()
    tx["flag"] = bytes([tx_bytes[index+1]]).hex()
    index += 2

# # 3. Inputs
# # 3.1. Input count
inputs_n = tx_bytes[index]  # Warning: this only works when < 255 inputs
index += 1
tx["inputs_count"] = bytes([inputs_n]).hex()
if inputs_n > 0:
    tx["inputs"] = []

# # 3.2. Inputs loop
for _ in range(inputs_n):
    input = collections.OrderedDict()

    # # 3.2.1. UTXO txid
    input["utxo_txid"] = tx_bytes[index:index+32].hex()
    index += 32

    # # 3.2.2. UTXO output number
    input["utxo_n"] = tx_bytes[index:index+4].hex()
    index += 4

    # # 3.2.3. ScriptSig
    # # 3.2.3.1 ScriptSig length
    script_length = tx_bytes[index]
    input["scriptSig_length"] = bytes([script_length]).hex()
    # Warning: previous line just for length < 255!
    index += 1
    # # 3.2.3.2 ScriptSig itself
    if script_length > 0:
        if script_length > 1:
            input["scriptSig"] = tx_bytes[index:index+script_length].hex()
        else:
            input["scriptSig"] = bytes([tx_bytes[index]]).hex()
    index += script_length

    # # 3.2.4. Sequence
    input["sequence"] = tx_bytes[index:index+4].hex()
    index += 4

    tx["inputs"].append(input)

# # 4. Outputs
# # 4.1 Outputs count
outputs_n = tx_bytes[index]  # Warning: this only works when < 255 outputs
index += 1
tx["outputs_count"] = bytes([outputs_n]).hex()
if outputs_n > 0:
    tx["outputs"] = []

# # 4.2. Outputs loop
for _ in range(outputs_n):
    output = collections.OrderedDict()

    # # 4.2.1. Value
    output["value"] = tx_bytes[index:index+8].hex()
    index += 8

    # # 4.2.2. ScriptPubkey
    # # 4.2.2.1 ScriptPubkey length
    script_length = tx_bytes[index]
    output["scriptPubkey_length"] = bytes([script_length]).hex()
    # Warning: previous line just for length < 255!
    index += 1
    # # 3.2.3.2 ScriptPubkey itself
    if script_length > 0:
        if script_length > 1:
            output["scriptPubkey"] = \
                tx_bytes[index:index + script_length].hex()
        else:
            output["scriptPubkey"] = bytes([tx_bytes[index]]).hex()
    index += script_length

    tx["outputs"].append(output)

# # 5. Witnesses if SegWit
if is_segwit:
    tx["witnesses"] = []

    # # One witness per input
    for _ in range(inputs_n):
        witnesses = collections.OrderedDict()

        # # 5.1. Number of witness items
        witnesses_items = tx_bytes[index]  # Warning: items < 255
        witnesses["count"] = bytes([witnesses_items]).hex()
        index += 1

        if witnesses_items > 0:
            witnesses["list"] = []

        # # 5.2. Loop items
        for __ in range(witnesses_items):
            witness = collections.OrderedDict()

            # # 5.2.1. Item size
            witness_length = tx_bytes[index]  # Warning: length < 255
            witness["length"] = bytes([witness_length]).hex()
            index += 1

            # # 5.2.2. Witness
            witness["value"] = tx_bytes[index:index+witness_length].hex()
            index += witness_length

            witnesses["list"].append(witness)

        tx["witnesses"].append(witnesses)

# # 6. Locktime
tx["locktime"] = tx_bytes[index:index+4].hex()
index += 4

# # Check end
if index != len(tx_bytes):
    print("/* WARNING: Not all transaction bytes decoded */")
    print("/*          index is at %d, transaction bytes are %d" % (
        index, len(tx_bytes)
    ))

# Printing
print(json.dumps(tx, indent=INDENT_SPACES))
print("/* You're done! */")
print("/* Checkout more cool things at https://www.btcassessors.com */")
