"""
Calculates a P2SH address given the prefix and redeem script. Shows all
intermediary steps. This script is for educational purposes

Requires:
    bitcoin-framework

Usage:
    python p2sh_address.py "prefix in hex" "redeem_script_in_hex"

Requires Python 3. Replace `python` with `python3` to use it in Debian-like  
systems.
"""
# Libraries
import sys
import os
import hashlib

# Change directory (if bitcoin-framework not yet installed)
# so we can dynamically load it from its sources
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from bitcoin import b58encode as base58_encode

# Arguments
if len(sys.argv) < 3:
    print("Usage: python %s \"address_prefix_hex\" \"redeem_script_hex\"" %
          sys.argv[0])
    print(
        "Prefixes should be \"05\" for `mainnet` P2SH addresses and \"C4\" "
        "for `testnet` P2SH addresses")
    sys.exit(1)

# Constants
KNOWN_PREFIXES = [b"\x05", b"\xC4"]
"""
    list: list of known prefixes (`mainnet` and `testnet` respectively)
"""


# Helpers
def hash(algorithm, value):
    hash_object = hashlib.new(algorithm)
    hash_object.update(value)
    return hash_object.digest()


def sha256(value):
    return hash("sha256", value)


def ripemd160(value):
    return hash("ripemd", value)


# Validation
print("STEP 0. VALIDATING ARGUMENTS")
print("============================")
prefix_hex, redeem_script_hex = sys.argv[1:3]
print("prefix_hex = \"%s\"" % prefix_hex)
print("redeem_script_hex = \"%s\"" % redeem_script_hex)
print('')

# # Transform to bytes
print("Transforming")
print("------------")
print("Transforming from hexadecimal to bytes...")

try:
    print("prefix = bytes.fromhex(prefix_hex)")
    prefix = bytes.fromhex(prefix_hex)
except Exception:
    print("Unable to transform prefix from hexadecimal to bytes")
    sys.exit(1)

try:
    print("redeem_script = bytes.fromhex(redeem_script_hex)")
    redeem_script = bytes.fromhex(redeem_script_hex)
except Exception:
    print("Unable to transform redeem script from hexadecimal to bytes")
    sys.exit(1)

print("done")
print("")

# # Check them
print("Basic integrity checks")
print("----------------------")

# # # Known prefix
print("Checking prefix...", end='')
if prefix not in KNOWN_PREFIXES:
    print("WARNING! The prefix passed is not known.")
else:
    print("done")

print("")
print("Validation ended")
print("")

# Address generation
# # Redeem script hash
print("STEP 1. Generate redeem script's hash")
print("=====================================")
print("redeem_script_hash = ripemd160(sha256(redeem_script))")
redeem_script_hash = ripemd160(sha256(redeem_script))
print("redeem_script_hash = 0x%s" % redeem_script_hash.hex().upper())
print("")

# # Checksum
print("STEP 2. Checksum generation")
print("===========================")
print("checksum = sha256(sha256(prefix + redeem_script_hash))[:4]")
checksum = sha256(sha256(prefix + redeem_script_hash))[:4]
print("checksum = 0x%s" % checksum.hex().upper())
print("")

# # Address
print("STEP 3. Address generation")
print("==========================")
print("address = base58_encode(prefix + redeem_script_hash + checksum)")
address = base58_encode(prefix + redeem_script_hash + checksum)
print("address = %s" % address)
print("")


print("You're done!")
print("Checkout more cool things at https://www.btcassessors.com")
