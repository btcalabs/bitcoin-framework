# Libraries
from setuptools import setup, find_packages

# README.md
with open("README.md", "r") as fh:
    long_description = fh.read()

# Setup configuration
setup(
    name="bitcoin-framework",
    packages=find_packages(),
    version="0.5.0",
    author="BTCA Labs",
    author_email="tech+bitcoin-framework@btcassessors.com",
    description="Python Bitcoin framework to create transactions with smart "
                "contracts based on puzzle-friendliness and OOP",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="Apache License v2.0",
    url="https://github.com/BTCAssessors/bitcoin-framework",
    download_url="https://pypi.org/project/bitcoin-framework/",
    keywords="bitcoin btc script scripting framework library transaction",
    classifiers=(
        "Development Status :: 2 - Pre-Alpha",
        "Framework :: Flake8",
        "Intended Audience :: Developers",
        "Intended Audience :: Education",
        "License :: OSI Approved :: Apache Software License"
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Other/Nonlisted Topic"
    )
)

